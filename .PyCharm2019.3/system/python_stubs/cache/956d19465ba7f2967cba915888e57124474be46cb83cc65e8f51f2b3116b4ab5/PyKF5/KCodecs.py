# encoding: utf-8
# module PyKF5.KCodecs
# from /usr/lib/python3.8/site-packages/PyKF5/KCodecs.so
# by generator 1.147
# no doc

# imports
import sip as __sip


# no functions
# classes

class KCharsets(__sip.wrapper):
    # no doc
    def availableEncodingNames(self, *args, **kwargs): # real signature unknown
        pass

    def charsets(self, *args, **kwargs): # real signature unknown
        pass

    def codecForName(self, *args, **kwargs): # real signature unknown
        pass

    def descriptionForEncoding(self, *args, **kwargs): # real signature unknown
        pass

    def descriptiveEncodingNames(self, *args, **kwargs): # real signature unknown
        pass

    def encodingForName(self, *args, **kwargs): # real signature unknown
        pass

    def encodingsByScript(self, *args, **kwargs): # real signature unknown
        pass

    def fromEntity(self, *args, **kwargs): # real signature unknown
        pass

    def resolveEntities(self, *args, **kwargs): # real signature unknown
        pass

    def toEntity(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KCodecs(__sip.simplewrapper):
    # no doc
    def base64Decode(self, *args, **kwargs): # real signature unknown
        pass

    def base64Encode(self, *args, **kwargs): # real signature unknown
        pass

    def decodeRFC2047String(self, *args, **kwargs): # real signature unknown
        pass

    def encodeRFC2047String(self, *args, **kwargs): # real signature unknown
        pass

    def quotedPrintableDecode(self, *args, **kwargs): # real signature unknown
        pass

    def quotedPrintableEncode(self, *args, **kwargs): # real signature unknown
        pass

    def uudecode(self, *args, **kwargs): # real signature unknown
        pass

    def uuencode(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    CharsetOption = None # (!) real value is "<class 'PyKF5.KCodecs.KCodecs.CharsetOption'>"
    Codec = None # (!) real value is "<class 'PyKF5.KCodecs.KCodecs.Codec'>"
    Decoder = None # (!) real value is "<class 'PyKF5.KCodecs.KCodecs.Decoder'>"
    Encoder = None # (!) real value is "<class 'PyKF5.KCodecs.KCodecs.Encoder'>"
    ForceDefaultCharset = 1
    NoOption = 0


class KEmailAddress(__sip.simplewrapper):
    # no doc
    def compareEmail(self, *args, **kwargs): # real signature unknown
        pass

    def decodeMailtoUrl(self, *args, **kwargs): # real signature unknown
        pass

    def emailParseResultToString(self, *args, **kwargs): # real signature unknown
        pass

    def encodeMailtoUrl(self, *args, **kwargs): # real signature unknown
        pass

    def extractEmailAddress(self, *args, **kwargs): # real signature unknown
        pass

    def extractEmailAddressAndName(self, *args, **kwargs): # real signature unknown
        pass

    def firstEmailAddress(self, *args, **kwargs): # real signature unknown
        pass

    def fromIdn(self, *args, **kwargs): # real signature unknown
        pass

    def isValidAddress(self, *args, **kwargs): # real signature unknown
        pass

    def isValidAddressList(self, *args, **kwargs): # real signature unknown
        pass

    def isValidSimpleAddress(self, *args, **kwargs): # real signature unknown
        pass

    def normalizeAddressesAndDecodeIdn(self, *args, **kwargs): # real signature unknown
        pass

    def normalizeAddressesAndEncodeIdn(self, *args, **kwargs): # real signature unknown
        pass

    def normalizedAddress(self, *args, **kwargs): # real signature unknown
        pass

    def quoteNameIfNecessary(self, *args, **kwargs): # real signature unknown
        pass

    def simpleEmailAddressErrorMsg(self, *args, **kwargs): # real signature unknown
        pass

    def splitAddress(self, *args, **kwargs): # real signature unknown
        pass

    def splitAddressList(self, *args, **kwargs): # real signature unknown
        pass

    def toIdn(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    AddressEmpty = 1
    AddressOk = 0
    DisallowedChar = 13
    EmailParseResult = None # (!) real value is "<class 'PyKF5.KCodecs.KEmailAddress.EmailParseResult'>"
    InvalidDisplayName = 14
    MissingDomainPart = 4
    MissingLocalPart = 10
    NoAddressSpec = 12
    TooFewAts = 9
    TooFewDots = 15
    TooManyAts = 7
    UnbalancedParens = 3
    UnbalancedQuote = 11
    UnclosedAngleAddr = 5
    UnexpectedComma = 8
    UnexpectedEnd = 2
    UnopenedAngleAddr = 6


class KEncodingProber(__sip.wrapper):
    # no doc
    def confidence(self, *args, **kwargs): # real signature unknown
        pass

    def encoding(self, *args, **kwargs): # real signature unknown
        pass

    def feed(self, *args, **kwargs): # real signature unknown
        pass

    def nameForProberType(self, *args, **kwargs): # real signature unknown
        pass

    def proberType(self, *args, **kwargs): # real signature unknown
        pass

    def proberTypeForName(self, *args, **kwargs): # real signature unknown
        pass

    def reset(self, *args, **kwargs): # real signature unknown
        pass

    def setProberType(self, *args, **kwargs): # real signature unknown
        pass

    def state(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    Arabic = 2
    Baltic = 3
    CentralEuropean = 4
    ChineseSimplified = 5
    ChineseTraditional = 6
    Cyrillic = 7
    FoundIt = 0
    Greek = 8
    Hebrew = 9
    Japanese = 10
    Korean = 11
    NorthernSaami = 12
    NotMe = 1
    Other = 13
    ProberState = None # (!) real value is "<class 'PyKF5.KCodecs.KEncodingProber.ProberState'>"
    ProberType = None # (!) real value is "<class 'PyKF5.KCodecs.KEncodingProber.ProberType'>"
    Probing = 2
    SouthEasternEurope = 14
    Thai = 15
    Turkish = 16
    Unicode = 17
    Universal = 1
    WesternEuropean = 18


# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>'

__spec__ = None # (!) real value is "ModuleSpec(name='PyKF5.KCodecs', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>, origin='/usr/lib/python3.8/site-packages/PyKF5/KCodecs.so')"

