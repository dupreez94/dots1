# encoding: utf-8
# module PyQt5.QtCore
# from /usr/lib/python3.8/site-packages/PyQt5/QtCore.abi3.so
# by generator 1.147
# no doc

# imports
import enum as __enum
import sip as __sip


from .QEvent import QEvent

class QDynamicPropertyChangeEvent(QEvent):
    """
    QDynamicPropertyChangeEvent(Union[QByteArray, bytes, bytearray])
    QDynamicPropertyChangeEvent(QDynamicPropertyChangeEvent)
    """
    def propertyName(self): # real signature unknown; restored from __doc__
        """ propertyName(self) -> QByteArray """
        return QByteArray

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


