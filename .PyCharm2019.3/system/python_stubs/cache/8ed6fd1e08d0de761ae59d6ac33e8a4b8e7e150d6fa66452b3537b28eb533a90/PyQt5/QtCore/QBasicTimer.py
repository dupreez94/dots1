# encoding: utf-8
# module PyQt5.QtCore
# from /usr/lib/python3.8/site-packages/PyQt5/QtCore.abi3.so
# by generator 1.147
# no doc

# imports
import enum as __enum
import sip as __sip


class QBasicTimer(__sip.simplewrapper):
    """
    QBasicTimer()
    QBasicTimer(QBasicTimer)
    """
    def isActive(self): # real signature unknown; restored from __doc__
        """ isActive(self) -> bool """
        return False

    def start(self, p_int, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        """
        start(self, int, Qt.TimerType, QObject)
        start(self, int, QObject)
        """
        pass

    def stop(self): # real signature unknown; restored from __doc__
        """ stop(self) """
        pass

    def swap(self, QBasicTimer): # real signature unknown; restored from __doc__
        """ swap(self, QBasicTimer) """
        pass

    def timerId(self): # real signature unknown; restored from __doc__
        """ timerId(self) -> int """
        return 0

    def __init__(self, QBasicTimer=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



