# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Quatd(__Boost_Python.instance):
    """ Quatd """
    def angle(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        angle( (Quatd)arg1) -> float :
            q.angle() -- returns the rotation angle
            (in radians) represented by quaternion q
        
            C++ signature :
                double angle(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def axis(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        axis( (Quatd)arg1) -> V3d :
            q.axis() -- returns the rotation axis
            represented by quaternion q
        
            C++ signature :
                Imath_2_4::Vec3<double> axis(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def exp(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        exp( (Quatd)arg1) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> exp(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def extract(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extract( (Quatd)arg1, (M44d)arg2) -> None :
            q.extract(m) -- extracts the rotation component
            from 4x4 matrix m and stores the result in q
        
            C++ signature :
                void extract(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def identity(self): # real signature unknown; restored from __doc__
        """
        identity() -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> identity()
        """
        return Quatd

    def inverse(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        inverse( (Quatd)arg1) -> Quatd :
            q.inverse() -- returns the inverse of
            quaternion q; q is not modified
            
        
            C++ signature :
                Imath_2_4::Quat<double> inverse(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def invert(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        invert( (Quatd)arg1) -> Quatd :
            q.invert() -- inverts quaternion q
            (modifying q); returns q
        
            C++ signature :
                Imath_2_4::Quat<double> {lvalue} invert(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def length(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (Quatd)arg1) -> float :
        
            C++ signature :
                double length(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def log(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        log( (Quatd)arg1) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> log(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def normalize(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (Quatd)arg1) -> Quatd :
            q.normalize() -- normalizes quaternion q
            (modifying q); returns q
        
            C++ signature :
                Imath_2_4::Quat<double> {lvalue} normalize(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def normalized(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (Quatd)arg1) -> Quatd :
            q.normalized() -- returns a normalized version
            of quaternion q; q is not modified
            
        
            C++ signature :
                Imath_2_4::Quat<double> normalized(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def r(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        r( (Quatd)arg1) -> float :
            q.r() -- returns the r (scalar) component
            of quaternion q
        
            C++ signature :
                double r(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def setAxisAngle(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setAxisAngle( (Quatd)arg1, (V3d)arg2, (float)arg3) -> Quatd :
            q.setAxisAngle(x,r) -- sets the value of
            quaternion q so that q represents a rotation
            of r radians around axis x
        
            C++ signature :
                Imath_2_4::Quat<double> {lvalue} setAxisAngle(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Vec3<double>,double)
        """
        pass

    def setR(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setR( (Quatd)arg1, (float)arg2) -> None :
            q.setR(s) -- sets the r (scalar) component
            of quaternion q to s
        
            C++ signature :
                void setR(Imath_2_4::Quat<double> {lvalue},double)
        """
        pass

    def setRotation(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setRotation( (Quatd)arg1, (V3d)arg2, (V3d)arg3) -> Quatd :
            q.setRotation(v,w) -- sets the value of
            quaternion q so that rotating vector v by
            q produces vector w
        
            C++ signature :
                Imath_2_4::Quat<double> {lvalue} setRotation(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        """
        pass

    def setV(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setV( (Quatd)arg1, (V3d)arg2) -> None :
            q.setV(w) -- sets the v (vector) component
            of quaternion q to w
        
            C++ signature :
                void setV(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Vec3<double>)
        """
        pass

    def slerp(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        slerp( (Quatd)arg1, (Quatd)arg2, (float)arg3) -> Quatd :
            q.slerp(p,t) -- performs sperical linear
            interpolation between quaternions q and p:
            q.slerp(p,0) returns q; q.slerp(p,1) returns p.
            q and p must be normalized
            
        
            C++ signature :
                Imath_2_4::Quat<double> slerp(Imath_2_4::Quat<double>,Imath_2_4::Quat<double>,double)
        """
        pass

    def toMatrix33(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        toMatrix33( (Quatd)arg1) -> M33d :
            q.toMatrix33() -- returns a 3x3 matrix that
            represents the same rotation as quaternion q
        
            C++ signature :
                Imath_2_4::Matrix33<double> toMatrix33(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def toMatrix44(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        toMatrix44( (Quatd)arg1) -> M44d :
            q.toMatrix44() -- returns a 4x4 matrix that
            represents the same rotation as quaternion q
        
            C++ signature :
                Imath_2_4::Matrix44<double> toMatrix44(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def v(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        v( (Quatd)arg1) -> V3d :
            q.v() -- returns the v (vector) component
            of quaternion q
        
            C++ signature :
                Imath_2_4::Vec3<double> v(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def __add__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (Quatd)arg1, (Quatd)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __add__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def __copy__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Quatd)arg1) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __copy__(Imath_2_4::Quat<double>)
        """
        pass

    def __deepcopy__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Quatd)arg1, (dict)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __deepcopy__(Imath_2_4::Quat<double>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (Quatd)arg1, (Quatd)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __div__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double> {lvalue})
        
        __div__( (Quatd)arg1, (float)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __div__(Imath_2_4::Quat<double> {lvalue},double)
        """
        pass

    def __eq__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Quatd)arg1, (Quatd)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double>)
        """
        pass

    def __iadd__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (Quatd)arg1, (Quatd)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __iadd__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double>)
        """
        pass

    def __idiv__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (Quatd)arg1, (Quatd)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __idiv__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double>)
        
        __idiv__( (Quatd)arg1, (float)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __idiv__(Imath_2_4::Quat<double> {lvalue},double)
        """
        pass

    def __imul__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (Quatd)arg1, (Quatd)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __imul__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double>)
        
        __imul__( (Quatd)arg1, (float)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __imul__(Imath_2_4::Quat<double> {lvalue},double)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (Quatd)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Quat<double>)
        
        __init__( (object)arg1) -> None :
            imath Quat initialization
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (Quatf)arg2) -> None :
            imath Quat copy initialization
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Quat<float>)
        
        __init__( (object)arg1, (Quatd)arg2) -> None :
            imath Quat copy initialization
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Quat<double>)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5) -> None :
            make Quat from components
        
            C++ signature :
                void __init__(_object*,double,double,double,double)
        
        __init__( (object)arg1, (float)arg2, (V3d)arg3) -> None :
            make Quat from components
        
            C++ signature :
                void __init__(_object*,double,Imath_2_4::Vec3<double>)
        
        __init__( (object)arg1, (Eulerd)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Euler<double>)
        
        __init__( (object)arg1, (M33d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix33<double>)
        
        __init__( (object)arg1, (M44d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix44<double>)
        """
        pass

    def __invert__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __invert__( (Quatd)arg1) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __invert__(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def __isub__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (Quatd)arg1, (Quatd)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __isub__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double>)
        """
        pass

    def __itruediv__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (Quatd)arg1, (Quatd)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __itruediv__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double>)
        
        __itruediv__( (Quatd)arg1, (float)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __itruediv__(Imath_2_4::Quat<double> {lvalue},double)
        """
        pass

    def __mul__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (Quatd)arg1, (M33d)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __mul__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Matrix33<double> {lvalue})
        
        __mul__( (Quatd)arg1, (Quatd)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __mul__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double> {lvalue})
        
        __mul__( (Quatd)arg1, (float)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __mul__(Imath_2_4::Quat<double> {lvalue},double)
        """
        pass

    def __neg__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (Quatd)arg1) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __neg__(Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def __ne__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Quatd)arg1, (Quatd)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Quatd)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Quat<double>)
        """
        pass

    def __rmul__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (Quatd)arg1, (M33d)arg2) -> M33d :
        
            C++ signature :
                Imath_2_4::Matrix33<double> __rmul__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Matrix33<double> {lvalue})
        
        __rmul__( (Quatd)arg1, (float)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __rmul__(Imath_2_4::Quat<double> {lvalue},double)
        
        __rmul__( (Quatd)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> __rmul__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Vec3<double>)
        
        __rmul__( (Quatd)arg1, (V3dArray)arg2) -> V3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<double> > __rmul__(Imath_2_4::Quat<double> {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def __str__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (Quatd)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Quat<double>)
        """
        pass

    def __sub__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (Quatd)arg1, (Quatd)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __sub__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double> {lvalue})
        """
        pass

    def __truediv__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (Quatd)arg1, (Quatd)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __truediv__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double> {lvalue})
        
        __truediv__( (Quatd)arg1, (float)arg2) -> Quatd :
        
            C++ signature :
                Imath_2_4::Quat<double> __truediv__(Imath_2_4::Quat<double> {lvalue},double)
        """
        pass

    def __xor__(self, Quatd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __xor__( (Quatd)arg1, (Quatd)arg2) -> float :
        
            C++ signature :
                double __xor__(Imath_2_4::Quat<double> {lvalue},Imath_2_4::Quat<double> {lvalue})
        """
        pass

    __instance_size__ = 48


