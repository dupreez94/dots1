# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Frustumd(__Boost_Python.instance):
    """ Frustumd """
    def aspect(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        aspect( (Frustumd)arg1) -> float :
            F.aspect() -- derives and returns the aspect ratio for frustum F
        
            C++ signature :
                double aspect(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def bottom(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        bottom( (Frustumd)arg1) -> float :
            F.bottom() -- returns the bottom coordinate of the near clipping window of frustum F
        
            C++ signature :
                double bottom(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def DepthToZ(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        DepthToZ( (Frustumd)arg1, (float)arg2, (int)arg3, (int)arg4) -> int :
            F.DepthToZ(depth, zMin, zMax) -- converts depth (Z in the local space of the frustum F) to z (a result of  transformation by F's projection matrix) which is normalized to [zMin, zMax]
        
            C++ signature :
                long DepthToZ(Imath_2_4::Frustum<double> {lvalue},double,long,long)
        """
        pass

    def far(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        far( (Frustumd)arg1) -> float :
            F.far() -- returns the coordinate of the far clipping plane of frustum F
        
            C++ signature :
                double far(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def farPlane(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        farPlane( (Frustumd)arg1) -> float :
            F.farPlane() -- returns the coordinate of the far clipping plane of frustum F
        
            C++ signature :
                double farPlane(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def fovx(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        fovx( (Frustumd)arg1) -> float :
            F.fovx() -- derives and returns the x field of view (in radians) for frustum F
        
            C++ signature :
                double fovx(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def fovy(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        fovy( (Frustumd)arg1) -> float :
            F.fovy() -- derives and returns the y field of view (in radians) for frustum F
        
            C++ signature :
                double fovy(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def left(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        left( (Frustumd)arg1) -> float :
            F.left() -- returns the left coordinate of the near clipping window of frustum F
        
            C++ signature :
                double left(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def modifyNearAndFar(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        modifyNearAndFar( (Frustumd)arg1, (float)arg2, (float)arg3) -> None :
            F.modifyNearAndFar(nearPlane, farPlane) -- modifies the already-valid frustum F as specified
        
            C++ signature :
                void modifyNearAndFar(Imath_2_4::Frustum<double> {lvalue},double,double)
        """
        pass

    def near(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        near( (Frustumd)arg1) -> float :
            F.near() -- returns the coordinate of the near clipping plane of frustum F
        
            C++ signature :
                double near(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def nearPlane(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        nearPlane( (Frustumd)arg1) -> float :
            F.nearPlane() -- returns the coordinate of the near clipping plane of frustum F
        
            C++ signature :
                double nearPlane(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def normalizedZToDepth(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedZToDepth( (Frustumd)arg1, (float)arg2) -> float :
            F.normalizedZToDepth(z) -- returns the depth (Z in the local space of the frustum F) corresponding to z (a result of transformation by F's projection matrix), which is assumed to have been normalized to [-1, 1]
        
            C++ signature :
                double normalizedZToDepth(Imath_2_4::Frustum<double> {lvalue},double)
        """
        pass

    def orthographic(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        orthographic( (Frustumd)arg1) -> bool :
            F.orthographic() -- returns whether frustum F is orthographic or not
        
            C++ signature :
                bool orthographic(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def planes(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        planes( (Frustumd)arg1, (Plane3d)arg2) -> None :
            F.planes([M]) -- returns a sequence of 6 Plane3s, the sides of the frustum F (top, right, bottom, left, nearPlane, farPlane), optionally transformed by the matrix M if specified
        
            C++ signature :
                void planes(Imath_2_4::Frustum<double> {lvalue},Imath_2_4::Plane3<double>*)
        
        planes( (Frustumd)arg1, (Plane3d)arg2, (M44d)arg3) -> None :
        
            C++ signature :
                void planes(Imath_2_4::Frustum<double> {lvalue},Imath_2_4::Plane3<double>*,Imath_2_4::Matrix44<double>)
        
        planes( (Frustumd)arg1 [, (M44d)arg2]) -> tuple :
        
            C++ signature :
                boost::python::tuple planes(Imath_2_4::Frustum<double> {lvalue} [,Imath_2_4::Matrix44<double>])
        """
        pass

    def projectionMatrix(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        projectionMatrix( (Frustumd)arg1) -> M44d :
            F.projectionMatrix() -- derives and returns the projection matrix for frustum F
        
            C++ signature :
                Imath_2_4::Matrix44<double> projectionMatrix(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def projectPointToScreen(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        projectPointToScreen( (Frustumd)arg1, (V3d)arg2) -> V2d :
            F.projectPointToScreen(V) -- returns the projection of V3 V into screen space
        
            C++ signature :
                Imath_2_4::Vec2<double> projectPointToScreen(Imath_2_4::Frustum<double> {lvalue},Imath_2_4::Vec3<double>)
        
        projectPointToScreen( (Frustumd)arg1, (tuple)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> projectPointToScreen(Imath_2_4::Frustum<double> {lvalue},boost::python::tuple)
        
        projectPointToScreen( (Frustumd)arg1, (object)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> projectPointToScreen(Imath_2_4::Frustum<double> {lvalue},boost::python::api::object)
        """
        pass

    def projectScreenToRay(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        projectScreenToRay( (Frustumd)arg1, (V2d)arg2) -> Line3d :
            F.projectScreenToRay(V) -- returns a Line3 through V, a V2 point in screen space
        
            C++ signature :
                Imath_2_4::Line3<double> projectScreenToRay(Imath_2_4::Frustum<double> {lvalue},Imath_2_4::Vec2<double>)
        
        projectScreenToRay( (Frustumd)arg1, (tuple)arg2) -> Line3d :
        
            C++ signature :
                Imath_2_4::Line3<double> projectScreenToRay(Imath_2_4::Frustum<double> {lvalue},boost::python::tuple)
        """
        pass

    def right(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        right( (Frustumd)arg1) -> float :
            F.right() -- returns the right coordinate of the near clipping window of frustum F
        
            C++ signature :
                double right(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def screenRadius(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        screenRadius( (Frustumd)arg1, (V3d)arg2, (float)arg3) -> float :
            F.screenRadius(V, r) -- returns the radius in screen space corresponding to the point V and radius r in F's local space
        
            C++ signature :
                double screenRadius(Imath_2_4::Frustum<double> {lvalue},Imath_2_4::Vec3<double>,double)
        
        screenRadius( (Frustumd)arg1, (tuple)arg2, (float)arg3) -> float :
        
            C++ signature :
                double screenRadius(Imath_2_4::Frustum<double> {lvalue},boost::python::tuple,double)
        """
        pass

    def set(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set( (Frustumd)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6, (float)arg7, (bool)arg8) -> None :
            F.set(nearPlane, farPlane, left, right, top, bottom, [ortho])
            F.set(nearPlane, farPlane, fovx, fovy, aspect)                -- sets the entire state of frustum F as specified.  Only one of fovx or fovy may be non-zero.
        
            C++ signature :
                void set(Imath_2_4::Frustum<double> {lvalue},double,double,double,double,double,double,bool)
        
        set( (Frustumd)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6) -> None :
        
            C++ signature :
                void set(Imath_2_4::Frustum<double> {lvalue},double,double,double,double,double)
        """
        pass

    def setOrthographic(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setOrthographic( (Frustumd)arg1, (bool)arg2) -> None :
            F.setOrthographic(b) -- modifies the already-valid frustum F to be orthographic or not
        
            C++ signature :
                void setOrthographic(Imath_2_4::Frustum<double> {lvalue},bool)
        """
        pass

    def top(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        top( (Frustumd)arg1) -> float :
            F.top() -- returns the top coordinate of the near clipping window of frustum F
        
            C++ signature :
                double top(Imath_2_4::Frustum<double> {lvalue})
        """
        pass

    def window(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        window( (Frustumd)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5) -> Frustumd :
            F.window(l,r,b,t) -- takes a rectangle in the screen space (i.e., -1 <= l <= r <= 1, -1 <= b <= t <= 1) of F and returns a new Frustum whose near clipping-plane window is that rectangle in local space
        
            C++ signature :
                Imath_2_4::Frustum<double> window(Imath_2_4::Frustum<double> {lvalue},double,double,double,double)
        """
        pass

    def worldRadius(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        worldRadius( (Frustumd)arg1, (V3d)arg2, (float)arg3) -> float :
            F.worldRadius(V, r) -- returns the radius in F's local space corresponding to the point V and radius r in screen space
        
            C++ signature :
                double worldRadius(Imath_2_4::Frustum<double> {lvalue},Imath_2_4::Vec3<double>,double)
        
        worldRadius( (Frustumd)arg1, (tuple)arg2, (float)arg3) -> float :
        
            C++ signature :
                double worldRadius(Imath_2_4::Frustum<double> {lvalue},boost::python::tuple,double)
        """
        pass

    def ZToDepth(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ZToDepth( (Frustumd)arg1, (int)arg2, (int)arg3, (int)arg4) -> float :
            F.ZToDepth(z, zMin, zMax) -- returns the depth (Z in the local space of the frustum F) corresponding to z (a result of transformation by F's projection matrix) after normalizing z to be between zMin and zMax
        
            C++ signature :
                double ZToDepth(Imath_2_4::Frustum<double> {lvalue},long,long,long)
        """
        pass

    def __copy__(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Frustumd)arg1) -> Frustumd :
        
            C++ signature :
                Imath_2_4::Frustum<double> __copy__(Imath_2_4::Frustum<double>)
        """
        pass

    def __deepcopy__(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Frustumd)arg1, (dict)arg2) -> Frustumd :
        
            C++ signature :
                Imath_2_4::Frustum<double> __deepcopy__(Imath_2_4::Frustum<double>,boost::python::dict {lvalue})
        """
        pass

    def __eq__(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Frustumd)arg1, (Frustumd)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Frustum<double> {lvalue},Imath_2_4::Frustum<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (Frustumd)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Frustum<double>)
        
        __init__( (object)arg1) -> None :
            Frustum() default construction
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6, (float)arg7, (bool)arg8) -> None :
            Frustum(nearPlane,farPlane,left,right,top,bottom,ortho) construction
        
            C++ signature :
                void __init__(_object*,double,double,double,double,double,double,bool)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6) -> None :
            Frustum(nearPlane,farPlane,fovx,fovy,aspect) construction
        
            C++ signature :
                void __init__(_object*,double,double,double,double,double)
        """
        pass

    def __ne__(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Frustumd)arg1, (Frustumd)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Frustum<double> {lvalue},Imath_2_4::Frustum<double>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Frustumd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Frustumd)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Frustum<double>)
        """
        pass

    __instance_size__ = 80


