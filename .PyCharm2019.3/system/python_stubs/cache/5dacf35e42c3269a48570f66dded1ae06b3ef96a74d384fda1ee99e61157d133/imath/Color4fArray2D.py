# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Color4fArray2D(__Boost_Python.instance):
    """ Fixed length 2d array of IMATH_NAMESPACE::Color4 """
    def ifelse(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (Color4fArray2D)arg1, (IntArray2D)arg2, (Color4f)arg3) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > ifelse(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<int>,Imath_2_4::Color4<float>)
        
        ifelse( (Color4fArray2D)arg1, (IntArray2D)arg2, (Color4fArray2D)arg3) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > ifelse(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        """
        pass

    def item(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        item( (Color4fArray2D)arg1, (int)arg2, (int)arg3) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> {lvalue} item(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},long,long)
        """
        pass

    def size(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        size( (Color4fArray2D)arg1) -> tuple :
        
            C++ signature :
                boost::python::tuple size(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue})
        """
        pass

    def __add__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __add__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __add__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __add__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,Imath_2_4::Color4<float>)
        """
        pass

    def __copy__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Color4fArray2D)arg1) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __copy__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        """
        pass

    def __deepcopy__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Color4fArray2D)arg1, (dict)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __deepcopy__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (Color4fArray2D)arg1, (float)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __div__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,float)
        
        __div__( (Color4fArray2D)arg1, (FloatArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __div__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,PyImath::FixedArray2D<float>)
        
        __div__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __div__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __div__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __div__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,Imath_2_4::Color4<float>)
        """
        pass

    def __eq__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __eq__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __eq__( (Color4fArray2D)arg1, (Color4f)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __eq__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,Imath_2_4::Color4<float>)
        """
        pass

    def __getitem__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (Color4fArray2D)arg1, (object)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __getitem__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},_object*)
        
        __getitem__( (Color4fArray2D)arg1, (IntArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __getitem__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<int>)
        """
        pass

    def __iadd__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __iadd__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __iadd__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __iadd__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __idiv__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (Color4fArray2D)arg1, (float)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __idiv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},float)
        
        __idiv__( (Color4fArray2D)arg1, (FloatArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __idiv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<float>)
        
        __idiv__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __idiv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __idiv__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __idiv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __imul__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (Color4fArray2D)arg1, (float)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __imul__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},float)
        
        __imul__( (Color4fArray2D)arg1, (FloatArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __imul__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<float>)
        
        __imul__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __imul__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __imul__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __imul__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long,unsigned long)
        
        __init__( (object)arg1, (Color4fArray2D)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __init__( (object)arg1, (Color4f)arg2, (int)arg3, (int)arg4) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Color4<float>,unsigned long,unsigned long)
        """
        pass

    def __isub__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __isub__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __isub__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __isub__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __itruediv__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (Color4fArray2D)arg1, (float)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __itruediv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},float)
        
        __itruediv__( (Color4fArray2D)arg1, (FloatArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __itruediv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<float>)
        
        __itruediv__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __itruediv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __itruediv__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __itruediv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __len__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (Color4fArray2D)arg1) -> int :
        
            C++ signature :
                unsigned long __len__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue})
        """
        pass

    def __mul__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (Color4fArray2D)arg1, (float)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __mul__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,float)
        
        __mul__( (Color4fArray2D)arg1, (FloatArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __mul__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,PyImath::FixedArray2D<float>)
        
        __mul__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __mul__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __mul__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __mul__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,Imath_2_4::Color4<float>)
        """
        pass

    def __neg__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (Color4fArray2D)arg1) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __neg__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        """
        pass

    def __ne__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ne__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __ne__( (Color4fArray2D)arg1, (Color4f)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ne__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,Imath_2_4::Color4<float>)
        """
        pass

    def __radd__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __radd__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,Imath_2_4::Color4<float>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (Color4fArray2D)arg1, (float)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __rmul__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,float)
        
        __rmul__( (Color4fArray2D)arg1, (FloatArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __rmul__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,PyImath::FixedArray2D<float>)
        
        __rmul__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __rmul__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,Imath_2_4::Color4<float>)
        """
        pass

    def __rsub__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __rsub__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,Imath_2_4::Color4<float>)
        """
        pass

    def __setitem__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (Color4fArray2D)arg1, (object)arg2, (Color4f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},_object*,Imath_2_4::Color4<float>)
        
        __setitem__( (Color4fArray2D)arg1, (IntArray2D)arg2, (Color4f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<int>,Imath_2_4::Color4<float>)
        
        __setitem__( (Color4fArray2D)arg1, (object)arg2, (Color4fArray2D)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},_object*,PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __setitem__( (Color4fArray2D)arg1, (IntArray2D)arg2, (Color4fArray2D)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __setitem__( (Color4fArray2D)arg1, (object)arg2, (C4fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Color4<float> >)
        
        __setitem__( (Color4fArray2D)arg1, (IntArray2D)arg2, (C4fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray<Imath_2_4::Color4<float> >)
        
        __setitem__( (Color4fArray2D)arg1, (tuple)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<float> > {lvalue},boost::python::tuple,boost::python::tuple)
        """
        pass

    def __sub__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __sub__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __sub__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __sub__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,Imath_2_4::Color4<float>)
        """
        pass

    def __truediv__(self, Color4fArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (Color4fArray2D)arg1, (float)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __truediv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,float)
        
        __truediv__( (Color4fArray2D)arg1, (FloatArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __truediv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,PyImath::FixedArray2D<float>)
        
        __truediv__( (Color4fArray2D)arg1, (Color4fArray2D)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __truediv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,PyImath::FixedArray2D<Imath_2_4::Color4<float> >)
        
        __truediv__( (Color4fArray2D)arg1, (Color4f)arg2) -> Color4fArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<float> > __truediv__(PyImath::FixedArray2D<Imath_2_4::Color4<float> >,Imath_2_4::Color4<float>)
        """
        pass

    a = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    b = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    g = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    r = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


