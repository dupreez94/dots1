# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Shear6d(__Boost_Python.instance):
    """ Shear6d """
    def baseTypeEpsilon(self): # real signature unknown; restored from __doc__
        """
        baseTypeEpsilon() -> float :
        
            C++ signature :
                double baseTypeEpsilon()
        """
        return 0.0

    def baseTypeMax(self): # real signature unknown; restored from __doc__
        """
        baseTypeMax() -> float :
        
            C++ signature :
                double baseTypeMax()
        """
        return 0.0

    def baseTypeMin(self): # real signature unknown; restored from __doc__
        """
        baseTypeMin() -> float :
        
            C++ signature :
                double baseTypeMin()
        """
        return 0.0

    def baseTypeSmallest(self): # real signature unknown; restored from __doc__
        """
        baseTypeSmallest() -> float :
        
            C++ signature :
                double baseTypeSmallest()
        """
        return 0.0

    def equalWithAbsError(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithAbsError( (Shear6d)arg1, (Shear6d)arg2, (float)arg3) -> bool :
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>,double)
        """
        pass

    def equalWithRelError(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithRelError( (Shear6d)arg1, (Shear6d)arg2, (float)arg3) -> bool :
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>,double)
        """
        pass

    def getValue(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        getValue( (Shear6d)arg1, (Shear6d)arg2) -> None :
        
            C++ signature :
                void getValue(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double> {lvalue})
        """
        pass

    def negate(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        negate( (Shear6d)arg1) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> negate(Imath_2_4::Shear6<double> {lvalue})
        """
        pass

    def setValue(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setValue( (Shear6d)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6, (float)arg7) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Shear6<double> {lvalue},double,double,double,double,double,double)
        
        setValue( (Shear6d)arg1, (Shear6d)arg2) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        """
        pass

    def __add__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (Shear6d)arg1, (Shear6d)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __add__(Imath_2_4::Shear6<double>,Imath_2_4::Shear6<double>)
        
        __add__( (Shear6d)arg1, (tuple)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __add__(Imath_2_4::Shear6<double> {lvalue},boost::python::tuple)
        
        __add__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __add__(Imath_2_4::Shear6<double> {lvalue},double)
        """
        pass

    def __copy__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Shear6d)arg1) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __copy__(Imath_2_4::Shear6<double>)
        """
        pass

    def __deepcopy__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Shear6d)arg1, (dict)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __deepcopy__(Imath_2_4::Shear6<double>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (Shear6d)arg1, (Shear6d)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __div__(Imath_2_4::Shear6<double>,Imath_2_4::Shear6<double>)
        
        __div__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __div__(Imath_2_4::Shear6<double>,double)
        
        __div__( (Shear6d)arg1, (tuple)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __div__(Imath_2_4::Shear6<double> {lvalue},boost::python::tuple)
        """
        pass

    def __eq__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Shear6d)arg1, (Shear6d)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        """
        pass

    def __getitem__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (Shear6d)arg1, (int)arg2) -> float :
        
            C++ signature :
                double __getitem__(Imath_2_4::Shear6<double> {lvalue},int)
        """
        pass

    def __ge__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (Shear6d)arg1, (Shear6d)arg2) -> bool :
        
            C++ signature :
                bool __ge__(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        """
        pass

    def __gt__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (Shear6d)arg1, (Shear6d)arg2) -> bool :
        
            C++ signature :
                bool __gt__(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        """
        pass

    def __iadd__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (Shear6d)arg1, (Shear6d)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __iadd__(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        """
        pass

    def __idiv__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (Shear6d)arg1, (Shear6d)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __idiv__(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        
        __idiv__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __idiv__(Imath_2_4::Shear6<double> {lvalue},double)
        """
        pass

    def __imul__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (Shear6d)arg1, (Shear6d)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __imul__(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        
        __imul__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __imul__(Imath_2_4::Shear6<double> {lvalue},double)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (Shear6d)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Shear6<double>)
        
        __init__( (object)arg1) -> None :
            default construction: (0 0 0 0 0 0)
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4) -> None :
            Shear(XY,XZ,YZ) construction: (XY XZ YZ 0 0 0)
        
            C++ signature :
                void __init__(_object*,double,double,double)
        
        __init__( (object)arg1, (V3f)arg2) -> None :
            Shear(v) construction: (v.x v.y v.z 0 0 0)
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<float>)
        
        __init__( (object)arg1, (V3d)arg2) -> None :
            Shear(v) construction: (v.x v.y v.z 0 0 0)
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<double>)
        
        __init__( (object)arg1, (V3i)arg2) -> None :
            Shear(v) construction: (v.x v.y v.z 0 0 0)
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<int>)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6, (float)arg7) -> None :
            Shear(XY, XZ, YZ, YX, ZX, ZY) construction
        
            C++ signature :
                void __init__(_object*,double,double,double,double,double,double)
        
        __init__( (object)arg1, (float)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,double)
        
        __init__( (object)arg1, (tuple)arg2) -> object :
            Construction from tuple
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple)
        
        __init__( (object)arg1, (Shear6f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Shear6<float>)
        
        __init__( (object)arg1, (Shear6d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Shear6<double>)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Shear6<int>)
        """
        pass

    def __isub__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (Shear6d)arg1, (Shear6d)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __isub__(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        """
        pass

    def __itruediv__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (Shear6d)arg1, (Shear6d)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __itruediv__(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        
        __itruediv__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __itruediv__(Imath_2_4::Shear6<double> {lvalue},double)
        """
        pass

    def __len__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (Shear6d)arg1) -> int :
        
            C++ signature :
                int __len__(Imath_2_4::Shear6<double> {lvalue})
        """
        pass

    def __le__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (Shear6d)arg1, (Shear6d)arg2) -> bool :
        
            C++ signature :
                bool __le__(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        """
        pass

    def __lt__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (Shear6d)arg1, (Shear6d)arg2) -> bool :
        
            C++ signature :
                bool __lt__(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        """
        pass

    def __mul__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (Shear6d)arg1, (Shear6d)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __mul__(Imath_2_4::Shear6<double>,Imath_2_4::Shear6<double>)
        
        __mul__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __mul__(Imath_2_4::Shear6<double>,double)
        
        __mul__( (Shear6d)arg1, (tuple)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __mul__(Imath_2_4::Shear6<double> {lvalue},boost::python::tuple)
        """
        pass

    def __neg__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (Shear6d)arg1) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __neg__(Imath_2_4::Shear6<double>)
        """
        pass

    def __ne__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Shear6d)arg1, (Shear6d)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Shear6<double> {lvalue},Imath_2_4::Shear6<double>)
        """
        pass

    def __radd__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (Shear6d)arg1, (tuple)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __radd__(Imath_2_4::Shear6<double> {lvalue},boost::python::tuple)
        
        __radd__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __radd__(Imath_2_4::Shear6<double> {lvalue},double)
        """
        pass

    def __rdiv__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rdiv__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __rdiv__(Imath_2_4::Shear6<double> {lvalue},double)
        
        __rdiv__( (Shear6d)arg1, (tuple)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __rdiv__(Imath_2_4::Shear6<double> {lvalue},boost::python::tuple)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Shear6d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Shear6<double>)
        """
        pass

    def __rmul__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __rmul__(Imath_2_4::Shear6<double>,double)
        
        __rmul__( (Shear6d)arg1, (tuple)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __rmul__(Imath_2_4::Shear6<double> {lvalue},boost::python::tuple)
        """
        pass

    def __rsub__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (Shear6d)arg1, (tuple)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __rsub__(Imath_2_4::Shear6<double> {lvalue},boost::python::tuple)
        
        __rsub__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __rsub__(Imath_2_4::Shear6<double> {lvalue},double)
        """
        pass

    def __setitem__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (Shear6d)arg1, (int)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(Imath_2_4::Shear6<double> {lvalue},int,double)
        """
        pass

    def __str__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (Shear6d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Shear6<double>)
        """
        pass

    def __sub__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (Shear6d)arg1, (Shear6d)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __sub__(Imath_2_4::Shear6<double>,Imath_2_4::Shear6<double>)
        
        __sub__( (Shear6d)arg1, (tuple)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __sub__(Imath_2_4::Shear6<double> {lvalue},boost::python::tuple)
        
        __sub__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __sub__(Imath_2_4::Shear6<double> {lvalue},double)
        """
        pass

    def __truediv__(self, Shear6d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (Shear6d)arg1, (Shear6d)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __truediv__(Imath_2_4::Shear6<double>,Imath_2_4::Shear6<double>)
        
        __truediv__( (Shear6d)arg1, (float)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __truediv__(Imath_2_4::Shear6<double>,double)
        
        __truediv__( (Shear6d)arg1, (tuple)arg2) -> Shear6d :
        
            C++ signature :
                Imath_2_4::Shear6<double> __truediv__(Imath_2_4::Shear6<double> {lvalue},boost::python::tuple)
        """
        pass

    __instance_size__ = 64


