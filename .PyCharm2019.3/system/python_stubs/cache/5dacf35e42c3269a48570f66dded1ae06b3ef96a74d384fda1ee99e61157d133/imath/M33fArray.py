# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class M33fArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Matrix33 """
    def ifelse(self, M33fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (M33fArray)arg1, (IntArray)arg2, (M33f)arg3) -> M33fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix33<float> > ifelse(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Matrix33<float>)
        
        ifelse( (M33fArray)arg1, (IntArray)arg2, (M33fArray)arg3) -> M33fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix33<float> > ifelse(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Matrix33<float> >)
        """
        pass

    def __getitem__(self, M33fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (M33fArray)arg1, (object)arg2) -> M33fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix33<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue},_object*)
        
        __getitem__( (M33fArray)arg1, (IntArray)arg2) -> M33fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix33<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (M33fArray)arg1, (int)arg2) -> M33f :
        
            C++ signature :
                Imath_2_4::Matrix33<float> __getitem__(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue},long)
        
        __getitem__( (M33fArray)arg1, (int)arg2) -> M33f :
        
            C++ signature :
                Imath_2_4::Matrix33<float> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (M33fArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Matrix33<float> >)
        
        __init__( (object)arg1, (M33f)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Matrix33<float>,unsigned long)
        
        __init__( (object)arg1, (M33fArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Matrix33<float> >)
        """
        pass

    def __len__(self, M33fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (M33fArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue})
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, M33fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (M33fArray)arg1, (object)arg2, (M33f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue},_object*,Imath_2_4::Matrix33<float>)
        
        __setitem__( (M33fArray)arg1, (IntArray)arg2, (M33f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Matrix33<float>)
        
        __setitem__( (M33fArray)arg1, (object)arg2, (M33fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Matrix33<float> >)
        
        __setitem__( (M33fArray)arg1, (IntArray)arg2, (M33fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Matrix33<float> >)
        
        __setitem__( (M33fArray)arg1, (int)arg2, (M33f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix33<float> > {lvalue},long,Imath_2_4::Matrix33<float>)
        """
        pass

    __instance_size__ = 72


