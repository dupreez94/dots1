# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class StringArray(__Boost_Python.instance):
    # no doc
    def __eq__(self, StringArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (StringArray)arg1, (StringArray)arg2) -> object :
        
            C++ signature :
                _object* __eq__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >)
        
        __eq__( (StringArray)arg1, (str)arg2) -> object :
        
            C++ signature :
                _object* __eq__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        
        __eq__( (StringArray)arg1, (str)arg2) -> object :
        
            C++ signature :
                _object* __eq__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def __getitem__(self, StringArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (StringArray)arg1, (object)arg2) -> StringArray :
        
            C++ signature :
                PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >* __getitem__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},_object*)
        
        __getitem__( (StringArray)arg1, (int)arg2) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __getitem__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,unsigned long)
        
        __init__( (object)arg1, (str)arg2, (int)arg3) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,unsigned long)
        """
        pass

    def __len__(self, StringArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (StringArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue})
        """
        pass

    def __ne__(self, StringArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (StringArray)arg1, (StringArray)arg2) -> object :
        
            C++ signature :
                _object* __ne__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >)
        
        __ne__( (StringArray)arg1, (str)arg2) -> object :
        
            C++ signature :
                _object* __ne__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        
        __ne__( (StringArray)arg1, (str)arg2) -> object :
        
            C++ signature :
                _object* __ne__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, StringArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (StringArray)arg1, (object)arg2, (str)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},_object*,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        
        __setitem__( (StringArray)arg1, (IntArray)arg2, (str)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},PyImath::FixedArray<int>,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        
        __setitem__( (StringArray)arg1, (object)arg2, (StringArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},_object*,PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >)
        
        __setitem__( (StringArray)arg1, (IntArray)arg2, (StringArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > {lvalue},PyImath::FixedArray<int>,PyImath::StringArrayT<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >)
        """
        pass


