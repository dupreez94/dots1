# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Color4f(__Boost_Python.instance):
    """ Color4f """
    def baseTypeEpsilon(self): # real signature unknown; restored from __doc__
        """
        baseTypeEpsilon() -> float :
            baseTypeEpsilon() epsilon value of the base type of the color
        
            C++ signature :
                float baseTypeEpsilon()
        """
        return 0.0

    def baseTypeMax(self): # real signature unknown; restored from __doc__
        """
        baseTypeMax() -> float :
            baseTypeMax() max value of the base type of the color
        
            C++ signature :
                float baseTypeMax()
        """
        return 0.0

    def baseTypeMin(self): # real signature unknown; restored from __doc__
        """
        baseTypeMin() -> float :
            baseTypeMin() min value of the base type of the color
        
            C++ signature :
                float baseTypeMin()
        """
        return 0.0

    def baseTypeSmallest(self): # real signature unknown; restored from __doc__
        """
        baseTypeSmallest() -> float :
            baseTypeSmallest() smallest value of the base type of the color
        
            C++ signature :
                float baseTypeSmallest()
        """
        return 0.0

    def dimensions(self): # real signature unknown; restored from __doc__
        """
        dimensions() -> int :
            dimensions() number of dimensions in the color
        
            C++ signature :
                unsigned int dimensions()
        """
        return 0

    def getValue(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        getValue( (Color4f)arg1, (Color4f)arg2) -> None :
            getValue()
        
            C++ signature :
                void getValue(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float> {lvalue})
        
        getValue( (Color4f)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5) -> None :
        
            C++ signature :
                void getValue(Imath_2_4::Color4<float> {lvalue},float {lvalue},float {lvalue},float {lvalue},float {lvalue})
        """
        pass

    def hsv2rgb(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        hsv2rgb( (Color4f)arg1) -> Color4f :
            C.hsv2rgb() -- returns a new color which is C converted from RGB to HSV
        
            C++ signature :
                Imath_2_4::Color4<float> hsv2rgb(Imath_2_4::Color4<float> {lvalue})
        
        hsv2rgb( (tuple)arg1) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> hsv2rgb(boost::python::tuple)
        """
        pass

    def negate(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        negate( (Color4f)arg1) -> Color4f :
            component-wise multiplication by -1
        
            C++ signature :
                Imath_2_4::Color4<float> negate(Imath_2_4::Color4<float> {lvalue})
        """
        pass

    def rgb2hsv(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rgb2hsv( (Color4f)arg1) -> Color4f :
            C.rgb2hsv() -- returns a new color which is C converted from HSV to RGB
        
            C++ signature :
                Imath_2_4::Color4<float> rgb2hsv(Imath_2_4::Color4<float> {lvalue})
        
        rgb2hsv( (tuple)arg1) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> rgb2hsv(boost::python::tuple)
        """
        pass

    def setValue(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setValue( (Color4f)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5) -> None :
            C1.setValue(C2)
            C1.setValue(a,b,c) -- set C1's  elements
        
            C++ signature :
                void setValue(Imath_2_4::Color4<float> {lvalue},float,float,float,float)
        
        setValue( (Color4f)arg1, (Color4f)arg2) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        
        setValue( (Color4f)arg1, (tuple)arg2) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Color4<float> {lvalue},boost::python::tuple)
        """
        pass

    def __add__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (Color4f)arg1, (Color4f)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __add__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        
        __add__( (Color4f)arg1, (tuple)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __add__(Imath_2_4::Color4<float> {lvalue},boost::python::tuple)
        
        __add__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __add__(Imath_2_4::Color4<float> {lvalue},float)
        """
        pass

    def __copy__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Color4f)arg1) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __copy__(Imath_2_4::Color4<float>)
        """
        pass

    def __deepcopy__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Color4f)arg1, (dict)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __deepcopy__(Imath_2_4::Color4<float>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (Color4f)arg1, (Color4f)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __div__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        
        __div__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __div__(Imath_2_4::Color4<float> {lvalue},float)
        
        __div__( (Color4f)arg1, (tuple)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __div__(Imath_2_4::Color4<float> {lvalue},boost::python::tuple)
        """
        pass

    def __eq__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Color4f)arg1, (Color4f)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __getitem__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (Color4f)arg1, (int)arg2) -> float :
        
            C++ signature :
                float {lvalue} __getitem__(Imath_2_4::Color4<float> {lvalue},long)
        """
        pass

    def __ge__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (Color4f)arg1, (Color4f)arg2) -> bool :
        
            C++ signature :
                bool __ge__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __gt__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (Color4f)arg1, (Color4f)arg2) -> bool :
        
            C++ signature :
                bool __gt__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __iadd__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (Color4f)arg1, (Color4f)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __iadd__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __idiv__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (Color4f)arg1, (Color4f)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __idiv__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        
        __idiv__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __idiv__(Imath_2_4::Color4<float> {lvalue},float)
        """
        pass

    def __imul__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (Color4f)arg1, (Color4f)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __imul__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        
        __imul__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __imul__(Imath_2_4::Color4<float> {lvalue},float)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (Color4f)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Color4<float>)
        
        __init__( (object)arg1) -> object :
            initialize to (0,0,0,0)
        
            C++ signature :
                void* __init__(boost::python::api::object)
        
        __init__( (object)arg1, (tuple)arg2) -> object :
            initialize to (r,g,b,a) with a python tuple
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple)
        
        __init__( (object)arg1, (list)arg2) -> object :
            initialize to (r,g,b,a) with a python list
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::list)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,float,float,float,float)
        
        __init__( (object)arg1, (int)arg2, (int)arg3, (int)arg4, (int)arg5) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,int,int,int,int)
        
        __init__( (object)arg1, (float)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,float)
        
        __init__( (object)arg1, (int)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,int)
        
        __init__( (object)arg1, (Color4f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Color4<float>)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Color4<int>)
        
        __init__( (object)arg1, (Color4c)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __isub__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (Color4f)arg1, (Color4f)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __isub__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __itruediv__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (Color4f)arg1, (Color4f)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __itruediv__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        
        __itruediv__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __itruediv__(Imath_2_4::Color4<float> {lvalue},float)
        """
        pass

    def __len__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (Color4f)arg1) -> int :
        
            C++ signature :
                long __len__(Imath_2_4::Color4<float>)
        """
        pass

    def __le__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (Color4f)arg1, (Color4f)arg2) -> bool :
        
            C++ signature :
                bool __le__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __lt__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (Color4f)arg1, (Color4f)arg2) -> bool :
        
            C++ signature :
                bool __lt__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __mul__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (Color4f)arg1, (Color4f)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __mul__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        
        __mul__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __mul__(Imath_2_4::Color4<float> {lvalue},float)
        
        __mul__( (Color4f)arg1, (tuple)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __mul__(Imath_2_4::Color4<float> {lvalue},boost::python::tuple)
        """
        pass

    def __neg__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (Color4f)arg1) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __neg__(Imath_2_4::Color4<float> {lvalue})
        """
        pass

    def __ne__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Color4f)arg1, (Color4f)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        """
        pass

    def __radd__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (Color4f)arg1, (tuple)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __radd__(Imath_2_4::Color4<float> {lvalue},boost::python::tuple)
        
        __radd__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __radd__(Imath_2_4::Color4<float> {lvalue},float)
        """
        pass

    def __rdiv__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rdiv__( (Color4f)arg1, (tuple)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __rdiv__(Imath_2_4::Color4<float> {lvalue},boost::python::tuple)
        
        __rdiv__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __rdiv__(Imath_2_4::Color4<float> {lvalue},float)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Color4f)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Color4<float>)
        
        __repr__( (Color4f)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Color4<float>)
        """
        pass

    def __rmul__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __rmul__(Imath_2_4::Color4<float> {lvalue},float)
        
        __rmul__( (Color4f)arg1, (tuple)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __rmul__(Imath_2_4::Color4<float> {lvalue},boost::python::tuple)
        """
        pass

    def __rsub__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (Color4f)arg1, (tuple)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __rsub__(Imath_2_4::Color4<float> {lvalue},boost::python::tuple)
        
        __rsub__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __rsub__(Imath_2_4::Color4<float>,float)
        """
        pass

    def __setitem__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (Color4f)arg1, (int)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(Imath_2_4::Color4<float> {lvalue},long,float)
        """
        pass

    def __str__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (Color4f)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Color4<float>)
        """
        pass

    def __sub__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (Color4f)arg1, (Color4f)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __sub__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        
        __sub__( (Color4f)arg1, (tuple)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __sub__(Imath_2_4::Color4<float> {lvalue},boost::python::tuple)
        
        __sub__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __sub__(Imath_2_4::Color4<float>,float)
        """
        pass

    def __truediv__(self, Color4f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (Color4f)arg1, (Color4f)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __truediv__(Imath_2_4::Color4<float> {lvalue},Imath_2_4::Color4<float>)
        
        __truediv__( (Color4f)arg1, (float)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __truediv__(Imath_2_4::Color4<float> {lvalue},float)
        
        __truediv__( (Color4f)arg1, (tuple)arg2) -> Color4f :
        
            C++ signature :
                Imath_2_4::Color4<float> __truediv__(Imath_2_4::Color4<float> {lvalue},boost::python::tuple)
        """
        pass

    a = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    b = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    g = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    r = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 32


