# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Line3f(__Boost_Python.instance):
    # no doc
    def closestPoints(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        closestPoints( (Line3f)arg1, (Line3f)arg2, (V3f)arg3, (V3f)arg4) -> None :
            l1.closestPoints(l2,p0,p1)
        
            C++ signature :
                void closestPoints(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Line3<float>,Imath_2_4::Vec3<float> {lvalue},Imath_2_4::Vec3<float> {lvalue})
        
        closestPoints( (Line3f)arg1, (Line3f)arg2) -> tuple :
            l1.closestPoints(l2) -- returns a tuple with
            two points:
               (l1.closestPoint(l2), l2.closestPoint(l1)
            
        
            C++ signature :
                boost::python::tuple closestPoints(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Line3<float>)
        """
        pass

    def closestPointTo(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        closestPointTo( (Line3f)arg1, (V3f)arg2) -> V3f :
            l.closestPointTo(p) -- returns the point on
               line l that is closest to point p
            
            
        
            C++ signature :
                Imath_2_4::Vec3<float> closestPointTo(Imath_2_4::Line3<float>,Imath_2_4::Vec3<float>)
        
        closestPointTo( (Line3f)arg1, (tuple)arg2) -> V3f :
        
            C++ signature :
                Imath_2_4::Vec3<float> closestPointTo(Imath_2_4::Line3<float>,boost::python::tuple)
        
        closestPointTo( (Line3f)arg1, (Line3f)arg2) -> V3f :
            l1.closestPointTo(l2) -- returns the point on
               line l1 that is closest to line l2
            
        
            C++ signature :
                Imath_2_4::Vec3<float> closestPointTo(Imath_2_4::Line3<float>,Imath_2_4::Line3<float>)
        """
        pass

    def closestTriangleVertex(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        closestTriangleVertex( (Line3f)arg1, (V3f)arg2, (V3f)arg3, (V3f)arg4) -> V3f :
            l.closestTriangleVertex(v0, v1, v2) -- returns
            a copy of v0, v1, or v2, depending on which is
            closest to line l.
            
        
            C++ signature :
                Imath_2_4::Vec3<float> closestTriangleVertex(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>)
        
        closestTriangleVertex( (Line3f)arg1, (tuple)arg2, (tuple)arg3, (tuple)arg4) -> V3f :
        
            C++ signature :
                Imath_2_4::Vec3<float> closestTriangleVertex(Imath_2_4::Line3<float> {lvalue},boost::python::tuple,boost::python::tuple,boost::python::tuple)
        """
        pass

    def dir(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dir( (Line3f)arg1) -> V3f :
            l.dir() -- returns the direction of line l
            
        
            C++ signature :
                Imath_2_4::Vec3<float> dir(Imath_2_4::Line3<float> {lvalue})
        """
        pass

    def distanceTo(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        distanceTo( (Line3f)arg1, (V3f)arg2) -> float :
            l.distanceTo(p) -- returns the distance from
               line l to point p
            
        
            C++ signature :
                float distanceTo(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Vec3<float> {lvalue})
        
        distanceTo( (Line3f)arg1, (Line3f)arg2) -> float :
            l1.distanceTo(l2) -- returns the distance from
               line l1 to line l2
            
        
            C++ signature :
                float distanceTo(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Line3<float> {lvalue})
        
        distanceTo( (Line3f)arg1, (tuple)arg2) -> float :
        
            C++ signature :
                float distanceTo(Imath_2_4::Line3<float>,boost::python::tuple)
        """
        pass

    def intersectWithTriangle(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        intersectWithTriangle( (Line3f)arg1, (V3f)arg2, (V3f)arg3, (V3f)arg4) -> object :
        
            C++ signature :
                boost::python::api::object intersectWithTriangle(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>)
        
        intersectWithTriangle( (Line3f)arg1, (V3f)arg2, (V3f)arg3, (V3f)arg4, (V3f)arg5, (V3f)arg6, (bool)arg7) -> bool :
            l.intersectWithTriangle(v0, v1, v2) -- computes the
            intersection of line l and triangle (v0, v1, v2).
            
            If the line and the triangle do not intersect,
            None is returned.
            If the line and the triangle intersect, a tuple
            (p, b, f) is returned:
            
               p  intersection point in 3D space
            
               b  intersection point in barycentric coordinates
            
               f  1 if the line hits the triangle from the
                  front (((v2-v1) % (v1-v2)) ^ l.dir() < 0),
                  0 if the line hits the trianble from the
                  back
            
            
        
            C++ signature :
                bool intersectWithTriangle(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float> {lvalue},Imath_2_4::Vec3<float> {lvalue},bool {lvalue})
        
        intersectWithTriangle( (Line3f)arg1, (tuple)arg2, (tuple)arg3, (tuple)arg4) -> tuple :
        
            C++ signature :
                boost::python::tuple intersectWithTriangle(Imath_2_4::Line3<float> {lvalue},boost::python::tuple,boost::python::tuple,boost::python::tuple)
        """
        pass

    def pointAt(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        pointAt( (Line3f)arg1, (float)arg2) -> V3f :
            l.pointAt(t) -- returns l.pos() + t * l.dir()
        
            C++ signature :
                Imath_2_4::Vec3<float> pointAt(Imath_2_4::Line3<float> {lvalue},float)
        """
        pass

    def pos(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        pos( (Line3f)arg1) -> V3f :
            l.pos() -- returns the start point of line l
        
            C++ signature :
                Imath_2_4::Vec3<float> pos(Imath_2_4::Line3<float> {lvalue})
        """
        pass

    def rotatePoint(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rotatePoint( (Line3f)arg1, (V3f)arg2, (float)arg3) -> V3f :
            l.rotatePoint(p,r) -- rotates point p around
            line by angle r (in radians), and returns the
            result (p is not modified)
            
        
            C++ signature :
                Imath_2_4::Vec3<float> rotatePoint(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Vec3<float>,float)
        
        rotatePoint( (Line3f)arg1, (tuple)arg2, (float)arg3) -> V3f :
        
            C++ signature :
                Imath_2_4::Vec3<float> rotatePoint(Imath_2_4::Line3<float> {lvalue},boost::python::tuple,float)
        """
        pass

    def set(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set( (Line3f)arg1, (V3f)arg2, (V3f)arg3) -> None :
            l.set(p1, p2) -- sets the start point
            and direction of line l by calling
               l.setPos (p1)
               l.setDir (p2 - p1)
            
        
            C++ signature :
                void set(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>)
        
        set( (Line3f)arg1, (tuple)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void set(Imath_2_4::Line3<float> {lvalue},boost::python::tuple,boost::python::tuple)
        """
        pass

    def setDir(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setDir( (Line3f)arg1, (V3f)arg2) -> None :
            l.setDir(d) -- sets the direction of line l
            to d.normalized().
            
        
            C++ signature :
                void setDir(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Vec3<float>)
        
        setDir( (Line3f)arg1, (tuple)arg2) -> None :
        
            C++ signature :
                void setDir(Imath_2_4::Line3<float> {lvalue},boost::python::tuple)
        """
        pass

    def setPos(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setPos( (Line3f)arg1, (V3f)arg2) -> None :
            l.setPos(p) -- sets the start point of line l to p
        
            C++ signature :
                void setPos(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Vec3<float>)
        
        setPos( (Line3f)arg1, (tuple)arg2) -> None :
        
            C++ signature :
                void setPos(Imath_2_4::Line3<float> {lvalue},boost::python::tuple)
        """
        pass

    def __copy__(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Line3f)arg1) -> Line3f :
        
            C++ signature :
                Imath_2_4::Line3<float> __copy__(Imath_2_4::Line3<float>)
        """
        pass

    def __deepcopy__(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Line3f)arg1, (dict)arg2) -> Line3f :
        
            C++ signature :
                Imath_2_4::Line3<float> __deepcopy__(Imath_2_4::Line3<float>,boost::python::dict {lvalue})
        """
        pass

    def __eq__(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Line3f)arg1, (Line3f)arg2) -> bool :
        
            C++ signature :
                bool __eq__(Imath_2_4::Line3<float>,Imath_2_4::Line3<float>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1) -> object :
            initialize point to (0,0,0) and direction to (1,0,0)
        
            C++ signature :
                void* __init__(boost::python::api::object)
        
        __init__( (object)arg1, (tuple)arg2, (tuple)arg3) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple,boost::python::tuple)
        
        __init__( (object)arg1, (Line3f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Line3<float>)
        
        __init__( (object)arg1, (Line3d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Line3<double>)
        
        __init__( (object)arg1, (V3f)arg2, (V3f)arg3) -> None :
            Line3(point1, point2) construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>)
        
        __init__( (object)arg1, (V3d)arg2, (V3d)arg3) -> None :
            Line3(point1, point2) construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        """
        pass

    def __mul__(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (Line3f)arg1, (M44f)arg2) -> object :
        
            C++ signature :
                _object* __mul__(Imath_2_4::Line3<float> {lvalue},Imath_2_4::Matrix44<float>)
        """
        pass

    def __ne__(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Line3f)arg1, (Line3f)arg2) -> bool :
        
            C++ signature :
                bool __ne__(Imath_2_4::Line3<float>,Imath_2_4::Line3<float>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Line3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Line3f)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Line3<float>)
        """
        pass

    __instance_size__ = 40


