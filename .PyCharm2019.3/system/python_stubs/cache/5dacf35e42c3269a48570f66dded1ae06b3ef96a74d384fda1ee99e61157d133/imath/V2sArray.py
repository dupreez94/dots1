# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V2sArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Vec2 """
    def bounds(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        bounds( (V2sArray)arg1) -> Box2s :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec2<short> > bounds(PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def cross(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        cross( (V2sArray)arg1, (V2s)x) -> ShortArray :
            cross(x) - return the cross product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<short> cross(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        cross( (V2sArray)arg1, (V2sArray)x) -> ShortArray :
            cross(x) - return the cross product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<short> cross(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def dot(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V2sArray)arg1, (V2s)x) -> ShortArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<short> dot(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        dot( (V2sArray)arg1, (V2sArray)x) -> ShortArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<short> dot(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def ifelse(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (V2sArray)arg1, (IntArray)arg2, (V2s)arg3) -> V2sArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > ifelse(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec2<short>)
        
        ifelse( (V2sArray)arg1, (IntArray)arg2, (V2sArray)arg3) -> V2sArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > ifelse(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def length(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V2sArray)arg1) -> ShortArray :
        
            C++ signature :
                PyImath::FixedArray<short> length(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue})
        """
        pass

    def length2(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V2sArray)arg1) -> ShortArray :
        
            C++ signature :
                PyImath::FixedArray<short> length2(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue})
        """
        pass

    def max(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max( (V2sArray)arg1) -> V2s :
        
            C++ signature :
                Imath_2_4::Vec2<short> max(PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def min(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        min( (V2sArray)arg1) -> V2s :
        
            C++ signature :
                Imath_2_4::Vec2<short> min(PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def normalize(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V2sArray)arg1) -> V2sArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} normalize(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue})
        """
        pass

    def normalized(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V2sArray)arg1) -> V2sArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > normalized(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue})
        """
        pass

    def reduce(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (V2sArray)arg1) -> V2s :
        
            C++ signature :
                Imath_2_4::Vec2<short> reduce(PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def __add__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __add__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __add__( (V2sArray)arg1, (V2sArray)x) -> V2sArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __add__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def __copy__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V2sArray)arg1) -> V2sArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __copy__(PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def __deepcopy__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V2sArray)arg1, (dict)arg2) -> V2sArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __deepcopy__(PyImath::FixedArray<Imath_2_4::Vec2<short> >,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __div__( (V2sArray)arg1, (V2sArray)x) -> V2sArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        
        __div__( (V2sArray)arg1, (int)x) -> V2sArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},short)
        
        __div__( (V2sArray)arg1, (ShortArray)x) -> V2sArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __eq__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V2sArray)arg1, (V2s)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __eq__( (V2sArray)arg1, (V2sArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def __getitem__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V2sArray)arg1, (object)arg2) -> V2sArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},_object*)
        
        __getitem__( (V2sArray)arg1, (IntArray)arg2) -> V2sArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (V2sArray)arg1, (int)arg2) -> V2s :
        
            C++ signature :
                Imath_2_4::Vec2<short> __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},long)
        
        __getitem__( (V2sArray)arg1, (int)arg2) -> V2s :
        
            C++ signature :
                Imath_2_4::Vec2<short> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},long)
        """
        pass

    def __iadd__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __iadd__( (V2sArray)arg1, (V2sArray)x) -> V2sArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def __idiv__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __idiv__( (V2sArray)arg1, (V2sArray)x) -> V2sArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        
        __idiv__( (V2sArray)arg1, (int)x) -> V2sArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},short)
        
        __idiv__( (V2sArray)arg1, (ShortArray)x) -> V2sArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __imul__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __imul__( (V2sArray)arg1, (V2sArray)x) -> V2sArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        
        __imul__( (V2sArray)arg1, (int)x) -> V2sArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},short)
        
        __imul__( (V2sArray)arg1, (ShortArray)x) -> V2sArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (V2sArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        
        __init__( (object)arg1, (V2s)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<short>,unsigned long)
        """
        pass

    def __isub__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __isub__( (V2sArray)arg1, (V2sArray)x) -> V2sArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def __itruediv__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __itruediv__( (V2sArray)arg1, (V2sArray)x) -> V2sArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        
        __itruediv__( (V2sArray)arg1, (int)x) -> V2sArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},short)
        
        __itruediv__( (V2sArray)arg1, (ShortArray)x) -> V2sArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __len__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V2sArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue})
        """
        pass

    def __mul__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __mul__( (V2sArray)arg1, (V2sArray)x) -> V2sArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        
        __mul__( (V2sArray)arg1, (int)x) -> V2sArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},short)
        
        __mul__( (V2sArray)arg1, (ShortArray)x) -> V2sArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __neg__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V2sArray)arg1) -> V2sArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __neg__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue})
        """
        pass

    def __ne__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V2sArray)arg1, (V2s)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __ne__( (V2sArray)arg1, (V2sArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def __radd__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __radd__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __rmul__( (V2sArray)arg1, (int)x) -> V2sArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},short)
        
        __rmul__( (V2sArray)arg1, (ShortArray)x) -> V2sArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __rsub__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __rsub__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        """
        pass

    def __setitem__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V2sArray)arg1, (object)arg2, (V2s)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},_object*,Imath_2_4::Vec2<short>)
        
        __setitem__( (V2sArray)arg1, (IntArray)arg2, (V2s)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec2<short>)
        
        __setitem__( (V2sArray)arg1, (object)arg2, (V2sArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        
        __setitem__( (V2sArray)arg1, (IntArray)arg2, (V2sArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        
        __setitem__( (V2sArray)arg1, (int)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},long,boost::python::tuple)
        
        __setitem__( (V2sArray)arg1, (int)arg2, (list)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},long,boost::python::list)
        """
        pass

    def __sub__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __sub__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __sub__( (V2sArray)arg1, (V2sArray)x) -> V2sArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __sub__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        """
        pass

    def __truediv__(self, V2sArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V2sArray)arg1, (V2s)x) -> V2sArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},Imath_2_4::Vec2<short>)
        
        __truediv__( (V2sArray)arg1, (V2sArray)x) -> V2sArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<short> >)
        
        __truediv__( (V2sArray)arg1, (int)x) -> V2sArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},short)
        
        __truediv__( (V2sArray)arg1, (ShortArray)x) -> V2sArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<short> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<short> > {lvalue},PyImath::FixedArray<short>)
        """
        pass

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


