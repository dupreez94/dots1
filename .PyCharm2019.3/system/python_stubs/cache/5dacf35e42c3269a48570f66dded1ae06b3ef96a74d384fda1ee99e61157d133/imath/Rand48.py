# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Rand48(__Boost_Python.instance):
    # no doc
    def init(self, Rand48, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        init( (Rand48)arg1, (int)arg2) -> None :
            r.init(i) -- initialize with integer seed i
        
            C++ signature :
                void init(Imath_2_4::Rand48 {lvalue},unsigned long)
        """
        pass

    def nextb(self, Rand48, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        nextb( (Rand48)arg1) -> bool :
            r.nextb() -- return the next boolean value in the uniformly-distributed sequence
        
            C++ signature :
                bool nextb(Imath_2_4::Rand48 {lvalue})
        """
        pass

    def nextf(self, Rand48, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        nextf( (Rand48)arg1) -> float :
            r.nextf() -- return the next double value in the uniformly-distributed sequence
            r.nextf(double,double) -- return the next double value in the uniformly-distributed sequence
        
            C++ signature :
                double nextf(Imath_2_4::Rand48 {lvalue})
        
        nextf( (Rand48)arg1, (float)arg2, (float)arg3) -> float :
        
            C++ signature :
                double nextf(Imath_2_4::Rand48 {lvalue},double,double)
        """
        pass

    def nextGauss(self, Rand48, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        nextGauss( (Rand48)arg1) -> float :
            r.nextGauss() -- returns the next floating-point value in the normally (Gaussian) distributed sequence
        
            C++ signature :
                float nextGauss(Imath_2_4::Rand48 {lvalue})
        """
        pass

    def nextGaussSphere(self, Rand48, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        nextGaussSphere( (Rand48)arg1, (V3f)arg2) -> V3f :
            r.nextGaussSphere(v) -- returns the next point whose distance from the origin has a normal (Gaussian) distribution with mean 0 and variance 1.  The vector argument, v, specifies the dimension and number type.
        
            C++ signature :
                Imath_2_4::Vec3<float> nextGaussSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec3<float>)
        
        nextGaussSphere( (Rand48)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> nextGaussSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec3<double>)
        
        nextGaussSphere( (Rand48)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> nextGaussSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec2<float>)
        
        nextGaussSphere( (Rand48)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> nextGaussSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec2<double>)
        """
        pass

    def nextHollowSphere(self, Rand48, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        nextHollowSphere( (Rand48)arg1, (V3f)arg2) -> V3f :
            r.nextHollowSphere(v) -- return the next point uniformly distributed on the surface of a sphere of radius 1 centered at the origin.  The vector argument, v, specifies the dimension and number type.
        
            C++ signature :
                Imath_2_4::Vec3<float> nextHollowSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec3<float>)
        
        nextHollowSphere( (Rand48)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> nextHollowSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec3<double>)
        
        nextHollowSphere( (Rand48)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> nextHollowSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec2<float>)
        
        nextHollowSphere( (Rand48)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> nextHollowSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec2<double>)
        """
        pass

    def nexti(self, Rand48, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        nexti( (Rand48)arg1) -> int :
            r.nexti() -- return the next integer value in the uniformly-distributed sequence
        
            C++ signature :
                long nexti(Imath_2_4::Rand48 {lvalue})
        """
        pass

    def nextSolidSphere(self, Rand48, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        nextSolidSphere( (Rand48)arg1, (V3f)arg2) -> V3f :
            r.nextSolidSphere(v) -- return the next point uniformly distributed in a sphere of radius 1 centered at the origin.  The vector argument, v, specifies the dimension and number type.
        
            C++ signature :
                Imath_2_4::Vec3<float> nextSolidSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec3<float>)
        
        nextSolidSphere( (Rand48)arg1, (V3d)arg2) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> nextSolidSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec3<double>)
        
        nextSolidSphere( (Rand48)arg1, (V2f)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> nextSolidSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec2<float>)
        
        nextSolidSphere( (Rand48)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> nextSolidSphere(Imath_2_4::Rand48 {lvalue},Imath_2_4::Vec2<double>)
        """
        pass

    def __copy__(self, Rand48, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Rand48)arg1) -> Rand48 :
        
            C++ signature :
                Imath_2_4::Rand48 __copy__(Imath_2_4::Rand48)
        """
        pass

    def __deepcopy__(self, Rand48, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Rand48)arg1, (dict)arg2) -> Rand48 :
        
            C++ signature :
                Imath_2_4::Rand48 __deepcopy__(Imath_2_4::Rand48,boost::python::dict {lvalue})
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1) -> None :
            default construction
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (int)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,unsigned long)
        
        __init__( (object)arg1, (Rand48)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Rand48)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    __instance_size__ = 24


