# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


# Variables with simple values

DBL_EPS = 2.220446049250313e-16
DBL_MAX = 1.7976931348623157e+308
DBL_MIN = -1.7976931348623157e+308
DBL_SMALLEST = 2.2250738585072014e-308

EULER_XYX = 273
EULER_XYXr = 8208
EULER_XYZ = 257
EULER_XYZr = 8192
EULER_XZX = 17
EULER_XZXr = 8464
EULER_XZY = 1
EULER_XZYr = 8448

EULER_X_AXIS = 0

EULER_YXY = 4113
EULER_YXYr = 4368
EULER_YXZ = 4097
EULER_YXZr = 4352
EULER_YZX = 4353
EULER_YZXr = 4096
EULER_YZY = 4369
EULER_YZYr = 4112

EULER_Y_AXIS = 1

EULER_ZXY = 8449
EULER_ZXYr = 0
EULER_ZXZ = 8465
EULER_ZXZr = 16
EULER_ZYX = 8193
EULER_ZYXr = 256
EULER_ZYZ = 8209
EULER_ZYZr = 272

EULER_Z_AXIS = 2

FLT_EPS = 1.1920928955078125e-07
FLT_MAX = 3.4028234663852886e+38
FLT_MIN = -3.4028234663852886e+38
FLT_SMALLEST = 1.1754943508222875e-38

INT_EPS = 1
INT_MAX = 2147483647
INT_MIN = -2147483648
INT_SMALLEST = 1

# functions

def abs(p_int, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    abs( (int)value) -> int :
        abs(value) - return the absolute value of 'value'
    
        C++ signature :
            int abs(int)
    
    abs( (IntArray)value) -> IntArray :
        abs(value) - return the absolute value of 'value'
    
        C++ signature :
            PyImath::FixedArray<int> abs(PyImath::FixedArray<int>)
    
    abs( (float)value) -> float :
        abs(value) - return the absolute value of 'value'
    
        C++ signature :
            float abs(float)
    
    abs( (FloatArray)value) -> FloatArray :
        abs(value) - return the absolute value of 'value'
    
        C++ signature :
            PyImath::FixedArray<float> abs(PyImath::FixedArray<float>)
    
    abs( (float)value) -> float :
        abs(value) - return the absolute value of 'value'
    
        C++ signature :
            double abs(double)
    
    abs( (DoubleArray)value) -> DoubleArray :
        abs(value) - return the absolute value of 'value'
    
        C++ signature :
            PyImath::FixedArray<double> abs(PyImath::FixedArray<double>)
    """
    pass

def bias(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    bias( (float)x, (float)b) -> float :
        bias(x,b) - bias(x,b) is a gamma correction that remaps the unit interval such that bias(0.5, b) = b.
    
        C++ signature :
            float bias(float,float)
    
    bias( (float)x, (FloatArray)b) -> FloatArray :
        bias(x,b) - bias(x,b) is a gamma correction that remaps the unit interval such that bias(0.5, b) = b.
    
        C++ signature :
            PyImath::FixedArray<float> bias(float,PyImath::FixedArray<float>)
    
    bias( (FloatArray)x, (float)b) -> FloatArray :
        bias(x,b) - bias(x,b) is a gamma correction that remaps the unit interval such that bias(0.5, b) = b.
    
        C++ signature :
            PyImath::FixedArray<float> bias(PyImath::FixedArray<float>,float)
    
    bias( (FloatArray)x, (FloatArray)b) -> FloatArray :
        bias(x,b) - bias(x,b) is a gamma correction that remaps the unit interval such that bias(0.5, b) = b.
    
        C++ signature :
            PyImath::FixedArray<float> bias(PyImath::FixedArray<float>,PyImath::FixedArray<float>)
    """
    pass

def ceil(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    ceil( (float)value) -> int :
        ceil(value) - return the closest integer greater than or equal to 'value'
    
        C++ signature :
            int ceil(float)
    
    ceil( (FloatArray)value) -> IntArray :
        ceil(value) - return the closest integer greater than or equal to 'value'
    
        C++ signature :
            PyImath::FixedArray<int> ceil(PyImath::FixedArray<float>)
    
    ceil( (float)value) -> int :
        ceil(value) - return the closest integer greater than or equal to 'value'
    
        C++ signature :
            int ceil(double)
    
    ceil( (DoubleArray)value) -> IntArray :
        ceil(value) - return the closest integer greater than or equal to 'value'
    
        C++ signature :
            PyImath::FixedArray<int> ceil(PyImath::FixedArray<double>)
    """
    pass

def clamp(p_int, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    clamp( (int)value, (int)low, (int)high) -> int :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            int clamp(int,int,int)
    
    clamp( (int)value, (int)low, (IntArray)high) -> IntArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<int> clamp(int,int,PyImath::FixedArray<int>)
    
    clamp( (int)value, (IntArray)low, (int)high) -> IntArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<int> clamp(int,PyImath::FixedArray<int>,int)
    
    clamp( (int)value, (IntArray)low, (IntArray)high) -> IntArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<int> clamp(int,PyImath::FixedArray<int>,PyImath::FixedArray<int>)
    
    clamp( (IntArray)value, (int)low, (int)high) -> IntArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<int> clamp(PyImath::FixedArray<int>,int,int)
    
    clamp( (IntArray)value, (int)low, (IntArray)high) -> IntArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<int> clamp(PyImath::FixedArray<int>,int,PyImath::FixedArray<int>)
    
    clamp( (IntArray)value, (IntArray)low, (int)high) -> IntArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<int> clamp(PyImath::FixedArray<int>,PyImath::FixedArray<int>,int)
    
    clamp( (IntArray)value, (IntArray)low, (IntArray)high) -> IntArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<int> clamp(PyImath::FixedArray<int>,PyImath::FixedArray<int>,PyImath::FixedArray<int>)
    
    clamp( (float)value, (float)low, (float)high) -> float :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            float clamp(float,float,float)
    
    clamp( (float)value, (float)low, (FloatArray)high) -> FloatArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<float> clamp(float,float,PyImath::FixedArray<float>)
    
    clamp( (float)value, (FloatArray)low, (float)high) -> FloatArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<float> clamp(float,PyImath::FixedArray<float>,float)
    
    clamp( (float)value, (FloatArray)low, (FloatArray)high) -> FloatArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<float> clamp(float,PyImath::FixedArray<float>,PyImath::FixedArray<float>)
    
    clamp( (FloatArray)value, (float)low, (float)high) -> FloatArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<float> clamp(PyImath::FixedArray<float>,float,float)
    
    clamp( (FloatArray)value, (float)low, (FloatArray)high) -> FloatArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<float> clamp(PyImath::FixedArray<float>,float,PyImath::FixedArray<float>)
    
    clamp( (FloatArray)value, (FloatArray)low, (float)high) -> FloatArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<float> clamp(PyImath::FixedArray<float>,PyImath::FixedArray<float>,float)
    
    clamp( (FloatArray)value, (FloatArray)low, (FloatArray)high) -> FloatArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<float> clamp(PyImath::FixedArray<float>,PyImath::FixedArray<float>,PyImath::FixedArray<float>)
    
    clamp( (float)value, (float)low, (float)high) -> float :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            double clamp(double,double,double)
    
    clamp( (float)value, (float)low, (DoubleArray)high) -> DoubleArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<double> clamp(double,double,PyImath::FixedArray<double>)
    
    clamp( (float)value, (DoubleArray)low, (float)high) -> DoubleArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<double> clamp(double,PyImath::FixedArray<double>,double)
    
    clamp( (float)value, (DoubleArray)low, (DoubleArray)high) -> DoubleArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<double> clamp(double,PyImath::FixedArray<double>,PyImath::FixedArray<double>)
    
    clamp( (DoubleArray)value, (float)low, (float)high) -> DoubleArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<double> clamp(PyImath::FixedArray<double>,double,double)
    
    clamp( (DoubleArray)value, (float)low, (DoubleArray)high) -> DoubleArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<double> clamp(PyImath::FixedArray<double>,double,PyImath::FixedArray<double>)
    
    clamp( (DoubleArray)value, (DoubleArray)low, (float)high) -> DoubleArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<double> clamp(PyImath::FixedArray<double>,PyImath::FixedArray<double>,double)
    
    clamp( (DoubleArray)value, (DoubleArray)low, (DoubleArray)high) -> DoubleArray :
        clamp(value,low,high) - return the value clamped to the range [low,high]
    
        C++ signature :
            PyImath::FixedArray<double> clamp(PyImath::FixedArray<double>,PyImath::FixedArray<double>,PyImath::FixedArray<double>)
    """
    pass

def cmp(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    cmp( (float)arg1, (float)arg2) -> int :
    
        C++ signature :
            int cmp(float,float)
    
    cmp( (float)arg1, (float)arg2) -> int :
    
        C++ signature :
            int cmp(double,double)
    """
    pass

def cmpt(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    cmpt( (float)arg1, (float)arg2, (float)arg3) -> int :
    
        C++ signature :
            int cmpt(float,float,float)
    
    cmpt( (float)arg1, (float)arg2, (float)arg3) -> int :
    
        C++ signature :
            int cmpt(double,double,double)
    """
    pass

def computeBoundingBox(V3fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    computeBoundingBox( (V3fArray)arg1) -> Box3f :
        computeBoundingBox(position) -- computes the bounding box from the position array.
    
        C++ signature :
            Imath_2_4::Box<Imath_2_4::Vec3<float> > computeBoundingBox(PyImath::FixedArray<Imath_2_4::Vec3<float> >)
    
    computeBoundingBox( (V3dArray)arg1) -> Box3d :
        computeBoundingBox(position) -- computes the bounding box from the position array.
    
        C++ signature :
            Imath_2_4::Box<Imath_2_4::Vec3<double> > computeBoundingBox(PyImath::FixedArray<Imath_2_4::Vec3<double> >)
    """
    pass

def divp(p_int, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    divp( (int)x, (int)y) -> int :
        divp(x,y) - return x/y where the remainder is always positive:
            divp(x,y) == floor (double(x) / double (y))
        
    
        C++ signature :
            int divp(int,int)
    
    divp( (int)x, (IntArray)y) -> IntArray :
        divp(x,y) - return x/y where the remainder is always positive:
            divp(x,y) == floor (double(x) / double (y))
        
    
        C++ signature :
            PyImath::FixedArray<int> divp(int,PyImath::FixedArray<int>)
    
    divp( (IntArray)x, (int)y) -> IntArray :
        divp(x,y) - return x/y where the remainder is always positive:
            divp(x,y) == floor (double(x) / double (y))
        
    
        C++ signature :
            PyImath::FixedArray<int> divp(PyImath::FixedArray<int>,int)
    
    divp( (IntArray)x, (IntArray)y) -> IntArray :
        divp(x,y) - return x/y where the remainder is always positive:
            divp(x,y) == floor (double(x) / double (y))
        
    
        C++ signature :
            PyImath::FixedArray<int> divp(PyImath::FixedArray<int>,PyImath::FixedArray<int>)
    """
    pass

def divs(p_int, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    divs( (int)x, (int)y) -> int :
        divs(x,y) - return x/y where the remainder has the same sign as x:
            divs(x,y) == (abs(x) / abs(y)) * (sign(x) * sign(y))
        
    
        C++ signature :
            int divs(int,int)
    
    divs( (int)x, (IntArray)y) -> IntArray :
        divs(x,y) - return x/y where the remainder has the same sign as x:
            divs(x,y) == (abs(x) / abs(y)) * (sign(x) * sign(y))
        
    
        C++ signature :
            PyImath::FixedArray<int> divs(int,PyImath::FixedArray<int>)
    
    divs( (IntArray)x, (int)y) -> IntArray :
        divs(x,y) - return x/y where the remainder has the same sign as x:
            divs(x,y) == (abs(x) / abs(y)) * (sign(x) * sign(y))
        
    
        C++ signature :
            PyImath::FixedArray<int> divs(PyImath::FixedArray<int>,int)
    
    divs( (IntArray)x, (IntArray)y) -> IntArray :
        divs(x,y) - return x/y where the remainder has the same sign as x:
            divs(x,y) == (abs(x) / abs(y)) * (sign(x) * sign(y))
        
    
        C++ signature :
            PyImath::FixedArray<int> divs(PyImath::FixedArray<int>,PyImath::FixedArray<int>)
    """
    pass

def equal(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    equal( (float)arg1, (float)arg2, (float)arg3) -> bool :
    
        C++ signature :
            bool equal(float,float,float)
    
    equal( (float)arg1, (float)arg2, (float)arg3) -> bool :
    
        C++ signature :
            bool equal(double,double,double)
    """
    pass

def floor(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    floor( (float)value) -> int :
        floor(value) - return the closest integer less than or equal to 'value'
    
        C++ signature :
            int floor(float)
    
    floor( (FloatArray)value) -> IntArray :
        floor(value) - return the closest integer less than or equal to 'value'
    
        C++ signature :
            PyImath::FixedArray<int> floor(PyImath::FixedArray<float>)
    
    floor( (float)value) -> int :
        floor(value) - return the closest integer less than or equal to 'value'
    
        C++ signature :
            int floor(double)
    
    floor( (DoubleArray)value) -> IntArray :
        floor(value) - return the closest integer less than or equal to 'value'
    
        C++ signature :
            PyImath::FixedArray<int> floor(PyImath::FixedArray<double>)
    """
    pass

def gain(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    gain( (float)x, (float)g) -> float :
        gain(x,g) - gain(x,g) is a gamma correction that remaps the unit interval with the property that gain(0.5, g) = 0.5.
        The gain function can be thought of as two scaled bias curves forming an 'S' shape in the unit interval.
    
        C++ signature :
            float gain(float,float)
    
    gain( (float)x, (FloatArray)g) -> FloatArray :
        gain(x,g) - gain(x,g) is a gamma correction that remaps the unit interval with the property that gain(0.5, g) = 0.5.
        The gain function can be thought of as two scaled bias curves forming an 'S' shape in the unit interval.
    
        C++ signature :
            PyImath::FixedArray<float> gain(float,PyImath::FixedArray<float>)
    
    gain( (FloatArray)x, (float)g) -> FloatArray :
        gain(x,g) - gain(x,g) is a gamma correction that remaps the unit interval with the property that gain(0.5, g) = 0.5.
        The gain function can be thought of as two scaled bias curves forming an 'S' shape in the unit interval.
    
        C++ signature :
            PyImath::FixedArray<float> gain(PyImath::FixedArray<float>,float)
    
    gain( (FloatArray)x, (FloatArray)g) -> FloatArray :
        gain(x,g) - gain(x,g) is a gamma correction that remaps the unit interval with the property that gain(0.5, g) = 0.5.
        The gain function can be thought of as two scaled bias curves forming an 'S' shape in the unit interval.
    
        C++ signature :
            PyImath::FixedArray<float> gain(PyImath::FixedArray<float>,PyImath::FixedArray<float>)
    """
    pass

def hollowSphereRand(Rand32, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    hollowSphereRand( (Rand32)randObj, (int)num) -> V3fArray :
        hollowSphereRand(randObj,num) return XYZ vectors uniformly distributed across the surface of a sphere generated from the given Rand32 object
    
        C++ signature :
            PyImath::FixedArray<Imath_2_4::Vec3<float> > hollowSphereRand(Imath_2_4::Rand32 {lvalue},int)
    """
    pass

def iszero(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    iszero( (float)arg1, (float)arg2) -> bool :
    
        C++ signature :
            bool iszero(float,float)
    
    iszero( (float)arg1, (float)arg2) -> bool :
    
        C++ signature :
            bool iszero(double,double)
    """
    pass

def lerp(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    lerp( (float)a, (float)b, (float)t) -> float :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            float lerp(float,float,float)
    
    lerp( (float)a, (float)b, (FloatArray)t) -> FloatArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<float> lerp(float,float,PyImath::FixedArray<float>)
    
    lerp( (float)a, (FloatArray)b, (float)t) -> FloatArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<float> lerp(float,PyImath::FixedArray<float>,float)
    
    lerp( (float)a, (FloatArray)b, (FloatArray)t) -> FloatArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<float> lerp(float,PyImath::FixedArray<float>,PyImath::FixedArray<float>)
    
    lerp( (FloatArray)a, (float)b, (float)t) -> FloatArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<float> lerp(PyImath::FixedArray<float>,float,float)
    
    lerp( (FloatArray)a, (float)b, (FloatArray)t) -> FloatArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<float> lerp(PyImath::FixedArray<float>,float,PyImath::FixedArray<float>)
    
    lerp( (FloatArray)a, (FloatArray)b, (float)t) -> FloatArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<float> lerp(PyImath::FixedArray<float>,PyImath::FixedArray<float>,float)
    
    lerp( (FloatArray)a, (FloatArray)b, (FloatArray)t) -> FloatArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<float> lerp(PyImath::FixedArray<float>,PyImath::FixedArray<float>,PyImath::FixedArray<float>)
    
    lerp( (float)a, (float)b, (float)t) -> float :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            double lerp(double,double,double)
    
    lerp( (float)a, (float)b, (DoubleArray)t) -> DoubleArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<double> lerp(double,double,PyImath::FixedArray<double>)
    
    lerp( (float)a, (DoubleArray)b, (float)t) -> DoubleArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<double> lerp(double,PyImath::FixedArray<double>,double)
    
    lerp( (float)a, (DoubleArray)b, (DoubleArray)t) -> DoubleArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<double> lerp(double,PyImath::FixedArray<double>,PyImath::FixedArray<double>)
    
    lerp( (DoubleArray)a, (float)b, (float)t) -> DoubleArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<double> lerp(PyImath::FixedArray<double>,double,double)
    
    lerp( (DoubleArray)a, (float)b, (DoubleArray)t) -> DoubleArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<double> lerp(PyImath::FixedArray<double>,double,PyImath::FixedArray<double>)
    
    lerp( (DoubleArray)a, (DoubleArray)b, (float)t) -> DoubleArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<double> lerp(PyImath::FixedArray<double>,PyImath::FixedArray<double>,double)
    
    lerp( (DoubleArray)a, (DoubleArray)b, (DoubleArray)t) -> DoubleArray :
        lerp(a,b,t) - return the linear interpolation of 'a' to 'b' using parameter 't'
    
        C++ signature :
            PyImath::FixedArray<double> lerp(PyImath::FixedArray<double>,PyImath::FixedArray<double>,PyImath::FixedArray<double>)
    """
    pass

def lerpfactor(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    lerpfactor( (float)m, (float)a, (float)b) -> float :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
        if:
            t = lerpfactor(m, a, b);
        then:
            m = lerp(a, b, t);
        
        If a==b, return 0.
        
    
        C++ signature :
            float lerpfactor(float,float,float)
    
    lerpfactor( (float)m, (float)a, (FloatArray)b) -> FloatArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
        if:
            t = lerpfactor(m, a, b);
        then:
            m = lerp(a, b, t);
        
        If a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<float> lerpfactor(float,float,PyImath::FixedArray<float>)
    
    lerpfactor( (float)m, (FloatArray)a, (float)b) -> FloatArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
        if:
            t = lerpfactor(m, a, b);
        then:
            m = lerp(a, b, t);
        
        If a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<float> lerpfactor(float,PyImath::FixedArray<float>,float)
    
    lerpfactor( (float)m, (FloatArray)a, (FloatArray)b) -> FloatArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
        if:
            t = lerpfactor(m, a, b);
        then:
            m = lerp(a, b, t);
        
        If a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<float> lerpfactor(float,PyImath::FixedArray<float>,PyImath::FixedArray<float>)
    
    lerpfactor( (FloatArray)m, (float)a, (float)b) -> FloatArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
        if:
            t = lerpfactor(m, a, b);
        then:
            m = lerp(a, b, t);
        
        If a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<float> lerpfactor(PyImath::FixedArray<float>,float,float)
    
    lerpfactor( (FloatArray)m, (float)a, (FloatArray)b) -> FloatArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
        if:
            t = lerpfactor(m, a, b);
        then:
            m = lerp(a, b, t);
        
        If a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<float> lerpfactor(PyImath::FixedArray<float>,float,PyImath::FixedArray<float>)
    
    lerpfactor( (FloatArray)m, (FloatArray)a, (float)b) -> FloatArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
        if:
            t = lerpfactor(m, a, b);
        then:
            m = lerp(a, b, t);
        
        If a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<float> lerpfactor(PyImath::FixedArray<float>,PyImath::FixedArray<float>,float)
    
    lerpfactor( (FloatArray)m, (FloatArray)a, (FloatArray)b) -> FloatArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
        if:
            t = lerpfactor(m, a, b);
        then:
            m = lerp(a, b, t);
        
        If a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<float> lerpfactor(PyImath::FixedArray<float>,PyImath::FixedArray<float>,PyImath::FixedArray<float>)
    
    lerpfactor( (float)m, (float)a, (float)b) -> float :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
            if:
                t = lerpfactor(m, a, b);
            then:
                m = lerp(a, b, t);
            if a==b, return 0.
        
    
        C++ signature :
            double lerpfactor(double,double,double)
    
    lerpfactor( (float)m, (float)a, (DoubleArray)b) -> DoubleArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
            if:
                t = lerpfactor(m, a, b);
            then:
                m = lerp(a, b, t);
            if a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<double> lerpfactor(double,double,PyImath::FixedArray<double>)
    
    lerpfactor( (float)m, (DoubleArray)a, (float)b) -> DoubleArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
            if:
                t = lerpfactor(m, a, b);
            then:
                m = lerp(a, b, t);
            if a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<double> lerpfactor(double,PyImath::FixedArray<double>,double)
    
    lerpfactor( (float)m, (DoubleArray)a, (DoubleArray)b) -> DoubleArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
            if:
                t = lerpfactor(m, a, b);
            then:
                m = lerp(a, b, t);
            if a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<double> lerpfactor(double,PyImath::FixedArray<double>,PyImath::FixedArray<double>)
    
    lerpfactor( (DoubleArray)m, (float)a, (float)b) -> DoubleArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
            if:
                t = lerpfactor(m, a, b);
            then:
                m = lerp(a, b, t);
            if a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<double> lerpfactor(PyImath::FixedArray<double>,double,double)
    
    lerpfactor( (DoubleArray)m, (float)a, (DoubleArray)b) -> DoubleArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
            if:
                t = lerpfactor(m, a, b);
            then:
                m = lerp(a, b, t);
            if a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<double> lerpfactor(PyImath::FixedArray<double>,double,PyImath::FixedArray<double>)
    
    lerpfactor( (DoubleArray)m, (DoubleArray)a, (float)b) -> DoubleArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
            if:
                t = lerpfactor(m, a, b);
            then:
                m = lerp(a, b, t);
            if a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<double> lerpfactor(PyImath::FixedArray<double>,PyImath::FixedArray<double>,double)
    
    lerpfactor( (DoubleArray)m, (DoubleArray)a, (DoubleArray)b) -> DoubleArray :
        lerpfactor(m,a,b) - return how far m is between a and b, that is return t such that
            if:
                t = lerpfactor(m, a, b);
            then:
                m = lerp(a, b, t);
            if a==b, return 0.
        
    
        C++ signature :
            PyImath::FixedArray<double> lerpfactor(PyImath::FixedArray<double>,PyImath::FixedArray<double>,PyImath::FixedArray<double>)
    """
    pass

def log(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    log( (float)value) -> float :
        log(value) - return the natural log of 'value'
    
        C++ signature :
            float log(float)
    
    log( (FloatArray)value) -> FloatArray :
        log(value) - return the natural log of 'value'
    
        C++ signature :
            PyImath::FixedArray<float> log(PyImath::FixedArray<float>)
    
    log( (float)value) -> float :
        log(value) - return the natural log of 'value'
    
        C++ signature :
            double log(double)
    
    log( (DoubleArray)value) -> DoubleArray :
        log(value) - return the natural log of 'value'
    
        C++ signature :
            PyImath::FixedArray<double> log(PyImath::FixedArray<double>)
    """
    pass

def log10(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    log10( (float)value) -> float :
        log10(value) - return the base 10 log of 'value'
    
        C++ signature :
            float log10(float)
    
    log10( (FloatArray)value) -> FloatArray :
        log10(value) - return the base 10 log of 'value'
    
        C++ signature :
            PyImath::FixedArray<float> log10(PyImath::FixedArray<float>)
    
    log10( (float)value) -> float :
        log10(value) - return the base 10 log of 'value'
    
        C++ signature :
            double log10(double)
    
    log10( (DoubleArray)value) -> DoubleArray :
        log10(value) - return the base 10 log of 'value'
    
        C++ signature :
            PyImath::FixedArray<double> log10(PyImath::FixedArray<double>)
    """
    pass

def modp(p_int, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    modp( (int)x, (int)y) -> int :
        modp(x,y) - return x%y where the remainder is always positive:
            modp(x,y) == x - y * divp(x,y)
        
    
        C++ signature :
            int modp(int,int)
    
    modp( (int)x, (IntArray)y) -> IntArray :
        modp(x,y) - return x%y where the remainder is always positive:
            modp(x,y) == x - y * divp(x,y)
        
    
        C++ signature :
            PyImath::FixedArray<int> modp(int,PyImath::FixedArray<int>)
    
    modp( (IntArray)x, (int)y) -> IntArray :
        modp(x,y) - return x%y where the remainder is always positive:
            modp(x,y) == x - y * divp(x,y)
        
    
        C++ signature :
            PyImath::FixedArray<int> modp(PyImath::FixedArray<int>,int)
    
    modp( (IntArray)x, (IntArray)y) -> IntArray :
        modp(x,y) - return x%y where the remainder is always positive:
            modp(x,y) == x - y * divp(x,y)
        
    
        C++ signature :
            PyImath::FixedArray<int> modp(PyImath::FixedArray<int>,PyImath::FixedArray<int>)
    """
    pass

def mods(p_int, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    mods( (int)x, (int)y) -> int :
        mods(x,y) - return x%y where the remainder has the same sign as x:
            mods(x,y) == x - y * divs(x,y)
        
    
        C++ signature :
            int mods(int,int)
    
    mods( (int)x, (IntArray)y) -> IntArray :
        mods(x,y) - return x%y where the remainder has the same sign as x:
            mods(x,y) == x - y * divs(x,y)
        
    
        C++ signature :
            PyImath::FixedArray<int> mods(int,PyImath::FixedArray<int>)
    
    mods( (IntArray)x, (int)y) -> IntArray :
        mods(x,y) - return x%y where the remainder has the same sign as x:
            mods(x,y) == x - y * divs(x,y)
        
    
        C++ signature :
            PyImath::FixedArray<int> mods(PyImath::FixedArray<int>,int)
    
    mods( (IntArray)x, (IntArray)y) -> IntArray :
        mods(x,y) - return x%y where the remainder has the same sign as x:
            mods(x,y) == x - y * divs(x,y)
        
    
        C++ signature :
            PyImath::FixedArray<int> mods(PyImath::FixedArray<int>,PyImath::FixedArray<int>)
    """
    pass

def procrustesRotationAndTranslation(p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    procrustesRotationAndTranslation( (object)fromPts, (object)toPts, (object)weights, (bool)doScale) -> M44d :
        Computes the orthogonal transform (consisting only of rotation and translation) mapping the 'fromPts' points as close as possible to the 'toPts' points in the least squares norm.  The 'fromPts' and 'toPts' lists must be the same length or the function will error out.  If weights are provided, then the points are weighted (that is, some points are considered more important than others while computing the transform).  If the 'doScale' parameter is True, then the resulting matrix is also allowed to have a uniform scale.
    
        C++ signature :
            Imath_2_4::Matrix44<double> procrustesRotationAndTranslation(_object*,_object*,_object*,bool)
    """
    pass

def rangeX(p_int, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    rangeX( (int)arg1, (int)arg2) -> IntArray2D :
    
        C++ signature :
            PyImath::FixedArray2D<int> rangeX(int,int)
    """
    pass

def rangeY(p_int, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    rangeY( (int)arg1, (int)arg2) -> IntArray2D :
    
        C++ signature :
            PyImath::FixedArray2D<int> rangeY(int,int)
    """
    pass

def rotationXYZWithUpDir(V3f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    rotationXYZWithUpDir( (V3f)fromDir, (V3f)toDir, (V3f)upDir) -> V3f :
        rotationXYZWithUpDir(fromDir,toDir,upDir) - return the XYZ rotation vector that rotates 'fromDir' to 'toDir'using the up vector 'upDir'
    
        C++ signature :
            Imath_2_4::Vec3<float> rotationXYZWithUpDir(Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>)
    
    rotationXYZWithUpDir( (V3f)fromDir, (V3f)toDir, (V3fArray)upDir) -> V3fArray :
        rotationXYZWithUpDir(fromDir,toDir,upDir) - return the XYZ rotation vector that rotates 'fromDir' to 'toDir'using the up vector 'upDir'
    
        C++ signature :
            PyImath::FixedArray<Imath_2_4::Vec3<float> > rotationXYZWithUpDir(Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>,PyImath::FixedArray<Imath_2_4::Vec3<float> >)
    
    rotationXYZWithUpDir( (V3f)fromDir, (V3fArray)toDir, (V3f)upDir) -> V3fArray :
        rotationXYZWithUpDir(fromDir,toDir,upDir) - return the XYZ rotation vector that rotates 'fromDir' to 'toDir'using the up vector 'upDir'
    
        C++ signature :
            PyImath::FixedArray<Imath_2_4::Vec3<float> > rotationXYZWithUpDir(Imath_2_4::Vec3<float>,PyImath::FixedArray<Imath_2_4::Vec3<float> >,Imath_2_4::Vec3<float>)
    
    rotationXYZWithUpDir( (V3f)fromDir, (V3fArray)toDir, (V3fArray)upDir) -> V3fArray :
        rotationXYZWithUpDir(fromDir,toDir,upDir) - return the XYZ rotation vector that rotates 'fromDir' to 'toDir'using the up vector 'upDir'
    
        C++ signature :
            PyImath::FixedArray<Imath_2_4::Vec3<float> > rotationXYZWithUpDir(Imath_2_4::Vec3<float>,PyImath::FixedArray<Imath_2_4::Vec3<float> >,PyImath::FixedArray<Imath_2_4::Vec3<float> >)
    
    rotationXYZWithUpDir( (V3fArray)fromDir, (V3f)toDir, (V3f)upDir) -> V3fArray :
        rotationXYZWithUpDir(fromDir,toDir,upDir) - return the XYZ rotation vector that rotates 'fromDir' to 'toDir'using the up vector 'upDir'
    
        C++ signature :
            PyImath::FixedArray<Imath_2_4::Vec3<float> > rotationXYZWithUpDir(PyImath::FixedArray<Imath_2_4::Vec3<float> >,Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>)
    
    rotationXYZWithUpDir( (V3fArray)fromDir, (V3f)toDir, (V3fArray)upDir) -> V3fArray :
        rotationXYZWithUpDir(fromDir,toDir,upDir) - return the XYZ rotation vector that rotates 'fromDir' to 'toDir'using the up vector 'upDir'
    
        C++ signature :
            PyImath::FixedArray<Imath_2_4::Vec3<float> > rotationXYZWithUpDir(PyImath::FixedArray<Imath_2_4::Vec3<float> >,Imath_2_4::Vec3<float>,PyImath::FixedArray<Imath_2_4::Vec3<float> >)
    
    rotationXYZWithUpDir( (V3fArray)fromDir, (V3fArray)toDir, (V3f)upDir) -> V3fArray :
        rotationXYZWithUpDir(fromDir,toDir,upDir) - return the XYZ rotation vector that rotates 'fromDir' to 'toDir'using the up vector 'upDir'
    
        C++ signature :
            PyImath::FixedArray<Imath_2_4::Vec3<float> > rotationXYZWithUpDir(PyImath::FixedArray<Imath_2_4::Vec3<float> >,PyImath::FixedArray<Imath_2_4::Vec3<float> >,Imath_2_4::Vec3<float>)
    
    rotationXYZWithUpDir( (V3fArray)fromDir, (V3fArray)toDir, (V3fArray)upDir) -> V3fArray :
        rotationXYZWithUpDir(fromDir,toDir,upDir) - return the XYZ rotation vector that rotates 'fromDir' to 'toDir'using the up vector 'upDir'
    
        C++ signature :
            PyImath::FixedArray<Imath_2_4::Vec3<float> > rotationXYZWithUpDir(PyImath::FixedArray<Imath_2_4::Vec3<float> >,PyImath::FixedArray<Imath_2_4::Vec3<float> >,PyImath::FixedArray<Imath_2_4::Vec3<float> >)
    """
    pass

def sign(p_int, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    sign( (int)value) -> int :
        sign(value) - return 1 or -1 based on the sign of 'value'
    
        C++ signature :
            int sign(int)
    
    sign( (IntArray)value) -> IntArray :
        sign(value) - return 1 or -1 based on the sign of 'value'
    
        C++ signature :
            PyImath::FixedArray<int> sign(PyImath::FixedArray<int>)
    
    sign( (float)value) -> float :
        sign(value) - return 1 or -1 based on the sign of 'value'
    
        C++ signature :
            float sign(float)
    
    sign( (FloatArray)value) -> FloatArray :
        sign(value) - return 1 or -1 based on the sign of 'value'
    
        C++ signature :
            PyImath::FixedArray<float> sign(PyImath::FixedArray<float>)
    
    sign( (float)value) -> float :
        sign(value) - return 1 or -1 based on the sign of 'value'
    
        C++ signature :
            double sign(double)
    
    sign( (DoubleArray)value) -> DoubleArray :
        sign(value) - return 1 or -1 based on the sign of 'value'
    
        C++ signature :
            PyImath::FixedArray<double> sign(PyImath::FixedArray<double>)
    """
    pass

def solidSphereRand(Rand32, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    solidSphereRand( (Rand32)randObj, (int)num) -> V3fArray :
        solidSphereRand(randObj,num) return XYZ vectors uniformly distributed through the volume of a sphere generated from the given Rand32 object
    
        C++ signature :
            PyImath::FixedArray<Imath_2_4::Vec3<float> > solidSphereRand(Imath_2_4::Rand32 {lvalue},int)
    """
    pass

def trunc(p_float, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    trunc( (float)value) -> int :
        trunc(value) - return the closest integer with magnitude less than or equal to 'value'
    
        C++ signature :
            int trunc(float)
    
    trunc( (FloatArray)value) -> IntArray :
        trunc(value) - return the closest integer with magnitude less than or equal to 'value'
    
        C++ signature :
            PyImath::FixedArray<int> trunc(PyImath::FixedArray<float>)
    
    trunc( (float)value) -> int :
        trunc(value) - return the closest integer with magnitude less than or equal to 'value'
    
        C++ signature :
            int trunc(double)
    
    trunc( (DoubleArray)value) -> IntArray :
        trunc(value) - return the closest integer with magnitude less than or equal to 'value'
    
        C++ signature :
            PyImath::FixedArray<int> trunc(PyImath::FixedArray<double>)
    """
    pass

# classes

from .BoolArray import BoolArray
from .Box2d import Box2d
from .Box2dArray import Box2dArray
from .Box2f import Box2f
from .Box2fArray import Box2fArray
from .Box2i import Box2i
from .Box2iArray import Box2iArray
from .Box2s import Box2s
from .Box2sArray import Box2sArray
from .Box3d import Box3d
from .Box3dArray import Box3dArray
from .Box3f import Box3f
from .Box3fArray import Box3fArray
from .Box3i import Box3i
from .Box3iArray import Box3iArray
from .Box3s import Box3s
from .Box3sArray import Box3sArray
from .C3cArray import C3cArray
from .C3fArray import C3fArray
from .C4cArray import C4cArray
from .C4fArray import C4fArray
from .V3c import V3c
from .Color3c import Color3c
from .V3f import V3f
from .Color3f import Color3f
from .Color4c import Color4c
from .Color4cArray2D import Color4cArray2D
from .Color4f import Color4f
from .Color4fArray2D import Color4fArray2D
from .DoubleArray import DoubleArray
from .DoubleArray2D import DoubleArray2D
from .DoubleMatrix import DoubleMatrix
from .V3d import V3d
from .Eulerd import Eulerd
from .EulerdArray import EulerdArray
from .Eulerf import Eulerf
from .EulerfArray import EulerfArray
from .FloatArray import FloatArray
from .FloatArray2D import FloatArray2D
from .FloatMatrix import FloatMatrix
from .Frustumd import Frustumd
from .Frustumf import Frustumf
from .FrustumTestd import FrustumTestd
from .FrustumTestf import FrustumTestf
from .IntArray import IntArray
from .IntArray2D import IntArray2D
from .IntMatrix import IntMatrix
from .IntVecNormalizeExc import IntVecNormalizeExc
from .Line3d import Line3d
from .Line3f import Line3f
from .M33d import M33d
from .M33dArray import M33dArray
from .M33dRow import M33dRow
from .M33f import M33f
from .M33fArray import M33fArray
from .M33fRow import M33fRow
from .M44d import M44d
from .M44dArray import M44dArray
from .M44dRow import M44dRow
from .M44f import M44f
from .M44fArray import M44fArray
from .M44fRow import M44fRow
from .NullQuatExc import NullQuatExc
from .NullVecExc import NullVecExc
from .Plane3d import Plane3d
from .Plane3f import Plane3f
from .Quatd import Quatd
from .QuatdArray import QuatdArray
from .Quatf import Quatf
from .QuatfArray import QuatfArray
from .Rand32 import Rand32
from .Rand48 import Rand48
from .Shear6d import Shear6d
from .Shear6f import Shear6f
from .ShortArray import ShortArray
from .SignedCharArray import SignedCharArray
from .SingMatrixExc import SingMatrixExc
from .StringArray import StringArray
from .UnsignedCharArray import UnsignedCharArray
from .UnsignedIntArray import UnsignedIntArray
from .UnsignedShortArray import UnsignedShortArray
from .V2d import V2d
from .V2dArray import V2dArray
from .V2f import V2f
from .V2fArray import V2fArray
from .V2i import V2i
from .V2iArray import V2iArray
from .V2s import V2s
from .V2sArray import V2sArray
from .V3dArray import V3dArray
from .V3fArray import V3fArray
from .V3i import V3i
from .V3iArray import V3iArray
from .V3s import V3s
from .V3sArray import V3sArray
from .V4c import V4c
from .V4d import V4d
from .V4dArray import V4dArray
from .V4f import V4f
from .V4fArray import V4fArray
from .V4i import V4i
from .V4iArray import V4iArray
from .V4s import V4s
from .V4sArray import V4sArray
from .VIntArray import VIntArray
from .WstringArray import WstringArray
from .ZeroScaleExc import ZeroScaleExc
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>'

__spec__ = None # (!) real value is "ModuleSpec(name='imath', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>, origin='/usr/lib/python3.8/site-packages/imath.so')"

