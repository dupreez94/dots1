# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class VIntArray(__Boost_Python.instance):
    """ Variable fixed length array of ints """
    def ifelse(self, VIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (VIntArray)arg1, (IntArray)arg2, (VIntArray)arg3) -> VIntArray :
        
            C++ signature :
                PyImath::FixedVArray<int> ifelse(PyImath::FixedVArray<int> {lvalue},PyImath::FixedArray<int>,PyImath::FixedVArray<int>)
        """
        pass

    def __getitem__(self, VIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (VIntArray)arg1, (object)arg2) -> VIntArray :
        
            C++ signature :
                PyImath::FixedVArray<int> __getitem__(PyImath::FixedVArray<int> {lvalue},_object*)
        
        __getitem__( (VIntArray)arg1, (IntArray)arg2) -> VIntArray :
        
            C++ signature :
                PyImath::FixedVArray<int> __getitem__(PyImath::FixedVArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            Construct a variable array of the specified length initialized to the default value for the given type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (VIntArray)arg2) -> None :
            Construct a variable array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedVArray<int>)
        
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            Construct a variable array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,int,unsigned long)
        """
        pass

    def __len__(self, VIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (VIntArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedVArray<int> {lvalue})
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, VIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (VIntArray)arg1, (object)arg2, (VIntArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedVArray<int> {lvalue},_object*,PyImath::FixedVArray<int>)
        
        __setitem__( (VIntArray)arg1, (IntArray)arg2, (VIntArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedVArray<int> {lvalue},PyImath::FixedArray<int>,PyImath::FixedVArray<int>)
        """
        pass

    __instance_size__ = 72


