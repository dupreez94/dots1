# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Box3dArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Box """
    def ifelse(self, Box3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (Box3dArray)arg1, (IntArray)arg2, (Box3d)arg3) -> Box3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > ifelse(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        
        ifelse( (Box3dArray)arg1, (IntArray)arg2, (Box3dArray)arg3) -> Box3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > ifelse(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > >)
        """
        pass

    def __copy__(self, Box3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Box3dArray)arg1) -> Box3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > __copy__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > >)
        """
        pass

    def __deepcopy__(self, Box3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Box3dArray)arg1, (dict)arg2) -> Box3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > __deepcopy__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > >,boost::python::dict {lvalue})
        """
        pass

    def __getitem__(self, Box3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (Box3dArray)arg1, (object)arg2) -> Box3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > __getitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue},_object*)
        
        __getitem__( (Box3dArray)arg1, (IntArray)arg2) -> Box3dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > __getitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (Box3dArray)arg1, (int)arg2) -> Box3d :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec3<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue},long)
        
        __getitem__( (Box3dArray)arg1, (int)arg2) -> Box3d :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (Box3dArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > >)
        
        __init__( (object)arg1, (Box3d)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Box<Imath_2_4::Vec3<double> >,unsigned long)
        """
        pass

    def __len__(self, Box3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (Box3dArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue})
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, Box3dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (Box3dArray)arg1, (object)arg2, (Box3d)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue},_object*,Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        
        __setitem__( (Box3dArray)arg1, (IntArray)arg2, (Box3d)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        
        __setitem__( (Box3dArray)arg1, (object)arg2, (Box3dArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > >)
        
        __setitem__( (Box3dArray)arg1, (IntArray)arg2, (Box3dArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > >)
        
        __setitem__( (Box3dArray)arg1, (int)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Box<Imath_2_4::Vec3<double> > > {lvalue},long,boost::python::tuple)
        """
        pass

    max = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    min = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


