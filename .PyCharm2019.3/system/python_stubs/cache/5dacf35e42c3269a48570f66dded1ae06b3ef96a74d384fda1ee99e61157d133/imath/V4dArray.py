# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V4dArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Vec4 """
    def dot(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V4dArray)arg1, (V4d)x) -> DoubleArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<double> dot(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        dot( (V4dArray)arg1, (V4dArray)x) -> DoubleArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<double> dot(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def ifelse(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (V4dArray)arg1, (IntArray)arg2, (V4d)arg3) -> V4dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > ifelse(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec4<double>)
        
        ifelse( (V4dArray)arg1, (IntArray)arg2, (V4dArray)arg3) -> V4dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > ifelse(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def length(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V4dArray)arg1) -> DoubleArray :
        
            C++ signature :
                PyImath::FixedArray<double> length(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue})
        """
        pass

    def length2(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V4dArray)arg1) -> DoubleArray :
        
            C++ signature :
                PyImath::FixedArray<double> length2(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue})
        """
        pass

    def max(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max( (V4dArray)arg1) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> max(PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def min(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        min( (V4dArray)arg1) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> min(PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def normalize(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V4dArray)arg1) -> V4dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} normalize(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue})
        """
        pass

    def normalized(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V4dArray)arg1) -> V4dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > normalized(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue})
        """
        pass

    def reduce(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (V4dArray)arg1) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> reduce(PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def __add__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __add__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __add__( (V4dArray)arg1, (V4dArray)x) -> V4dArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __add__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def __copy__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V4dArray)arg1) -> V4dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __copy__(PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def __deepcopy__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V4dArray)arg1, (dict)arg2) -> V4dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __deepcopy__(PyImath::FixedArray<Imath_2_4::Vec4<double> >,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __div__( (V4dArray)arg1, (V4dArray)x) -> V4dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        
        __div__( (V4dArray)arg1, (float)x) -> V4dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},double)
        
        __div__( (V4dArray)arg1, (DoubleArray)x) -> V4dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __eq__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V4dArray)arg1, (V4d)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __eq__( (V4dArray)arg1, (V4dArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def __getitem__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V4dArray)arg1, (object)arg2) -> V4dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},_object*)
        
        __getitem__( (V4dArray)arg1, (IntArray)arg2) -> V4dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (V4dArray)arg1, (int)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __getitem__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},long)
        
        __getitem__( (V4dArray)arg1, (int)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},long)
        """
        pass

    def __iadd__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __iadd__( (V4dArray)arg1, (V4dArray)x) -> V4dArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def __idiv__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __idiv__( (V4dArray)arg1, (V4dArray)x) -> V4dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        
        __idiv__( (V4dArray)arg1, (float)x) -> V4dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},double)
        
        __idiv__( (V4dArray)arg1, (DoubleArray)x) -> V4dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __imul__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __imul__( (V4dArray)arg1, (V4dArray)x) -> V4dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        
        __imul__( (V4dArray)arg1, (float)x) -> V4dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},double)
        
        __imul__( (V4dArray)arg1, (DoubleArray)x) -> V4dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (V4dArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        
        __init__( (object)arg1, (V4d)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec4<double>,unsigned long)
        
        __init__( (object)arg1, (V4iArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec4<int> >)
        """
        pass

    def __isub__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __isub__( (V4dArray)arg1, (V4dArray)x) -> V4dArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def __itruediv__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __itruediv__( (V4dArray)arg1, (V4dArray)x) -> V4dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        
        __itruediv__( (V4dArray)arg1, (float)x) -> V4dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},double)
        
        __itruediv__( (V4dArray)arg1, (DoubleArray)x) -> V4dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __len__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V4dArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue})
        """
        pass

    def __mul__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __mul__( (V4dArray)arg1, (V4dArray)x) -> V4dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        
        __mul__( (V4dArray)arg1, (float)x) -> V4dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},double)
        
        __mul__( (V4dArray)arg1, (DoubleArray)x) -> V4dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __neg__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V4dArray)arg1) -> V4dArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __neg__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue})
        """
        pass

    def __ne__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V4dArray)arg1, (V4d)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __ne__( (V4dArray)arg1, (V4dArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def __radd__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __radd__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __rmul__( (V4dArray)arg1, (float)x) -> V4dArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},double)
        
        __rmul__( (V4dArray)arg1, (DoubleArray)x) -> V4dArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __rsub__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __rsub__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        """
        pass

    def __setitem__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V4dArray)arg1, (object)arg2, (V4d)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},_object*,Imath_2_4::Vec4<double>)
        
        __setitem__( (V4dArray)arg1, (IntArray)arg2, (V4d)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec4<double>)
        
        __setitem__( (V4dArray)arg1, (object)arg2, (V4dArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        
        __setitem__( (V4dArray)arg1, (IntArray)arg2, (V4dArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        
        __setitem__( (V4dArray)arg1, (int)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},long,boost::python::tuple)
        """
        pass

    def __sub__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __sub__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __sub__( (V4dArray)arg1, (V4dArray)x) -> V4dArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __sub__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def __truediv__(self, V4dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V4dArray)arg1, (V4d)x) -> V4dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},Imath_2_4::Vec4<double>)
        
        __truediv__( (V4dArray)arg1, (V4dArray)x) -> V4dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        
        __truediv__( (V4dArray)arg1, (float)x) -> V4dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},double)
        
        __truediv__( (V4dArray)arg1, (DoubleArray)x) -> V4dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec4<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    w = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    z = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


