# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V3s(__Boost_Python.instance):
    """ V3s """
    def baseTypeEpsilon(self): # real signature unknown; restored from __doc__
        """
        baseTypeEpsilon() -> int :
            baseTypeEpsilon() epsilon value of the base type of the vector
        
            C++ signature :
                short baseTypeEpsilon()
        """
        return 0

    def baseTypeMax(self): # real signature unknown; restored from __doc__
        """
        baseTypeMax() -> int :
            baseTypeMax() max value of the base type of the vector
        
            C++ signature :
                short baseTypeMax()
        """
        return 0

    def baseTypeMin(self): # real signature unknown; restored from __doc__
        """
        baseTypeMin() -> int :
            baseTypeMin() min value of the base type of the vector
        
            C++ signature :
                short baseTypeMin()
        """
        return 0

    def baseTypeSmallest(self): # real signature unknown; restored from __doc__
        """
        baseTypeSmallest() -> int :
            baseTypeSmallest() smallest value of the base type of the vector
        
            C++ signature :
                short baseTypeSmallest()
        """
        return 0

    def closestVertex(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        closestVertex( (V3s)arg1, (V3s)arg2, (V3s)arg3, (V3s)arg4) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> closestVertex(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        """
        pass

    def cross(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        cross( (V3s)arg1, (V3s)arg2) -> V3s :
            v1.cross(v2) right handed cross product
        
            C++ signature :
                Imath_2_4::Vec3<short> cross(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        
        cross( (V3s)arg1, (V3sArray)arg2) -> V3sArray :
            v1.cross(v2) right handed array cross product
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<short> > cross(Imath_2_4::Vec3<short>,PyImath::FixedArray<Imath_2_4::Vec3<short> >)
        """
        pass

    def dimensions(self): # real signature unknown; restored from __doc__
        """
        dimensions() -> int :
            dimensions() number of dimensions in the vector
        
            C++ signature :
                unsigned int dimensions()
        """
        return 0

    def dot(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V3s)arg1, (V3s)arg2) -> int :
            v1.dot(v2) inner product of the two vectors
        
            C++ signature :
                short dot(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        
        dot( (V3s)arg1, (V3sArray)arg2) -> ShortArray :
            v1.dot(v2) array inner product
        
            C++ signature :
                PyImath::FixedArray<short> dot(Imath_2_4::Vec3<short>,PyImath::FixedArray<Imath_2_4::Vec3<short> >)
        """
        pass

    def equalWithAbsError(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithAbsError( (V3s)arg1, (V3s)arg2, (int)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<short>,short)
        
        equalWithAbsError( (V3s)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec3<short>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def equalWithRelError(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithRelError( (V3s)arg1, (V3s)arg2, (int)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e * abs(v1[i])
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<short>,short)
        
        equalWithRelError( (V3s)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec3<short>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def length(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V3s)arg1) -> int :
            length() magnitude of the vector
        
            C++ signature :
                short length(Imath_2_4::Vec3<short>)
        """
        pass

    def length2(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V3s)arg1) -> int :
            length2() square magnitude of the vector
        
            C++ signature :
                short length2(Imath_2_4::Vec3<short>)
        """
        pass

    def negate(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        negate( (V3s)arg1) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> negate(Imath_2_4::Vec3<short> {lvalue})
        """
        pass

    def normalize(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V3s)arg1) -> V3s :
            v.normalize() destructively normalizes v and returns a reference to it
        
            C++ signature :
                Imath_2_4::Vec3<short> normalize(Imath_2_4::Vec3<short> {lvalue})
        """
        pass

    def normalized(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V3s)arg1) -> V3s :
            v.normalized() returns a normalized copy of v
        
            C++ signature :
                Imath_2_4::Vec3<short> normalized(Imath_2_4::Vec3<short>)
        """
        pass

    def normalizedExc(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedExc( (V3s)arg1) -> V3s :
            v.normalizedExc() returns a normalized copy of v, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec3<short> normalizedExc(Imath_2_4::Vec3<short>)
        """
        pass

    def normalizedNonNull(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedNonNull( (V3s)arg1) -> V3s :
            v.normalizedNonNull() returns a normalized copy of v, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec3<short> normalizedNonNull(Imath_2_4::Vec3<short>)
        """
        pass

    def normalizeExc(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeExc( (V3s)arg1) -> V3s :
            v.normalizeExc() destructively normalizes V and returns a reference to it, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec3<short> normalizeExc(Imath_2_4::Vec3<short> {lvalue})
        """
        pass

    def normalizeNonNull(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeNonNull( (V3s)arg1) -> V3s :
            v.normalizeNonNull() destructively normalizes V and returns a reference to it, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec3<short> normalizeNonNull(Imath_2_4::Vec3<short> {lvalue})
        """
        pass

    def orthogonal(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        orthogonal( (V3s)arg1, (V3s)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> orthogonal(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        """
        pass

    def project(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        project( (V3s)arg1, (V3s)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> project(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        """
        pass

    def reflect(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reflect( (V3s)arg1, (V3s)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> reflect(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        """
        pass

    def setValue(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setValue( (V3s)arg1, (int)arg2, (int)arg3, (int)arg4) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Vec3<short> {lvalue},short,short,short)
        """
        pass

    def __add__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V3s)arg1, (V3s)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __add__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        
        __add__( (V3s)arg1, (V3i)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __add__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<int>)
        
        __add__( (V3s)arg1, (V3f)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __add__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<float>)
        
        __add__( (V3s)arg1, (V3d)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __add__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<double>)
        
        __add__( (V3s)arg1, (int)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __add__(Imath_2_4::Vec3<short>,short)
        
        __add__( (V3s)arg1, (tuple)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __add__(Imath_2_4::Vec3<short>,boost::python::tuple)
        
        __add__( (V3s)arg1, (list)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __add__(Imath_2_4::Vec3<short>,boost::python::list)
        """
        pass

    def __copy__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V3s)arg1) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __copy__(Imath_2_4::Vec3<short>)
        """
        pass

    def __deepcopy__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V3s)arg1, (dict)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __deepcopy__(Imath_2_4::Vec3<short>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V3s)arg1, (V3s)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __div__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        
        __div__( (V3s)arg1, (V3i)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __div__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<int> {lvalue})
        
        __div__( (V3s)arg1, (V3f)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __div__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<float> {lvalue})
        
        __div__( (V3s)arg1, (V3d)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __div__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<double> {lvalue})
        
        __div__( (V3s)arg1, (tuple)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __div__(Imath_2_4::Vec3<short>,boost::python::tuple)
        
        __div__( (V3s)arg1, (list)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __div__(Imath_2_4::Vec3<short>,boost::python::list)
        
        __div__( (V3s)arg1, (int)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __div__(Imath_2_4::Vec3<short>,short)
        """
        pass

    def __eq__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V3s)arg1, (V3s)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<short>)
        
        __eq__( (V3s)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __eq__(Imath_2_4::Vec3<short>,boost::python::tuple)
        """
        pass

    def __getitem__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V3s)arg1, (int)arg2) -> int :
        
            C++ signature :
                short {lvalue} __getitem__(Imath_2_4::Vec3<short> {lvalue},long)
        """
        pass

    def __ge__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (V3s)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __ge__(Imath_2_4::Vec3<short>,boost::python::api::object)
        """
        pass

    def __gt__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (V3s)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __gt__(Imath_2_4::Vec3<short>,boost::python::api::object)
        """
        pass

    def __iadd__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V3s)arg1, (V3i)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __iadd__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<int>)
        
        __iadd__( (V3s)arg1, (V3f)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __iadd__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<float>)
        
        __iadd__( (V3s)arg1, (V3d)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __iadd__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<double>)
        """
        pass

    def __idiv__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V3s)arg1, (object)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __idiv__(Imath_2_4::Vec3<short> {lvalue},boost::python::api::object)
        """
        pass

    def __imul__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V3s)arg1, (V3i)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __imul__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<int>)
        
        __imul__( (V3s)arg1, (V3f)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __imul__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<float>)
        
        __imul__( (V3s)arg1, (V3d)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __imul__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<double>)
        
        __imul__( (V3s)arg1, (int)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __imul__(Imath_2_4::Vec3<short> {lvalue},short)
        
        __imul__( (V3s)arg1, (M44f)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __imul__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Matrix44<float>)
        
        __imul__( (V3s)arg1, (M44d)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __imul__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (V3s)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<short>)
        
        __init__( (object)arg1) -> object :
            initialize to (0,0,0)
        
            C++ signature :
                void* __init__(boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2, (object)arg3, (object)arg4) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object,boost::python::api::object,boost::python::api::object)
        """
        pass

    def __isub__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V3s)arg1, (V3i)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __isub__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<int>)
        
        __isub__( (V3s)arg1, (V3f)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __isub__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<float>)
        
        __isub__( (V3s)arg1, (V3d)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __isub__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<double>)
        """
        pass

    def __itruediv__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V3s)arg1, (object)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __itruediv__(Imath_2_4::Vec3<short> {lvalue},boost::python::api::object)
        """
        pass

    def __len__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V3s)arg1) -> int :
        
            C++ signature :
                long __len__(Imath_2_4::Vec3<short>)
        """
        pass

    def __le__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (V3s)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __le__(Imath_2_4::Vec3<short>,boost::python::api::object)
        """
        pass

    def __lt__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (V3s)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __lt__(Imath_2_4::Vec3<short>,boost::python::api::object)
        """
        pass

    def __mod__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mod__( (V3s)arg1, (V3s)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __mod__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        """
        pass

    def __mul__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V3s)arg1, (V3i)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __mul__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<int> {lvalue})
        
        __mul__( (V3s)arg1, (V3f)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __mul__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<float> {lvalue})
        
        __mul__( (V3s)arg1, (V3d)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __mul__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<double> {lvalue})
        
        __mul__( (V3s)arg1, (int)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __mul__(Imath_2_4::Vec3<short>,short)
        
        __mul__( (V3s)arg1, (ShortArray)arg2) -> V3sArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<short> > __mul__(Imath_2_4::Vec3<short>,PyImath::FixedArray<short>)
        
        __mul__( (V3s)arg1, (M33f)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __mul__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Matrix33<float>)
        
        __mul__( (V3s)arg1, (M33d)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __mul__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Matrix33<double>)
        
        __mul__( (V3s)arg1, (M44f)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __mul__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Matrix44<float>)
        
        __mul__( (V3s)arg1, (M44d)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __mul__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Matrix44<double>)
        
        __mul__( (V3s)arg1, (V3s)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __mul__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        
        __mul__( (V3s)arg1, (tuple)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __mul__(Imath_2_4::Vec3<short>,boost::python::tuple)
        """
        pass

    def __neg__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V3s)arg1) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __neg__(Imath_2_4::Vec3<short>)
        """
        pass

    def __ne__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V3s)arg1, (V3s)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<short>)
        
        __ne__( (V3s)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __ne__(Imath_2_4::Vec3<short>,boost::python::tuple)
        """
        pass

    def __radd__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V3s)arg1, (int)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __radd__(Imath_2_4::Vec3<short>,short)
        
        __radd__( (V3s)arg1, (tuple)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __radd__(Imath_2_4::Vec3<short>,boost::python::tuple)
        
        __radd__( (V3s)arg1, (list)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __radd__(Imath_2_4::Vec3<short>,boost::python::list)
        
        __radd__( (V3s)arg1, (V3s)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __radd__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        """
        pass

    def __rdiv__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rdiv__( (V3s)arg1, (tuple)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __rdiv__(Imath_2_4::Vec3<short>,boost::python::tuple)
        
        __rdiv__( (V3s)arg1, (list)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __rdiv__(Imath_2_4::Vec3<short>,boost::python::list)
        
        __rdiv__( (V3s)arg1, (int)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __rdiv__(Imath_2_4::Vec3<short>,short)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (V3s)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Vec3<short>)
        """
        pass

    def __rmul__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V3s)arg1, (int)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __rmul__(Imath_2_4::Vec3<short> {lvalue},short)
        
        __rmul__( (V3s)arg1, (ShortArray)arg2) -> V3sArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<short> > __rmul__(Imath_2_4::Vec3<short>,PyImath::FixedArray<short>)
        
        __rmul__( (V3s)arg1, (tuple)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __rmul__(Imath_2_4::Vec3<short>,boost::python::tuple)
        """
        pass

    def __rsub__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V3s)arg1, (int)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __rsub__(Imath_2_4::Vec3<short>,short)
        
        __rsub__( (V3s)arg1, (tuple)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __rsub__(Imath_2_4::Vec3<short>,boost::python::tuple)
        
        __rsub__( (V3s)arg1, (list)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __rsub__(Imath_2_4::Vec3<short>,boost::python::list)
        """
        pass

    def __setitem__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V3s)arg1, (int)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(Imath_2_4::Vec3<short> {lvalue},long,short)
        """
        pass

    def __str__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (V3s)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Vec3<short>)
        """
        pass

    def __sub__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V3s)arg1, (V3s)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __sub__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        
        __sub__( (V3s)arg1, (V3i)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __sub__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<int>)
        
        __sub__( (V3s)arg1, (V3f)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __sub__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<float>)
        
        __sub__( (V3s)arg1, (V3d)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __sub__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<double>)
        
        __sub__( (V3s)arg1, (int)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __sub__(Imath_2_4::Vec3<short>,short)
        
        __sub__( (V3s)arg1, (tuple)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __sub__(Imath_2_4::Vec3<short>,boost::python::tuple)
        
        __sub__( (V3s)arg1, (list)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __sub__(Imath_2_4::Vec3<short>,boost::python::list)
        """
        pass

    def __truediv__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V3s)arg1, (V3s)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __truediv__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        
        __truediv__( (V3s)arg1, (V3i)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __truediv__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<int> {lvalue})
        
        __truediv__( (V3s)arg1, (V3f)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __truediv__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<float> {lvalue})
        
        __truediv__( (V3s)arg1, (V3d)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __truediv__(Imath_2_4::Vec3<short> {lvalue},Imath_2_4::Vec3<double> {lvalue})
        
        __truediv__( (V3s)arg1, (tuple)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __truediv__(Imath_2_4::Vec3<short>,boost::python::tuple)
        
        __truediv__( (V3s)arg1, (list)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __truediv__(Imath_2_4::Vec3<short>,boost::python::list)
        
        __truediv__( (V3s)arg1, (int)arg2) -> V3s :
        
            C++ signature :
                Imath_2_4::Vec3<short> __truediv__(Imath_2_4::Vec3<short>,short)
        """
        pass

    def __xor__(self, V3s, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __xor__( (V3s)arg1, (V3s)arg2) -> int :
        
            C++ signature :
                short __xor__(Imath_2_4::Vec3<short>,Imath_2_4::Vec3<short>)
        """
        pass

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    z = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 24


