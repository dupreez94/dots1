# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class FloatArray(__Boost_Python.instance):
    """ Fixed length array of floats """
    def ifelse(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (FloatArray)arg1, (IntArray)arg2, (float)arg3) -> FloatArray :
        
            C++ signature :
                PyImath::FixedArray<float> ifelse(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<int>,float)
        
        ifelse( (FloatArray)arg1, (IntArray)arg2, (FloatArray)arg3) -> FloatArray :
        
            C++ signature :
                PyImath::FixedArray<float> ifelse(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<float>)
        """
        pass

    def reduce(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (FloatArray)arg1) -> float :
        
            C++ signature :
                float reduce(PyImath::FixedArray<float>)
        """
        pass

    def __add__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (FloatArray)arg1, (float)x) -> FloatArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<float> __add__(PyImath::FixedArray<float> {lvalue},float)
        
        __add__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<float> __add__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __div__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (FloatArray)arg1, (float)x) -> FloatArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<float> __div__(PyImath::FixedArray<float> {lvalue},float)
        
        __div__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<float> __div__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __eq__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (FloatArray)arg1, (float)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<float> {lvalue},float)
        
        __eq__( (FloatArray)arg1, (FloatArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __getitem__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (FloatArray)arg1, (object)arg2) -> FloatArray :
        
            C++ signature :
                PyImath::FixedArray<float> __getitem__(PyImath::FixedArray<float> {lvalue},_object*)
        
        __getitem__( (FloatArray)arg1, (IntArray)arg2) -> FloatArray :
        
            C++ signature :
                PyImath::FixedArray<float> __getitem__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (FloatArray)arg1, (int)arg2) -> float :
        
            C++ signature :
                float __getitem__(PyImath::FixedArray<float> {lvalue},long)
        
        __getitem__( (FloatArray)arg1, (int)arg2) -> float :
        
            C++ signature :
                float __getitem__(PyImath::FixedArray<float> {lvalue},long)
        """
        pass

    def __ge__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (FloatArray)arg1, (float)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<float> {lvalue},float)
        
        __ge__( (FloatArray)arg1, (FloatArray)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __gt__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (FloatArray)arg1, (float)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<float> {lvalue},float)
        
        __gt__( (FloatArray)arg1, (FloatArray)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __iadd__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (FloatArray)arg1, (float)x) -> FloatArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __iadd__(PyImath::FixedArray<float> {lvalue},float)
        
        __iadd__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __iadd__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __idiv__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (FloatArray)arg1, (float)x) -> FloatArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __idiv__(PyImath::FixedArray<float> {lvalue},float)
        
        __idiv__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __idiv__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __imul__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (FloatArray)arg1, (float)x) -> FloatArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __imul__(PyImath::FixedArray<float> {lvalue},float)
        
        __imul__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __imul__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (FloatArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<float>)
        
        __init__( (object)arg1, (float)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,float,unsigned long)
        
        __init__( (object)arg1, (IntArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<int>)
        
        __init__( (object)arg1, (DoubleArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<double>)
        """
        pass

    def __ipow__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ipow__( (FloatArray)arg1, (float)x) -> FloatArray :
            __ipow__(x) - x**=self
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __ipow__(PyImath::FixedArray<float> {lvalue},float)
        
        __ipow__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __ipow__(x) - x**=self
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __ipow__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __isub__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (FloatArray)arg1, (float)x) -> FloatArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __isub__(PyImath::FixedArray<float> {lvalue},float)
        
        __isub__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __isub__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __itruediv__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (FloatArray)arg1, (float)x) -> FloatArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __itruediv__(PyImath::FixedArray<float> {lvalue},float)
        
        __itruediv__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<float> {lvalue} __itruediv__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __len__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (FloatArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<float> {lvalue})
        """
        pass

    def __le__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (FloatArray)arg1, (float)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<float> {lvalue},float)
        
        __le__( (FloatArray)arg1, (FloatArray)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __lt__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (FloatArray)arg1, (float)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<float> {lvalue},float)
        
        __lt__( (FloatArray)arg1, (FloatArray)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __mul__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (FloatArray)arg1, (float)x) -> FloatArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<float> __mul__(PyImath::FixedArray<float> {lvalue},float)
        
        __mul__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<float> __mul__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __neg__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (FloatArray)arg1) -> FloatArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<float> __neg__(PyImath::FixedArray<float> {lvalue})
        """
        pass

    def __ne__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (FloatArray)arg1, (float)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<float> {lvalue},float)
        
        __ne__( (FloatArray)arg1, (FloatArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __pow__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __pow__( (FloatArray)arg1, (float)x) -> FloatArray :
            __pow__(x) - self**x
        
            C++ signature :
                PyImath::FixedArray<float> __pow__(PyImath::FixedArray<float> {lvalue},float)
        
        __pow__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __pow__(x) - self**x
        
            C++ signature :
                PyImath::FixedArray<float> __pow__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __radd__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (FloatArray)arg1, (float)x) -> FloatArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<float> __radd__(PyImath::FixedArray<float> {lvalue},float)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (FloatArray)arg1, (float)x) -> FloatArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<float> __rmul__(PyImath::FixedArray<float> {lvalue},float)
        """
        pass

    def __rpow__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rpow__( (FloatArray)arg1, (float)x) -> FloatArray :
            __rpow__(x) - x**self
        
            C++ signature :
                PyImath::FixedArray<float> __rpow__(PyImath::FixedArray<float> {lvalue},float)
        """
        pass

    def __rsub__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (FloatArray)arg1, (float)x) -> FloatArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<float> __rsub__(PyImath::FixedArray<float> {lvalue},float)
        """
        pass

    def __setitem__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (FloatArray)arg1, (object)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<float> {lvalue},_object*,float)
        
        __setitem__( (FloatArray)arg1, (IntArray)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<int>,float)
        
        __setitem__( (FloatArray)arg1, (object)arg2, (FloatArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<float> {lvalue},_object*,PyImath::FixedArray<float>)
        
        __setitem__( (FloatArray)arg1, (IntArray)arg2, (FloatArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<float>)
        """
        pass

    def __sub__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (FloatArray)arg1, (float)x) -> FloatArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<float> __sub__(PyImath::FixedArray<float> {lvalue},float)
        
        __sub__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<float> __sub__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    def __truediv__(self, FloatArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (FloatArray)arg1, (float)x) -> FloatArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<float> __truediv__(PyImath::FixedArray<float> {lvalue},float)
        
        __truediv__( (FloatArray)arg1, (FloatArray)x) -> FloatArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<float> __truediv__(PyImath::FixedArray<float> {lvalue},PyImath::FixedArray<float>)
        """
        pass

    __instance_size__ = 72


