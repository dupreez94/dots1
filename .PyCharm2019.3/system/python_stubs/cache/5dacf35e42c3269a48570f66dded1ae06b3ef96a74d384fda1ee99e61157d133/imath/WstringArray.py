# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class WstringArray(__Boost_Python.instance):
    # no doc
    def __eq__(self, WstringArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (WstringArray)arg1, (WstringArray)arg2) -> object :
        
            C++ signature :
                _object* __eq__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > >)
        
        __eq__( (WstringArray)arg1, (str)arg2) -> object :
        
            C++ signature :
                _object* __eq__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >)
        
        __eq__( (WstringArray)arg1, (str)arg2) -> object :
        
            C++ signature :
                _object* __eq__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >)
        """
        pass

    def __getitem__(self, WstringArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (WstringArray)arg1, (object)arg2) -> WstringArray :
        
            C++ signature :
                PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > >* __getitem__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},_object*)
        
        __getitem__( (WstringArray)arg1, (int)arg2) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > __getitem__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,unsigned long)
        
        __init__( (object)arg1, (str)arg2, (int)arg3) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >,unsigned long)
        """
        pass

    def __len__(self, WstringArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (WstringArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue})
        """
        pass

    def __ne__(self, WstringArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (WstringArray)arg1, (WstringArray)arg2) -> object :
        
            C++ signature :
                _object* __ne__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > >)
        
        __ne__( (WstringArray)arg1, (str)arg2) -> object :
        
            C++ signature :
                _object* __ne__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >)
        
        __ne__( (WstringArray)arg1, (str)arg2) -> object :
        
            C++ signature :
                _object* __ne__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, WstringArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (WstringArray)arg1, (object)arg2, (str)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},_object*,std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >)
        
        __setitem__( (WstringArray)arg1, (IntArray)arg2, (str)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},PyImath::FixedArray<int>,std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >)
        
        __setitem__( (WstringArray)arg1, (object)arg2, (WstringArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},_object*,PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > >)
        
        __setitem__( (WstringArray)arg1, (IntArray)arg2, (WstringArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > > {lvalue},PyImath::FixedArray<int>,PyImath::StringArrayT<std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > >)
        """
        pass


