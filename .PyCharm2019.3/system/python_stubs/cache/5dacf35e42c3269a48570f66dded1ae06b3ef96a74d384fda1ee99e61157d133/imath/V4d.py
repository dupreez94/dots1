# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V4d(__Boost_Python.instance):
    """ V4d """
    def baseTypeEpsilon(self): # real signature unknown; restored from __doc__
        """
        baseTypeEpsilon() -> float :
            baseTypeEpsilon() epsilon value of the base type of the vector
        
            C++ signature :
                double baseTypeEpsilon()
        """
        return 0.0

    def baseTypeMax(self): # real signature unknown; restored from __doc__
        """
        baseTypeMax() -> float :
            baseTypeMax() max value of the base type of the vector
        
            C++ signature :
                double baseTypeMax()
        """
        return 0.0

    def baseTypeMin(self): # real signature unknown; restored from __doc__
        """
        baseTypeMin() -> float :
            baseTypeMin() min value of the base type of the vector
        
            C++ signature :
                double baseTypeMin()
        """
        return 0.0

    def baseTypeSmallest(self): # real signature unknown; restored from __doc__
        """
        baseTypeSmallest() -> float :
            baseTypeSmallest() smallest value of the base type of the vector
        
            C++ signature :
                double baseTypeSmallest()
        """
        return 0.0

    def dimensions(self): # real signature unknown; restored from __doc__
        """
        dimensions() -> int :
            dimensions() number of dimensions in the vector
        
            C++ signature :
                unsigned int dimensions()
        """
        return 0

    def dot(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V4d)arg1, (V4d)arg2) -> float :
            v1.dot(v2) inner product of the two vectors
        
            C++ signature :
                double dot(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        
        dot( (V4d)arg1, (V4dArray)arg2) -> DoubleArray :
            v1.dot(v2) array inner product
        
            C++ signature :
                PyImath::FixedArray<double> dot(Imath_2_4::Vec4<double>,PyImath::FixedArray<Imath_2_4::Vec4<double> >)
        """
        pass

    def equalWithAbsError(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithAbsError( (V4d)arg1, (V4d)arg2, (float)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<double>,double)
        
        equalWithAbsError( (V4d)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec4<double>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def equalWithRelError(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithRelError( (V4d)arg1, (V4d)arg2, (float)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e * abs(v1[i])
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<double>,double)
        
        equalWithRelError( (V4d)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec4<double>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def length(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V4d)arg1) -> float :
            length() magnitude of the vector
        
            C++ signature :
                double length(Imath_2_4::Vec4<double>)
        """
        pass

    def length2(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V4d)arg1) -> float :
            length2() square magnitude of the vector
        
            C++ signature :
                double length2(Imath_2_4::Vec4<double>)
        """
        pass

    def negate(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        negate( (V4d)arg1) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> negate(Imath_2_4::Vec4<double> {lvalue})
        """
        pass

    def normalize(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V4d)arg1) -> V4d :
            v.normalize() destructively normalizes v and returns a reference to it
        
            C++ signature :
                Imath_2_4::Vec4<double> normalize(Imath_2_4::Vec4<double> {lvalue})
        """
        pass

    def normalized(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V4d)arg1) -> V4d :
            v.normalized() returns a normalized copy of v
        
            C++ signature :
                Imath_2_4::Vec4<double> normalized(Imath_2_4::Vec4<double>)
        """
        pass

    def normalizedExc(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedExc( (V4d)arg1) -> V4d :
            v.normalizedExc() returns a normalized copy of v, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec4<double> normalizedExc(Imath_2_4::Vec4<double>)
        """
        pass

    def normalizedNonNull(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedNonNull( (V4d)arg1) -> V4d :
            v.normalizedNonNull() returns a normalized copy of v, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec4<double> normalizedNonNull(Imath_2_4::Vec4<double>)
        """
        pass

    def normalizeExc(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeExc( (V4d)arg1) -> V4d :
            v.normalizeExc() destructively normalizes V and returns a reference to it, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec4<double> normalizeExc(Imath_2_4::Vec4<double> {lvalue})
        """
        pass

    def normalizeNonNull(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeNonNull( (V4d)arg1) -> V4d :
            v.normalizeNonNull() destructively normalizes V and returns a reference to it, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec4<double> normalizeNonNull(Imath_2_4::Vec4<double> {lvalue})
        """
        pass

    def orthogonal(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        orthogonal( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> orthogonal(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        """
        pass

    def project(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        project( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> project(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        """
        pass

    def reflect(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reflect( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> reflect(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        """
        pass

    def setValue(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setValue( (V4d)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Vec4<double> {lvalue},double,double,double,double)
        """
        pass

    def __add__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __add__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        
        __add__( (V4d)arg1, (V4i)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __add__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<int>)
        
        __add__( (V4d)arg1, (V4f)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __add__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<float>)
        
        __add__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __add__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        
        __add__( (V4d)arg1, (float)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __add__(Imath_2_4::Vec4<double>,double)
        
        __add__( (V4d)arg1, (tuple)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __add__(Imath_2_4::Vec4<double>,boost::python::tuple)
        
        __add__( (V4d)arg1, (list)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __add__(Imath_2_4::Vec4<double>,boost::python::list)
        """
        pass

    def __copy__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V4d)arg1) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __copy__(Imath_2_4::Vec4<double>)
        """
        pass

    def __deepcopy__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V4d)arg1, (dict)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __deepcopy__(Imath_2_4::Vec4<double>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __div__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        
        __div__( (V4d)arg1, (V4i)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __div__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<int> {lvalue})
        
        __div__( (V4d)arg1, (V4f)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __div__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<float> {lvalue})
        
        __div__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __div__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<double> {lvalue})
        
        __div__( (V4d)arg1, (tuple)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __div__(Imath_2_4::Vec4<double>,boost::python::tuple)
        
        __div__( (V4d)arg1, (list)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __div__(Imath_2_4::Vec4<double>,boost::python::list)
        
        __div__( (V4d)arg1, (float)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __div__(Imath_2_4::Vec4<double>,double)
        """
        pass

    def __eq__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V4d)arg1, (V4d)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<double>)
        
        __eq__( (V4d)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __eq__(Imath_2_4::Vec4<double>,boost::python::tuple)
        """
        pass

    def __getitem__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V4d)arg1, (int)arg2) -> float :
        
            C++ signature :
                double {lvalue} __getitem__(Imath_2_4::Vec4<double> {lvalue},long)
        """
        pass

    def __ge__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (V4d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __ge__(Imath_2_4::Vec4<double>,boost::python::api::object)
        """
        pass

    def __gt__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (V4d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __gt__(Imath_2_4::Vec4<double>,boost::python::api::object)
        """
        pass

    def __iadd__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V4d)arg1, (V4i)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __iadd__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<int>)
        
        __iadd__( (V4d)arg1, (V4f)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __iadd__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<float>)
        
        __iadd__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __iadd__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<double>)
        """
        pass

    def __idiv__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V4d)arg1, (object)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __idiv__(Imath_2_4::Vec4<double> {lvalue},boost::python::api::object)
        """
        pass

    def __imul__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V4d)arg1, (V4i)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __imul__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<int>)
        
        __imul__( (V4d)arg1, (V4f)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __imul__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<float>)
        
        __imul__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __imul__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<double>)
        
        __imul__( (V4d)arg1, (float)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __imul__(Imath_2_4::Vec4<double> {lvalue},double)
        
        __imul__( (V4d)arg1, (M44f)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __imul__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Matrix44<float>)
        
        __imul__( (V4d)arg1, (M44d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __imul__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (V4d)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec4<double>)
        
        __init__( (object)arg1) -> object :
            initialize to (0,0,0,0)
        
            C++ signature :
                void* __init__(boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2, (object)arg3, (object)arg4, (object)arg5) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object,boost::python::api::object,boost::python::api::object,boost::python::api::object)
        """
        pass

    def __isub__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V4d)arg1, (V4i)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __isub__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<int>)
        
        __isub__( (V4d)arg1, (V4f)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __isub__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<float>)
        
        __isub__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __isub__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<double>)
        """
        pass

    def __itruediv__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V4d)arg1, (object)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __itruediv__(Imath_2_4::Vec4<double> {lvalue},boost::python::api::object)
        """
        pass

    def __len__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V4d)arg1) -> int :
        
            C++ signature :
                long __len__(Imath_2_4::Vec4<double>)
        """
        pass

    def __le__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (V4d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __le__(Imath_2_4::Vec4<double>,boost::python::api::object)
        """
        pass

    def __lt__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (V4d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __lt__(Imath_2_4::Vec4<double>,boost::python::api::object)
        """
        pass

    def __mul__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V4d)arg1, (V4i)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __mul__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<int> {lvalue})
        
        __mul__( (V4d)arg1, (V4f)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __mul__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<float> {lvalue})
        
        __mul__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __mul__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double> {lvalue})
        
        __mul__( (V4d)arg1, (float)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __mul__(Imath_2_4::Vec4<double>,double)
        
        __mul__( (V4d)arg1, (DoubleArray)arg2) -> V4dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __mul__(Imath_2_4::Vec4<double>,PyImath::FixedArray<double>)
        
        __mul__( (V4d)arg1, (M44f)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __mul__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Matrix44<float>)
        
        __mul__( (V4d)arg1, (M44d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __mul__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Matrix44<double>)
        
        __mul__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __mul__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        
        __mul__( (V4d)arg1, (tuple)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __mul__(Imath_2_4::Vec4<double>,boost::python::tuple)
        """
        pass

    def __neg__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V4d)arg1) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __neg__(Imath_2_4::Vec4<double>)
        """
        pass

    def __ne__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V4d)arg1, (V4d)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<double>)
        
        __ne__( (V4d)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __ne__(Imath_2_4::Vec4<double>,boost::python::tuple)
        """
        pass

    def __radd__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V4d)arg1, (float)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __radd__(Imath_2_4::Vec4<double>,double)
        
        __radd__( (V4d)arg1, (tuple)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __radd__(Imath_2_4::Vec4<double>,boost::python::tuple)
        
        __radd__( (V4d)arg1, (list)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __radd__(Imath_2_4::Vec4<double>,boost::python::list)
        
        __radd__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __radd__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        """
        pass

    def __rdiv__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rdiv__( (V4d)arg1, (tuple)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __rdiv__(Imath_2_4::Vec4<double>,boost::python::tuple)
        
        __rdiv__( (V4d)arg1, (list)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __rdiv__(Imath_2_4::Vec4<double>,boost::python::list)
        
        __rdiv__( (V4d)arg1, (float)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __rdiv__(Imath_2_4::Vec4<double>,double)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (V4d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Vec4<double>)
        """
        pass

    def __rmul__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V4d)arg1, (float)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __rmul__(Imath_2_4::Vec4<double> {lvalue},double)
        
        __rmul__( (V4d)arg1, (DoubleArray)arg2) -> V4dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<double> > __rmul__(Imath_2_4::Vec4<double>,PyImath::FixedArray<double>)
        
        __rmul__( (V4d)arg1, (tuple)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __rmul__(Imath_2_4::Vec4<double>,boost::python::tuple)
        """
        pass

    def __rsub__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V4d)arg1, (float)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __rsub__(Imath_2_4::Vec4<double>,double)
        
        __rsub__( (V4d)arg1, (tuple)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __rsub__(Imath_2_4::Vec4<double>,boost::python::tuple)
        
        __rsub__( (V4d)arg1, (list)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __rsub__(Imath_2_4::Vec4<double>,boost::python::list)
        """
        pass

    def __setitem__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V4d)arg1, (int)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(Imath_2_4::Vec4<double> {lvalue},long,double)
        """
        pass

    def __str__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (V4d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Vec4<double>)
        """
        pass

    def __sub__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __sub__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        
        __sub__( (V4d)arg1, (V4i)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __sub__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<int>)
        
        __sub__( (V4d)arg1, (V4f)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __sub__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<float>)
        
        __sub__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __sub__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        
        __sub__( (V4d)arg1, (float)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __sub__(Imath_2_4::Vec4<double>,double)
        
        __sub__( (V4d)arg1, (tuple)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __sub__(Imath_2_4::Vec4<double>,boost::python::tuple)
        
        __sub__( (V4d)arg1, (list)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __sub__(Imath_2_4::Vec4<double>,boost::python::list)
        """
        pass

    def __truediv__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __truediv__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        
        __truediv__( (V4d)arg1, (V4i)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __truediv__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<int> {lvalue})
        
        __truediv__( (V4d)arg1, (V4f)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __truediv__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<float> {lvalue})
        
        __truediv__( (V4d)arg1, (V4d)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __truediv__(Imath_2_4::Vec4<double> {lvalue},Imath_2_4::Vec4<double> {lvalue})
        
        __truediv__( (V4d)arg1, (tuple)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __truediv__(Imath_2_4::Vec4<double>,boost::python::tuple)
        
        __truediv__( (V4d)arg1, (list)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __truediv__(Imath_2_4::Vec4<double>,boost::python::list)
        
        __truediv__( (V4d)arg1, (float)arg2) -> V4d :
        
            C++ signature :
                Imath_2_4::Vec4<double> __truediv__(Imath_2_4::Vec4<double>,double)
        """
        pass

    def __xor__(self, V4d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __xor__( (V4d)arg1, (V4d)arg2) -> float :
        
            C++ signature :
                double __xor__(Imath_2_4::Vec4<double>,Imath_2_4::Vec4<double>)
        """
        pass

    w = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    z = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 48


