# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class EulerdArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Euler """
    def ifelse(self, EulerdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (EulerdArray)arg1, (IntArray)arg2, (Eulerd)arg3) -> EulerdArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Euler<double> > ifelse(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Euler<double>)
        
        ifelse( (EulerdArray)arg1, (IntArray)arg2, (EulerdArray)arg3) -> EulerdArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Euler<double> > ifelse(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Euler<double> >)
        """
        pass

    def __eq__(self, EulerdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (EulerdArray)arg1, (Eulerd)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},Imath_2_4::Euler<double>)
        
        __eq__( (EulerdArray)arg1, (EulerdArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Euler<double> >)
        """
        pass

    def __getitem__(self, EulerdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (EulerdArray)arg1, (object)arg2) -> EulerdArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Euler<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},_object*)
        
        __getitem__( (EulerdArray)arg1, (IntArray)arg2) -> EulerdArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Euler<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (EulerdArray)arg1, (int)arg2) -> Eulerd :
        
            C++ signature :
                Imath_2_4::Euler<double> __getitem__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},long)
        
        __getitem__( (EulerdArray)arg1, (int)arg2) -> Eulerd :
        
            C++ signature :
                Imath_2_4::Euler<double> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (EulerdArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Euler<double> >)
        
        __init__( (object)arg1, (Eulerd)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Euler<double>,unsigned long)
        
        __init__( (object)arg1, (QuatdArray)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,PyImath::FixedArray<Imath_2_4::Quat<double> >)
        
        __init__( (object)arg1, (M33dArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Matrix33<double> >)
        
        __init__( (object)arg1, (M44dArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Matrix44<double> >)
        
        __init__( (object)arg1, (EulerfArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Euler<float> >)
        """
        pass

    def __len__(self, EulerdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (EulerdArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue})
        """
        pass

    def __ne__(self, EulerdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (EulerdArray)arg1, (Eulerd)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},Imath_2_4::Euler<double>)
        
        __ne__( (EulerdArray)arg1, (EulerdArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Euler<double> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, EulerdArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (EulerdArray)arg1, (object)arg2, (Eulerd)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},_object*,Imath_2_4::Euler<double>)
        
        __setitem__( (EulerdArray)arg1, (IntArray)arg2, (Eulerd)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Euler<double>)
        
        __setitem__( (EulerdArray)arg1, (object)arg2, (EulerdArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Euler<double> >)
        
        __setitem__( (EulerdArray)arg1, (IntArray)arg2, (EulerdArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Euler<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Euler<double> >)
        """
        pass

    __instance_size__ = 72


