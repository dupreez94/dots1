# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class DoubleArray2D(__Boost_Python.instance):
    """ Fixed length array of doubles """
    def ifelse(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (DoubleArray2D)arg1, (IntArray2D)arg2, (float)arg3) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> ifelse(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<int>,double)
        
        ifelse( (DoubleArray2D)arg1, (IntArray2D)arg2, (DoubleArray2D)arg3) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> ifelse(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray2D<double>)
        """
        pass

    def item(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        item( (DoubleArray2D)arg1, (int)arg2, (int)arg3) -> float :
        
            C++ signature :
                double item(PyImath::FixedArray2D<double> {lvalue},long,long)
        """
        pass

    def size(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        size( (DoubleArray2D)arg1) -> tuple :
        
            C++ signature :
                boost::python::tuple size(PyImath::FixedArray2D<double> {lvalue})
        """
        pass

    def __add__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __add__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __add__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __add__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __div__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __div__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __div__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __div__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __eq__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __eq__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __eq__( (DoubleArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __eq__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __getitem__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (DoubleArray2D)arg1, (object)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __getitem__(PyImath::FixedArray2D<double> {lvalue},_object*)
        
        __getitem__( (DoubleArray2D)arg1, (IntArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __getitem__(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<int>)
        """
        pass

    def __ge__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ge__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __ge__( (DoubleArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ge__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __gt__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __gt__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __gt__( (DoubleArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __gt__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __iadd__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __iadd__(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<double>)
        
        __iadd__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __iadd__(PyImath::FixedArray2D<double> {lvalue},double)
        """
        pass

    def __idiv__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __idiv__(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<double>)
        
        __idiv__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __idiv__(PyImath::FixedArray2D<double> {lvalue},double)
        """
        pass

    def __imul__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __imul__(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<double>)
        
        __imul__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __imul__(PyImath::FixedArray2D<double> {lvalue},double)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long,unsigned long)
        
        __init__( (object)arg1, (DoubleArray2D)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray2D<double>)
        
        __init__( (object)arg1, (float)arg2, (int)arg3, (int)arg4) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,double,unsigned long,unsigned long)
        
        __init__( (object)arg1, (IntArray2D)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray2D<int>)
        
        __init__( (object)arg1, (FloatArray2D)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray2D<float>)
        """
        pass

    def __ipow__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ipow__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __ipow__(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<double>)
        
        __ipow__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __ipow__(PyImath::FixedArray2D<double> {lvalue},double)
        """
        pass

    def __isub__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __isub__(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<double>)
        
        __isub__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __isub__(PyImath::FixedArray2D<double> {lvalue},double)
        """
        pass

    def __itruediv__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __itruediv__(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<double>)
        
        __itruediv__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> {lvalue} __itruediv__(PyImath::FixedArray2D<double> {lvalue},double)
        """
        pass

    def __len__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (DoubleArray2D)arg1) -> int :
        
            C++ signature :
                unsigned long __len__(PyImath::FixedArray2D<double> {lvalue})
        """
        pass

    def __le__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __le__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __le__( (DoubleArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __le__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __lt__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __lt__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __lt__( (DoubleArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __lt__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __mul__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __mul__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __mul__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __mul__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __neg__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (DoubleArray2D)arg1) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __neg__(PyImath::FixedArray2D<double>)
        """
        pass

    def __ne__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ne__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __ne__( (DoubleArray2D)arg1, (float)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ne__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __pow__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __pow__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __pow__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __pow__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __pow__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __radd__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __radd__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __rmul__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __rpow__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rpow__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __rpow__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __rsub__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __rsub__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __setitem__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (DoubleArray2D)arg1, (object)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<double> {lvalue},_object*,double)
        
        __setitem__( (DoubleArray2D)arg1, (IntArray2D)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<int>,double)
        
        __setitem__( (DoubleArray2D)arg1, (object)arg2, (DoubleArray2D)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<double> {lvalue},_object*,PyImath::FixedArray2D<double>)
        
        __setitem__( (DoubleArray2D)arg1, (IntArray2D)arg2, (DoubleArray2D)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray2D<double>)
        
        __setitem__( (DoubleArray2D)arg1, (object)arg2, (DoubleArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<double> {lvalue},_object*,PyImath::FixedArray<double>)
        
        __setitem__( (DoubleArray2D)arg1, (IntArray2D)arg2, (DoubleArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<double> {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray<double>)
        """
        pass

    def __sub__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __sub__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __sub__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __sub__(PyImath::FixedArray2D<double>,double)
        """
        pass

    def __truediv__(self, DoubleArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (DoubleArray2D)arg1, (DoubleArray2D)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __truediv__(PyImath::FixedArray2D<double>,PyImath::FixedArray2D<double>)
        
        __truediv__( (DoubleArray2D)arg1, (float)arg2) -> DoubleArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<double> __truediv__(PyImath::FixedArray2D<double>,double)
        """
        pass

    __instance_size__ = 72


