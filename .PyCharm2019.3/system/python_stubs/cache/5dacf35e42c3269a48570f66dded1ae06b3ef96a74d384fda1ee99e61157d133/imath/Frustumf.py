# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Frustumf(__Boost_Python.instance):
    """ Frustumf """
    def aspect(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        aspect( (Frustumf)arg1) -> float :
            F.aspect() -- derives and returns the aspect ratio for frustum F
        
            C++ signature :
                float aspect(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def bottom(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        bottom( (Frustumf)arg1) -> float :
            F.bottom() -- returns the bottom coordinate of the near clipping window of frustum F
        
            C++ signature :
                float bottom(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def DepthToZ(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        DepthToZ( (Frustumf)arg1, (float)arg2, (int)arg3, (int)arg4) -> int :
            F.DepthToZ(depth, zMin, zMax) -- converts depth (Z in the local space of the frustum F) to z (a result of  transformation by F's projection matrix) which is normalized to [zMin, zMax]
        
            C++ signature :
                long DepthToZ(Imath_2_4::Frustum<float> {lvalue},float,long,long)
        """
        pass

    def far(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        far( (Frustumf)arg1) -> float :
            F.far() -- returns the coordinate of the far clipping plane of frustum F
        
            C++ signature :
                float far(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def farPlane(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        farPlane( (Frustumf)arg1) -> float :
            F.farPlane() -- returns the coordinate of the far clipping plane of frustum F
        
            C++ signature :
                float farPlane(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def fovx(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        fovx( (Frustumf)arg1) -> float :
            F.fovx() -- derives and returns the x field of view (in radians) for frustum F
        
            C++ signature :
                float fovx(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def fovy(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        fovy( (Frustumf)arg1) -> float :
            F.fovy() -- derives and returns the y field of view (in radians) for frustum F
        
            C++ signature :
                float fovy(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def left(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        left( (Frustumf)arg1) -> float :
            F.left() -- returns the left coordinate of the near clipping window of frustum F
        
            C++ signature :
                float left(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def modifyNearAndFar(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        modifyNearAndFar( (Frustumf)arg1, (float)arg2, (float)arg3) -> None :
            F.modifyNearAndFar(nearPlane, farPlane) -- modifies the already-valid frustum F as specified
        
            C++ signature :
                void modifyNearAndFar(Imath_2_4::Frustum<float> {lvalue},float,float)
        """
        pass

    def near(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        near( (Frustumf)arg1) -> float :
            F.near() -- returns the coordinate of the near clipping plane of frustum F
        
            C++ signature :
                float near(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def nearPlane(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        nearPlane( (Frustumf)arg1) -> float :
            F.nearPlane() -- returns the coordinate of the near clipping plane of frustum F
        
            C++ signature :
                float nearPlane(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def normalizedZToDepth(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedZToDepth( (Frustumf)arg1, (float)arg2) -> float :
            F.normalizedZToDepth(z) -- returns the depth (Z in the local space of the frustum F) corresponding to z (a result of transformation by F's projection matrix), which is assumed to have been normalized to [-1, 1]
        
            C++ signature :
                float normalizedZToDepth(Imath_2_4::Frustum<float> {lvalue},float)
        """
        pass

    def orthographic(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        orthographic( (Frustumf)arg1) -> bool :
            F.orthographic() -- returns whether frustum F is orthographic or not
        
            C++ signature :
                bool orthographic(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def planes(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        planes( (Frustumf)arg1, (Plane3f)arg2) -> None :
            F.planes([M]) -- returns a sequence of 6 Plane3s, the sides of the frustum F (top, right, bottom, left, nearPlane, farPlane), optionally transformed by the matrix M if specified
        
            C++ signature :
                void planes(Imath_2_4::Frustum<float> {lvalue},Imath_2_4::Plane3<float>*)
        
        planes( (Frustumf)arg1, (Plane3f)arg2, (M44f)arg3) -> None :
        
            C++ signature :
                void planes(Imath_2_4::Frustum<float> {lvalue},Imath_2_4::Plane3<float>*,Imath_2_4::Matrix44<float>)
        
        planes( (Frustumf)arg1 [, (M44f)arg2]) -> tuple :
        
            C++ signature :
                boost::python::tuple planes(Imath_2_4::Frustum<float> {lvalue} [,Imath_2_4::Matrix44<float>])
        """
        pass

    def projectionMatrix(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        projectionMatrix( (Frustumf)arg1) -> M44f :
            F.projectionMatrix() -- derives and returns the projection matrix for frustum F
        
            C++ signature :
                Imath_2_4::Matrix44<float> projectionMatrix(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def projectPointToScreen(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        projectPointToScreen( (Frustumf)arg1, (V3f)arg2) -> V2f :
            F.projectPointToScreen(V) -- returns the projection of V3 V into screen space
        
            C++ signature :
                Imath_2_4::Vec2<float> projectPointToScreen(Imath_2_4::Frustum<float> {lvalue},Imath_2_4::Vec3<float>)
        
        projectPointToScreen( (Frustumf)arg1, (tuple)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> projectPointToScreen(Imath_2_4::Frustum<float> {lvalue},boost::python::tuple)
        
        projectPointToScreen( (Frustumf)arg1, (object)arg2) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> projectPointToScreen(Imath_2_4::Frustum<float> {lvalue},boost::python::api::object)
        """
        pass

    def projectScreenToRay(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        projectScreenToRay( (Frustumf)arg1, (V2f)arg2) -> Line3f :
            F.projectScreenToRay(V) -- returns a Line3 through V, a V2 point in screen space
        
            C++ signature :
                Imath_2_4::Line3<float> projectScreenToRay(Imath_2_4::Frustum<float> {lvalue},Imath_2_4::Vec2<float>)
        
        projectScreenToRay( (Frustumf)arg1, (tuple)arg2) -> Line3f :
        
            C++ signature :
                Imath_2_4::Line3<float> projectScreenToRay(Imath_2_4::Frustum<float> {lvalue},boost::python::tuple)
        """
        pass

    def right(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        right( (Frustumf)arg1) -> float :
            F.right() -- returns the right coordinate of the near clipping window of frustum F
        
            C++ signature :
                float right(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def screenRadius(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        screenRadius( (Frustumf)arg1, (V3f)arg2, (float)arg3) -> float :
            F.screenRadius(V, r) -- returns the radius in screen space corresponding to the point V and radius r in F's local space
        
            C++ signature :
                float screenRadius(Imath_2_4::Frustum<float> {lvalue},Imath_2_4::Vec3<float>,float)
        
        screenRadius( (Frustumf)arg1, (tuple)arg2, (float)arg3) -> float :
        
            C++ signature :
                float screenRadius(Imath_2_4::Frustum<float> {lvalue},boost::python::tuple,float)
        """
        pass

    def set(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set( (Frustumf)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6, (float)arg7, (bool)arg8) -> None :
            F.set(nearPlane, farPlane, left, right, top, bottom, [ortho])
            F.set(nearPlane, farPlane, fovx, fovy, aspect)                -- sets the entire state of frustum F as specified.  Only one of fovx or fovy may be non-zero.
        
            C++ signature :
                void set(Imath_2_4::Frustum<float> {lvalue},float,float,float,float,float,float,bool)
        
        set( (Frustumf)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6) -> None :
        
            C++ signature :
                void set(Imath_2_4::Frustum<float> {lvalue},float,float,float,float,float)
        """
        pass

    def setOrthographic(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setOrthographic( (Frustumf)arg1, (bool)arg2) -> None :
            F.setOrthographic(b) -- modifies the already-valid frustum F to be orthographic or not
        
            C++ signature :
                void setOrthographic(Imath_2_4::Frustum<float> {lvalue},bool)
        """
        pass

    def top(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        top( (Frustumf)arg1) -> float :
            F.top() -- returns the top coordinate of the near clipping window of frustum F
        
            C++ signature :
                float top(Imath_2_4::Frustum<float> {lvalue})
        """
        pass

    def window(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        window( (Frustumf)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5) -> Frustumf :
            F.window(l,r,b,t) -- takes a rectangle in the screen space (i.e., -1 <= l <= r <= 1, -1 <= b <= t <= 1) of F and returns a new Frustum whose near clipping-plane window is that rectangle in local space
        
            C++ signature :
                Imath_2_4::Frustum<float> window(Imath_2_4::Frustum<float> {lvalue},float,float,float,float)
        """
        pass

    def worldRadius(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        worldRadius( (Frustumf)arg1, (V3f)arg2, (float)arg3) -> float :
            F.worldRadius(V, r) -- returns the radius in F's local space corresponding to the point V and radius r in screen space
        
            C++ signature :
                float worldRadius(Imath_2_4::Frustum<float> {lvalue},Imath_2_4::Vec3<float>,float)
        
        worldRadius( (Frustumf)arg1, (tuple)arg2, (float)arg3) -> float :
        
            C++ signature :
                float worldRadius(Imath_2_4::Frustum<float> {lvalue},boost::python::tuple,float)
        """
        pass

    def ZToDepth(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ZToDepth( (Frustumf)arg1, (int)arg2, (int)arg3, (int)arg4) -> float :
            F.ZToDepth(z, zMin, zMax) -- returns the depth (Z in the local space of the frustum F) corresponding to z (a result of transformation by F's projection matrix) after normalizing z to be between zMin and zMax
        
            C++ signature :
                float ZToDepth(Imath_2_4::Frustum<float> {lvalue},long,long,long)
        """
        pass

    def __copy__(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Frustumf)arg1) -> Frustumf :
        
            C++ signature :
                Imath_2_4::Frustum<float> __copy__(Imath_2_4::Frustum<float>)
        """
        pass

    def __deepcopy__(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Frustumf)arg1, (dict)arg2) -> Frustumf :
        
            C++ signature :
                Imath_2_4::Frustum<float> __deepcopy__(Imath_2_4::Frustum<float>,boost::python::dict {lvalue})
        """
        pass

    def __eq__(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Frustumf)arg1, (Frustumf)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Frustum<float> {lvalue},Imath_2_4::Frustum<float>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (Frustumf)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Frustum<float>)
        
        __init__( (object)arg1) -> None :
            Frustum() default construction
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6, (float)arg7, (bool)arg8) -> None :
            Frustum(nearPlane,farPlane,left,right,top,bottom,ortho) construction
        
            C++ signature :
                void __init__(_object*,float,float,float,float,float,float,bool)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5, (float)arg6) -> None :
            Frustum(nearPlane,farPlane,fovx,fovy,aspect) construction
        
            C++ signature :
                void __init__(_object*,float,float,float,float,float)
        """
        pass

    def __ne__(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Frustumf)arg1, (Frustumf)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Frustum<float> {lvalue},Imath_2_4::Frustum<float>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Frustumf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Frustumf)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Frustum<float>)
        """
        pass

    __instance_size__ = 56


