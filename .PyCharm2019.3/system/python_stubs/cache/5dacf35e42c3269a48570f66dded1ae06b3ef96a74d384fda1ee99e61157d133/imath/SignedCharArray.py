# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class SignedCharArray(__Boost_Python.instance):
    """ Fixed length array of signed chars """
    def ifelse(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (SignedCharArray)arg1, (IntArray)arg2, (int)arg3) -> SignedCharArray :
        
            C++ signature :
                PyImath::FixedArray<signed char> ifelse(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<int>,signed char)
        
        ifelse( (SignedCharArray)arg1, (IntArray)arg2, (SignedCharArray)arg3) -> SignedCharArray :
        
            C++ signature :
                PyImath::FixedArray<signed char> ifelse(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<signed char>)
        """
        pass

    def reduce(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (SignedCharArray)arg1) -> int :
        
            C++ signature :
                signed char reduce(PyImath::FixedArray<signed char>)
        """
        pass

    def __add__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<signed char> __add__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __add__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<signed char> __add__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __div__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<signed char> __div__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __div__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<signed char> __div__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __eq__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (SignedCharArray)arg1, (int)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __eq__( (SignedCharArray)arg1, (SignedCharArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __getitem__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (SignedCharArray)arg1, (object)arg2) -> SignedCharArray :
        
            C++ signature :
                PyImath::FixedArray<signed char> __getitem__(PyImath::FixedArray<signed char> {lvalue},_object*)
        
        __getitem__( (SignedCharArray)arg1, (IntArray)arg2) -> SignedCharArray :
        
            C++ signature :
                PyImath::FixedArray<signed char> __getitem__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (SignedCharArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                signed char __getitem__(PyImath::FixedArray<signed char> {lvalue},long)
        
        __getitem__( (SignedCharArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                signed char __getitem__(PyImath::FixedArray<signed char> {lvalue},long)
        """
        pass

    def __ge__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (SignedCharArray)arg1, (int)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __ge__( (SignedCharArray)arg1, (SignedCharArray)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __gt__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (SignedCharArray)arg1, (int)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __gt__( (SignedCharArray)arg1, (SignedCharArray)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __iadd__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __iadd__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __iadd__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __iadd__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __idiv__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __idiv__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __idiv__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __idiv__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __imod__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imod__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __imod__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __imod__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __imod__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __imul__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __imul__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __imul__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __imul__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (SignedCharArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<signed char>)
        
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,signed char,unsigned long)
        """
        pass

    def __isub__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __isub__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __isub__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __isub__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __itruediv__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __itruediv__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __itruediv__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<signed char> {lvalue} __itruediv__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __len__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (SignedCharArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<signed char> {lvalue})
        """
        pass

    def __le__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (SignedCharArray)arg1, (int)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __le__( (SignedCharArray)arg1, (SignedCharArray)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __lt__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (SignedCharArray)arg1, (int)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __lt__( (SignedCharArray)arg1, (SignedCharArray)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __mod__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mod__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<signed char> __mod__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __mod__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<signed char> __mod__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __mul__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<signed char> __mul__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __mul__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<signed char> __mul__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __neg__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (SignedCharArray)arg1) -> SignedCharArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<signed char> __neg__(PyImath::FixedArray<signed char> {lvalue})
        """
        pass

    def __ne__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (SignedCharArray)arg1, (int)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __ne__( (SignedCharArray)arg1, (SignedCharArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __radd__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<signed char> __radd__(PyImath::FixedArray<signed char> {lvalue},signed char)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<signed char> __rmul__(PyImath::FixedArray<signed char> {lvalue},signed char)
        """
        pass

    def __rsub__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<signed char> __rsub__(PyImath::FixedArray<signed char> {lvalue},signed char)
        """
        pass

    def __setitem__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (SignedCharArray)arg1, (object)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<signed char> {lvalue},_object*,signed char)
        
        __setitem__( (SignedCharArray)arg1, (IntArray)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<int>,signed char)
        
        __setitem__( (SignedCharArray)arg1, (object)arg2, (SignedCharArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<signed char> {lvalue},_object*,PyImath::FixedArray<signed char>)
        
        __setitem__( (SignedCharArray)arg1, (IntArray)arg2, (SignedCharArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<signed char>)
        """
        pass

    def __sub__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<signed char> __sub__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __sub__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<signed char> __sub__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    def __truediv__(self, SignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (SignedCharArray)arg1, (int)x) -> SignedCharArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<signed char> __truediv__(PyImath::FixedArray<signed char> {lvalue},signed char)
        
        __truediv__( (SignedCharArray)arg1, (SignedCharArray)x) -> SignedCharArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<signed char> __truediv__(PyImath::FixedArray<signed char> {lvalue},PyImath::FixedArray<signed char>)
        """
        pass

    __instance_size__ = 72


