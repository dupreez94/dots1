# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class FloatMatrix(__Boost_Python.instance):
    """ Fixed size matrix of floats """
    def columns(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        columns( (FloatMatrix)arg1) -> int :
        
            C++ signature :
                int columns(PyImath::FixedMatrix<float> {lvalue})
        """
        pass

    def rows(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rows( (FloatMatrix)arg1) -> int :
        
            C++ signature :
                int rows(PyImath::FixedMatrix<float> {lvalue})
        """
        pass

    def __add__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __add__(PyImath::FixedMatrix<float>,PyImath::FixedMatrix<float>)
        
        __add__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __add__(PyImath::FixedMatrix<float>,float)
        """
        pass

    def __div__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __div__(PyImath::FixedMatrix<float>,PyImath::FixedMatrix<float>)
        
        __div__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __div__(PyImath::FixedMatrix<float>,float)
        """
        pass

    def __getitem__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (FloatMatrix)arg1, (object)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __getitem__(PyImath::FixedMatrix<float> {lvalue},_object*)
        
        __getitem__( (FloatMatrix)arg1, (int)arg2) -> FloatArray :
        
            C++ signature :
                PyImath::FixedArray<float> const* __getitem__(PyImath::FixedMatrix<float> {lvalue},int)
        """
        pass

    def __iadd__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __iadd__(PyImath::FixedMatrix<float> {lvalue},PyImath::FixedMatrix<float>)
        
        __iadd__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __iadd__(PyImath::FixedMatrix<float> {lvalue},float)
        """
        pass

    def __idiv__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __idiv__(PyImath::FixedMatrix<float> {lvalue},PyImath::FixedMatrix<float>)
        
        __idiv__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __idiv__(PyImath::FixedMatrix<float> {lvalue},float)
        """
        pass

    def __imul__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __imul__(PyImath::FixedMatrix<float> {lvalue},PyImath::FixedMatrix<float>)
        
        __imul__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __imul__(PyImath::FixedMatrix<float> {lvalue},float)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            return an unitialized array of the specified rows and cols
        
            C++ signature :
                void __init__(_object*,int,int)
        """
        pass

    def __ipow__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ipow__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __ipow__(PyImath::FixedMatrix<float> {lvalue},float)
        
        __ipow__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __ipow__(PyImath::FixedMatrix<float> {lvalue},PyImath::FixedMatrix<float>)
        """
        pass

    def __isub__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __isub__(PyImath::FixedMatrix<float> {lvalue},PyImath::FixedMatrix<float>)
        
        __isub__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __isub__(PyImath::FixedMatrix<float> {lvalue},float)
        """
        pass

    def __itruediv__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __itruediv__(PyImath::FixedMatrix<float> {lvalue},PyImath::FixedMatrix<float>)
        
        __itruediv__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> {lvalue} __itruediv__(PyImath::FixedMatrix<float> {lvalue},float)
        """
        pass

    def __len__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (FloatMatrix)arg1) -> int :
        
            C++ signature :
                int __len__(PyImath::FixedMatrix<float> {lvalue})
        """
        pass

    def __mul__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __mul__(PyImath::FixedMatrix<float>,PyImath::FixedMatrix<float>)
        
        __mul__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __mul__(PyImath::FixedMatrix<float>,float)
        """
        pass

    def __neg__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (FloatMatrix)arg1) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __neg__(PyImath::FixedMatrix<float>)
        """
        pass

    def __pow__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __pow__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __pow__(PyImath::FixedMatrix<float>,float)
        
        __pow__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __pow__(PyImath::FixedMatrix<float>,PyImath::FixedMatrix<float>)
        """
        pass

    def __radd__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __radd__(PyImath::FixedMatrix<float>,float)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __rmul__(PyImath::FixedMatrix<float>,float)
        """
        pass

    def __rsub__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __rsub__(PyImath::FixedMatrix<float>,float)
        """
        pass

    def __setitem__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (FloatMatrix)arg1, (object)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedMatrix<float> {lvalue},_object*,float)
        
        __setitem__( (FloatMatrix)arg1, (object)arg2, (FloatArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedMatrix<float> {lvalue},_object*,PyImath::FixedArray<float>)
        
        __setitem__( (FloatMatrix)arg1, (object)arg2, (FloatMatrix)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedMatrix<float> {lvalue},_object*,PyImath::FixedMatrix<float>)
        """
        pass

    def __sub__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __sub__(PyImath::FixedMatrix<float>,PyImath::FixedMatrix<float>)
        
        __sub__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __sub__(PyImath::FixedMatrix<float>,float)
        """
        pass

    def __truediv__(self, FloatMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (FloatMatrix)arg1, (FloatMatrix)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __truediv__(PyImath::FixedMatrix<float>,PyImath::FixedMatrix<float>)
        
        __truediv__( (FloatMatrix)arg1, (float)arg2) -> FloatMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<float> __truediv__(PyImath::FixedMatrix<float>,float)
        """
        pass

    __instance_size__ = 48


