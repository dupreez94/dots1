# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Box2f(__Boost_Python.instance):
    # no doc
    def center(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        center( (Box2f)arg1) -> V2f :
            center() center of the box
        
            C++ signature :
                Imath_2_4::Vec2<float> center(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def extendBy(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extendBy( (Box2f)arg1, (V2f)arg2) -> None :
            extendBy(point) extend the box by a point
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        extendBy( (Box2f)arg1, (V2fArray)arg2) -> None :
            extendBy(array) extend the box the values in the array
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        
        extendBy( (Box2f)arg1, (Box2f)arg2) -> None :
            extendBy(box) extend the box by a box
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<float> >)
        """
        pass

    def hasVolume(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        hasVolume( (Box2f)arg1) -> bool :
            hasVolume() returns true if the box has volume
        
            C++ signature :
                bool hasVolume(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def intersects(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        intersects( (Box2f)arg1, (V2f)arg2) -> bool :
            intersects(point) returns true if the box intersects the given point
        
            C++ signature :
                bool intersects(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        
        intersects( (Box2f)arg1, (Box2f)arg2) -> bool :
            intersects(box) returns true if the box intersects the given box
        
            C++ signature :
                bool intersects(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<float> >)
        """
        pass

    def isEmpty(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        isEmpty( (Box2f)arg1) -> bool :
            isEmpty() returns true if the box is empty
        
            C++ signature :
                bool isEmpty(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def isInfinite(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        isInfinite( (Box2f)arg1) -> bool :
            isInfinite() returns true if the box covers all space
        
            C++ signature :
                bool isInfinite(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def majorAxis(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        majorAxis( (Box2f)arg1) -> int :
            majorAxis() major axis of the box
        
            C++ signature :
                unsigned int majorAxis(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def makeEmpty(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        makeEmpty( (Box2f)arg1) -> None :
            makeEmpty() make the box empty
        
            C++ signature :
                void makeEmpty(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def makeInfinite(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        makeInfinite( (Box2f)arg1) -> None :
            makeInfinite() make the box cover all space
        
            C++ signature :
                void makeInfinite(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def max(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max( (Box2f)arg1) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> max(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def min(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        min( (Box2f)arg1) -> V2f :
        
            C++ signature :
                Imath_2_4::Vec2<float> min(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def setMax(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setMax( (Box2f)arg1, (V2f)arg2) -> None :
            setMax() sets the max value of the box
        
            C++ signature :
                void setMax(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        """
        pass

    def setMin(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setMin( (Box2f)arg1, (V2f)arg2) -> None :
            setMin() sets the min value of the box
        
            C++ signature :
                void setMin(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Vec2<float>)
        """
        pass

    def size(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        size( (Box2f)arg1) -> V2f :
            size() size of the box
        
            C++ signature :
                Imath_2_4::Vec2<float> size(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue})
        """
        pass

    def __eq__(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Box2f)arg1, (Box2f)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<float> >)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1) -> None :
            Box() create empty box
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (V2f)arg2) -> None :
            Box(point)create box containing the given point
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<float>)
        
        __init__( (object)arg1, (V2f)arg2, (V2f)arg3) -> None :
            Box(point,point) create box continaing min and max
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<float>,Imath_2_4::Vec2<float>)
        
        __init__( (object)arg1, (tuple)arg2) -> object :
            Box(point) where point is a python tuple
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple)
        
        __init__( (object)arg1, (tuple)arg2, (tuple)arg3) -> object :
            Box(point,point) where point is a python tuple
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple,boost::python::tuple)
        
        __init__( (object)arg1, (Box2f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec2<float> >)
        
        __init__( (object)arg1, (Box2d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec2<double> >)
        
        __init__( (object)arg1, (Box2i)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec2<int> >)
        """
        pass

    def __ne__(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Box2f)arg1, (Box2f)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Box<Imath_2_4::Vec2<float> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<float> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Box2f, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Box2f)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Box<Imath_2_4::Vec2<float> >)
        """
        pass

    __instance_size__ = 32


