# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Box3d(__Boost_Python.instance):
    # no doc
    def center(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        center( (Box3d)arg1) -> V3d :
            center() center of the box
        
            C++ signature :
                Imath_2_4::Vec3<double> center(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def extendBy(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extendBy( (Box3d)arg1, (V3d)arg2) -> None :
            extendBy(point) extend the box by a point
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        extendBy( (Box3d)arg1, (V3dArray)arg2) -> None :
            extendBy(array) extend the box the values in the array
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        
        extendBy( (Box3d)arg1, (Box3d)arg2) -> None :
            extendBy(box) extend the box by a box
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        """
        pass

    def hasVolume(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        hasVolume( (Box3d)arg1) -> bool :
            hasVolume() returns true if the box has volume
        
            C++ signature :
                bool hasVolume(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def intersects(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        intersects( (Box3d)arg1, (V3d)arg2) -> bool :
            intersects(point) returns true if the box intersects the given point
        
            C++ signature :
                bool intersects(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        
        intersects( (Box3d)arg1, (Box3d)arg2) -> bool :
            intersects(box) returns true if the box intersects the given box
        
            C++ signature :
                bool intersects(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        
        intersects( (Box3d)arg1, (V3dArray)arg2) -> IntArray :
            intersects(array) returns an int array where 0 indicates the point is not in the box and 1 indicates that it is
        
            C++ signature :
                PyImath::FixedArray<int> intersects(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<double> >)
        """
        pass

    def isEmpty(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        isEmpty( (Box3d)arg1) -> bool :
            isEmpty() returns true if the box is empty
        
            C++ signature :
                bool isEmpty(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def isInfinite(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        isInfinite( (Box3d)arg1) -> bool :
            isInfinite() returns true if the box covers all space
        
            C++ signature :
                bool isInfinite(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def majorAxis(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        majorAxis( (Box3d)arg1) -> int :
            majorAxis() major axis of the box
        
            C++ signature :
                unsigned int majorAxis(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def makeEmpty(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        makeEmpty( (Box3d)arg1) -> None :
            makeEmpty() make the box empty
        
            C++ signature :
                void makeEmpty(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def makeInfinite(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        makeInfinite( (Box3d)arg1) -> None :
            makeInfinite() make the box cover all space
        
            C++ signature :
                void makeInfinite(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def max(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max( (Box3d)arg1) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> max(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def min(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        min( (Box3d)arg1) -> V3d :
        
            C++ signature :
                Imath_2_4::Vec3<double> min(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def setMax(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setMax( (Box3d)arg1, (V3d)arg2) -> None :
            setMax() sets the max value of the box
        
            C++ signature :
                void setMax(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        """
        pass

    def setMin(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setMin( (Box3d)arg1, (V3d)arg2) -> None :
            setMin() sets the min value of the box
        
            C++ signature :
                void setMin(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Vec3<double>)
        """
        pass

    def size(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        size( (Box3d)arg1) -> V3d :
            size() size of the box
        
            C++ signature :
                Imath_2_4::Vec3<double> size(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue})
        """
        pass

    def __copy__(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Box3d)arg1) -> Box3d :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec3<double> > __copy__(Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        """
        pass

    def __deepcopy__(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Box3d)arg1, (dict)arg2) -> Box3d :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec3<double> > __deepcopy__(Imath_2_4::Box<Imath_2_4::Vec3<double> >,boost::python::dict {lvalue})
        """
        pass

    def __eq__(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Box3d)arg1, (Box3d)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        """
        pass

    def __imul__(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (Box3d)arg1, (M44f)arg2) -> Box3d :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec3<double> > __imul__(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Matrix44<float>)
        
        __imul__( (Box3d)arg1, (M44d)arg2) -> Box3d :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec3<double> > __imul__(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1) -> None :
            Box() create empty box
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (V3d)arg2) -> None :
            Box(point)create box containing the given point
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<double>)
        
        __init__( (object)arg1, (V3d)arg2, (V3d)arg3) -> None :
            Box(point,point) create box continaing min and max
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec3<double>,Imath_2_4::Vec3<double>)
        
        __init__( (object)arg1, (tuple)arg2) -> object :
            Box(point) where point is a python tuple
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple)
        
        __init__( (object)arg1, (tuple)arg2, (tuple)arg3) -> object :
            Box(point,point) where point is a python tuple
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple,boost::python::tuple)
        
        __init__( (object)arg1, (Box3f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec3<float> >)
        
        __init__( (object)arg1, (Box3d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        
        __init__( (object)arg1, (Box3i)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec3<int> >)
        """
        pass

    def __mul__(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (Box3d)arg1, (M44f)arg2) -> Box3d :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec3<double> > __mul__(Imath_2_4::Box<Imath_2_4::Vec3<double> >,Imath_2_4::Matrix44<float>)
        
        __mul__( (Box3d)arg1, (M44d)arg2) -> Box3d :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec3<double> > __mul__(Imath_2_4::Box<Imath_2_4::Vec3<double> >,Imath_2_4::Matrix44<double>)
        """
        pass

    def __ne__(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Box3d)arg1, (Box3d)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Box<Imath_2_4::Vec3<double> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Box3d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Box3d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        """
        pass

    __instance_size__ = 64


