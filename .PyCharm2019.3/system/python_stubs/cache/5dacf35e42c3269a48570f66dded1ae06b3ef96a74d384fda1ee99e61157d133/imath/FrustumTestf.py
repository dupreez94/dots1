# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class FrustumTestf(__Boost_Python.instance):
    """ FrustumTestf """
    def completelyContains(self, FrustumTestf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        completelyContains( (FrustumTestf)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool completelyContains(Imath_2_4::FrustumTest<float> {lvalue},Imath_2_4::Sphere3<float>)
        
        completelyContains( (FrustumTestf)arg1, (Box3f)arg2) -> bool :
        
            C++ signature :
                bool completelyContains(Imath_2_4::FrustumTest<float> {lvalue},Imath_2_4::Box<Imath_2_4::Vec3<float> >)
        """
        pass

    def isVisible(self, FrustumTestf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        isVisible( (FrustumTestf)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool isVisible(Imath_2_4::FrustumTest<float> {lvalue},Imath_2_4::Sphere3<float>)
        
        isVisible( (FrustumTestf)arg1, (Box3f)arg2) -> bool :
        
            C++ signature :
                bool isVisible(Imath_2_4::FrustumTest<float> {lvalue},Imath_2_4::Box<Imath_2_4::Vec3<float> >)
        
        isVisible( (FrustumTestf)arg1, (V3f)arg2) -> bool :
        
            C++ signature :
                bool isVisible(Imath_2_4::FrustumTest<float> {lvalue},Imath_2_4::Vec3<float>)
        
        isVisible( (FrustumTestf)arg1, (V3fArray)arg2) -> IntArray :
        
            C++ signature :
                PyImath::FixedArray<int> isVisible(Imath_2_4::FrustumTest<float> {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<float> >)
        """
        pass

    def __copy__(self, FrustumTestf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (FrustumTestf)arg1) -> FrustumTestf :
        
            C++ signature :
                Imath_2_4::FrustumTest<float> __copy__(Imath_2_4::FrustumTest<float>)
        """
        pass

    def __deepcopy__(self, FrustumTestf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (FrustumTestf)arg1, (dict)arg2) -> FrustumTestf :
        
            C++ signature :
                Imath_2_4::FrustumTest<float> __deepcopy__(Imath_2_4::FrustumTest<float>,boost::python::dict {lvalue})
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (Frustumf)arg2, (M44f)arg3) -> None :
            create a frustum test object from a frustum and transform
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Frustum<float>,Imath_2_4::Matrix44<float>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    __instance_size__ = 288


