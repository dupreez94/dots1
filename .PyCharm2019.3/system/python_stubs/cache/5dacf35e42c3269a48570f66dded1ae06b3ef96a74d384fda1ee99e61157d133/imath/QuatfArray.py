# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class QuatfArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Quat """
    def angle(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        angle( (QuatfArray)arg1) -> FloatArray :
            get rotation angle about the axis returned by axis() for each quat
        
            C++ signature :
                PyImath::FixedArray<float> angle(PyImath::FixedArray<Imath_2_4::Quat<float> >)
        """
        pass

    def axis(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        axis( (QuatfArray)arg1) -> V3fArray :
            get rotation axis for each quat
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<float> > axis(PyImath::FixedArray<Imath_2_4::Quat<float> >)
        """
        pass

    def ifelse(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (QuatfArray)arg1, (IntArray)arg2, (Quatf)arg3) -> QuatfArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<float> > ifelse(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Quat<float>)
        
        ifelse( (QuatfArray)arg1, (IntArray)arg2, (QuatfArray)arg3) -> QuatfArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<float> > ifelse(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Quat<float> >)
        """
        pass

    def orientToVectors(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        orientToVectors( (QuatfArray)arg1, (V3fArray)forward, (V3fArray)up, (bool)alignForward) -> None :
            Sets the orientations to match the given forward and up vectors, matching the forward vector exactly if 'alignForward' is True, matching the up vector exactly if 'alignForward' is False.  If the vectors are already orthogonal, both vectors will be matched exactly.
        
            C++ signature :
                void orientToVectors(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<float> >,PyImath::FixedArray<Imath_2_4::Vec3<float> >,bool)
        """
        pass

    def setAxisAngle(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setAxisAngle( (QuatfArray)arg1, (V3fArray)axis, (FloatArray)angle) -> None :
            set the quaternion arrays from a given axis and angle
        
            C++ signature :
                void setAxisAngle(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<float> >,PyImath::FixedArray<float>)
        """
        pass

    def setEulerXYZ(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setEulerXYZ( (QuatfArray)arg1, (V3fArray)euler) -> None :
            set the quaternion arrays from a given euler XYZ angle vector
        
            C++ signature :
                void setEulerXYZ(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<float> >)
        """
        pass

    def setRotation(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setRotation( (QuatfArray)arg1, (V3fArray)from, (V3fArray)to) -> None :
            set rotation angles for each quat
        
            C++ signature :
                void setRotation(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<float> >,PyImath::FixedArray<Imath_2_4::Vec3<float> >)
        """
        pass

    def __copy__(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (QuatfArray)arg1) -> QuatfArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<float> > __copy__(PyImath::FixedArray<Imath_2_4::Quat<float> >)
        """
        pass

    def __deepcopy__(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (QuatfArray)arg1, (dict)arg2) -> QuatfArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<float> > __deepcopy__(PyImath::FixedArray<Imath_2_4::Quat<float> >,boost::python::dict {lvalue})
        """
        pass

    def __eq__(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (QuatfArray)arg1, (Quatf)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},Imath_2_4::Quat<float>)
        
        __eq__( (QuatfArray)arg1, (QuatfArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Quat<float> >)
        """
        pass

    def __getitem__(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (QuatfArray)arg1, (object)arg2) -> QuatfArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},_object*)
        
        __getitem__( (QuatfArray)arg1, (IntArray)arg2) -> QuatfArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (QuatfArray)arg1, (int)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __getitem__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},long)
        
        __getitem__( (QuatfArray)arg1, (int)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (QuatfArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Quat<float> >)
        
        __init__( (object)arg1, (Quatf)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Quat<float>,unsigned long)
        
        __init__( (object)arg1, (EulerfArray)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,PyImath::FixedArray<Imath_2_4::Euler<float> >)
        
        __init__( (object)arg1, (QuatdArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Quat<double> >)
        """
        pass

    def __len__(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (QuatfArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue})
        """
        pass

    def __mul__(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (QuatfArray)arg1, (QuatfArray)arg2) -> QuatfArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Quat<float> > __mul__(PyImath::FixedArray<Imath_2_4::Quat<float> >,PyImath::FixedArray<Imath_2_4::Quat<float> >)
        """
        pass

    def __ne__(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (QuatfArray)arg1, (Quatf)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},Imath_2_4::Quat<float>)
        
        __ne__( (QuatfArray)arg1, (QuatfArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},PyImath::FixedArray<Imath_2_4::Quat<float> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (QuatfArray)arg1, (V3f)arg2) -> V3fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<float> > __rmul__(PyImath::FixedArray<Imath_2_4::Quat<float> >,Imath_2_4::Vec3<float>)
        
        __rmul__( (QuatfArray)arg1, (V3fArray)arg2) -> V3fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<float> > __rmul__(PyImath::FixedArray<Imath_2_4::Quat<float> >,PyImath::FixedArray<Imath_2_4::Vec3<float> >)
        """
        pass

    def __setitem__(self, QuatfArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (QuatfArray)arg1, (object)arg2, (Quatf)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},_object*,Imath_2_4::Quat<float>)
        
        __setitem__( (QuatfArray)arg1, (IntArray)arg2, (Quatf)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Quat<float>)
        
        __setitem__( (QuatfArray)arg1, (object)arg2, (QuatfArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Quat<float> >)
        
        __setitem__( (QuatfArray)arg1, (IntArray)arg2, (QuatfArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Quat<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Quat<float> >)
        """
        pass

    r = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    z = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


