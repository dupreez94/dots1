# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Color4cArray2D(__Boost_Python.instance):
    """ Fixed length 2d array of IMATH_NAMESPACE::Color4 """
    def ifelse(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (Color4cArray2D)arg1, (IntArray2D)arg2, (Color4c)arg3) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > ifelse(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<int>,Imath_2_4::Color4<unsigned char>)
        
        ifelse( (Color4cArray2D)arg1, (IntArray2D)arg2, (Color4cArray2D)arg3) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > ifelse(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        """
        pass

    def item(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        item( (Color4cArray2D)arg1, (int)arg2, (int)arg3) -> Color4c :
        
            C++ signature :
                Imath_2_4::Color4<unsigned char> {lvalue} item(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},long,long)
        """
        pass

    def size(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        size( (Color4cArray2D)arg1) -> tuple :
        
            C++ signature :
                boost::python::tuple size(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue})
        """
        pass

    def __add__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __add__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __add__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __add__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __copy__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Color4cArray2D)arg1) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __copy__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        """
        pass

    def __deepcopy__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Color4cArray2D)arg1, (dict)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __deepcopy__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (Color4cArray2D)arg1, (int)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __div__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,unsigned char)
        
        __div__( (Color4cArray2D)arg1, (object)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __div__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,PyImath::FixedArray2D<unsigned char>)
        
        __div__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __div__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __div__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __div__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __eq__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __eq__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __eq__( (Color4cArray2D)arg1, (Color4c)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __eq__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __getitem__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (Color4cArray2D)arg1, (object)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __getitem__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},_object*)
        
        __getitem__( (Color4cArray2D)arg1, (IntArray2D)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __getitem__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<int>)
        """
        pass

    def __iadd__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __iadd__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __iadd__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __iadd__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __idiv__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (Color4cArray2D)arg1, (int)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __idiv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},unsigned char)
        
        __idiv__( (Color4cArray2D)arg1, (object)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __idiv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<unsigned char>)
        
        __idiv__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __idiv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __idiv__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __idiv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __imul__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (Color4cArray2D)arg1, (int)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __imul__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},unsigned char)
        
        __imul__( (Color4cArray2D)arg1, (object)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __imul__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<unsigned char>)
        
        __imul__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __imul__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __imul__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __imul__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long,unsigned long)
        
        __init__( (object)arg1, (Color4cArray2D)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __init__( (object)arg1, (Color4c)arg2, (int)arg3, (int)arg4) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Color4<unsigned char>,unsigned long,unsigned long)
        """
        pass

    def __isub__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __isub__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __isub__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __isub__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __itruediv__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (Color4cArray2D)arg1, (int)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __itruediv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},unsigned char)
        
        __itruediv__( (Color4cArray2D)arg1, (object)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __itruediv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<unsigned char>)
        
        __itruediv__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __itruediv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __itruediv__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __itruediv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __len__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (Color4cArray2D)arg1) -> int :
        
            C++ signature :
                unsigned long __len__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue})
        """
        pass

    def __mul__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (Color4cArray2D)arg1, (int)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __mul__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,unsigned char)
        
        __mul__( (Color4cArray2D)arg1, (object)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __mul__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,PyImath::FixedArray2D<unsigned char>)
        
        __mul__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __mul__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __mul__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __mul__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __neg__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (Color4cArray2D)arg1) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __neg__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        """
        pass

    def __ne__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ne__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __ne__( (Color4cArray2D)arg1, (Color4c)arg2) -> IntArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<int> __ne__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __radd__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __radd__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (Color4cArray2D)arg1, (int)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __rmul__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,unsigned char)
        
        __rmul__( (Color4cArray2D)arg1, (object)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __rmul__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,PyImath::FixedArray2D<unsigned char>)
        
        __rmul__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __rmul__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __rsub__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __rsub__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __setitem__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (Color4cArray2D)arg1, (object)arg2, (Color4c)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},_object*,Imath_2_4::Color4<unsigned char>)
        
        __setitem__( (Color4cArray2D)arg1, (IntArray2D)arg2, (Color4c)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<int>,Imath_2_4::Color4<unsigned char>)
        
        __setitem__( (Color4cArray2D)arg1, (object)arg2, (Color4cArray2D)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},_object*,PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __setitem__( (Color4cArray2D)arg1, (IntArray2D)arg2, (Color4cArray2D)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __setitem__( (Color4cArray2D)arg1, (object)arg2, (C4cArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Color4<unsigned char> >)
        
        __setitem__( (Color4cArray2D)arg1, (IntArray2D)arg2, (C4cArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray2D<int>,PyImath::FixedArray<Imath_2_4::Color4<unsigned char> >)
        
        __setitem__( (Color4cArray2D)arg1, (tuple)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > {lvalue},boost::python::tuple,boost::python::tuple)
        """
        pass

    def __sub__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __sub__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __sub__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __sub__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,Imath_2_4::Color4<unsigned char>)
        """
        pass

    def __truediv__(self, Color4cArray2D, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (Color4cArray2D)arg1, (int)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __truediv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,unsigned char)
        
        __truediv__( (Color4cArray2D)arg1, (object)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __truediv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,PyImath::FixedArray2D<unsigned char>)
        
        __truediv__( (Color4cArray2D)arg1, (Color4cArray2D)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __truediv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >)
        
        __truediv__( (Color4cArray2D)arg1, (Color4c)arg2) -> Color4cArray2D :
        
            C++ signature :
                PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> > __truediv__(PyImath::FixedArray2D<Imath_2_4::Color4<unsigned char> >,Imath_2_4::Color4<unsigned char>)
        """
        pass

    a = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    b = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    g = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    r = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


