# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


from .QGraphicsItem import QGraphicsItem

class QGraphicsItemGroup(QGraphicsItem):
    """ QGraphicsItemGroup(parent: QGraphicsItem = None) """
    def addToGroup(self, QGraphicsItem): # real signature unknown; restored from __doc__
        """ addToGroup(self, QGraphicsItem) """
        pass

    def boundingRect(self): # real signature unknown; restored from __doc__
        """ boundingRect(self) -> QRectF """
        pass

    def isObscuredBy(self, QGraphicsItem): # real signature unknown; restored from __doc__
        """ isObscuredBy(self, QGraphicsItem) -> bool """
        return False

    def opaqueArea(self): # real signature unknown; restored from __doc__
        """ opaqueArea(self) -> QPainterPath """
        pass

    def paint(self, QPainter, QStyleOptionGraphicsItem, widget=None): # real signature unknown; restored from __doc__
        """ paint(self, QPainter, QStyleOptionGraphicsItem, widget: QWidget = None) """
        pass

    def removeFromGroup(self, QGraphicsItem): # real signature unknown; restored from __doc__
        """ removeFromGroup(self, QGraphicsItem) """
        pass

    def type(self): # real signature unknown; restored from __doc__
        """ type(self) -> int """
        return 0

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


