# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


from .QAbstractButton import QAbstractButton

class QPushButton(QAbstractButton):
    """
    QPushButton(parent: QWidget = None)
    QPushButton(str, parent: QWidget = None)
    QPushButton(QIcon, str, parent: QWidget = None)
    """
    def autoDefault(self): # real signature unknown; restored from __doc__
        """ autoDefault(self) -> bool """
        return False

    def event(self, QEvent): # real signature unknown; restored from __doc__
        """ event(self, QEvent) -> bool """
        return False

    def focusInEvent(self, QFocusEvent): # real signature unknown; restored from __doc__
        """ focusInEvent(self, QFocusEvent) """
        pass

    def focusOutEvent(self, QFocusEvent): # real signature unknown; restored from __doc__
        """ focusOutEvent(self, QFocusEvent) """
        pass

    def initStyleOption(self, QStyleOptionButton): # real signature unknown; restored from __doc__
        """ initStyleOption(self, QStyleOptionButton) """
        pass

    def isDefault(self): # real signature unknown; restored from __doc__
        """ isDefault(self) -> bool """
        return False

    def isFlat(self): # real signature unknown; restored from __doc__
        """ isFlat(self) -> bool """
        return False

    def keyPressEvent(self, QKeyEvent): # real signature unknown; restored from __doc__
        """ keyPressEvent(self, QKeyEvent) """
        pass

    def menu(self): # real signature unknown; restored from __doc__
        """ menu(self) -> QMenu """
        return QMenu

    def minimumSizeHint(self): # real signature unknown; restored from __doc__
        """ minimumSizeHint(self) -> QSize """
        pass

    def paintEvent(self, QPaintEvent): # real signature unknown; restored from __doc__
        """ paintEvent(self, QPaintEvent) """
        pass

    def setAutoDefault(self, bool): # real signature unknown; restored from __doc__
        """ setAutoDefault(self, bool) """
        pass

    def setDefault(self, bool): # real signature unknown; restored from __doc__
        """ setDefault(self, bool) """
        pass

    def setFlat(self, bool): # real signature unknown; restored from __doc__
        """ setFlat(self, bool) """
        pass

    def setMenu(self, QMenu): # real signature unknown; restored from __doc__
        """ setMenu(self, QMenu) """
        pass

    def showMenu(self): # real signature unknown; restored from __doc__
        """ showMenu(self) """
        pass

    def sizeHint(self): # real signature unknown; restored from __doc__
        """ sizeHint(self) -> QSize """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


