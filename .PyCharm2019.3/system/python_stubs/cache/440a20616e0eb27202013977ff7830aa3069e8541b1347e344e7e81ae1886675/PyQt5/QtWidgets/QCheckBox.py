# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


from .QAbstractButton import QAbstractButton

class QCheckBox(QAbstractButton):
    """
    QCheckBox(parent: QWidget = None)
    QCheckBox(str, parent: QWidget = None)
    """
    def checkState(self): # real signature unknown; restored from __doc__
        """ checkState(self) -> Qt.CheckState """
        pass

    def checkStateSet(self): # real signature unknown; restored from __doc__
        """ checkStateSet(self) """
        pass

    def event(self, QEvent): # real signature unknown; restored from __doc__
        """ event(self, QEvent) -> bool """
        return False

    def hitButton(self, QPoint): # real signature unknown; restored from __doc__
        """ hitButton(self, QPoint) -> bool """
        return False

    def initStyleOption(self, QStyleOptionButton): # real signature unknown; restored from __doc__
        """ initStyleOption(self, QStyleOptionButton) """
        pass

    def isTristate(self): # real signature unknown; restored from __doc__
        """ isTristate(self) -> bool """
        return False

    def minimumSizeHint(self): # real signature unknown; restored from __doc__
        """ minimumSizeHint(self) -> QSize """
        pass

    def mouseMoveEvent(self, QMouseEvent): # real signature unknown; restored from __doc__
        """ mouseMoveEvent(self, QMouseEvent) """
        pass

    def nextCheckState(self): # real signature unknown; restored from __doc__
        """ nextCheckState(self) """
        pass

    def paintEvent(self, QPaintEvent): # real signature unknown; restored from __doc__
        """ paintEvent(self, QPaintEvent) """
        pass

    def setCheckState(self, Qt_CheckState): # real signature unknown; restored from __doc__
        """ setCheckState(self, Qt.CheckState) """
        pass

    def setTristate(self, on=True): # real signature unknown; restored from __doc__
        """ setTristate(self, on: bool = True) """
        pass

    def sizeHint(self): # real signature unknown; restored from __doc__
        """ sizeHint(self) -> QSize """
        pass

    def stateChanged(self, p_int): # real signature unknown; restored from __doc__
        """ stateChanged(self, int) [signal] """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


