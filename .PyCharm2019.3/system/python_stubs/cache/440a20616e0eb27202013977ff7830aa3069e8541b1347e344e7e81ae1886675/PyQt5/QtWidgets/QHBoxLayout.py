# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


from .QBoxLayout import QBoxLayout

class QHBoxLayout(QBoxLayout):
    """
    QHBoxLayout()
    QHBoxLayout(QWidget)
    """
    def __init__(self, QWidget=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


