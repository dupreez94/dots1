# encoding: utf-8
# module PyKF5.KConfigCore
# from /usr/lib/python3.8/site-packages/PyKF5/KConfigCore.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


# no functions
# classes

class ConversionCheck(__sip.simplewrapper):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KAuthorized(__sip.simplewrapper):
    # no doc
    def authorize(self, *args, **kwargs): # real signature unknown
        pass

    def authorizeAction(self, *args, **kwargs): # real signature unknown
        pass

    def authorizeControlModule(self, *args, **kwargs): # real signature unknown
        pass

    def authorizeControlModules(self, *args, **kwargs): # real signature unknown
        pass

    def authorizeKAction(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KConfigBase(__sip.wrapper):
    # no doc
    def accessMode(self, *args, **kwargs): # real signature unknown
        pass

    def deleteGroup(self, *args, **kwargs): # real signature unknown
        pass

    def group(self, *args, **kwargs): # real signature unknown
        pass

    def groupList(self, *args, **kwargs): # real signature unknown
        pass

    def hasGroup(self, *args, **kwargs): # real signature unknown
        pass

    def isGroupImmutable(self, *args, **kwargs): # real signature unknown
        pass

    def isImmutable(self, *args, **kwargs): # real signature unknown
        pass

    def markAsClean(self, *args, **kwargs): # real signature unknown
        pass

    def sync(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    AccessMode = None # (!) real value is "<class 'PyKF5.KConfigCore.KConfigBase.AccessMode'>"
    Global = 2
    Localized = 4
    NoAccess = 0
    Normal = 1
    Notify = 9
    Persistent = 1
    ReadOnly = 1
    ReadWrite = 2
    WriteConfigFlag = None # (!) real value is "<class 'PyKF5.KConfigCore.KConfigBase.WriteConfigFlag'>"
    WriteConfigFlags = None # (!) real value is "<class 'PyKF5.KConfigCore.KConfigBase.WriteConfigFlags'>"


class KConfig(KConfigBase):
    # no doc
    def accessMode(self, *args, **kwargs): # real signature unknown
        pass

    def addConfigSources(self, *args, **kwargs): # real signature unknown
        pass

    def additionalConfigSources(self, *args, **kwargs): # real signature unknown
        pass

    def checkUpdate(self, *args, **kwargs): # real signature unknown
        pass

    def copyTo(self, *args, **kwargs): # real signature unknown
        pass

    def deleteGroupImpl(self, *args, **kwargs): # real signature unknown
        pass

    def entryMap(self, *args, **kwargs): # real signature unknown
        pass

    def forceGlobal(self, *args, **kwargs): # real signature unknown
        pass

    def groupImpl(self, *args, **kwargs): # real signature unknown
        pass

    def groupList(self, *args, **kwargs): # real signature unknown
        pass

    def hasGroupImpl(self, *args, **kwargs): # real signature unknown
        pass

    def isConfigWritable(self, *args, **kwargs): # real signature unknown
        pass

    def isDirty(self, *args, **kwargs): # real signature unknown
        pass

    def isGroupImmutableImpl(self, *args, **kwargs): # real signature unknown
        pass

    def isImmutable(self, *args, **kwargs): # real signature unknown
        pass

    def locale(self, *args, **kwargs): # real signature unknown
        pass

    def locationType(self, *args, **kwargs): # real signature unknown
        pass

    def markAsClean(self, *args, **kwargs): # real signature unknown
        pass

    def name(self, *args, **kwargs): # real signature unknown
        pass

    def openFlags(self, *args, **kwargs): # real signature unknown
        pass

    def readDefaults(self, *args, **kwargs): # real signature unknown
        pass

    def reparseConfiguration(self, *args, **kwargs): # real signature unknown
        pass

    def setForceGlobal(self, *args, **kwargs): # real signature unknown
        pass

    def setLocale(self, *args, **kwargs): # real signature unknown
        pass

    def setMainConfigName(self, *args, **kwargs): # real signature unknown
        pass

    def setReadDefaults(self, *args, **kwargs): # real signature unknown
        pass

    def sync(self, *args, **kwargs): # real signature unknown
        pass

    def virtual_hook(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    CascadeConfig = 2
    FullConfig = 3
    IncludeGlobals = 1
    NoCascade = 1
    NoGlobals = 2
    OpenFlag = None # (!) real value is "<class 'PyKF5.KConfigCore.KConfig.OpenFlag'>"
    OpenFlags = None # (!) real value is "<class 'PyKF5.KConfigCore.KConfig.OpenFlags'>"
    SimpleConfig = 0


class KConfigGroup(KConfigBase):
    # no doc
    def accessMode(self, *args, **kwargs): # real signature unknown
        pass

    def changeGroup(self, *args, **kwargs): # real signature unknown
        pass

    def config(self, *args, **kwargs): # real signature unknown
        pass

    def copyTo(self, *args, **kwargs): # real signature unknown
        pass

    def deleteEntry(self, *args, **kwargs): # real signature unknown
        pass

    def deleteGroup(self, *args, **kwargs): # real signature unknown
        pass

    def deleteGroupImpl(self, *args, **kwargs): # real signature unknown
        pass

    def entryMap(self, *args, **kwargs): # real signature unknown
        pass

    def exists(self, *args, **kwargs): # real signature unknown
        pass

    def groupImpl(self, *args, **kwargs): # real signature unknown
        pass

    def groupList(self, *args, **kwargs): # real signature unknown
        pass

    def hasDefault(self, *args, **kwargs): # real signature unknown
        pass

    def hasGroupImpl(self, *args, **kwargs): # real signature unknown
        pass

    def hasKey(self, *args, **kwargs): # real signature unknown
        pass

    def isEntryImmutable(self, *args, **kwargs): # real signature unknown
        pass

    def isGroupImmutableImpl(self, *args, **kwargs): # real signature unknown
        pass

    def isImmutable(self, *args, **kwargs): # real signature unknown
        pass

    def isValid(self, *args, **kwargs): # real signature unknown
        pass

    def keyList(self, *args, **kwargs): # real signature unknown
        pass

    def markAsClean(self, *args, **kwargs): # real signature unknown
        pass

    def name(self, *args, **kwargs): # real signature unknown
        pass

    def readEntry(self, *args, **kwargs): # real signature unknown
        pass

    def readEntryUntranslated(self, *args, **kwargs): # real signature unknown
        pass

    def readPathEntry(self, *args, **kwargs): # real signature unknown
        pass

    def readXdgListEntry(self, *args, **kwargs): # real signature unknown
        pass

    def reparent(self, *args, **kwargs): # real signature unknown
        pass

    def revertToDefault(self, *args, **kwargs): # real signature unknown
        pass

    def sync(self, *args, **kwargs): # real signature unknown
        pass

    def virtual_hook(self, *args, **kwargs): # real signature unknown
        pass

    def writeEntry(self, *args, **kwargs): # real signature unknown
        pass

    def writePathEntry(self, *args, **kwargs): # real signature unknown
        pass

    def writeXdgListEntry(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KConfigSkeletonItem(__sip.wrapper):
    # no doc
    def group(self, *args, **kwargs): # real signature unknown
        pass

    def isDefault(self, *args, **kwargs): # real signature unknown
        pass

    def isEqual(self, *args, **kwargs): # real signature unknown
        pass

    def isImmutable(self, *args, **kwargs): # real signature unknown
        pass

    def isSaveNeeded(self, *args, **kwargs): # real signature unknown
        pass

    def key(self, *args, **kwargs): # real signature unknown
        pass

    def label(self, *args, **kwargs): # real signature unknown
        pass

    def maxValue(self, *args, **kwargs): # real signature unknown
        pass

    def minValue(self, *args, **kwargs): # real signature unknown
        pass

    def name(self, *args, **kwargs): # real signature unknown
        pass

    def property(self, *args, **kwargs): # real signature unknown
        pass

    def readConfig(self, *args, **kwargs): # real signature unknown
        pass

    def readDefault(self, *args, **kwargs): # real signature unknown
        pass

    def readImmutability(self, *args, **kwargs): # real signature unknown
        pass

    def setDefault(self, *args, **kwargs): # real signature unknown
        pass

    def setGroup(self, *args, **kwargs): # real signature unknown
        pass

    def setKey(self, *args, **kwargs): # real signature unknown
        pass

    def setLabel(self, *args, **kwargs): # real signature unknown
        pass

    def setName(self, *args, **kwargs): # real signature unknown
        pass

    def setProperty(self, *args, **kwargs): # real signature unknown
        pass

    def setToolTip(self, *args, **kwargs): # real signature unknown
        pass

    def setWhatsThis(self, *args, **kwargs): # real signature unknown
        pass

    def setWriteFlags(self, *args, **kwargs): # real signature unknown
        pass

    def swapDefault(self, *args, **kwargs): # real signature unknown
        pass

    def toolTip(self, *args, **kwargs): # real signature unknown
        pass

    def whatsThis(self, *args, **kwargs): # real signature unknown
        pass

    def writeConfig(self, *args, **kwargs): # real signature unknown
        pass

    def writeFlags(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KConfigWatcher(__PyQt5_QtCore.QObject):
    # no doc
    def configChanged(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KCoreConfigSkeleton(__PyQt5_QtCore.QObject):
    # no doc
    def addItem(self, *args, **kwargs): # real signature unknown
        pass

    def addItemBool(self, *args, **kwargs): # real signature unknown
        pass

    def addItemDateTime(self, *args, **kwargs): # real signature unknown
        pass

    def addItemDouble(self, *args, **kwargs): # real signature unknown
        pass

    def addItemInt(self, *args, **kwargs): # real signature unknown
        pass

    def addItemInt64(self, *args, **kwargs): # real signature unknown
        pass

    def addItemIntList(self, *args, **kwargs): # real signature unknown
        pass

    def addItemLongLong(self, *args, **kwargs): # real signature unknown
        pass

    def addItemPassword(self, *args, **kwargs): # real signature unknown
        pass

    def addItemPath(self, *args, **kwargs): # real signature unknown
        pass

    def addItemPoint(self, *args, **kwargs): # real signature unknown
        pass

    def addItemProperty(self, *args, **kwargs): # real signature unknown
        pass

    def addItemRect(self, *args, **kwargs): # real signature unknown
        pass

    def addItemSize(self, *args, **kwargs): # real signature unknown
        pass

    def addItemString(self, *args, **kwargs): # real signature unknown
        pass

    def addItemStringList(self, *args, **kwargs): # real signature unknown
        pass

    def addItemUInt(self, *args, **kwargs): # real signature unknown
        pass

    def addItemUInt64(self, *args, **kwargs): # real signature unknown
        pass

    def addItemULongLong(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def clearItems(self, *args, **kwargs): # real signature unknown
        pass

    def config(self, *args, **kwargs): # real signature unknown
        pass

    def configChanged(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def currentGroup(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def findItem(self, *args, **kwargs): # real signature unknown
        pass

    def isDefaults(self, *args, **kwargs): # real signature unknown
        pass

    def isImmutable(self, *args, **kwargs): # real signature unknown
        pass

    def isSaveNeeded(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def items(self, *args, **kwargs): # real signature unknown
        pass

    def load(self, *args, **kwargs): # real signature unknown
        pass

    def read(self, *args, **kwargs): # real signature unknown
        pass

    def readConfig(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def removeItem(self, *args, **kwargs): # real signature unknown
        pass

    def save(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setCurrentGroup(self, *args, **kwargs): # real signature unknown
        pass

    def setDefaults(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def useDefaults(self, *args, **kwargs): # real signature unknown
        pass

    def usrRead(self, *args, **kwargs): # real signature unknown
        pass

    def usrReadConfig(self, *args, **kwargs): # real signature unknown
        pass

    def usrSave(self, *args, **kwargs): # real signature unknown
        pass

    def usrSetDefaults(self, *args, **kwargs): # real signature unknown
        pass

    def usrUseDefaults(self, *args, **kwargs): # real signature unknown
        pass

    def usrWriteConfig(self, *args, **kwargs): # real signature unknown
        pass

    def writeConfig(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    ItemBool = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemBool'>"
    ItemDateTime = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemDateTime'>"
    ItemDouble = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemDouble'>"
    ItemEnum = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemEnum'>"
    ItemInt = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemInt'>"
    ItemIntList = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemIntList'>"
    ItemLongLong = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemLongLong'>"
    ItemPassword = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemPassword'>"
    ItemPath = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemPath'>"
    ItemPathList = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemPathList'>"
    ItemPoint = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemPoint'>"
    ItemProperty = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemProperty'>"
    ItemRect = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemRect'>"
    ItemSize = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemSize'>"
    ItemString = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemString'>"
    ItemStringList = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemStringList'>"
    ItemUInt = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemUInt'>"
    ItemULongLong = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemULongLong'>"
    ItemUrl = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemUrl'>"
    ItemUrlList = None # (!) real value is "<class 'PyKF5.KConfigCore.KCoreConfigSkeleton.ItemUrlList'>"


class KDesktopFile(KConfig):
    # no doc
    def actionGroup(self, *args, **kwargs): # real signature unknown
        pass

    def copyTo(self, *args, **kwargs): # real signature unknown
        pass

    def deleteGroupImpl(self, *args, **kwargs): # real signature unknown
        pass

    def desktopGroup(self, *args, **kwargs): # real signature unknown
        pass

    def fileName(self, *args, **kwargs): # real signature unknown
        pass

    def groupImpl(self, *args, **kwargs): # real signature unknown
        pass

    def hasActionGroup(self, *args, **kwargs): # real signature unknown
        pass

    def hasApplicationType(self, *args, **kwargs): # real signature unknown
        pass

    def hasDeviceType(self, *args, **kwargs): # real signature unknown
        pass

    def hasGroupImpl(self, *args, **kwargs): # real signature unknown
        pass

    def hasLinkType(self, *args, **kwargs): # real signature unknown
        pass

    def isAuthorizedDesktopFile(self, *args, **kwargs): # real signature unknown
        pass

    def isDesktopFile(self, *args, **kwargs): # real signature unknown
        pass

    def isGroupImmutableImpl(self, *args, **kwargs): # real signature unknown
        pass

    def locateLocal(self, *args, **kwargs): # real signature unknown
        pass

    def noDisplay(self, *args, **kwargs): # real signature unknown
        pass

    def readActions(self, *args, **kwargs): # real signature unknown
        pass

    def readComment(self, *args, **kwargs): # real signature unknown
        pass

    def readDevice(self, *args, **kwargs): # real signature unknown
        pass

    def readDocPath(self, *args, **kwargs): # real signature unknown
        pass

    def readGenericName(self, *args, **kwargs): # real signature unknown
        pass

    def readIcon(self, *args, **kwargs): # real signature unknown
        pass

    def readMimeTypes(self, *args, **kwargs): # real signature unknown
        pass

    def readName(self, *args, **kwargs): # real signature unknown
        pass

    def readPath(self, *args, **kwargs): # real signature unknown
        pass

    def readType(self, *args, **kwargs): # real signature unknown
        pass

    def readUrl(self, *args, **kwargs): # real signature unknown
        pass

    def resource(self, *args, **kwargs): # real signature unknown
        pass

    def sortOrder(self, *args, **kwargs): # real signature unknown
        pass

    def tryExec(self, *args, **kwargs): # real signature unknown
        pass

    def virtual_hook(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KEMailSettings(__sip.wrapper):
    # no doc
    def currentProfileName(self, *args, **kwargs): # real signature unknown
        pass

    def defaultProfileName(self, *args, **kwargs): # real signature unknown
        pass

    def getSetting(self, *args, **kwargs): # real signature unknown
        pass

    def profiles(self, *args, **kwargs): # real signature unknown
        pass

    def setDefault(self, *args, **kwargs): # real signature unknown
        pass

    def setProfile(self, *args, **kwargs): # real signature unknown
        pass

    def setSetting(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    ClientProgram = 0
    ClientTerminal = 1
    EmailAddress = 3
    Extension = None # (!) real value is "<class 'PyKF5.KConfigCore.KEMailSettings.Extension'>"
    InServer = 12
    InServerLogin = 13
    InServerMBXType = 16
    InServerPass = 14
    InServerTLS = 17
    InServerType = 15
    Organization = 5
    OTHER = 2
    OutServer = 6
    OutServerCommand = 10
    OutServerLogin = 7
    OutServerPass = 8
    OutServerTLS = 11
    OutServerType = 9
    POP3 = 0
    RealName = 2
    ReplyToAddress = 4
    Setting = None # (!) real value is "<class 'PyKF5.KConfigCore.KEMailSettings.Setting'>"
    SMTP = 1


class KPropertySkeletonItem(KConfigSkeletonItem):
    # no doc
    def isEqual(self, *args, **kwargs): # real signature unknown
        pass

    def property(self, *args, **kwargs): # real signature unknown
        pass

    def readConfig(self, *args, **kwargs): # real signature unknown
        pass

    def readDefault(self, *args, **kwargs): # real signature unknown
        pass

    def readImmutability(self, *args, **kwargs): # real signature unknown
        pass

    def setDefault(self, *args, **kwargs): # real signature unknown
        pass

    def setProperty(self, *args, **kwargs): # real signature unknown
        pass

    def swapDefault(self, *args, **kwargs): # real signature unknown
        pass

    def writeConfig(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KSharedConfig(KConfig):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass


# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>'

__spec__ = None # (!) real value is "ModuleSpec(name='PyKF5.KConfigCore', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>, origin='/usr/lib/python3.8/site-packages/PyKF5/KConfigCore.so')"

