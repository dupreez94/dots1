# encoding: utf-8
# module PyQt5.pyrcc
# from /usr/lib/python3.8/site-packages/PyQt5/pyrcc.abi3.so
# by generator 1.147
# no doc

# imports
import sip as __sip


# Variables with simple values

CONSTANT_COMPRESSLEVEL_DEFAULT = -1

CONSTANT_COMPRESSTHRESHOLD_DEFAULT = 70

# no functions
# classes

class RCCResourceLibrary(__sip.simplewrapper):
    """
    RCCResourceLibrary()
    RCCResourceLibrary(RCCResourceLibrary)
    """
    def dataFiles(self): # real signature unknown; restored from __doc__
        """ dataFiles(self) -> List[str] """
        return []

    def output(self, p_str): # real signature unknown; restored from __doc__
        """ output(self, str) -> bool """
        return False

    def readFiles(self): # real signature unknown; restored from __doc__
        """ readFiles(self) -> bool """
        return False

    def setCompressLevel(self, p_int): # real signature unknown; restored from __doc__
        """ setCompressLevel(self, int) """
        pass

    def setCompressThreshold(self, p_int): # real signature unknown; restored from __doc__
        """ setCompressThreshold(self, int) """
        pass

    def setInputFiles(self, Iterable, p_str=None): # real signature unknown; restored from __doc__
        """ setInputFiles(self, Iterable[str]) """
        pass

    def setResourceRoot(self, p_str): # real signature unknown; restored from __doc__
        """ setResourceRoot(self, str) """
        pass

    def setVerbose(self, bool): # real signature unknown; restored from __doc__
        """ setVerbose(self, bool) """
        pass

    def __init__(self, RCCResourceLibrary=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



# variables with complex values



