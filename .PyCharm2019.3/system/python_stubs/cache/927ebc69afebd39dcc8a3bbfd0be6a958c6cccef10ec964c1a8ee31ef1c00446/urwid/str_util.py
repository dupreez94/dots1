# encoding: utf-8
# module urwid.str_util
# from /usr/lib/python3.8/site-packages/urwid/str_util.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc
# no imports

# functions

def calc_text_pos(string, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    calc_text_pos(string/unicode text, int start_offs, int end_offs, int pref_col)
    -> (int pos, int actual_col)
    
    Calculate the closest position to the screen column pref_col in text
    where start_offs is the offset into text assumed to be screen column 0
    and end_offs is the end of the range to search.
    
    Returns (position, actual_col).
    
    text -- string or unicode text
    start_offs -- start offset
    end_offs -- end offset
    pref_col -- preferred column
    """
    pass

def calc_width(string, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    calc_width(string/unicode text, int start_off, int end_offs) -> int width
    
    Return the screen column width of text between start_offs and end_offs.
    
    text -- string or unicode text
    start_offs -- start offset
    end_offs -- end offset
    """
    pass

def decode_one(string_text, int_pos): # real signature unknown; restored from __doc__
    """
    decode_one(string text, int pos) -> (int ord, int nextpos)
    
    Return (ordinal at pos, next position) for UTF-8 encoded text.
    
    text -- string text
    pos -- position in text
    """
    pass

def decode_one_right(string_text, int_pos): # real signature unknown; restored from __doc__
    """
    decode_one_right(string text, int pos) -> (int ord, int nextpos)
    
    Return (ordinal at pos, next position) for UTF-8 encoded text.
    pos is assumed to be on the trailing byte of a utf-8 sequence.
    text -- text string 
    pos -- position in text
    """
    pass

def get_byte_encoding(): # real signature unknown; restored from __doc__
    """
    get_byte_encoding() -> string encoding
    
    Get byte encoding ('utf8', 'wide', or 'narrow').
    """
    return ""

def get_width(int_ord): # real signature unknown; restored from __doc__
    """
    get_width(int ord) -> int width
    
    Return the screen column width for unicode ordinal ord.
    
    ord -- ordinal
    """
    return 0

def is_wide_char(string, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    is_wide_char(string/unicode text, int offs) -> bool iswide
    
    Test if the character at offs within text is wide.
    
    text -- string or unicode text
    offs -- offset
    """
    pass

def move_next_char(string, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    move_next_char(string/unicode text, int start_offs, int end_offs) -> int pos
    
    Return the position of the character after start_offs.
    
    text -- string or unicode text
    start_offs -- start offset
    end_offs -- end offset
    """
    pass

def move_prev_char(string, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    move_prev_char(string/unicode text, int start_offs, int end_offs) -> int pos
    
    Return the position of the character before end_offs.
    
    text -- string or unicode text
    start_offs -- start offset
    end_offs -- end offset
    """
    pass

def set_byte_encoding(string_encoding): # real signature unknown; restored from __doc__
    """
    set_byte_encoding(string encoding) -> None
    
    Set byte encoding. 
    
    encoding -- one of 'utf8', 'wide', 'narrow'
    """
    pass

def within_double_byte(strint_text, int_line_start, int_pos): # real signature unknown; restored from __doc__
    """
    within_double_byte(strint text, int line_start, int pos) -> int withindb
    
    Return whether pos is within a double-byte encoded character.
    
    str -- string in question
    line_start -- offset of beginning of line (< pos)
    pos -- offset in question
    
    Return values:
    0 -- not within dbe char, or double_byte_encoding == False
    1 -- pos is on the 1st half of a dbe char
    2 -- pos is on the 2nd half of a dbe char
    """
    return 0

# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b18af3d0>'

__spec__ = None # (!) real value is "ModuleSpec(name='urwid.str_util', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b18af3d0>, origin='/usr/lib/python3.8/site-packages/urwid/str_util.cpython-38-x86_64-linux-gnu.so')"

