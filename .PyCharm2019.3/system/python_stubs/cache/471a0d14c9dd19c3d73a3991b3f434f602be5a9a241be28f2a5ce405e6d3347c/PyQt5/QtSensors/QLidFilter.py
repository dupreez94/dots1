# encoding: utf-8
# module PyQt5.QtSensors
# from /usr/lib/python3.8/site-packages/PyQt5/QtSensors.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QSensorFilter import QSensorFilter

class QLidFilter(QSensorFilter):
    """
    QLidFilter()
    QLidFilter(QLidFilter)
    """
    def filter(self, QLidReading): # real signature unknown; restored from __doc__
        """ filter(self, QLidReading) -> bool """
        return False

    def __init__(self, QLidFilter=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


