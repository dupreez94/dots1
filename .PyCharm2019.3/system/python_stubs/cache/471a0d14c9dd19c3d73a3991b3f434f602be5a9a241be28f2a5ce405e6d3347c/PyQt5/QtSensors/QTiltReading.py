# encoding: utf-8
# module PyQt5.QtSensors
# from /usr/lib/python3.8/site-packages/PyQt5/QtSensors.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QSensorReading import QSensorReading

class QTiltReading(QSensorReading):
    # no doc
    def setXRotation(self, p_float): # real signature unknown; restored from __doc__
        """ setXRotation(self, float) """
        pass

    def setYRotation(self, p_float): # real signature unknown; restored from __doc__
        """ setYRotation(self, float) """
        pass

    def xRotation(self): # real signature unknown; restored from __doc__
        """ xRotation(self) -> float """
        return 0.0

    def yRotation(self): # real signature unknown; restored from __doc__
        """ yRotation(self) -> float """
        return 0.0

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


