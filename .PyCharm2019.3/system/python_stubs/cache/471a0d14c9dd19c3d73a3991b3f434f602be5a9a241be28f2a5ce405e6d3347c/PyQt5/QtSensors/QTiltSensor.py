# encoding: utf-8
# module PyQt5.QtSensors
# from /usr/lib/python3.8/site-packages/PyQt5/QtSensors.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QSensor import QSensor

class QTiltSensor(QSensor):
    """ QTiltSensor(parent: QObject = None) """
    def calibrate(self): # real signature unknown; restored from __doc__
        """ calibrate(self) """
        pass

    def reading(self): # real signature unknown; restored from __doc__
        """ reading(self) -> QTiltReading """
        return QTiltReading

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


