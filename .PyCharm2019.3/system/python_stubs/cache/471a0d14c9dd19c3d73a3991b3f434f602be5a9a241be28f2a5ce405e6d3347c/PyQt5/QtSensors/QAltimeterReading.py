# encoding: utf-8
# module PyQt5.QtSensors
# from /usr/lib/python3.8/site-packages/PyQt5/QtSensors.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QSensorReading import QSensorReading

class QAltimeterReading(QSensorReading):
    # no doc
    def altitude(self): # real signature unknown; restored from __doc__
        """ altitude(self) -> float """
        return 0.0

    def setAltitude(self, p_float): # real signature unknown; restored from __doc__
        """ setAltitude(self, float) """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


