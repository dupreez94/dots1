# encoding: utf-8
# module PyQt5.QtQuick
# from /usr/lib/python3.8/site-packages/PyQt5/QtQuick.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml
import sip as __sip


class QQuickRenderControl(__PyQt5_QtCore.QObject):
    """ QQuickRenderControl(parent: QObject = None) """
    def grab(self): # real signature unknown; restored from __doc__
        """ grab(self) -> QImage """
        pass

    def initialize(self, QOpenGLContext): # real signature unknown; restored from __doc__
        """ initialize(self, QOpenGLContext) """
        pass

    def invalidate(self): # real signature unknown; restored from __doc__
        """ invalidate(self) """
        pass

    def polishItems(self): # real signature unknown; restored from __doc__
        """ polishItems(self) """
        pass

    def prepareThread(self, QThread): # real signature unknown; restored from __doc__
        """ prepareThread(self, QThread) """
        pass

    def render(self): # real signature unknown; restored from __doc__
        """ render(self) """
        pass

    def renderRequested(self): # real signature unknown; restored from __doc__
        """ renderRequested(self) [signal] """
        pass

    def renderWindow(self, QPoint): # real signature unknown; restored from __doc__
        """ renderWindow(self, QPoint) -> QWindow """
        pass

    def renderWindowFor(self, QQuickWindow, offset=None): # real signature unknown; restored from __doc__
        """ renderWindowFor(QQuickWindow, offset: QPoint = None) -> QWindow """
        pass

    def sceneChanged(self): # real signature unknown; restored from __doc__
        """ sceneChanged(self) [signal] """
        pass

    def sync(self): # real signature unknown; restored from __doc__
        """ sync(self) -> bool """
        return False

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


