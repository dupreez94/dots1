# encoding: utf-8
# module PyQt5.QtQuick
# from /usr/lib/python3.8/site-packages/PyQt5/QtQuick.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml
import sip as __sip


class QQuickItemGrabResult(__PyQt5_QtCore.QObject):
    # no doc
    def event(self, QEvent): # real signature unknown; restored from __doc__
        """ event(self, QEvent) -> bool """
        return False

    def image(self): # real signature unknown; restored from __doc__
        """ image(self) -> QImage """
        pass

    def ready(self): # real signature unknown; restored from __doc__
        """ ready(self) [signal] """
        pass

    def saveToFile(self, p_str): # real signature unknown; restored from __doc__
        """ saveToFile(self, str) -> bool """
        return False

    def url(self): # real signature unknown; restored from __doc__
        """ url(self) -> QUrl """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


