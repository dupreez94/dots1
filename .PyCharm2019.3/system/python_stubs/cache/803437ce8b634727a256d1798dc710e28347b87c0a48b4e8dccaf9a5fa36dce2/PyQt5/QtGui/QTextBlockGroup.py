# encoding: utf-8
# module PyQt5.QtGui
# from /usr/lib/python3.8/site-packages/PyQt5/QtGui.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QTextObject import QTextObject

class QTextBlockGroup(QTextObject):
    """ QTextBlockGroup(QTextDocument) """
    def blockFormatChanged(self, QTextBlock): # real signature unknown; restored from __doc__
        """ blockFormatChanged(self, QTextBlock) """
        pass

    def blockInserted(self, QTextBlock): # real signature unknown; restored from __doc__
        """ blockInserted(self, QTextBlock) """
        pass

    def blockList(self): # real signature unknown; restored from __doc__
        """ blockList(self) -> List[QTextBlock] """
        return []

    def blockRemoved(self, QTextBlock): # real signature unknown; restored from __doc__
        """ blockRemoved(self, QTextBlock) """
        pass

    def __init__(self, QTextDocument): # real signature unknown; restored from __doc__
        pass


