# encoding: utf-8
# module PyQt5.QtGui
# from /usr/lib/python3.8/site-packages/PyQt5/QtGui.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QTextObject import QTextObject

class QTextFrame(QTextObject):
    """ QTextFrame(QTextDocument) """
    def begin(self): # real signature unknown; restored from __doc__
        """ begin(self) -> QTextFrame.iterator """
        pass

    def childFrames(self): # real signature unknown; restored from __doc__
        """ childFrames(self) -> List[QTextFrame] """
        return []

    def end(self): # real signature unknown; restored from __doc__
        """ end(self) -> QTextFrame.iterator """
        pass

    def firstCursorPosition(self): # real signature unknown; restored from __doc__
        """ firstCursorPosition(self) -> QTextCursor """
        return QTextCursor

    def firstPosition(self): # real signature unknown; restored from __doc__
        """ firstPosition(self) -> int """
        return 0

    def frameFormat(self): # real signature unknown; restored from __doc__
        """ frameFormat(self) -> QTextFrameFormat """
        return QTextFrameFormat

    def lastCursorPosition(self): # real signature unknown; restored from __doc__
        """ lastCursorPosition(self) -> QTextCursor """
        return QTextCursor

    def lastPosition(self): # real signature unknown; restored from __doc__
        """ lastPosition(self) -> int """
        return 0

    def parentFrame(self): # real signature unknown; restored from __doc__
        """ parentFrame(self) -> QTextFrame """
        return QTextFrame

    def setFrameFormat(self, QTextFrameFormat): # real signature unknown; restored from __doc__
        """ setFrameFormat(self, QTextFrameFormat) """
        pass

    def __init__(self, QTextDocument): # real signature unknown; restored from __doc__
        pass



