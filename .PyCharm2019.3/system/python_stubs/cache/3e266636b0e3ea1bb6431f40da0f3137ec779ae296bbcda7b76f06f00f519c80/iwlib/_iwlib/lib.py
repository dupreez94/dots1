# encoding: utf-8
# module iwlib._iwlib.lib
# from /usr/lib/python3.8/site-packages/iwlib/_iwlib.abi3.so
# by generator 1.147
# no doc

# imports
from _cffi_backend import ioctl, iw_operation_mode


# Variables with simple values

IFNAMSIZ = 16

IW_ENCODE_DISABLED = 32768

IW_ENCODING_TOKEN_MAX = 64

IW_ESSID_MAX_SIZE = 32

IW_MODE_ADHOC = 1
IW_MODE_AUTO = 0
IW_MODE_INFRA = 2
IW_MODE_MASTER = 3
IW_MODE_MONITOR = 6
IW_MODE_REPEAT = 4
IW_MODE_SECOND = 5

IW_NUM_OPER_MODE = 7

SIOCGIFFLAGS = 35091
SIOCGIWAP = 35605
SIOCGIWAPLIST = 35607
SIOCGIWAUTH = 35635
SIOCGIWENCODE = 35627
SIOCGIWENCODEEXT = 35637
SIOCGIWESSID = 35611
SIOCGIWFRAG = 35621
SIOCGIWFREQ = 35589
SIOCGIWGENIE = 35633
SIOCGIWMODE = 35591
SIOCGIWMODUL = 35631
SIOCGIWNAME = 35585
SIOCGIWNICKN = 35613
SIOCGIWNWID = 35587
SIOCGIWPOWER = 35629
SIOCGIWPRIV = 35597
SIOCGIWRANGE = 35595
SIOCGIWRATE = 35617
SIOCGIWRETRY = 35625
SIOCGIWRTS = 35619
SIOCGIWSCAN = 35609
SIOCGIWSENS = 35593
SIOCGIWSPY = 35601
SIOCGIWSTATS = 35599
SIOCGIWTHRSPY = 35603
SIOCGIWTXPOW = 35623
SIOCIWFIRSTPRIV = 35808
SIOCIWLASTPRIV = 35839
SIOCSIWAP = 35604
SIOCSIWAUTH = 35634
SIOCSIWCOMMIT = 35584
SIOCSIWENCODE = 35626
SIOCSIWENCODEEXT = 35636
SIOCSIWESSID = 35610
SIOCSIWFRAG = 35620
SIOCSIWFREQ = 35588
SIOCSIWGENIE = 35632
SIOCSIWMLME = 35606
SIOCSIWMODE = 35590
SIOCSIWMODUL = 35630
SIOCSIWNICKN = 35612
SIOCSIWNWID = 35586
SIOCSIWPMKSA = 35638
SIOCSIWPOWER = 35628
SIOCSIWPRIV = 35596
SIOCSIWRANGE = 35594
SIOCSIWRATE = 35616
SIOCSIWRETRY = 35624
SIOCSIWRTS = 35618
SIOCSIWSCAN = 35608
SIOCSIWSENS = 35592
SIOCSIWSPY = 35600
SIOCSIWSTATS = 35598
SIOCSIWTHRSPY = 35602
SIOCSIWTXPOW = 35622

# functions

def iw_ether_ntop(struct_ether_addr, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void iw_ether_ntop(struct ether_addr *, char *);
    
    CFFI C function from iwlib._iwlib.lib
    """
    pass

def iw_freq2float(struct_iw_freq, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    double iw_freq2float(struct iw_freq *);
    
    CFFI C function from iwlib._iwlib.lib
    """
    pass

def iw_get_ext(p_int, char, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int iw_get_ext(int, char *, int, struct iwreq *);
    
    CFFI C function from iwlib._iwlib.lib
    """
    pass

def iw_get_kernel_we_version(): # real signature unknown; restored from __doc__
    """
    int iw_get_kernel_we_version();
    
    CFFI C function from iwlib._iwlib.lib
    """
    return 0

def iw_get_range_info(p_int, char, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int iw_get_range_info(int, char *, struct iw_range *);
    
    CFFI C function from iwlib._iwlib.lib
    """
    pass

def iw_get_stats(p_int, char, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int iw_get_stats(int, char *, struct iw_statistics *, struct iw_range *, int);
    
    CFFI C function from iwlib._iwlib.lib
    """
    pass

def iw_print_bitrate(char, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void iw_print_bitrate(char *, int, int);
    
    CFFI C function from iwlib._iwlib.lib
    """
    pass

def iw_print_freq_value(char, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void iw_print_freq_value(char *, int, double);
    
    CFFI C function from iwlib._iwlib.lib
    """
    pass

def iw_scan(p_int, char, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int iw_scan(int, char *, int, struct wireless_scan_head *);
    
    CFFI C function from iwlib._iwlib.lib
    """
    pass

def iw_set_ext(p_int, char, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int iw_set_ext(int, char *, int, struct iwreq *);
    
    CFFI C function from iwlib._iwlib.lib
    """
    pass

def iw_sockets_close(p_int): # real signature unknown; restored from __doc__
    """
    void iw_sockets_close(int);
    
    CFFI C function from iwlib._iwlib.lib
    """
    pass

def iw_sockets_open(): # real signature unknown; restored from __doc__
    """
    int iw_sockets_open();
    
    CFFI C function from iwlib._iwlib.lib
    """
    return 0

# no classes
