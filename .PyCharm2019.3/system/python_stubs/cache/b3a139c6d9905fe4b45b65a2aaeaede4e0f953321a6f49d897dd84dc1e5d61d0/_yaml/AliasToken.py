# encoding: utf-8
# module _yaml
# from /usr/lib/python3.8/site-packages/_yaml.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import builtins as __builtins__ # <module 'builtins' (built-in)>
import yaml as yaml # /usr/lib/python3.8/site-packages/yaml/__init__.py
import yaml.error as __yaml_error
import yaml.events as __yaml_events
import yaml.nodes as __yaml_nodes
import yaml.tokens as __yaml_tokens


class AliasToken(__yaml_tokens.Token):
    # no doc
    def __init__(self, value, start_mark, end_mark): # reliably restored by inspect
        # no doc
        pass

    id = '<alias>'


