# encoding: utf-8
# module pygit2._libgit2.lib
# from /usr/lib/python3.8/site-packages/pygit2/_libgit2.abi3.so
# by generator 1.147
# no doc
# no imports

# Variables with simple values

GIT_ATTR_CHECK_FILE_THEN_INDEX = 0

GIT_ATTR_CHECK_INDEX_ONLY = 2

GIT_ATTR_CHECK_INDEX_THEN_FILE = 1

GIT_ATTR_CHECK_NO_SYSTEM = 4

GIT_ATTR_FALSE_T = 2

GIT_ATTR_TRUE_T = 1

GIT_ATTR_UNSPECIFIED_T = 0

GIT_ATTR_VALUE_T = 3

GIT_BLAME_OPTIONS_VERSION = 1

GIT_CERT_HOSTKEY_LIBSSH2 = 2

GIT_CERT_NONE = 0

GIT_CERT_SSH_MD5 = 1
GIT_CERT_SSH_SHA1 = 2

GIT_CERT_STRARRAY = 3
GIT_CERT_X509 = 1

GIT_CHECKOUT_NOTIFY_ALL = 65535
GIT_CHECKOUT_NOTIFY_CONFLICT = 1
GIT_CHECKOUT_NOTIFY_DIRTY = 2
GIT_CHECKOUT_NOTIFY_IGNORED = 16
GIT_CHECKOUT_NOTIFY_NONE = 0
GIT_CHECKOUT_NOTIFY_UNTRACKED = 8
GIT_CHECKOUT_NOTIFY_UPDATED = 4

GIT_CLONE_LOCAL = 1

GIT_CLONE_LOCAL_AUTO = 0

GIT_CLONE_LOCAL_NO_LINKS = 3

GIT_CLONE_NO_LOCAL = 2

GIT_CLONE_OPTIONS_VERSION = 1

GIT_CONFIG_HIGHEST_LEVEL = -1

GIT_CONFIG_LEVEL_APP = 6
GIT_CONFIG_LEVEL_GLOBAL = 4
GIT_CONFIG_LEVEL_LOCAL = 5
GIT_CONFIG_LEVEL_PROGRAMDATA = 1
GIT_CONFIG_LEVEL_SYSTEM = 2
GIT_CONFIG_LEVEL_XDG = 3

GIT_CREDTYPE_DEFAULT = 8

GIT_CREDTYPE_SSH_CUSTOM = 4
GIT_CREDTYPE_SSH_INTERACTIVE = 16
GIT_CREDTYPE_SSH_KEY = 2
GIT_CREDTYPE_SSH_MEMORY = 64

GIT_CREDTYPE_USERNAME = 32

GIT_CREDTYPE_USERPASS_PLAINTEXT = 1

GIT_DELTA_ADDED = 1
GIT_DELTA_CONFLICTED = 10
GIT_DELTA_COPIED = 5
GIT_DELTA_DELETED = 2
GIT_DELTA_IGNORED = 6
GIT_DELTA_MODIFIED = 3
GIT_DELTA_RENAMED = 4
GIT_DELTA_TYPECHANGE = 8
GIT_DELTA_UNMODIFIED = 0
GIT_DELTA_UNREADABLE = 9
GIT_DELTA_UNTRACKED = 7

GIT_DESCRIBE_ALL = 2
GIT_DESCRIBE_DEFAULT = 0

GIT_DESCRIBE_FORMAT_OPTIONS_VERSION = 1

GIT_DESCRIBE_OPTIONS_VERSION = 1

GIT_DESCRIBE_TAGS = 1

GIT_DIRECTION_FETCH = 0
GIT_DIRECTION_PUSH = 1

GIT_EAMBIGUOUS = -5
GIT_EAPPLIED = -18
GIT_EAPPLYFAIL = -35
GIT_EAUTH = -16
GIT_EBAREREPO = -8
GIT_EBUFS = -6
GIT_ECERTIFICATE = -17
GIT_ECONFLICT = -13
GIT_EDIRECTORY = -23
GIT_EEOF = -20
GIT_EEXISTS = -4
GIT_EINDEXDIRTY = -34
GIT_EINVALID = -21
GIT_EINVALIDSPEC = -12
GIT_ELOCKED = -14
GIT_EMERGECONFLICT = -24
GIT_EMISMATCH = -33
GIT_EMODIFIED = -15
GIT_ENONFASTFORWARD = -11
GIT_ENOTFOUND = -3
GIT_EPEEL = -19
GIT_ERROR = -1
GIT_EUNBORNBRANCH = -9
GIT_EUNCOMMITTED = -22
GIT_EUNMERGED = -10
GIT_EUSER = -7

GIT_FEATURE_HTTPS = 2
GIT_FEATURE_NSEC = 8
GIT_FEATURE_SSH = 4
GIT_FEATURE_THREADS = 1

GIT_FETCH_NO_PRUNE = 2

GIT_FETCH_OPTIONS_VERSION = 1

GIT_FETCH_PRUNE = 1

GIT_FETCH_PRUNE_UNSPECIFIED = 0

GIT_ITEROVER = -31

GIT_MERGE_FAIL_ON_CONFLICT = 2

GIT_MERGE_FILE_DEFAULT = 0

GIT_MERGE_FILE_DIFF_MINIMAL = 128
GIT_MERGE_FILE_DIFF_PATIENCE = 64

GIT_MERGE_FILE_FAVOR_NORMAL = 0
GIT_MERGE_FILE_FAVOR_OURS = 1
GIT_MERGE_FILE_FAVOR_THEIRS = 2
GIT_MERGE_FILE_FAVOR_UNION = 3

GIT_MERGE_FILE_IGNORE_WHITESPACE = 8

GIT_MERGE_FILE_IGNORE_WHITESPACE_CHANGE = 16
GIT_MERGE_FILE_IGNORE_WHITESPACE_EOL = 32

GIT_MERGE_FILE_SIMPLIFY_ALNUM = 4

GIT_MERGE_FILE_STYLE_DIFF3 = 2
GIT_MERGE_FILE_STYLE_MERGE = 1

GIT_MERGE_FIND_RENAMES = 1

GIT_MERGE_NO_RECURSIVE = 8

GIT_MERGE_OPTIONS_VERSION = 1

GIT_MERGE_SKIP_REUC = 4

GIT_OK = 0
GIT_PASSTHROUGH = -30

GIT_PATH_MAX = 4096

GIT_PROXY_AUTO = 1
GIT_PROXY_NONE = 0

GIT_PROXY_OPTIONS_VERSION = 1

GIT_PROXY_SPECIFIED = 2

GIT_PUSH_OPTIONS_VERSION = 1

GIT_REFERENCE_ALL = 3
GIT_REFERENCE_DIRECT = 1
GIT_REFERENCE_INVALID = 0
GIT_REFERENCE_SYMBOLIC = 2

GIT_REMOTE_CALLBACKS_VERSION = 1

GIT_REMOTE_COMPLETION_DOWNLOAD = 0
GIT_REMOTE_COMPLETION_ERROR = 2
GIT_REMOTE_COMPLETION_INDEXING = 1

GIT_REMOTE_DOWNLOAD_TAGS_ALL = 3
GIT_REMOTE_DOWNLOAD_TAGS_AUTO = 1
GIT_REMOTE_DOWNLOAD_TAGS_NONE = 2
GIT_REMOTE_DOWNLOAD_TAGS_UNSPECIFIED = 0

GIT_REPOSITORY_INIT_BARE = 1

GIT_REPOSITORY_INIT_EXTERNAL_TEMPLATE = 32

GIT_REPOSITORY_INIT_MKDIR = 8
GIT_REPOSITORY_INIT_MKPATH = 16

GIT_REPOSITORY_INIT_NO_DOTGIT_DIR = 4

GIT_REPOSITORY_INIT_NO_REINIT = 2

GIT_REPOSITORY_INIT_OPTIONS_VERSION = 1

GIT_REPOSITORY_INIT_RELATIVE_GITLINK = 64

GIT_REPOSITORY_INIT_SHARED_ALL = 1535
GIT_REPOSITORY_INIT_SHARED_GROUP = 1533
GIT_REPOSITORY_INIT_SHARED_UMASK = 0

GIT_RETRY = -32

GIT_STASH_APPLY_DEFAULT = 0

GIT_STASH_APPLY_OPTIONS_VERSION = 1

GIT_STASH_APPLY_PROGRESS_ANALYZE_INDEX = 2
GIT_STASH_APPLY_PROGRESS_ANALYZE_MODIFIED = 3
GIT_STASH_APPLY_PROGRESS_ANALYZE_UNTRACKED = 4

GIT_STASH_APPLY_PROGRESS_CHECKOUT_MODIFIED = 6
GIT_STASH_APPLY_PROGRESS_CHECKOUT_UNTRACKED = 5

GIT_STASH_APPLY_PROGRESS_DONE = 7

GIT_STASH_APPLY_PROGRESS_LOADING_STASH = 1

GIT_STASH_APPLY_PROGRESS_NONE = 0

GIT_STASH_APPLY_REINSTATE_INDEX = 1

GIT_STASH_DEFAULT = 0

GIT_STASH_INCLUDE_IGNORED = 4
GIT_STASH_INCLUDE_UNTRACKED = 2

GIT_STASH_KEEP_INDEX = 1

GIT_SUBMODULE_IGNORE_ALL = 4
GIT_SUBMODULE_IGNORE_DIRTY = 3
GIT_SUBMODULE_IGNORE_NONE = 1
GIT_SUBMODULE_IGNORE_UNSPECIFIED = -1
GIT_SUBMODULE_IGNORE_UNTRACKED = 2

GIT_SUBMODULE_UPDATE_OPTIONS_VERSION = 1

# functions

def git_attr_get(char, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_attr_get(char * *, struct git_repository *, uint32_t, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_attr_value(char, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    git_attr_t git_attr_value(char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_blame_file(struct_git_blame, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_blame_file(struct git_blame * *, struct git_repository *, char *, struct git_blame_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_blame_free(struct_git_blame, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_blame_free(struct git_blame *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_blame_get_hunk_byindex(struct_git_blame, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    struct git_blame_hunk *git_blame_get_hunk_byindex(struct git_blame *, uint32_t);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_blame_get_hunk_byline(struct_git_blame, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    struct git_blame_hunk *git_blame_get_hunk_byline(struct git_blame *, size_t);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_blame_get_hunk_count(struct_git_blame, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    uint32_t git_blame_get_hunk_count(struct git_blame *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_blame_init_options(struct_git_blame_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_blame_init_options(struct git_blame_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_buf_dispose(git_buf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_buf_dispose(git_buf *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_checkout_head(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_checkout_head(struct git_repository *, struct git_checkout_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_checkout_index(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_checkout_index(struct git_repository *, struct git_index *, struct git_checkout_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_checkout_init_options(struct_git_checkout_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_checkout_init_options(struct git_checkout_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_checkout_tree(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_checkout_tree(struct git_repository *, struct git_object *, struct git_checkout_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_clone(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_clone(struct git_repository * *, char *, char *, struct git_clone_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_clone_init_options(struct_git_clone_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_clone_init_options(struct git_clone_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_add_file_ondisk(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_add_file_ondisk(struct git_config *, char *, git_config_level_t, struct git_repository *, int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_delete_entry(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_delete_entry(struct git_config *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_entry_free(struct_git_config_entry, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_config_entry_free(struct git_config_entry *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_find_global(git_buf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_find_global(git_buf *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_find_system(git_buf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_find_system(git_buf *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_find_xdg(git_buf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_find_xdg(git_buf *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_free(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_config_free(struct git_config *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_get_entry(struct_git_config_entry, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_get_entry(struct git_config_entry * *, struct git_config *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_get_string(char, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_get_string(char * *, struct git_config *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_iterator_free(struct_git_config_iterator, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_config_iterator_free(struct git_config_iterator *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_iterator_new(struct_git_config_iterator, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_iterator_new(struct git_config_iterator * *, struct git_config *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_multivar_iterator_new(struct_git_config_iterator, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_multivar_iterator_new(struct git_config_iterator * *, struct git_config *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_new(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_new(struct git_config * *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_next(struct_git_config_entry, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_next(struct git_config_entry * *, struct git_config_iterator *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_open_ondisk(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_open_ondisk(struct git_config * *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_parse_bool(p_int, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_parse_bool(int *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_parse_int64(int64_t, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_parse_int64(int64_t *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_set_bool(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_set_bool(struct git_config *, char *, int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_set_int64(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_set_int64(struct git_config *, char *, int64_t);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_set_multivar(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_set_multivar(struct git_config *, char *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_set_string(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_set_string(struct git_config *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_config_snapshot(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_config_snapshot(struct git_config * *, struct git_config *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_cred_ssh_key_from_agent(struct_git_cred, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_cred_ssh_key_from_agent(struct git_cred * *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_cred_ssh_key_memory_new(struct_git_cred, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_cred_ssh_key_memory_new(struct git_cred * *, char *, char *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_cred_ssh_key_new(struct_git_cred, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_cred_ssh_key_new(struct git_cred * *, char *, char *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_cred_username_new(struct_git_cred, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_cred_username_new(struct git_cred * *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_cred_userpass_plaintext_new(struct_git_cred, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_cred_userpass_plaintext_new(struct git_cred * *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_describe_commit(struct_git_describe_result, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_describe_commit(struct git_describe_result * *, struct git_object *, struct git_describe_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_describe_format(git_buf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_describe_format(git_buf *, struct git_describe_result *, git_describe_format_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_describe_init_format_options(git_describe_format_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_describe_init_format_options(git_describe_format_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_describe_init_options(struct_git_describe_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_describe_init_options(struct git_describe_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_describe_result_free(struct_git_describe_result, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_describe_result_free(struct git_describe_result *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_describe_workdir(struct_git_describe_result, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_describe_workdir(struct git_describe_result * *, struct git_repository *, struct git_describe_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_diff_index_to_workdir(struct_git_diff, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_diff_index_to_workdir(struct git_diff * *, struct git_repository *, struct git_index *, git_diff_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_diff_init_options(git_diff_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_diff_init_options(git_diff_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_diff_tree_to_index(struct_git_diff, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_diff_tree_to_index(struct git_diff * *, struct git_repository *, struct git_tree *, struct git_index *, git_diff_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_error_last(): # real signature unknown; restored from __doc__
    """
    git_error *git_error_last();
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_fetch_init_options(git_fetch_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_fetch_init_options(git_fetch_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_graph_ahead_behind(size_t, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_graph_ahead_behind(size_t *, size_t *, struct git_repository *, struct git_oid *, struct git_oid *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_add(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_add(struct git_index *, struct git_index_entry *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_add_all(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_add_all(struct git_index *, struct git_strarray *, unsigned int, int(*)(char *, char *, void *), void *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_add_bypath(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_add_bypath(struct git_index *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_clear(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_clear(struct git_index *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_conflict_get(struct_git_index_entry, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_conflict_get(struct git_index_entry * *, struct git_index_entry * *, struct git_index_entry * *, struct git_index *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_conflict_iterator_free(struct_git_index_conflict_iterator, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_index_conflict_iterator_free(struct git_index_conflict_iterator *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_conflict_iterator_new(struct_git_index_conflict_iterator, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_conflict_iterator_new(struct git_index_conflict_iterator * *, struct git_index *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_conflict_next(struct_git_index_entry, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_conflict_next(struct git_index_entry * *, struct git_index_entry * *, struct git_index_entry * *, struct git_index_conflict_iterator *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_conflict_remove(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_conflict_remove(struct git_index *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_entrycount(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    size_t git_index_entrycount(struct git_index *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_find(size_t, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_find(size_t *, struct git_index *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_free(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_index_free(struct git_index *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_get_byindex(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    struct git_index_entry *git_index_get_byindex(struct git_index *, size_t);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_get_bypath(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    struct git_index_entry *git_index_get_bypath(struct git_index *, char *, int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_has_conflicts(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_has_conflicts(struct git_index *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_open(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_open(struct git_index * *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_read(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_read(struct git_index *, int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_read_tree(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_read_tree(struct git_index *, struct git_tree *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_remove(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_remove(struct git_index *, char *, int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_write(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_write(struct git_index *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_write_tree(struct_git_oid, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_write_tree(struct git_oid *, struct git_index *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_index_write_tree_to(struct_git_oid, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_index_write_tree_to(struct git_oid *, struct git_index *, struct git_repository *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_libgit2_features(): # real signature unknown; restored from __doc__
    """
    int git_libgit2_features();
    
    CFFI C function from pygit2._libgit2.lib
    """
    return 0

def git_merge_commits(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_merge_commits(struct git_index * *, struct git_repository *, struct git_commit *, struct git_commit *, git_merge_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_merge_file_from_index(git_merge_file_result, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_merge_file_from_index(git_merge_file_result *, struct git_repository *, struct git_index_entry *, struct git_index_entry *, struct git_index_entry *, git_merge_file_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_merge_file_result_free(git_merge_file_result, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_merge_file_result_free(git_merge_file_result *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_merge_init_options(git_merge_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_merge_init_options(git_merge_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_merge_trees(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_merge_trees(struct git_index * *, struct git_repository *, struct git_tree *, struct git_tree *, struct git_tree *, git_merge_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_proxy_init_options(git_proxy_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_proxy_init_options(git_proxy_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_push_init_options(git_push_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_push_init_options(git_push_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_refspec_direction(struct_git_refspec, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    git_direction git_refspec_direction(struct git_refspec *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_refspec_dst(struct_git_refspec, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    char *git_refspec_dst(struct git_refspec *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_refspec_dst_matches(struct_git_refspec, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_refspec_dst_matches(struct git_refspec *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_refspec_force(struct_git_refspec, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_refspec_force(struct git_refspec *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_refspec_rtransform(git_buf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_refspec_rtransform(git_buf *, struct git_refspec *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_refspec_src(struct_git_refspec, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    char *git_refspec_src(struct git_refspec *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_refspec_src_matches(struct_git_refspec, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_refspec_src_matches(struct git_refspec *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_refspec_string(struct_git_refspec, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    char *git_refspec_string(struct git_refspec *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_refspec_transform(git_buf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_refspec_transform(git_buf *, struct git_refspec *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_add_fetch(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_add_fetch(struct git_repository *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_add_push(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_add_push(struct git_repository *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_create(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_create(struct git_remote * *, struct git_repository *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_create_with_fetchspec(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_create_with_fetchspec(struct git_remote * *, struct git_repository *, char *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_delete(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_delete(struct git_repository *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_fetch(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_fetch(struct git_remote *, struct git_strarray *, git_fetch_options *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_free(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_remote_free(struct git_remote *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_get_fetch_refspecs(struct_git_strarray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_get_fetch_refspecs(struct git_strarray *, struct git_remote *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_get_push_refspecs(struct_git_strarray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_get_push_refspecs(struct git_strarray *, struct git_remote *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_get_refspec(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    struct git_refspec *git_remote_get_refspec(struct git_remote *, size_t);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_init_callbacks(struct_git_remote_callbacks, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_init_callbacks(struct git_remote_callbacks *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_list(struct_git_strarray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_list(struct git_strarray *, struct git_repository *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_lookup(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_lookup(struct git_remote * *, struct git_repository *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_name(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    char *git_remote_name(struct git_remote *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_prune(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_prune(struct git_remote *, struct git_remote_callbacks *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_push(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_push(struct git_remote *, struct git_strarray *, git_push_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_pushurl(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    char *git_remote_pushurl(struct git_remote *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_refspec_count(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    size_t git_remote_refspec_count(struct git_remote *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_rename(struct_git_strarray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_rename(struct git_strarray *, struct git_repository *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_set_pushurl(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_set_pushurl(struct git_repository *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_set_url(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_remote_set_url(struct git_repository *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_stats(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    struct git_transfer_progress *git_remote_stats(struct git_remote *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_remote_url(struct_git_remote, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    char *git_remote_url(struct git_remote *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_config(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_repository_config(struct git_config * *, struct git_repository *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_config_snapshot(struct_git_config, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_repository_config_snapshot(struct git_config * *, struct git_repository *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_free(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_repository_free(struct git_repository *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_ident(char, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_repository_ident(char * *, char * *, struct git_repository *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_index(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_repository_index(struct git_index * *, struct git_repository *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_init(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_repository_init(struct git_repository * *, char *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_init_ext(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_repository_init_ext(struct git_repository * *, char *, git_repository_init_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_init_init_options(git_repository_init_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_repository_init_init_options(git_repository_init_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_set_head(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_repository_set_head(struct git_repository *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_set_head_detached(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_repository_set_head_detached(struct git_repository *, struct git_oid *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_set_ident(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_repository_set_ident(struct git_repository *, char *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_repository_state_cleanup(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_repository_state_cleanup(struct git_repository *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_revert_commit(struct_git_index, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_revert_commit(struct git_index * *, struct git_repository *, struct git_commit *, struct git_commit *, unsigned int, git_merge_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_stash_apply(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_stash_apply(struct git_repository *, size_t, struct git_stash_apply_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_stash_apply_init_options(struct_git_stash_apply_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_stash_apply_init_options(struct git_stash_apply_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_stash_drop(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_stash_drop(struct git_repository *, size_t);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_stash_foreach(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_stash_foreach(struct git_repository *, int(*)(size_t, char *, struct git_oid *, void *), void *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_stash_pop(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_stash_pop(struct git_repository *, size_t, struct git_stash_apply_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_stash_save(struct_git_oid, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_stash_save(struct git_oid *, struct git_repository *, struct git_signature *, char *, uint32_t);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_strarray_free(struct_git_strarray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_strarray_free(struct git_strarray *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_submodule_branch(struct_git_submodule, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    char *git_submodule_branch(struct git_submodule *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_submodule_free(struct_git_submodule, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    void git_submodule_free(struct git_submodule *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_submodule_head_id(struct_git_submodule, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    struct git_oid *git_submodule_head_id(struct git_submodule *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_submodule_lookup(struct_git_submodule, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_submodule_lookup(struct git_submodule * *, struct git_repository *, char *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_submodule_name(struct_git_submodule, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    char *git_submodule_name(struct git_submodule *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_submodule_open(struct_git_repository, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_submodule_open(struct git_repository * *, struct git_submodule *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_submodule_path(struct_git_submodule, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    char *git_submodule_path(struct git_submodule *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_submodule_update(struct_git_submodule, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_submodule_update(struct git_submodule *, int, struct git_submodule_update_options *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_submodule_update_init_options(struct_git_submodule_update_options, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    int git_submodule_update_init_options(struct git_submodule_update_options *, unsigned int);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

def git_submodule_url(struct_git_submodule, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
    """
    char *git_submodule_url(struct git_submodule *);
    
    CFFI C function from pygit2._libgit2.lib
    """
    pass

# no classes
