# encoding: utf-8
# module ixion
# from /usr/lib/python3.8/site-packages/ixion.so
# by generator 1.147
# no doc
# no imports

# functions

def column_label(*args, **kwargs): # real signature unknown
    """ Return a list of column label strings based on specified column range values. """
    pass

def info(*args, **kwargs): # real signature unknown
    """ Print ixion module information. """
    pass

# classes

class Document(object):
    """ ixion document object """
    def append_sheet(self, *args, **kwargs): # real signature unknown
        """ append new sheet to the document """
        pass

    def calculate(self, threads=None): # real signature unknown; restored from __doc__
        """
        Document.calculate([threads])
        
        (Re-)calculate all modified formula cells in the document.
        
        Keyword arguments:
        
        threads -- number of threads to use besides the main thread, or 0 if all
                   calculations are to be performed on the main thread. (default 0)
        """
        pass

    def get_sheet(self, *args, **kwargs): # real signature unknown
        """ get a sheet object either by index or name """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    sheet_names = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """A tuple of sheet names"""



class DocumentError(Exception):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class FormulaError(Exception):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class Sheet(object):
    """ ixion sheet object """
    def erase_cell(self, *args, **kwargs): # real signature unknown
        """ erase cell at specified position """
        pass

    def get_formula_expression(self, *args, **kwargs): # real signature unknown
        """ get formula expression string from specified cell position """
        pass

    def get_numeric_value(self, *args, **kwargs): # real signature unknown
        """ get numeric value from specified cell """
        pass

    def get_string_value(self, *args, **kwargs): # real signature unknown
        """ get string value from specified cell """
        pass

    def set_formula_cell(self, *args, **kwargs): # real signature unknown
        """ set formula to specified cell """
        pass

    def set_numeric_cell(self, *args, **kwargs): # real signature unknown
        """ set numeric value to specified cell """
        pass

    def set_string_cell(self, *args, **kwargs): # real signature unknown
        """ set string to specified cell """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """sheet name"""



class SheetError(Exception):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>'

__spec__ = None # (!) real value is "ModuleSpec(name='ixion', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>, origin='/usr/lib/python3.8/site-packages/ixion.so')"

