# encoding: utf-8
# module PyQt5.QtLocation
# from /usr/lib/python3.8/site-packages/PyQt5/QtLocation.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QPlaceSearchResult import QPlaceSearchResult

class QPlaceProposedSearchResult(QPlaceSearchResult):
    """
    QPlaceProposedSearchResult()
    QPlaceProposedSearchResult(QPlaceSearchResult)
    QPlaceProposedSearchResult(QPlaceProposedSearchResult)
    """
    def searchRequest(self): # real signature unknown; restored from __doc__
        """ searchRequest(self) -> QPlaceSearchRequest """
        return QPlaceSearchRequest

    def setSearchRequest(self, QPlaceSearchRequest): # real signature unknown; restored from __doc__
        """ setSearchRequest(self, QPlaceSearchRequest) """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


