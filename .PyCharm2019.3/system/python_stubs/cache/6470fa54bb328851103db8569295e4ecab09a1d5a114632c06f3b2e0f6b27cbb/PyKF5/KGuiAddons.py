# encoding: utf-8
# module PyKF5.KGuiAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KGuiAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


# no functions
# classes

class KColorCollection(__sip.wrapper):
    # no doc
    def addColor(self, *args, **kwargs): # real signature unknown
        pass

    def changeColor(self, *args, **kwargs): # real signature unknown
        pass

    def color(self, *args, **kwargs): # real signature unknown
        pass

    def count(self, *args, **kwargs): # real signature unknown
        pass

    def description(self, *args, **kwargs): # real signature unknown
        pass

    def editable(self, *args, **kwargs): # real signature unknown
        pass

    def findColor(self, *args, **kwargs): # real signature unknown
        pass

    def installedCollections(self, *args, **kwargs): # real signature unknown
        pass

    def name(self, *args, **kwargs): # real signature unknown
        pass

    def save(self, *args, **kwargs): # real signature unknown
        pass

    def setDescription(self, *args, **kwargs): # real signature unknown
        pass

    def setEditable(self, *args, **kwargs): # real signature unknown
        pass

    def setName(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    Ask = 2
    Editable = None # (!) real value is "<class 'PyKF5.KGuiAddons.KColorCollection.Editable'>"
    No = 1
    Yes = 0


class KColorMimeData(__sip.simplewrapper):
    # no doc
    def canDecode(self, *args, **kwargs): # real signature unknown
        pass

    def createDrag(self, *args, **kwargs): # real signature unknown
        pass

    def fromMimeData(self, *args, **kwargs): # real signature unknown
        pass

    def populateMimeData(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KColorUtils(__sip.simplewrapper):
    # no doc
    def contrastRatio(self, *args, **kwargs): # real signature unknown
        pass

    def darken(self, *args, **kwargs): # real signature unknown
        pass

    def getHcy(self, *args, **kwargs): # real signature unknown
        pass

    def lighten(self, *args, **kwargs): # real signature unknown
        pass

    def luma(self, *args, **kwargs): # real signature unknown
        pass

    def mix(self, *args, **kwargs): # real signature unknown
        pass

    def overlayColors(self, *args, **kwargs): # real signature unknown
        pass

    def shade(self, *args, **kwargs): # real signature unknown
        pass

    def tint(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KDateValidator(__PyQt5_QtGui.QValidator):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def date(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def fixup(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def validate(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KFontUtils(__sip.simplewrapper):
    # no doc
    def adaptFontSize(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    AdaptFontSizeOption = None # (!) real value is "<class 'PyKF5.KGuiAddons.KFontUtils.AdaptFontSizeOption'>"
    AdaptFontSizeOptions = None # (!) real value is "<class 'PyKF5.KGuiAddons.KFontUtils.AdaptFontSizeOptions'>"
    DoNotAllowWordWrap = 2
    NoFlags = 1


class KIconUtils(__sip.simplewrapper):
    # no doc
    def addOverlay(self, *args, **kwargs): # real signature unknown
        pass

    def addOverlays(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KModifierKeyInfo(__PyQt5_QtCore.QObject):
    # no doc
    def buttonPressed(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def isButtonPressed(self, *args, **kwargs): # real signature unknown
        pass

    def isKeyLatched(self, *args, **kwargs): # real signature unknown
        pass

    def isKeyLocked(self, *args, **kwargs): # real signature unknown
        pass

    def isKeyPressed(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def keyAdded(self, *args, **kwargs): # real signature unknown
        pass

    def keyLatched(self, *args, **kwargs): # real signature unknown
        pass

    def keyLocked(self, *args, **kwargs): # real signature unknown
        pass

    def keyPressed(self, *args, **kwargs): # real signature unknown
        pass

    def keyRemoved(self, *args, **kwargs): # real signature unknown
        pass

    def knownKeys(self, *args, **kwargs): # real signature unknown
        pass

    def knowsKey(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setKeyLatched(self, *args, **kwargs): # real signature unknown
        pass

    def setKeyLocked(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KWordWrap(__sip.wrapper):
    # no doc
    def boundingRect(self, *args, **kwargs): # real signature unknown
        pass

    def drawFadeoutText(self, *args, **kwargs): # real signature unknown
        pass

    def drawText(self, *args, **kwargs): # real signature unknown
        pass

    def drawTruncateText(self, *args, **kwargs): # real signature unknown
        pass

    def formatText(self, *args, **kwargs): # real signature unknown
        pass

    def truncatedString(self, *args, **kwargs): # real signature unknown
        pass

    def wrappedString(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    FadeOut = 268435456
    Truncate = 536870912


# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>'

__spec__ = None # (!) real value is "ModuleSpec(name='PyKF5.KGuiAddons', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>, origin='/usr/lib/python3.8/site-packages/PyKF5/KGuiAddons.so')"

