# encoding: utf-8
# module _orcus
# from /usr/lib/python3.8/site-packages/_orcus.so
# by generator 1.147
# no doc
# no imports

# functions

def info(*args, **kwargs): # real signature unknown
    """ Print orcus module information. """
    pass

def _csv_read(*args, **kwargs): # real signature unknown
    """ Load specified csv file into a document model. """
    pass

def _gnumeric_read(*args, **kwargs): # real signature unknown
    """ Load specified gnumeric file into a document model. """
    pass

def _ods_read(*args, **kwargs): # real signature unknown
    """ Load specified ods file into a document model. """
    pass

def _xlsx_read(*args, **kwargs): # real signature unknown
    """ Load specified xlsx file into a document model. """
    pass

def _xls_xml_read(*args, **kwargs): # real signature unknown
    """ Load specified xls-xml file into a document model. """
    pass

# classes

class Document(object):
    """ orcus document object """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    sheets = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """sheet objects"""



class Sheet(object):
    """ orcus sheet object """
    def get_rows(self, *args, **kwargs): # real signature unknown
        """ Get a sheet row iterator. """
        pass

    def write(self, *args, **kwargs): # real signature unknown
        """ Write sheet content to specified file object. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    data_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """data size"""

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """sheet name"""

    sheet_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """sheet size"""



class SheetRows(object):
    """ orcus sheet rows iterator """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __iter__(self, *args, **kwargs): # real signature unknown
        """ Implement iter(self). """
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __next__(self, *args, **kwargs): # real signature unknown
        """ Implement next(self). """
        pass


# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15b0>'

__spec__ = None # (!) real value is "ModuleSpec(name='_orcus', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15b0>, origin='/usr/lib/python3.8/site-packages/_orcus.so')"

