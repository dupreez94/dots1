# encoding: utf-8
# module cairo._cairo calls itself cairo
# from /usr/lib/python3.8/site-packages/cairo/_cairo.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import cairo as __cairo


class SVGSurface(__cairo.Surface):
    # no doc
    def get_document_unit(self, *args, **kwargs): # real signature unknown
        pass

    def get_versions(self, *args, **kwargs): # real signature unknown
        pass

    def restrict_to_version(self, *args, **kwargs): # real signature unknown
        pass

    def set_document_unit(self, *args, **kwargs): # real signature unknown
        pass

    def version_to_string(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass


