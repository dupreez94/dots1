# encoding: utf-8
# module PyQt5.QtNetwork
# from /usr/lib/python3.8/site-packages/PyQt5/QtNetwork.abi3.so
# by generator 1.147
# no doc

# imports
import enum as __enum
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QAbstractSocket import QAbstractSocket

class QTcpSocket(QAbstractSocket):
    """ QTcpSocket(parent: QObject = None) """
    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


