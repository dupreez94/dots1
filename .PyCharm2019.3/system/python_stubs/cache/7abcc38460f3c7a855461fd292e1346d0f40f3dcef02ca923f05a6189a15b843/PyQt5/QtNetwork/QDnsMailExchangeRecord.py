# encoding: utf-8
# module PyQt5.QtNetwork
# from /usr/lib/python3.8/site-packages/PyQt5/QtNetwork.abi3.so
# by generator 1.147
# no doc

# imports
import enum as __enum
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class QDnsMailExchangeRecord(__sip.simplewrapper):
    """
    QDnsMailExchangeRecord()
    QDnsMailExchangeRecord(QDnsMailExchangeRecord)
    """
    def exchange(self): # real signature unknown; restored from __doc__
        """ exchange(self) -> str """
        return ""

    def name(self): # real signature unknown; restored from __doc__
        """ name(self) -> str """
        return ""

    def preference(self): # real signature unknown; restored from __doc__
        """ preference(self) -> int """
        return 0

    def swap(self, QDnsMailExchangeRecord): # real signature unknown; restored from __doc__
        """ swap(self, QDnsMailExchangeRecord) """
        pass

    def timeToLive(self): # real signature unknown; restored from __doc__
        """ timeToLive(self) -> int """
        return 0

    def __init__(self, QDnsMailExchangeRecord=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



