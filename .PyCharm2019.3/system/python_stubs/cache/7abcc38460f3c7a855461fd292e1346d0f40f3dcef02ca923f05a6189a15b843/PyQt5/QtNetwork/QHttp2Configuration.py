# encoding: utf-8
# module PyQt5.QtNetwork
# from /usr/lib/python3.8/site-packages/PyQt5/QtNetwork.abi3.so
# by generator 1.147
# no doc

# imports
import enum as __enum
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class QHttp2Configuration(__sip.simplewrapper):
    """
    QHttp2Configuration()
    QHttp2Configuration(QHttp2Configuration)
    """
    def huffmanCompressionEnabled(self): # real signature unknown; restored from __doc__
        """ huffmanCompressionEnabled(self) -> bool """
        return False

    def maxFrameSize(self): # real signature unknown; restored from __doc__
        """ maxFrameSize(self) -> int """
        return 0

    def serverPushEnabled(self): # real signature unknown; restored from __doc__
        """ serverPushEnabled(self) -> bool """
        return False

    def sessionReceiveWindowSize(self): # real signature unknown; restored from __doc__
        """ sessionReceiveWindowSize(self) -> int """
        return 0

    def setHuffmanCompressionEnabled(self, bool): # real signature unknown; restored from __doc__
        """ setHuffmanCompressionEnabled(self, bool) """
        pass

    def setMaxFrameSize(self, p_int): # real signature unknown; restored from __doc__
        """ setMaxFrameSize(self, int) -> bool """
        return False

    def setServerPushEnabled(self, bool): # real signature unknown; restored from __doc__
        """ setServerPushEnabled(self, bool) """
        pass

    def setSessionReceiveWindowSize(self, p_int): # real signature unknown; restored from __doc__
        """ setSessionReceiveWindowSize(self, int) -> bool """
        return False

    def setStreamReceiveWindowSize(self, p_int): # real signature unknown; restored from __doc__
        """ setStreamReceiveWindowSize(self, int) -> bool """
        return False

    def streamReceiveWindowSize(self): # real signature unknown; restored from __doc__
        """ streamReceiveWindowSize(self) -> int """
        return 0

    def swap(self, QHttp2Configuration): # real signature unknown; restored from __doc__
        """ swap(self, QHttp2Configuration) """
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, QHttp2Configuration=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    __hash__ = None


