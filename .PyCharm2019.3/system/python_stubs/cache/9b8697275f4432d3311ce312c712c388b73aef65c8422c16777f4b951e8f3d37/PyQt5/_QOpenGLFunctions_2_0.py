# encoding: utf-8
# module PyQt5._QOpenGLFunctions_2_0
# from /usr/lib/python3.8/site-packages/PyQt5/_QOpenGLFunctions_2_0.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtGui as __PyQt5_QtGui


# no functions
# classes

class QOpenGLFunctions_2_0(__PyQt5_QtGui.QAbstractOpenGLFunctions):
    """ QOpenGLFunctions_2_0() """
    def glAccum(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glAccum(self, int, float) """
        pass

    def glActiveTexture(self, p_int): # real signature unknown; restored from __doc__
        """ glActiveTexture(self, int) """
        pass

    def glAlphaFunc(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glAlphaFunc(self, int, float) """
        pass

    def glArrayElement(self, p_int): # real signature unknown; restored from __doc__
        """ glArrayElement(self, int) """
        pass

    def glAttachShader(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glAttachShader(self, int, int) """
        pass

    def glBegin(self, p_int): # real signature unknown; restored from __doc__
        """ glBegin(self, int) """
        pass

    def glBeginQuery(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBeginQuery(self, int, int) """
        pass

    def glBindAttribLocation(self, p_int, p_int_1, p_str): # real signature unknown; restored from __doc__
        """ glBindAttribLocation(self, int, int, str) """
        pass

    def glBindBuffer(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBindBuffer(self, int, int) """
        pass

    def glBindTexture(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBindTexture(self, int, int) """
        pass

    def glBitmap(self, p_int, p_int_1, p_float, p_float_1, p_float_2, p_float_3, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glBitmap(self, int, int, float, float, float, float, PYQT_OPENGL_ARRAY) """
        pass

    def glBlendColor(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glBlendColor(self, float, float, float, float) """
        pass

    def glBlendEquation(self, p_int): # real signature unknown; restored from __doc__
        """ glBlendEquation(self, int) """
        pass

    def glBlendEquationSeparate(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBlendEquationSeparate(self, int, int) """
        pass

    def glBlendFunc(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glBlendFunc(self, int, int) """
        pass

    def glBlendFuncSeparate(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glBlendFuncSeparate(self, int, int, int, int) """
        pass

    def glBufferData(self, p_int, p_int_1, PYQT_OPENGL_ARRAY, p_int_2): # real signature unknown; restored from __doc__
        """ glBufferData(self, int, int, PYQT_OPENGL_ARRAY, int) """
        pass

    def glBufferSubData(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glBufferSubData(self, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCallList(self, p_int): # real signature unknown; restored from __doc__
        """ glCallList(self, int) """
        pass

    def glClear(self, p_int): # real signature unknown; restored from __doc__
        """ glClear(self, int) """
        pass

    def glClearAccum(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glClearAccum(self, float, float, float, float) """
        pass

    def glClearColor(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glClearColor(self, float, float, float, float) """
        pass

    def glClearDepth(self, p_float): # real signature unknown; restored from __doc__
        """ glClearDepth(self, float) """
        pass

    def glClearIndex(self, p_float): # real signature unknown; restored from __doc__
        """ glClearIndex(self, float) """
        pass

    def glClearStencil(self, p_int): # real signature unknown; restored from __doc__
        """ glClearStencil(self, int) """
        pass

    def glClientActiveTexture(self, p_int): # real signature unknown; restored from __doc__
        """ glClientActiveTexture(self, int) """
        pass

    def glClipPlane(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glClipPlane(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glColor3b(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glColor3b(self, int, int, int) """
        pass

    def glColor3bv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor3bv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor3d(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glColor3d(self, float, float, float) """
        pass

    def glColor3dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor3dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor3f(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glColor3f(self, float, float, float) """
        pass

    def glColor3fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor3fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor3i(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glColor3i(self, int, int, int) """
        pass

    def glColor3iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor3iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor3s(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glColor3s(self, int, int, int) """
        pass

    def glColor3sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor3sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor3ub(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glColor3ub(self, int, int, int) """
        pass

    def glColor3ubv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor3ubv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor3ui(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glColor3ui(self, int, int, int) """
        pass

    def glColor3uiv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor3uiv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor3us(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glColor3us(self, int, int, int) """
        pass

    def glColor3usv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor3usv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor4b(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glColor4b(self, int, int, int, int) """
        pass

    def glColor4bv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor4bv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor4d(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glColor4d(self, float, float, float, float) """
        pass

    def glColor4dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor4dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor4f(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glColor4f(self, float, float, float, float) """
        pass

    def glColor4fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor4fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor4i(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glColor4i(self, int, int, int, int) """
        pass

    def glColor4iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor4iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor4s(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glColor4s(self, int, int, int, int) """
        pass

    def glColor4sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor4sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor4ub(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glColor4ub(self, int, int, int, int) """
        pass

    def glColor4ubv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor4ubv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor4ui(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glColor4ui(self, int, int, int, int) """
        pass

    def glColor4uiv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor4uiv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColor4us(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glColor4us(self, int, int, int, int) """
        pass

    def glColor4usv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColor4usv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glColorMask(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glColorMask(self, int, int, int, int) """
        pass

    def glColorMaterial(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glColorMaterial(self, int, int) """
        pass

    def glColorPointer(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_BOUND_ARRAY): # real signature unknown; restored from __doc__
        """ glColorPointer(self, int, int, int, PYQT_OPENGL_BOUND_ARRAY) """
        pass

    def glColorSubTable(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColorSubTable(self, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glColorTable(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColorTable(self, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glColorTableParameterfv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColorTableParameterfv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glColorTableParameteriv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glColorTableParameteriv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCompileShader(self, p_int): # real signature unknown; restored from __doc__
        """ glCompileShader(self, int) """
        pass

    def glCompressedTexImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexImage1D(self, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCompressedTexImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexImage2D(self, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCompressedTexImage3D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexImage3D(self, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCompressedTexSubImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexSubImage1D(self, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCompressedTexSubImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexSubImage2D(self, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCompressedTexSubImage3D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, p_int_8, p_int_9, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glCompressedTexSubImage3D(self, int, int, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glConvolutionFilter1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glConvolutionFilter1D(self, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glConvolutionFilter2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glConvolutionFilter2D(self, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glConvolutionParameterf(self, p_int, p_int_1, p_float): # real signature unknown; restored from __doc__
        """ glConvolutionParameterf(self, int, int, float) """
        pass

    def glConvolutionParameterfv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glConvolutionParameterfv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glConvolutionParameteri(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glConvolutionParameteri(self, int, int, int) """
        pass

    def glConvolutionParameteriv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glConvolutionParameteriv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glCopyColorSubTable(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glCopyColorSubTable(self, int, int, int, int, int) """
        pass

    def glCopyColorTable(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glCopyColorTable(self, int, int, int, int, int) """
        pass

    def glCopyConvolutionFilter1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glCopyConvolutionFilter1D(self, int, int, int, int, int) """
        pass

    def glCopyConvolutionFilter2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5): # real signature unknown; restored from __doc__
        """ glCopyConvolutionFilter2D(self, int, int, int, int, int, int) """
        pass

    def glCopyPixels(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glCopyPixels(self, int, int, int, int, int) """
        pass

    def glCopyTexImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6): # real signature unknown; restored from __doc__
        """ glCopyTexImage1D(self, int, int, int, int, int, int, int) """
        pass

    def glCopyTexImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7): # real signature unknown; restored from __doc__
        """ glCopyTexImage2D(self, int, int, int, int, int, int, int, int) """
        pass

    def glCopyTexSubImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5): # real signature unknown; restored from __doc__
        """ glCopyTexSubImage1D(self, int, int, int, int, int, int) """
        pass

    def glCopyTexSubImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7): # real signature unknown; restored from __doc__
        """ glCopyTexSubImage2D(self, int, int, int, int, int, int, int, int) """
        pass

    def glCopyTexSubImage3D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, p_int_8): # real signature unknown; restored from __doc__
        """ glCopyTexSubImage3D(self, int, int, int, int, int, int, int, int, int) """
        pass

    def glCreateProgram(self): # real signature unknown; restored from __doc__
        """ glCreateProgram(self) -> int """
        return 0

    def glCreateShader(self, p_int): # real signature unknown; restored from __doc__
        """ glCreateShader(self, int) -> int """
        return 0

    def glCullFace(self, p_int): # real signature unknown; restored from __doc__
        """ glCullFace(self, int) """
        pass

    def glDeleteBuffers(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDeleteBuffers(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDeleteLists(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glDeleteLists(self, int, int) """
        pass

    def glDeleteProgram(self, p_int): # real signature unknown; restored from __doc__
        """ glDeleteProgram(self, int) """
        pass

    def glDeleteQueries(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDeleteQueries(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDeleteShader(self, p_int): # real signature unknown; restored from __doc__
        """ glDeleteShader(self, int) """
        pass

    def glDeleteTextures(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDeleteTextures(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDepthFunc(self, p_int): # real signature unknown; restored from __doc__
        """ glDepthFunc(self, int) """
        pass

    def glDepthMask(self, p_int): # real signature unknown; restored from __doc__
        """ glDepthMask(self, int) """
        pass

    def glDepthRange(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glDepthRange(self, float, float) """
        pass

    def glDetachShader(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glDetachShader(self, int, int) """
        pass

    def glDisable(self, p_int): # real signature unknown; restored from __doc__
        """ glDisable(self, int) """
        pass

    def glDisableClientState(self, p_int): # real signature unknown; restored from __doc__
        """ glDisableClientState(self, int) """
        pass

    def glDisableVertexAttribArray(self, p_int): # real signature unknown; restored from __doc__
        """ glDisableVertexAttribArray(self, int) """
        pass

    def glDrawArrays(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glDrawArrays(self, int, int, int) """
        pass

    def glDrawBuffer(self, p_int): # real signature unknown; restored from __doc__
        """ glDrawBuffer(self, int) """
        pass

    def glDrawBuffers(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDrawBuffers(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDrawElements(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDrawElements(self, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDrawPixels(self, p_int, p_int_1, p_int_2, p_int_3, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDrawPixels(self, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glDrawRangeElements(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glDrawRangeElements(self, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glEdgeFlag(self, p_int): # real signature unknown; restored from __doc__
        """ glEdgeFlag(self, int) """
        pass

    def glEdgeFlagPointer(self, p_int, PYQT_OPENGL_BOUND_ARRAY): # real signature unknown; restored from __doc__
        """ glEdgeFlagPointer(self, int, PYQT_OPENGL_BOUND_ARRAY) """
        pass

    def glEdgeFlagv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glEdgeFlagv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glEnable(self, p_int): # real signature unknown; restored from __doc__
        """ glEnable(self, int) """
        pass

    def glEnableClientState(self, p_int): # real signature unknown; restored from __doc__
        """ glEnableClientState(self, int) """
        pass

    def glEnableVertexAttribArray(self, p_int): # real signature unknown; restored from __doc__
        """ glEnableVertexAttribArray(self, int) """
        pass

    def glEnd(self): # real signature unknown; restored from __doc__
        """ glEnd(self) """
        pass

    def glEndList(self): # real signature unknown; restored from __doc__
        """ glEndList(self) """
        pass

    def glEndQuery(self, p_int): # real signature unknown; restored from __doc__
        """ glEndQuery(self, int) """
        pass

    def glEvalCoord1d(self, p_float): # real signature unknown; restored from __doc__
        """ glEvalCoord1d(self, float) """
        pass

    def glEvalCoord1dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glEvalCoord1dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glEvalCoord1f(self, p_float): # real signature unknown; restored from __doc__
        """ glEvalCoord1f(self, float) """
        pass

    def glEvalCoord1fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glEvalCoord1fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glEvalCoord2d(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glEvalCoord2d(self, float, float) """
        pass

    def glEvalCoord2dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glEvalCoord2dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glEvalCoord2f(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glEvalCoord2f(self, float, float) """
        pass

    def glEvalCoord2fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glEvalCoord2fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glEvalMesh1(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glEvalMesh1(self, int, int, int) """
        pass

    def glEvalMesh2(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glEvalMesh2(self, int, int, int, int, int) """
        pass

    def glEvalPoint1(self, p_int): # real signature unknown; restored from __doc__
        """ glEvalPoint1(self, int) """
        pass

    def glEvalPoint2(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glEvalPoint2(self, int, int) """
        pass

    def glFinish(self): # real signature unknown; restored from __doc__
        """ glFinish(self) """
        pass

    def glFlush(self): # real signature unknown; restored from __doc__
        """ glFlush(self) """
        pass

    def glFogCoordd(self, p_float): # real signature unknown; restored from __doc__
        """ glFogCoordd(self, float) """
        pass

    def glFogCoorddv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glFogCoorddv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glFogCoordf(self, p_float): # real signature unknown; restored from __doc__
        """ glFogCoordf(self, float) """
        pass

    def glFogCoordfv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glFogCoordfv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glFogCoordPointer(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glFogCoordPointer(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glFogf(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glFogf(self, int, float) """
        pass

    def glFogfv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glFogfv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glFogi(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glFogi(self, int, int) """
        pass

    def glFogiv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glFogiv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glFrontFace(self, p_int): # real signature unknown; restored from __doc__
        """ glFrontFace(self, int) """
        pass

    def glFrustum(self, p_float, p_float_1, p_float_2, p_float_3, p_float_4, p_float_5): # real signature unknown; restored from __doc__
        """ glFrustum(self, float, float, float, float, float, float) """
        pass

    def glGenBuffers(self, p_int): # real signature unknown; restored from __doc__
        """ glGenBuffers(self, int) -> Union[int, Tuple[int, ...]] """
        pass

    def glGenLists(self, p_int): # real signature unknown; restored from __doc__
        """ glGenLists(self, int) -> int """
        return 0

    def glGenQueries(self, p_int): # real signature unknown; restored from __doc__
        """ glGenQueries(self, int) -> Union[int, Tuple[int, ...]] """
        pass

    def glGenTextures(self, p_int): # real signature unknown; restored from __doc__
        """ glGenTextures(self, int) -> Union[int, Tuple[int, ...]] """
        pass

    def glGetActiveAttrib(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetActiveAttrib(self, int, int) -> Tuple[str, int, int] """
        pass

    def glGetActiveUniform(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetActiveUniform(self, int, int) -> Tuple[str, int, int] """
        pass

    def glGetAttachedShaders(self, p_int): # real signature unknown; restored from __doc__
        """ glGetAttachedShaders(self, int) -> Tuple[int, ...] """
        pass

    def glGetAttribLocation(self, p_int, p_str): # real signature unknown; restored from __doc__
        """ glGetAttribLocation(self, int, str) -> int """
        return 0

    def glGetBooleanv(self, p_int): # real signature unknown; restored from __doc__
        """ glGetBooleanv(self, int) -> Union[bool, Tuple[bool, ...]] """
        pass

    def glGetBufferParameteriv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetBufferParameteriv(self, int, int) -> int """
        return 0

    def glGetClipPlane(self, p_int): # real signature unknown; restored from __doc__
        """ glGetClipPlane(self, int) -> Tuple[float, float, float, float] """
        pass

    def glGetColorTableParameterfv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetColorTableParameterfv(self, int, int) -> Union[float, Tuple[float, float, float, float]] """
        pass

    def glGetColorTableParameteriv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetColorTableParameteriv(self, int, int) -> Union[int, Tuple[int, int, int, int]] """
        pass

    def glGetConvolutionParameterfv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetConvolutionParameterfv(self, int, int) -> Union[float, Tuple[float, float, float, float]] """
        pass

    def glGetConvolutionParameteriv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetConvolutionParameteriv(self, int, int) -> Union[int, Tuple[int, int, int, int]] """
        pass

    def glGetDoublev(self, p_int): # real signature unknown; restored from __doc__
        """ glGetDoublev(self, int) -> Union[float, Tuple[float, ...]] """
        pass

    def glGetError(self): # real signature unknown; restored from __doc__
        """ glGetError(self) -> int """
        return 0

    def glGetFloatv(self, p_int): # real signature unknown; restored from __doc__
        """ glGetFloatv(self, int) -> Union[float, Tuple[float, ...]] """
        pass

    def glGetIntegerv(self, p_int): # real signature unknown; restored from __doc__
        """ glGetIntegerv(self, int) -> Union[int, Tuple[int, ...]] """
        pass

    def glGetLightfv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetLightfv(self, int, int) -> Union[float, Tuple[float, float, float], Tuple[float, float, float, float]] """
        pass

    def glGetLightiv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetLightiv(self, int, int) -> Union[int, Tuple[int, int, int], Tuple[int, int, int, int]] """
        pass

    def glGetMaterialfv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetMaterialfv(self, int, int) -> Union[float, Tuple[float, float, float], Tuple[float, float, float, float]] """
        pass

    def glGetMaterialiv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetMaterialiv(self, int, int) -> Union[int, Tuple[int, int, int], Tuple[int, int, int, int]] """
        pass

    def glGetProgramInfoLog(self, p_int): # real signature unknown; restored from __doc__
        """ glGetProgramInfoLog(self, int) -> bytes """
        return b""

    def glGetProgramiv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetProgramiv(self, int, int) -> Union[int, Tuple[int, int, int]] """
        pass

    def glGetQueryiv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetQueryiv(self, int, int) -> int """
        return 0

    def glGetShaderInfoLog(self, p_int): # real signature unknown; restored from __doc__
        """ glGetShaderInfoLog(self, int) -> bytes """
        return b""

    def glGetShaderiv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetShaderiv(self, int, int) -> int """
        return 0

    def glGetShaderSource(self, p_int): # real signature unknown; restored from __doc__
        """ glGetShaderSource(self, int) -> bytes """
        return b""

    def glGetString(self, p_int): # real signature unknown; restored from __doc__
        """ glGetString(self, int) -> str """
        return ""

    def glGetTexEnvfv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetTexEnvfv(self, int, int) -> Union[float, Tuple[float, float, float, float]] """
        pass

    def glGetTexEnviv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetTexEnviv(self, int, int) -> Union[int, Tuple[int, int, int, int]] """
        pass

    def glGetTexGendv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetTexGendv(self, int, int) -> Union[float, Tuple[float, float, float, float]] """
        pass

    def glGetTexGenfv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetTexGenfv(self, int, int) -> Union[float, Tuple[float, float, float, float]] """
        pass

    def glGetTexGeniv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetTexGeniv(self, int, int) -> Union[int, Tuple[int, int, int, int]] """
        pass

    def glGetTexLevelParameterfv(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glGetTexLevelParameterfv(self, int, int, int) -> float """
        return 0.0

    def glGetTexLevelParameteriv(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glGetTexLevelParameteriv(self, int, int, int) -> int """
        return 0

    def glGetTexParameterfv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetTexParameterfv(self, int, int) -> Union[float, Tuple[float, float, float, float]] """
        pass

    def glGetTexParameteriv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetTexParameteriv(self, int, int) -> Union[int, Tuple[int, int, int, int]] """
        pass

    def glGetUniformLocation(self, p_int, p_str): # real signature unknown; restored from __doc__
        """ glGetUniformLocation(self, int, str) -> int """
        return 0

    def glGetVertexAttribdv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetVertexAttribdv(self, int, int) -> Union[float, Tuple[float, float, float, float]] """
        pass

    def glGetVertexAttribfv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetVertexAttribfv(self, int, int) -> Union[float, Tuple[float, float, float, float]] """
        pass

    def glGetVertexAttribiv(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glGetVertexAttribiv(self, int, int) -> Union[int, Tuple[int, int, int, int]] """
        pass

    def glHint(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glHint(self, int, int) """
        pass

    def glHistogram(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glHistogram(self, int, int, int, int) """
        pass

    def glIndexd(self, p_float): # real signature unknown; restored from __doc__
        """ glIndexd(self, float) """
        pass

    def glIndexdv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glIndexdv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glIndexf(self, p_float): # real signature unknown; restored from __doc__
        """ glIndexf(self, float) """
        pass

    def glIndexfv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glIndexfv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glIndexi(self, p_int): # real signature unknown; restored from __doc__
        """ glIndexi(self, int) """
        pass

    def glIndexiv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glIndexiv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glIndexMask(self, p_int): # real signature unknown; restored from __doc__
        """ glIndexMask(self, int) """
        pass

    def glIndexPointer(self, p_int, p_int_1, PYQT_OPENGL_BOUND_ARRAY): # real signature unknown; restored from __doc__
        """ glIndexPointer(self, int, int, PYQT_OPENGL_BOUND_ARRAY) """
        pass

    def glIndexs(self, p_int): # real signature unknown; restored from __doc__
        """ glIndexs(self, int) """
        pass

    def glIndexsv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glIndexsv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glIndexub(self, p_int): # real signature unknown; restored from __doc__
        """ glIndexub(self, int) """
        pass

    def glIndexubv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glIndexubv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glInitNames(self): # real signature unknown; restored from __doc__
        """ glInitNames(self) """
        pass

    def glIsBuffer(self, p_int): # real signature unknown; restored from __doc__
        """ glIsBuffer(self, int) -> int """
        return 0

    def glIsEnabled(self, p_int): # real signature unknown; restored from __doc__
        """ glIsEnabled(self, int) -> int """
        return 0

    def glIsList(self, p_int): # real signature unknown; restored from __doc__
        """ glIsList(self, int) -> int """
        return 0

    def glIsProgram(self, p_int): # real signature unknown; restored from __doc__
        """ glIsProgram(self, int) -> int """
        return 0

    def glIsQuery(self, p_int): # real signature unknown; restored from __doc__
        """ glIsQuery(self, int) -> int """
        return 0

    def glIsShader(self, p_int): # real signature unknown; restored from __doc__
        """ glIsShader(self, int) -> int """
        return 0

    def glIsTexture(self, p_int): # real signature unknown; restored from __doc__
        """ glIsTexture(self, int) -> int """
        return 0

    def glLightf(self, p_int, p_int_1, p_float): # real signature unknown; restored from __doc__
        """ glLightf(self, int, int, float) """
        pass

    def glLightfv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glLightfv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glLighti(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glLighti(self, int, int, int) """
        pass

    def glLightiv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glLightiv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glLightModelf(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glLightModelf(self, int, float) """
        pass

    def glLightModelfv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glLightModelfv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glLightModeli(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glLightModeli(self, int, int) """
        pass

    def glLightModeliv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glLightModeliv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glLineStipple(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glLineStipple(self, int, int) """
        pass

    def glLineWidth(self, p_float): # real signature unknown; restored from __doc__
        """ glLineWidth(self, float) """
        pass

    def glLinkProgram(self, p_int): # real signature unknown; restored from __doc__
        """ glLinkProgram(self, int) """
        pass

    def glListBase(self, p_int): # real signature unknown; restored from __doc__
        """ glListBase(self, int) """
        pass

    def glLoadIdentity(self): # real signature unknown; restored from __doc__
        """ glLoadIdentity(self) """
        pass

    def glLoadMatrixd(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glLoadMatrixd(self, PYQT_OPENGL_ARRAY) """
        pass

    def glLoadMatrixf(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glLoadMatrixf(self, PYQT_OPENGL_ARRAY) """
        pass

    def glLoadName(self, p_int): # real signature unknown; restored from __doc__
        """ glLoadName(self, int) """
        pass

    def glLoadTransposeMatrixd(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glLoadTransposeMatrixd(self, PYQT_OPENGL_ARRAY) """
        pass

    def glLoadTransposeMatrixf(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glLoadTransposeMatrixf(self, PYQT_OPENGL_ARRAY) """
        pass

    def glLogicOp(self, p_int): # real signature unknown; restored from __doc__
        """ glLogicOp(self, int) """
        pass

    def glMap1d(self, p_int, p_float, p_float_1, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMap1d(self, int, float, float, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMap1f(self, p_int, p_float, p_float_1, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMap1f(self, int, float, float, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMap2d(self, p_int, p_float, p_float_1, p_int_1, p_int_2, p_float_2, p_float_3, p_int_3, p_int_4, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMap2d(self, int, float, float, int, int, float, float, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMap2f(self, p_int, p_float, p_float_1, p_int_1, p_int_2, p_float_2, p_float_3, p_int_3, p_int_4, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMap2f(self, int, float, float, int, int, float, float, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMapGrid1d(self, p_int, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glMapGrid1d(self, int, float, float) """
        pass

    def glMapGrid1f(self, p_int, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glMapGrid1f(self, int, float, float) """
        pass

    def glMapGrid2d(self, p_int, p_float, p_float_1, p_int_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glMapGrid2d(self, int, float, float, int, float, float) """
        pass

    def glMapGrid2f(self, p_int, p_float, p_float_1, p_int_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glMapGrid2f(self, int, float, float, int, float, float) """
        pass

    def glMaterialf(self, p_int, p_int_1, p_float): # real signature unknown; restored from __doc__
        """ glMaterialf(self, int, int, float) """
        pass

    def glMaterialfv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMaterialfv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMateriali(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glMateriali(self, int, int, int) """
        pass

    def glMaterialiv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMaterialiv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMatrixMode(self, p_int): # real signature unknown; restored from __doc__
        """ glMatrixMode(self, int) """
        pass

    def glMinmax(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glMinmax(self, int, int, int) """
        pass

    def glMultiTexCoord1d(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glMultiTexCoord1d(self, int, float) """
        pass

    def glMultiTexCoord1dv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord1dv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord1f(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glMultiTexCoord1f(self, int, float) """
        pass

    def glMultiTexCoord1fv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord1fv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord1i(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glMultiTexCoord1i(self, int, int) """
        pass

    def glMultiTexCoord1iv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord1iv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord1s(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glMultiTexCoord1s(self, int, int) """
        pass

    def glMultiTexCoord1sv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord1sv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord2d(self, p_int, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glMultiTexCoord2d(self, int, float, float) """
        pass

    def glMultiTexCoord2dv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord2dv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord2f(self, p_int, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glMultiTexCoord2f(self, int, float, float) """
        pass

    def glMultiTexCoord2fv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord2fv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord2i(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glMultiTexCoord2i(self, int, int, int) """
        pass

    def glMultiTexCoord2iv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord2iv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord2s(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glMultiTexCoord2s(self, int, int, int) """
        pass

    def glMultiTexCoord2sv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord2sv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord3d(self, p_int, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glMultiTexCoord3d(self, int, float, float, float) """
        pass

    def glMultiTexCoord3dv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord3dv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord3f(self, p_int, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glMultiTexCoord3f(self, int, float, float, float) """
        pass

    def glMultiTexCoord3fv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord3fv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord3i(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glMultiTexCoord3i(self, int, int, int, int) """
        pass

    def glMultiTexCoord3iv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord3iv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord3s(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glMultiTexCoord3s(self, int, int, int, int) """
        pass

    def glMultiTexCoord3sv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord3sv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord4d(self, p_int, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glMultiTexCoord4d(self, int, float, float, float, float) """
        pass

    def glMultiTexCoord4dv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord4dv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord4f(self, p_int, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glMultiTexCoord4f(self, int, float, float, float, float) """
        pass

    def glMultiTexCoord4fv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord4fv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord4i(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glMultiTexCoord4i(self, int, int, int, int, int) """
        pass

    def glMultiTexCoord4iv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord4iv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultiTexCoord4s(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glMultiTexCoord4s(self, int, int, int, int, int) """
        pass

    def glMultiTexCoord4sv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultiTexCoord4sv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glMultMatrixd(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultMatrixd(self, PYQT_OPENGL_ARRAY) """
        pass

    def glMultMatrixf(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultMatrixf(self, PYQT_OPENGL_ARRAY) """
        pass

    def glMultTransposeMatrixd(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultTransposeMatrixd(self, PYQT_OPENGL_ARRAY) """
        pass

    def glMultTransposeMatrixf(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glMultTransposeMatrixf(self, PYQT_OPENGL_ARRAY) """
        pass

    def glNewList(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glNewList(self, int, int) """
        pass

    def glNormal3b(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glNormal3b(self, int, int, int) """
        pass

    def glNormal3bv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glNormal3bv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glNormal3d(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glNormal3d(self, float, float, float) """
        pass

    def glNormal3dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glNormal3dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glNormal3f(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glNormal3f(self, float, float, float) """
        pass

    def glNormal3fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glNormal3fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glNormal3i(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glNormal3i(self, int, int, int) """
        pass

    def glNormal3iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glNormal3iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glNormal3s(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glNormal3s(self, int, int, int) """
        pass

    def glNormal3sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glNormal3sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glNormalPointer(self, p_int, p_int_1, PYQT_OPENGL_BOUND_ARRAY): # real signature unknown; restored from __doc__
        """ glNormalPointer(self, int, int, PYQT_OPENGL_BOUND_ARRAY) """
        pass

    def glOrtho(self, p_float, p_float_1, p_float_2, p_float_3, p_float_4, p_float_5): # real signature unknown; restored from __doc__
        """ glOrtho(self, float, float, float, float, float, float) """
        pass

    def glPassThrough(self, p_float): # real signature unknown; restored from __doc__
        """ glPassThrough(self, float) """
        pass

    def glPixelMapfv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glPixelMapfv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glPixelMapuiv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glPixelMapuiv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glPixelMapusv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glPixelMapusv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glPixelStoref(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glPixelStoref(self, int, float) """
        pass

    def glPixelStorei(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glPixelStorei(self, int, int) """
        pass

    def glPixelTransferf(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glPixelTransferf(self, int, float) """
        pass

    def glPixelTransferi(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glPixelTransferi(self, int, int) """
        pass

    def glPixelZoom(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glPixelZoom(self, float, float) """
        pass

    def glPointParameterf(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glPointParameterf(self, int, float) """
        pass

    def glPointParameterfv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glPointParameterfv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glPointParameteri(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glPointParameteri(self, int, int) """
        pass

    def glPointParameteriv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glPointParameteriv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glPointSize(self, p_float): # real signature unknown; restored from __doc__
        """ glPointSize(self, float) """
        pass

    def glPolygonMode(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glPolygonMode(self, int, int) """
        pass

    def glPolygonOffset(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glPolygonOffset(self, float, float) """
        pass

    def glPolygonStipple(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glPolygonStipple(self, PYQT_OPENGL_ARRAY) """
        pass

    def glPopAttrib(self): # real signature unknown; restored from __doc__
        """ glPopAttrib(self) """
        pass

    def glPopClientAttrib(self): # real signature unknown; restored from __doc__
        """ glPopClientAttrib(self) """
        pass

    def glPopMatrix(self): # real signature unknown; restored from __doc__
        """ glPopMatrix(self) """
        pass

    def glPopName(self): # real signature unknown; restored from __doc__
        """ glPopName(self) """
        pass

    def glPushAttrib(self, p_int): # real signature unknown; restored from __doc__
        """ glPushAttrib(self, int) """
        pass

    def glPushClientAttrib(self, p_int): # real signature unknown; restored from __doc__
        """ glPushClientAttrib(self, int) """
        pass

    def glPushMatrix(self): # real signature unknown; restored from __doc__
        """ glPushMatrix(self) """
        pass

    def glPushName(self, p_int): # real signature unknown; restored from __doc__
        """ glPushName(self, int) """
        pass

    def glRasterPos2d(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glRasterPos2d(self, float, float) """
        pass

    def glRasterPos2dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos2dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glRasterPos2f(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glRasterPos2f(self, float, float) """
        pass

    def glRasterPos2fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos2fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glRasterPos2i(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glRasterPos2i(self, int, int) """
        pass

    def glRasterPos2iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos2iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glRasterPos2s(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glRasterPos2s(self, int, int) """
        pass

    def glRasterPos2sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos2sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glRasterPos3d(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glRasterPos3d(self, float, float, float) """
        pass

    def glRasterPos3dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos3dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glRasterPos3f(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glRasterPos3f(self, float, float, float) """
        pass

    def glRasterPos3fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos3fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glRasterPos3i(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glRasterPos3i(self, int, int, int) """
        pass

    def glRasterPos3iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos3iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glRasterPos3s(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glRasterPos3s(self, int, int, int) """
        pass

    def glRasterPos3sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos3sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glRasterPos4d(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glRasterPos4d(self, float, float, float, float) """
        pass

    def glRasterPos4dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos4dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glRasterPos4f(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glRasterPos4f(self, float, float, float, float) """
        pass

    def glRasterPos4fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos4fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glRasterPos4i(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glRasterPos4i(self, int, int, int, int) """
        pass

    def glRasterPos4iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos4iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glRasterPos4s(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glRasterPos4s(self, int, int, int, int) """
        pass

    def glRasterPos4sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glRasterPos4sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glReadBuffer(self, p_int): # real signature unknown; restored from __doc__
        """ glReadBuffer(self, int) """
        pass

    def glReadPixels(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5): # real signature unknown; restored from __doc__
        """ glReadPixels(self, int, int, int, int, int, int) -> Union[Tuple[float, ...], Tuple[int, ...]] """
        pass

    def glRectd(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glRectd(self, float, float, float, float) """
        pass

    def glRectf(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glRectf(self, float, float, float, float) """
        pass

    def glRecti(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glRecti(self, int, int, int, int) """
        pass

    def glRects(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glRects(self, int, int, int, int) """
        pass

    def glRenderMode(self, p_int): # real signature unknown; restored from __doc__
        """ glRenderMode(self, int) -> int """
        return 0

    def glResetHistogram(self, p_int): # real signature unknown; restored from __doc__
        """ glResetHistogram(self, int) """
        pass

    def glResetMinmax(self, p_int): # real signature unknown; restored from __doc__
        """ glResetMinmax(self, int) """
        pass

    def glRotated(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glRotated(self, float, float, float, float) """
        pass

    def glRotatef(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glRotatef(self, float, float, float, float) """
        pass

    def glSampleCoverage(self, p_float, p_int): # real signature unknown; restored from __doc__
        """ glSampleCoverage(self, float, int) """
        pass

    def glScaled(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glScaled(self, float, float, float) """
        pass

    def glScalef(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glScalef(self, float, float, float) """
        pass

    def glScissor(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glScissor(self, int, int, int, int) """
        pass

    def glSecondaryColor3b(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glSecondaryColor3b(self, int, int, int) """
        pass

    def glSecondaryColor3bv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glSecondaryColor3bv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glSecondaryColor3d(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glSecondaryColor3d(self, float, float, float) """
        pass

    def glSecondaryColor3dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glSecondaryColor3dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glSecondaryColor3f(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glSecondaryColor3f(self, float, float, float) """
        pass

    def glSecondaryColor3fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glSecondaryColor3fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glSecondaryColor3i(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glSecondaryColor3i(self, int, int, int) """
        pass

    def glSecondaryColor3iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glSecondaryColor3iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glSecondaryColor3s(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glSecondaryColor3s(self, int, int, int) """
        pass

    def glSecondaryColor3sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glSecondaryColor3sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glSecondaryColor3ub(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glSecondaryColor3ub(self, int, int, int) """
        pass

    def glSecondaryColor3ubv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glSecondaryColor3ubv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glSecondaryColor3ui(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glSecondaryColor3ui(self, int, int, int) """
        pass

    def glSecondaryColor3uiv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glSecondaryColor3uiv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glSecondaryColor3us(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glSecondaryColor3us(self, int, int, int) """
        pass

    def glSecondaryColor3usv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glSecondaryColor3usv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glSecondaryColorPointer(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glSecondaryColorPointer(self, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glShadeModel(self, p_int): # real signature unknown; restored from __doc__
        """ glShadeModel(self, int) """
        pass

    def glStencilFunc(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glStencilFunc(self, int, int, int) """
        pass

    def glStencilFuncSeparate(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glStencilFuncSeparate(self, int, int, int, int) """
        pass

    def glStencilMask(self, p_int): # real signature unknown; restored from __doc__
        """ glStencilMask(self, int) """
        pass

    def glStencilMaskSeparate(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glStencilMaskSeparate(self, int, int) """
        pass

    def glStencilOp(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glStencilOp(self, int, int, int) """
        pass

    def glStencilOpSeparate(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glStencilOpSeparate(self, int, int, int, int) """
        pass

    def glTexCoord1d(self, p_float): # real signature unknown; restored from __doc__
        """ glTexCoord1d(self, float) """
        pass

    def glTexCoord1dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord1dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord1f(self, p_float): # real signature unknown; restored from __doc__
        """ glTexCoord1f(self, float) """
        pass

    def glTexCoord1fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord1fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord1i(self, p_int): # real signature unknown; restored from __doc__
        """ glTexCoord1i(self, int) """
        pass

    def glTexCoord1iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord1iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord1s(self, p_int): # real signature unknown; restored from __doc__
        """ glTexCoord1s(self, int) """
        pass

    def glTexCoord1sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord1sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord2d(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glTexCoord2d(self, float, float) """
        pass

    def glTexCoord2dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord2dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord2f(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glTexCoord2f(self, float, float) """
        pass

    def glTexCoord2fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord2fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord2i(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glTexCoord2i(self, int, int) """
        pass

    def glTexCoord2iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord2iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord2s(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glTexCoord2s(self, int, int) """
        pass

    def glTexCoord2sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord2sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord3d(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glTexCoord3d(self, float, float, float) """
        pass

    def glTexCoord3dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord3dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord3f(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glTexCoord3f(self, float, float, float) """
        pass

    def glTexCoord3fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord3fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord3i(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glTexCoord3i(self, int, int, int) """
        pass

    def glTexCoord3iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord3iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord3s(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glTexCoord3s(self, int, int, int) """
        pass

    def glTexCoord3sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord3sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord4d(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glTexCoord4d(self, float, float, float, float) """
        pass

    def glTexCoord4dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord4dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord4f(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glTexCoord4f(self, float, float, float, float) """
        pass

    def glTexCoord4fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord4fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord4i(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glTexCoord4i(self, int, int, int, int) """
        pass

    def glTexCoord4iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord4iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoord4s(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glTexCoord4s(self, int, int, int, int) """
        pass

    def glTexCoord4sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoord4sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glTexCoordPointer(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_BOUND_ARRAY): # real signature unknown; restored from __doc__
        """ glTexCoordPointer(self, int, int, int, PYQT_OPENGL_BOUND_ARRAY) """
        pass

    def glTexEnvf(self, p_int, p_int_1, p_float): # real signature unknown; restored from __doc__
        """ glTexEnvf(self, int, int, float) """
        pass

    def glTexEnvfv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexEnvfv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexEnvi(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glTexEnvi(self, int, int, int) """
        pass

    def glTexEnviv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexEnviv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexGend(self, p_int, p_int_1, p_float): # real signature unknown; restored from __doc__
        """ glTexGend(self, int, int, float) """
        pass

    def glTexGendv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexGendv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexGenf(self, p_int, p_int_1, p_float): # real signature unknown; restored from __doc__
        """ glTexGenf(self, int, int, float) """
        pass

    def glTexGenfv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexGenfv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexGeni(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glTexGeni(self, int, int, int) """
        pass

    def glTexGeniv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexGeniv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexImage1D(self, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexImage2D(self, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexImage3D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, p_int_8, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexImage3D(self, int, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexParameterf(self, p_int, p_int_1, p_float): # real signature unknown; restored from __doc__
        """ glTexParameterf(self, int, int, float) """
        pass

    def glTexParameterfv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexParameterfv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexParameteri(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glTexParameteri(self, int, int, int) """
        pass

    def glTexParameteriv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexParameteriv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexSubImage1D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexSubImage1D(self, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexSubImage2D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexSubImage2D(self, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTexSubImage3D(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, p_int_5, p_int_6, p_int_7, p_int_8, p_int_9, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glTexSubImage3D(self, int, int, int, int, int, int, int, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glTranslated(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glTranslated(self, float, float, float) """
        pass

    def glTranslatef(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glTranslatef(self, float, float, float) """
        pass

    def glUniform1f(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glUniform1f(self, int, float) """
        pass

    def glUniform1fv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform1fv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform1i(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glUniform1i(self, int, int) """
        pass

    def glUniform1iv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform1iv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform2f(self, p_int, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glUniform2f(self, int, float, float) """
        pass

    def glUniform2fv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform2fv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform2i(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glUniform2i(self, int, int, int) """
        pass

    def glUniform2iv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform2iv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform3f(self, p_int, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glUniform3f(self, int, float, float, float) """
        pass

    def glUniform3fv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform3fv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform3i(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glUniform3i(self, int, int, int, int) """
        pass

    def glUniform3iv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform3iv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform4f(self, p_int, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glUniform4f(self, int, float, float, float, float) """
        pass

    def glUniform4fv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform4fv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniform4i(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glUniform4i(self, int, int, int, int, int) """
        pass

    def glUniform4iv(self, p_int, p_int_1, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniform4iv(self, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniformMatrix2fv(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniformMatrix2fv(self, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniformMatrix3fv(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniformMatrix3fv(self, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUniformMatrix4fv(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glUniformMatrix4fv(self, int, int, int, PYQT_OPENGL_ARRAY) """
        pass

    def glUnmapBuffer(self, p_int): # real signature unknown; restored from __doc__
        """ glUnmapBuffer(self, int) -> int """
        return 0

    def glUseProgram(self, p_int): # real signature unknown; restored from __doc__
        """ glUseProgram(self, int) """
        pass

    def glValidateProgram(self, p_int): # real signature unknown; restored from __doc__
        """ glValidateProgram(self, int) """
        pass

    def glVertex2d(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glVertex2d(self, float, float) """
        pass

    def glVertex2dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex2dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertex2f(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glVertex2f(self, float, float) """
        pass

    def glVertex2fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex2fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertex2i(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glVertex2i(self, int, int) """
        pass

    def glVertex2iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex2iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertex2s(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glVertex2s(self, int, int) """
        pass

    def glVertex2sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex2sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertex3d(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glVertex3d(self, float, float, float) """
        pass

    def glVertex3dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex3dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertex3f(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glVertex3f(self, float, float, float) """
        pass

    def glVertex3fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex3fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertex3i(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glVertex3i(self, int, int, int) """
        pass

    def glVertex3iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex3iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertex3s(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glVertex3s(self, int, int, int) """
        pass

    def glVertex3sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex3sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertex4d(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glVertex4d(self, float, float, float, float) """
        pass

    def glVertex4dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex4dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertex4f(self, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glVertex4f(self, float, float, float, float) """
        pass

    def glVertex4fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex4fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertex4i(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glVertex4i(self, int, int, int, int) """
        pass

    def glVertex4iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex4iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertex4s(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glVertex4s(self, int, int, int, int) """
        pass

    def glVertex4sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertex4sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib1d(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glVertexAttrib1d(self, int, float) """
        pass

    def glVertexAttrib1dv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib1dv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib1f(self, p_int, p_float): # real signature unknown; restored from __doc__
        """ glVertexAttrib1f(self, int, float) """
        pass

    def glVertexAttrib1fv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib1fv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib1s(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glVertexAttrib1s(self, int, int) """
        pass

    def glVertexAttrib1sv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib1sv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib2d(self, p_int, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glVertexAttrib2d(self, int, float, float) """
        pass

    def glVertexAttrib2dv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib2dv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib2f(self, p_int, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glVertexAttrib2f(self, int, float, float) """
        pass

    def glVertexAttrib2fv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib2fv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib2s(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glVertexAttrib2s(self, int, int, int) """
        pass

    def glVertexAttrib2sv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib2sv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib3d(self, p_int, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glVertexAttrib3d(self, int, float, float, float) """
        pass

    def glVertexAttrib3dv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib3dv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib3f(self, p_int, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glVertexAttrib3f(self, int, float, float, float) """
        pass

    def glVertexAttrib3fv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib3fv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib3s(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glVertexAttrib3s(self, int, int, int, int) """
        pass

    def glVertexAttrib3sv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib3sv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4bv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4bv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4d(self, p_int, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glVertexAttrib4d(self, int, float, float, float, float) """
        pass

    def glVertexAttrib4dv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4dv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4f(self, p_int, p_float, p_float_1, p_float_2, p_float_3): # real signature unknown; restored from __doc__
        """ glVertexAttrib4f(self, int, float, float, float, float) """
        pass

    def glVertexAttrib4fv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4fv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4iv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4iv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4Nbv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4Nbv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4Niv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4Niv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4Nsv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4Nsv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4Nub(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glVertexAttrib4Nub(self, int, int, int, int, int) """
        pass

    def glVertexAttrib4Nubv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4Nubv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4Nuiv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4Nuiv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4Nusv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4Nusv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4s(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4): # real signature unknown; restored from __doc__
        """ glVertexAttrib4s(self, int, int, int, int, int) """
        pass

    def glVertexAttrib4sv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4sv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4ubv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4ubv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4uiv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4uiv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttrib4usv(self, p_int, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttrib4usv(self, int, PYQT_OPENGL_ARRAY) """
        pass

    def glVertexAttribPointer(self, p_int, p_int_1, p_int_2, p_int_3, p_int_4, PYQT_OPENGL_BOUND_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexAttribPointer(self, int, int, int, int, int, PYQT_OPENGL_BOUND_ARRAY) """
        pass

    def glVertexPointer(self, p_int, p_int_1, p_int_2, PYQT_OPENGL_BOUND_ARRAY): # real signature unknown; restored from __doc__
        """ glVertexPointer(self, int, int, int, PYQT_OPENGL_BOUND_ARRAY) """
        pass

    def glViewport(self, p_int, p_int_1, p_int_2, p_int_3): # real signature unknown; restored from __doc__
        """ glViewport(self, int, int, int, int) """
        pass

    def glWindowPos2d(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glWindowPos2d(self, float, float) """
        pass

    def glWindowPos2dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glWindowPos2dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glWindowPos2f(self, p_float, p_float_1): # real signature unknown; restored from __doc__
        """ glWindowPos2f(self, float, float) """
        pass

    def glWindowPos2fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glWindowPos2fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glWindowPos2i(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glWindowPos2i(self, int, int) """
        pass

    def glWindowPos2iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glWindowPos2iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glWindowPos2s(self, p_int, p_int_1): # real signature unknown; restored from __doc__
        """ glWindowPos2s(self, int, int) """
        pass

    def glWindowPos2sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glWindowPos2sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glWindowPos3d(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glWindowPos3d(self, float, float, float) """
        pass

    def glWindowPos3dv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glWindowPos3dv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glWindowPos3f(self, p_float, p_float_1, p_float_2): # real signature unknown; restored from __doc__
        """ glWindowPos3f(self, float, float, float) """
        pass

    def glWindowPos3fv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glWindowPos3fv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glWindowPos3i(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glWindowPos3i(self, int, int, int) """
        pass

    def glWindowPos3iv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glWindowPos3iv(self, PYQT_OPENGL_ARRAY) """
        pass

    def glWindowPos3s(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ glWindowPos3s(self, int, int, int) """
        pass

    def glWindowPos3sv(self, PYQT_OPENGL_ARRAY): # real signature unknown; restored from __doc__
        """ glWindowPos3sv(self, PYQT_OPENGL_ARRAY) """
        pass

    def initializeOpenGLFunctions(self): # real signature unknown; restored from __doc__
        """ initializeOpenGLFunctions(self) -> bool """
        return False

    def __init__(self): # real signature unknown; restored from __doc__
        pass


# variables with complex values



