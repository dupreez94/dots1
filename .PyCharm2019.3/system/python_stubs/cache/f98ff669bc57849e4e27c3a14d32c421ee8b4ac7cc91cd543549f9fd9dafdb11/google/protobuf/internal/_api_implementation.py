# encoding: utf-8
# module google.protobuf.internal._api_implementation
# from /usr/lib/python3.8/site-packages/google/protobuf/internal/_api_implementation.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
"""
_api_implementation is a module that exposes compile-time constants that
determine the default API implementation to use for Python proto2.

It complements api_implementation.py by setting defaults using compile-time
constants defined in C, such that one can set defaults at compilation
(e.g. with blaze flag --copt=-DPYTHON_PROTO2_CPP_IMPL_V2).
"""
# no imports

# Variables with simple values

api_version = 2

# no functions
# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1520>'

__spec__ = None # (!) real value is "ModuleSpec(name='google.protobuf.internal._api_implementation', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1520>, origin='/usr/lib/python3.8/site-packages/google/protobuf/internal/_api_implementation.cpython-38-x86_64-linux-gnu.so')"

