# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


from .alert import alert

class listen_failed_alert(alert):
    # no doc
    def listen_interface(self, listen_failed_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        listen_interface( (listen_failed_alert)arg1) -> str :
        
            C++ signature :
                char const* listen_interface(libtorrent::listen_failed_alert {lvalue})
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    address = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    endpoint = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    error = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    op = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    operation = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    port = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    socket_type = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    sock_type = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default



