# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class tracker_source(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    names = {
        'source_client': 2,
        'source_magnet_link': 4,
        'source_tex': 8,
        'source_torrent': 1,
    }
    source_client = 2
    source_magnet_link = 4
    source_tex = 8
    source_torrent = 1
    values = {
        1: 1,
        2: 2,
        4: 4,
        8: 8,
    }
    __slots__ = ()


