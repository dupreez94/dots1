# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class ip_filter(__Boost_Python.instance):
    # no doc
    def access(self, ip_filter, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        access( (ip_filter)arg1, (str)arg2) -> int :
        
            C++ signature :
                int access(libtorrent::ip_filter {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def add_rule(self, ip_filter, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_rule( (ip_filter)arg1, (str)arg2, (str)arg3, (int)arg4) -> None :
        
            C++ signature :
                void add_rule(libtorrent::ip_filter {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,int)
        """
        pass

    def export_filter(self, ip_filter, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        export_filter( (ip_filter)arg1) -> object :
        
            C++ signature :
                std::tuple<std::vector<libtorrent::ip_range<boost::asio::ip::address_v4>, std::allocator<libtorrent::ip_range<boost::asio::ip::address_v4> > >, std::vector<libtorrent::ip_range<boost::asio::ip::address_v6>, std::allocator<libtorrent::ip_range<boost::asio::ip::address_v6> > > > export_filter(libtorrent::ip_filter {lvalue})
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    __instance_size__ = 112


