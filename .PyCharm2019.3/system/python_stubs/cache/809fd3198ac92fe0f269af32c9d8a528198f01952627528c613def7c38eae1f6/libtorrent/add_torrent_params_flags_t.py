# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class add_torrent_params_flags_t(__Boost_Python.instance):
    # no doc
    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    default_flags = 352440
    flag_apply_ip_filter = 8
    flag_auto_managed = 32
    flag_duplicate_is_error = 64
    flag_merge_resume_http_seeds = 262144
    flag_merge_resume_trackers = 65536
    flag_override_resume_data = 32768
    flag_override_trackers = 2048
    flag_override_web_seeds = 4096
    flag_paused = 16
    flag_pinned = 16384
    flag_seed_mode = 1
    flag_sequential_download = 512
    flag_share_mode = 4
    flag_stop_when_ready = 1024
    flag_super_seeding = 256
    flag_update_subscribe = 128
    flag_upload_mode = 2
    flag_use_resume_save_path = 131072
    __instance_size__ = 24


