# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class enc_level(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    both = 3
    names = {
        'both': 3,
        'pe_both': 3,
        'pe_plaintext': 1,
        'pe_rc4': 2,
        'plaintext': 1,
        'rc4': 2,
    }
    pe_both = 3
    pe_plaintext = 1
    pe_rc4 = 2
    plaintext = 1
    rc4 = 2
    values = {
        1: 1,
        2: 2,
        3: 3,
    }
    __slots__ = ()


