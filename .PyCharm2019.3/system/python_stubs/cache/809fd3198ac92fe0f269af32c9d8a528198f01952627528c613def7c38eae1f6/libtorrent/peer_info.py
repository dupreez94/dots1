# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class peer_info(__Boost_Python.instance):
    # no doc
    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    client = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    connection_type = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    downloading_block_index = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    downloading_piece_index = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    downloading_progress = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    downloading_total = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    download_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    download_queue_length = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    download_queue_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    download_rate_peak = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    down_speed = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    estimated_reciprocation_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    failcount = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    flags = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    ip = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    last_active = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    last_request = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    load_balancing = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    local_endpoint = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_hashfails = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_pieces = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    payload_down_speed = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    payload_up_speed = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    pending_disk_bytes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    pid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    pieces = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    progress = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    progress_ppm = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    queue_bytes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    read_state = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    receive_buffer_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    receive_quota = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    remote_dl_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    request_timeout = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    rtt = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    send_buffer_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    send_quota = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    source = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    upload_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    upload_queue_length = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    upload_rate_peak = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    up_speed = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    used_receive_buffer = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    used_send_buffer = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    write_state = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    bw_disk = 16
    bw_global = 2
    bw_idle = 1
    bw_limit = 2
    bw_network = 4
    bw_torrent = 2
    choked = 2
    connecting = 128
    dht = 2
    endgame_mode = 16384
    handshake = 64
    holepunched = 32768
    interesting = 1
    local_connection = 32
    lsd = 8
    on_parole = 512
    optimistic_unchoke = 2048
    pex = 4
    plaintext_encrypted = 1048576
    queued = 256
    rc4_encrypted = 524288
    remote_choked = 8
    remote_interested = 4
    resume_data = 16
    seed = 1024
    snubbed = 4096
    standard_bittorrent = 0
    supports_extensions = 16
    tracker = 1
    upload_only = 8192
    web_seed = 1
    __instance_size__ = 344


