# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class torrent_status(__Boost_Python.instance):
    # no doc
    def __eq__(self, torrent_status, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (torrent_status)arg1, (torrent_status)arg2) -> object :
        
            C++ signature :
                _object* __eq__(libtorrent::torrent_status {lvalue},libtorrent::torrent_status)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    active_duration = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    active_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    added_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    all_time_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    all_time_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    announce_interval = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    announcing_to_dht = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    announcing_to_lsd = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    announcing_to_trackers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    auto_managed = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    block_size = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    completed_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    connections_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    connect_candidates = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    current_tracker = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    distributed_copies = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    distributed_fraction = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    distributed_full_copies = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    download_payload_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    download_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    down_bandwidth_queue = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    errc = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    error = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    error_file = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    finished_duration = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    finished_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    flags = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    handle = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    has_incoming = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    has_metadata = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    info_hash = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    ip_filter_applies = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    is_finished = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    is_loaded = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    is_seeding = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    last_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    last_scrape = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    last_seen_complete = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    last_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    list_peers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    list_seeds = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    moving_storage = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    need_save_resume = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    next_announce = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_complete = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_connections = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_incomplete = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_peers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_pieces = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_seeds = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_uploads = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    paused = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    pieces = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    priority = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    progress = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    progress_ppm = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    queue_position = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    save_path = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    seeding_duration = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    seeding_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    seed_mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    seed_rank = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    sequential_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    share_mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    state = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    stop_when_ready = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    storage_mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    super_seeding = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    time_since_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    time_since_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    torrent_file = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_done = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_failed_bytes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_payload_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_payload_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_redundant_bytes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_wanted = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_wanted_done = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    uploads_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    upload_mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    upload_payload_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    upload_rate = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    up_bandwidth_queue = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    verified_pieces = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    allocating = 6
    checking_files = 1
    checking_resume_data = 7
    downloading = 3
    downloading_metadata = 2
    finished = 4
    queued_for_checking = 0
    seeding = 5
    states = None # (!) real value is "<class 'libtorrent.states'>"
    __instance_size__ = 584


