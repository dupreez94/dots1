# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class move_flags_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    always_replace_files = 0
    dont_replace = 2
    fail_if_exist = 1
    names = {
        'always_replace_files': 0,
        'dont_replace': 2,
        'fail_if_exist': 1,
    }
    values = {
        0: 0,
        1: 1,
        2: 2,
    }
    __slots__ = ()


