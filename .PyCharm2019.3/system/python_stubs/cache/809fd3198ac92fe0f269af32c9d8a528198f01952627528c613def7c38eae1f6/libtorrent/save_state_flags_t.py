# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class save_state_flags_t(__Boost_Python.instance):
    # no doc
    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    save_as_map = 16
    save_dht_proxy = 128
    save_dht_settings = 2
    save_dht_state = 4
    save_encryption_settings = 8
    save_i2p_proxy = 64
    save_peer_proxy = 256
    save_proxy = 32
    save_settings = 1
    save_tracker_proxy = 1024
    save_web_proxy = 512
    __instance_size__ = 24


