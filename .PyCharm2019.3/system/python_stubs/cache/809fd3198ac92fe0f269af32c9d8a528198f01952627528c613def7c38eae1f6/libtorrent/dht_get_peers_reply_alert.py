# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


from .alert import alert

class dht_get_peers_reply_alert(alert):
    # no doc
    def num_peers(self, dht_get_peers_reply_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        num_peers( (dht_get_peers_reply_alert)arg1) -> int :
        
            C++ signature :
                int num_peers(libtorrent::dht_get_peers_reply_alert {lvalue})
        """
        pass

    def peers(self, dht_get_peers_reply_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        peers( (dht_get_peers_reply_alert)arg1) -> object :
        
            C++ signature :
                std::vector<boost::asio::ip::basic_endpoint<boost::asio::ip::tcp>, std::allocator<boost::asio::ip::basic_endpoint<boost::asio::ip::tcp> > > peers(libtorrent::dht_get_peers_reply_alert {lvalue})
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    info_hash = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default



