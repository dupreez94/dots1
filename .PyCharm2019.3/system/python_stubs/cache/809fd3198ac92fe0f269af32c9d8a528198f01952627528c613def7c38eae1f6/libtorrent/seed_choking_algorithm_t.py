# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class seed_choking_algorithm_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    anti_leech = 2
    fastest_upload = 1
    names = {
        'anti_leech': 2,
        'fastest_upload': 1,
        'round_robin': 0,
    }
    round_robin = 0
    values = {
        0: 0,
        1: 1,
        2: 2,
    }
    __slots__ = ()


