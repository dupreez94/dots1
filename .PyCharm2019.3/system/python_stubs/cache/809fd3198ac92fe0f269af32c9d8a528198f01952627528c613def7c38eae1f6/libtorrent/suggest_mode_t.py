# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class suggest_mode_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    names = {
        'no_piece_suggestions': 0,
        'suggest_read_cache': 1,
    }
    no_piece_suggestions = 0
    suggest_read_cache = 1
    values = {
        0: 0,
        1: 1,
    }
    __slots__ = ()


