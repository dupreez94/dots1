# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class add_torrent_params(__Boost_Python.instance):
    # no doc
    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    active_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    added_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    banned_peers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    completed_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    dht_nodes = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    download_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    file_priorities = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    finished_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    flags = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    have_pieces = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    http_seeds = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    info_hash = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    last_download = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    last_seen_complete = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    last_upload = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_connections = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    max_uploads = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    merkle_tree = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_complete = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_downloaded = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    num_incomplete = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    peers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    piece_priorities = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    renamed_files = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    resume_data = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    save_path = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    seeding_time = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    storage_mode = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    ti = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_downloaded = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    total_uploaded = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    trackerid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    trackers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    tracker_tiers = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    unfinished_pieces = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    upload_limit = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    url = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    url_seeds = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    uuid = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    verified_pieces = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    version = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 800


