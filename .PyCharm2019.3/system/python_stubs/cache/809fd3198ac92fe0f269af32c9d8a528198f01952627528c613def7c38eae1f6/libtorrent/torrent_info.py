# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class torrent_info(__Boost_Python.instance):
    # no doc
    def add_http_seed(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_http_seed( (torrent_info)arg1, (str)arg2, (str)arg3, (object)arg4) -> None :
        
            C++ signature :
                void add_http_seed(libtorrent::torrent_info {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::vector<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > >)
        """
        pass

    def add_node(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_node( (torrent_info)arg1, (str)arg2, (int)arg3) -> None :
        
            C++ signature :
                void add_node(libtorrent::torrent_info {lvalue},char const*,int)
        """
        pass

    def add_tracker(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_tracker( (torrent_info)arg1, (str)arg2, (int)arg3, (tracker_source)url) -> None :
        
            C++ signature :
                void add_tracker(libtorrent::torrent_info {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,int,libtorrent::announce_entry::tracker_source)
        """
        pass

    def add_url_seed(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_url_seed( (torrent_info)arg1, (str)arg2, (str)arg3, (object)arg4) -> None :
        
            C++ signature :
                void add_url_seed(libtorrent::torrent_info {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::vector<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > >, std::allocator<std::pair<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > >)
        """
        pass

    def collections(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        collections( (torrent_info)arg1) -> object :
        
            C++ signature :
                std::vector<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >, std::allocator<std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > > > collections(libtorrent::torrent_info {lvalue})
        """
        pass

    def comment(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        comment( (torrent_info)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > comment(libtorrent::torrent_info {lvalue})
        """
        pass

    def creation_date(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        creation_date( (torrent_info)arg1) -> int :
        
            C++ signature :
                long creation_date(libtorrent::torrent_info {lvalue})
        """
        pass

    def creator(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        creator( (torrent_info)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > creator(libtorrent::torrent_info {lvalue})
        """
        pass

    def files(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        files( (torrent_info)arg1) -> file_storage :
        
            C++ signature :
                libtorrent::file_storage files(libtorrent::torrent_info {lvalue})
        """
        pass

    def file_at(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_at( (torrent_info)arg1, (int)arg2) -> file_entry :
        
            C++ signature :
                libtorrent::file_entry file_at(libtorrent::torrent_info {lvalue},int)
        """
        pass

    def file_at_offset(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_at_offset( (torrent_info)arg1, (int)arg2) -> object :
        
            C++ signature :
                __gnu_cxx::__normal_iterator<libtorrent::internal_file_entry const*, std::vector<libtorrent::internal_file_entry, std::allocator<libtorrent::internal_file_entry> > > file_at_offset(libtorrent::torrent_info {lvalue},long)
        """
        pass

    def hash_for_piece(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        hash_for_piece( (torrent_info)arg1, (object)arg2) -> object :
        
            C++ signature :
                bytes hash_for_piece(libtorrent::torrent_info,libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>)
        """
        pass

    def info_hash(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        info_hash( (torrent_info)arg1) -> sha1_hash :
        
            C++ signature :
                libtorrent::digest32<160l> info_hash(libtorrent::torrent_info {lvalue})
        """
        pass

    def is_i2p(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_i2p( (torrent_info)arg1) -> bool :
        
            C++ signature :
                bool is_i2p(libtorrent::torrent_info {lvalue})
        """
        pass

    def is_merkle_torrent(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_merkle_torrent( (torrent_info)arg1) -> bool :
        
            C++ signature :
                bool is_merkle_torrent(libtorrent::torrent_info {lvalue})
        """
        pass

    def is_valid(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_valid( (torrent_info)arg1) -> bool :
        
            C++ signature :
                bool is_valid(libtorrent::torrent_info {lvalue})
        """
        pass

    def map_block(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        map_block( (torrent_info)arg1, (object)arg2, (int)arg3, (int)arg4) -> list :
        
            C++ signature :
                boost::python::list map_block(libtorrent::torrent_info {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>,long,int)
        """
        pass

    def map_file(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        map_file( (torrent_info)arg1, (object)arg2, (int)arg3, (int)arg4) -> peer_request :
        
            C++ signature :
                libtorrent::peer_request map_file(libtorrent::torrent_info {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>,long,int)
        """
        pass

    def merkle_tree(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        merkle_tree( (torrent_info)arg1) -> list :
        
            C++ signature :
                boost::python::list merkle_tree(libtorrent::torrent_info)
        """
        pass

    def metadata(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        metadata( (torrent_info)arg1) -> object :
        
            C++ signature :
                bytes metadata(libtorrent::torrent_info)
        """
        pass

    def metadata_size(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        metadata_size( (torrent_info)arg1) -> int :
        
            C++ signature :
                int metadata_size(libtorrent::torrent_info {lvalue})
        """
        pass

    def name(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        name( (torrent_info)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > name(libtorrent::torrent_info {lvalue})
        """
        pass

    def nodes(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        nodes( (torrent_info)arg1) -> list :
        
            C++ signature :
                boost::python::list nodes(libtorrent::torrent_info)
        """
        pass

    def num_files(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        num_files( (torrent_info)arg1) -> int :
        
            C++ signature :
                int num_files(libtorrent::torrent_info {lvalue})
        """
        pass

    def num_pieces(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        num_pieces( (torrent_info)arg1) -> int :
        
            C++ signature :
                int num_pieces(libtorrent::torrent_info {lvalue})
        """
        pass

    def orig_files(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        orig_files( (torrent_info)arg1) -> file_storage :
        
            C++ signature :
                libtorrent::file_storage orig_files(libtorrent::torrent_info {lvalue})
        """
        pass

    def piece_length(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        piece_length( (torrent_info)arg1) -> int :
        
            C++ signature :
                int piece_length(libtorrent::torrent_info {lvalue})
        """
        pass

    def piece_size(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        piece_size( (torrent_info)arg1, (object)arg2) -> int :
        
            C++ signature :
                int piece_size(libtorrent::torrent_info {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>)
        """
        pass

    def priv(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        priv( (torrent_info)arg1) -> bool :
        
            C++ signature :
                bool priv(libtorrent::torrent_info {lvalue})
        """
        pass

    def remap_files(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        remap_files( (torrent_info)arg1, (file_storage)arg2) -> None :
        
            C++ signature :
                void remap_files(libtorrent::torrent_info {lvalue},libtorrent::file_storage)
        """
        pass

    def rename_file(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rename_file( (torrent_info)arg1, (object)arg2, (str)arg3) -> None :
        
            C++ signature :
                void rename_file(libtorrent::torrent_info {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        
        rename_file( (torrent_info)arg1, (object)arg2, (str)arg3) -> None :
        
            C++ signature :
                void rename_file(libtorrent::torrent_info {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>,std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >)
        """
        pass

    def set_merkle_tree(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_merkle_tree( (torrent_info)arg1, (list)arg2) -> None :
        
            C++ signature :
                void set_merkle_tree(libtorrent::torrent_info {lvalue},boost::python::list)
        """
        pass

    def set_web_seeds(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_web_seeds( (torrent_info)arg1, (list)arg2) -> None :
        
            C++ signature :
                void set_web_seeds(libtorrent::torrent_info {lvalue},boost::python::list)
        """
        pass

    def similar_torrents(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        similar_torrents( (torrent_info)arg1) -> object :
        
            C++ signature :
                std::vector<libtorrent::digest32<160l>, std::allocator<libtorrent::digest32<160l> > > similar_torrents(libtorrent::torrent_info {lvalue})
        """
        pass

    def ssl_cert(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ssl_cert( (torrent_info)arg1) -> object :
        
            C++ signature :
                boost::basic_string_view<char, std::char_traits<char> > ssl_cert(libtorrent::torrent_info {lvalue})
        """
        pass

    def total_size(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        total_size( (torrent_info)arg1) -> int :
        
            C++ signature :
                long total_size(libtorrent::torrent_info {lvalue})
        """
        pass

    def trackers(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        trackers( (object)arg1) -> object :
        
            C++ signature :
                boost::python::objects::iterator_range<boost::python::return_value_policy<boost::python::return_by_value, boost::python::default_call_policies>, __gnu_cxx::__normal_iterator<libtorrent::announce_entry const*, std::vector<libtorrent::announce_entry, std::allocator<libtorrent::announce_entry> > > > trackers(boost::python::back_reference<libtorrent::torrent_info&>)
        """
        pass

    def web_seeds(self, torrent_info, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        web_seeds( (torrent_info)arg1) -> list :
        
            C++ signature :
                boost::python::list web_seeds(libtorrent::torrent_info)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (sha1_hash)info_hash) -> None :
        
            C++ signature :
                void __init__(_object*,libtorrent::digest32<160l>)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,libtorrent::entry)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,bytes)
        
        __init__( (object)arg1, (str)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        
        __init__( (object)arg1, (torrent_info)ti) -> None :
        
            C++ signature :
                void __init__(_object*,libtorrent::torrent_info)
        
        __init__( (object)arg1, (str)file) -> None :
        
            C++ signature :
                void __init__(_object*,std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass


