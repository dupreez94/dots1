# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class stats_channel(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    download_dht_protocol = 8
    download_ip_protocol = 7
    download_payload = 2
    download_protocol = 3
    download_tracker_protocol = 9
    names = {
        'download_dht_protocol': 8,
        'download_ip_protocol': 7,
        'download_payload': 2,
        'download_protocol': 3,
        'download_tracker_protocol': 9,
        'upload_dht_protocol': 5,
        'upload_ip_protocol': 4,
        'upload_payload': 0,
        'upload_protocol': 1,
        'upload_tracker_protocol': 6,
    }
    upload_dht_protocol = 5
    upload_ip_protocol = 4
    upload_payload = 0
    upload_protocol = 1
    upload_tracker_protocol = 6
    values = {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        5: 5,
        6: 6,
        7: 7,
        8: 8,
        9: 9,
    }
    __slots__ = ()


