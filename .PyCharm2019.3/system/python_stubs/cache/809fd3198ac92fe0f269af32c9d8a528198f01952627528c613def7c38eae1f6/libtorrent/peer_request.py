# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class peer_request(__Boost_Python.instance):
    # no doc
    def __eq__(self, peer_request, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (peer_request)arg1, (peer_request)arg2) -> object :
        
            C++ signature :
                _object* __eq__(libtorrent::peer_request {lvalue},libtorrent::peer_request)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    length = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    piece = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    start = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 32


