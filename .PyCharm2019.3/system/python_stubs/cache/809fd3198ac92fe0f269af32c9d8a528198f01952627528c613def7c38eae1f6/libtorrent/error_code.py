# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class error_code(__Boost_Python.instance):
    # no doc
    def assign(self, error_code, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        assign( (error_code)arg1, (int)arg2, (error_category)arg3) -> None :
        
            C++ signature :
                void assign(boost::system::error_code {lvalue},int,category_holder)
        """
        pass

    def category(self, error_code, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        category( (error_code)arg1) -> error_category :
        
            C++ signature :
                category_holder category(boost::system::error_code)
        """
        pass

    def clear(self, error_code, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        clear( (error_code)arg1) -> None :
        
            C++ signature :
                void clear(boost::system::error_code {lvalue})
        """
        pass

    def message(self, error_code, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        message( (error_code)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > message(boost::system::error_code {lvalue})
        """
        pass

    def value(self, error_code, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        value( (error_code)arg1) -> int :
        
            C++ signature :
                int value(boost::system::error_code {lvalue})
        """
        pass

    def __getinitargs__(self, error_code, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getinitargs__( (error_code)arg1) -> tuple :
        
            C++ signature :
                boost::python::tuple __getinitargs__(boost::system::error_code)
        """
        pass

    def __getstate__(self, error_code, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getstate__( (error_code)arg1) -> tuple :
        
            C++ signature :
                boost::python::tuple __getstate__(boost::system::error_code)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (int)arg2, (error_category)arg3) -> None :
        
            C++ signature :
                void __init__(_object*,int,category_holder)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setstate__(self, error_code, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setstate__( (error_code)arg1, (tuple)arg2) -> None :
        
            C++ signature :
                void __setstate__(boost::system::error_code {lvalue},boost::python::tuple)
        """
        pass

    __instance_size__ = 32
    __safe_for_unpickling__ = True


