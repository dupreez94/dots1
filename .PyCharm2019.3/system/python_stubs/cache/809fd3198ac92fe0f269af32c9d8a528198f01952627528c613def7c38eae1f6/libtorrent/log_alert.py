# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


from .alert import alert

class log_alert(alert):
    # no doc
    def log_message(self, log_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        log_message( (log_alert)arg1) -> str :
        
            C++ signature :
                char const* log_message(libtorrent::log_alert {lvalue})
        """
        pass

    def msg(self, log_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        msg( (log_alert)arg1) -> str :
        
            C++ signature :
                char const* msg(libtorrent::log_alert {lvalue})
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass


