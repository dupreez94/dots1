# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class torrent_handle(__Boost_Python.instance):
    # no doc
    def add_http_seed(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_http_seed( (torrent_handle)arg1, (str)arg2) -> None :
        
            C++ signature :
                void add_http_seed(libtorrent::torrent_handle {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def add_piece(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_piece( (torrent_handle)arg1, (object)arg2, (str)arg3, (object)arg4) -> None :
        
            C++ signature :
                void add_piece(libtorrent::torrent_handle {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>,char const*,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::add_piece_flags_tag, void>)
        """
        pass

    def add_tracker(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_tracker( (torrent_handle)arg1, (dict)arg2) -> None :
        
            C++ signature :
                void add_tracker(libtorrent::torrent_handle {lvalue},boost::python::dict)
        """
        pass

    def add_url_seed(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_url_seed( (torrent_handle)arg1, (str)arg2) -> None :
        
            C++ signature :
                void add_url_seed(libtorrent::torrent_handle {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def apply_ip_filter(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        apply_ip_filter( (torrent_handle)arg1, (bool)arg2) -> None :
        
            C++ signature :
                void apply_ip_filter(libtorrent::torrent_handle {lvalue},bool)
        """
        pass

    def auto_managed(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        auto_managed( (torrent_handle)arg1, (bool)arg2) -> None :
        
            C++ signature :
                void auto_managed(libtorrent::torrent_handle {lvalue},bool)
        """
        pass

    def clear_error(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        clear_error( (torrent_handle)arg1) -> None :
        
            C++ signature :
                void clear_error(libtorrent::torrent_handle {lvalue})
        """
        pass

    def clear_piece_deadlines(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        clear_piece_deadlines( (torrent_handle)index) -> None :
        
            C++ signature :
                void clear_piece_deadlines(libtorrent::torrent_handle {lvalue})
        """
        pass

    def connect_peer(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        connect_peer( (torrent_handle)arg1, (object)endpoint [, (object)source=0 [, (object)flags=13]]) -> None :
        
            C++ signature :
                void connect_peer(libtorrent::torrent_handle {lvalue},boost::asio::ip::basic_endpoint<boost::asio::ip::tcp> [,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::peer_source_flags_tag, void>=0 [,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::pex_flags_tag, void>=13]])
        """
        pass

    def download_limit(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        download_limit( (torrent_handle)arg1) -> int :
        
            C++ signature :
                int download_limit(libtorrent::torrent_handle {lvalue})
        """
        pass

    def file_priorities(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_priorities( (torrent_handle)arg1) -> list :
        
            C++ signature :
                boost::python::list file_priorities(libtorrent::torrent_handle {lvalue})
        """
        pass

    def file_priority(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_priority( (torrent_handle)arg1, (object)arg2) -> object :
        
            C++ signature :
                libtorrent::aux::strong_typedef<unsigned char, libtorrent::download_priority_tag, void> file_priority(libtorrent::torrent_handle {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>)
        
        file_priority( (torrent_handle)arg1, (object)arg2, (object)arg3) -> None :
        
            C++ signature :
                void file_priority(libtorrent::torrent_handle {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>,libtorrent::aux::strong_typedef<unsigned char, libtorrent::download_priority_tag, void>)
        """
        pass

    def file_progress(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_progress( (torrent_handle)arg1 [, (int)flags=0]) -> list :
        
            C++ signature :
                boost::python::list file_progress(libtorrent::torrent_handle {lvalue} [,int=0])
        """
        pass

    def file_status(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_status( (torrent_handle)arg1) -> object :
        
            C++ signature :
                std::vector<libtorrent::open_file_state, std::allocator<libtorrent::open_file_state> > file_status(libtorrent::torrent_handle {lvalue})
        """
        pass

    def flags(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        flags( (torrent_handle)arg1) -> object :
        
            C++ signature :
                libtorrent::flags::bitfield_flag<unsigned long, libtorrent::torrent_flags_tag, void> flags(libtorrent::torrent_handle {lvalue})
        """
        pass

    def flush_cache(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        flush_cache( (torrent_handle)arg1) -> None :
        
            C++ signature :
                void flush_cache(libtorrent::torrent_handle {lvalue})
        """
        pass

    def force_dht_announce(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        force_dht_announce( (torrent_handle)arg1) -> None :
        
            C++ signature :
                void force_dht_announce(libtorrent::torrent_handle {lvalue})
        """
        pass

    def force_reannounce(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        force_reannounce( (torrent_handle)arg1 [, (int)seconds=0 [, (int)tracker_idx=-1 [, (object)flags=0]]]) -> None :
        
            C++ signature :
                void force_reannounce(libtorrent::torrent_handle {lvalue} [,int=0 [,int=-1 [,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::reannounce_flags_tag, void>=0]]])
        """
        pass

    def force_recheck(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        force_recheck( (torrent_handle)arg1) -> None :
        
            C++ signature :
                void force_recheck(libtorrent::torrent_handle {lvalue})
        """
        pass

    def get_download_queue(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_download_queue( (torrent_handle)arg1) -> list :
        
            C++ signature :
                boost::python::list get_download_queue(libtorrent::torrent_handle {lvalue})
        """
        pass

    def get_file_priorities(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_file_priorities( (torrent_handle)arg1) -> list :
        
            C++ signature :
                boost::python::list get_file_priorities(libtorrent::torrent_handle {lvalue})
        """
        pass

    def get_peer_info(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_peer_info( (torrent_handle)arg1) -> list :
        
            C++ signature :
                boost::python::list get_peer_info(libtorrent::torrent_handle)
        """
        pass

    def get_piece_priorities(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_piece_priorities( (torrent_handle)arg1) -> list :
        
            C++ signature :
                boost::python::list get_piece_priorities(libtorrent::torrent_handle {lvalue})
        """
        pass

    def get_torrent_info(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_torrent_info( (torrent_handle)arg1) -> torrent_info :
        
            C++ signature :
                std::shared_ptr<libtorrent::torrent_info const> get_torrent_info(libtorrent::torrent_handle)
        """
        pass

    def has_metadata(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        has_metadata( (torrent_handle)arg1) -> bool :
        
            C++ signature :
                bool has_metadata(libtorrent::torrent_handle {lvalue})
        """
        pass

    def have_piece(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        have_piece( (torrent_handle)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool have_piece(libtorrent::torrent_handle {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>)
        """
        pass

    def http_seeds(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        http_seeds( (torrent_handle)arg1) -> list :
        
            C++ signature :
                boost::python::list http_seeds(libtorrent::torrent_handle {lvalue})
        """
        pass

    def info_hash(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        info_hash( (torrent_handle)arg1) -> sha1_hash :
        
            C++ signature :
                libtorrent::digest32<160l> info_hash(libtorrent::torrent_handle {lvalue})
        """
        pass

    def is_auto_managed(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_auto_managed( (torrent_handle)arg1) -> bool :
        
            C++ signature :
                bool is_auto_managed(libtorrent::torrent_handle {lvalue})
        """
        pass

    def is_finished(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_finished( (torrent_handle)arg1) -> bool :
        
            C++ signature :
                bool is_finished(libtorrent::torrent_handle {lvalue})
        """
        pass

    def is_paused(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_paused( (torrent_handle)arg1) -> bool :
        
            C++ signature :
                bool is_paused(libtorrent::torrent_handle {lvalue})
        """
        pass

    def is_seed(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_seed( (torrent_handle)arg1) -> bool :
        
            C++ signature :
                bool is_seed(libtorrent::torrent_handle {lvalue})
        """
        pass

    def is_valid(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_valid( (torrent_handle)arg1) -> bool :
        
            C++ signature :
                bool is_valid(libtorrent::torrent_handle {lvalue})
        """
        pass

    def max_connections(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max_connections( (torrent_handle)arg1) -> int :
        
            C++ signature :
                int max_connections(libtorrent::torrent_handle {lvalue})
        """
        pass

    def max_uploads(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max_uploads( (torrent_handle)arg1) -> int :
        
            C++ signature :
                int max_uploads(libtorrent::torrent_handle {lvalue})
        """
        pass

    def move_storage(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        move_storage( (torrent_handle)arg1, (str)path [, (move_flags_t)flags=libtorrent.move_flags_t.always_replace_files]) -> None :
        
            C++ signature :
                void move_storage(libtorrent::torrent_handle {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > [,libtorrent::move_flags_t=libtorrent.move_flags_t.always_replace_files])
        
        move_storage( (torrent_handle)arg1, (str)path [, (int)flags=libtorrent.deprecated_move_flags_t.always_replace_files]) -> None :
        
            C++ signature :
                void move_storage(libtorrent::torrent_handle {lvalue},std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> > [,int=libtorrent.deprecated_move_flags_t.always_replace_files])
        """
        pass

    def name(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        name( (torrent_handle)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > name(libtorrent::torrent_handle {lvalue})
        """
        pass

    def need_save_resume_data(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        need_save_resume_data( (torrent_handle)arg1) -> bool :
        
            C++ signature :
                bool need_save_resume_data(libtorrent::torrent_handle {lvalue})
        """
        pass

    def pause(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        pause( (torrent_handle)arg1 [, (object)flags=0]) -> None :
        
            C++ signature :
                void pause(libtorrent::torrent_handle {lvalue} [,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::pause_flags_tag, void>=0])
        """
        pass

    def piece_availability(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        piece_availability( (torrent_handle)arg1) -> list :
        
            C++ signature :
                boost::python::list piece_availability(libtorrent::torrent_handle {lvalue})
        """
        pass

    def piece_priorities(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        piece_priorities( (torrent_handle)arg1) -> list :
        
            C++ signature :
                boost::python::list piece_priorities(libtorrent::torrent_handle {lvalue})
        """
        pass

    def piece_priority(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        piece_priority( (torrent_handle)arg1, (object)arg2) -> object :
        
            C++ signature :
                libtorrent::aux::strong_typedef<unsigned char, libtorrent::download_priority_tag, void> piece_priority(libtorrent::torrent_handle {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>)
        
        piece_priority( (torrent_handle)arg1, (object)arg2, (object)arg3) -> None :
        
            C++ signature :
                void piece_priority(libtorrent::torrent_handle {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>,libtorrent::aux::strong_typedef<unsigned char, libtorrent::download_priority_tag, void>)
        """
        pass

    def prioritize_files(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        prioritize_files( (torrent_handle)arg1, (object)arg2) -> None :
        
            C++ signature :
                void prioritize_files(libtorrent::torrent_handle {lvalue},boost::python::api::object)
        """
        pass

    def prioritize_pieces(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        prioritize_pieces( (torrent_handle)arg1, (object)arg2) -> None :
        
            C++ signature :
                void prioritize_pieces(libtorrent::torrent_handle {lvalue},boost::python::api::object)
        """
        pass

    def queue_position(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        queue_position( (torrent_handle)arg1) -> object :
        
            C++ signature :
                libtorrent::aux::strong_typedef<int, libtorrent::queue_position_tag, void> queue_position(libtorrent::torrent_handle {lvalue})
        """
        pass

    def queue_position_bottom(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        queue_position_bottom( (torrent_handle)arg1) -> None :
        
            C++ signature :
                void queue_position_bottom(libtorrent::torrent_handle {lvalue})
        """
        pass

    def queue_position_down(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        queue_position_down( (torrent_handle)arg1) -> None :
        
            C++ signature :
                void queue_position_down(libtorrent::torrent_handle {lvalue})
        """
        pass

    def queue_position_top(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        queue_position_top( (torrent_handle)arg1) -> None :
        
            C++ signature :
                void queue_position_top(libtorrent::torrent_handle {lvalue})
        """
        pass

    def queue_position_up(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        queue_position_up( (torrent_handle)arg1) -> None :
        
            C++ signature :
                void queue_position_up(libtorrent::torrent_handle {lvalue})
        """
        pass

    def read_piece(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        read_piece( (torrent_handle)arg1, (object)arg2) -> None :
        
            C++ signature :
                void read_piece(libtorrent::torrent_handle {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>)
        """
        pass

    def remove_http_seed(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        remove_http_seed( (torrent_handle)arg1, (str)arg2) -> None :
        
            C++ signature :
                void remove_http_seed(libtorrent::torrent_handle {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def remove_url_seed(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        remove_url_seed( (torrent_handle)arg1, (str)arg2) -> None :
        
            C++ signature :
                void remove_url_seed(libtorrent::torrent_handle {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def rename_file(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rename_file( (torrent_handle)arg1, (object)arg2, (str)arg3) -> None :
        
            C++ signature :
                void rename_file(libtorrent::torrent_handle {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        
        rename_file( (torrent_handle)arg1, (object)arg2, (str)arg3) -> None :
        
            C++ signature :
                void rename_file(libtorrent::torrent_handle {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>,std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >)
        """
        pass

    def replace_trackers(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        replace_trackers( (torrent_handle)arg1, (object)arg2) -> None :
        
            C++ signature :
                void replace_trackers(libtorrent::torrent_handle {lvalue},boost::python::api::object)
        """
        pass

    def reset_piece_deadline(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reset_piece_deadline( (torrent_handle)arg1, (object)index) -> None :
        
            C++ signature :
                void reset_piece_deadline(libtorrent::torrent_handle {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>)
        """
        pass

    def resume(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        resume( (torrent_handle)arg1) -> None :
        
            C++ signature :
                void resume(libtorrent::torrent_handle {lvalue})
        """
        pass

    def save_path(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        save_path( (torrent_handle)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > save_path(libtorrent::torrent_handle {lvalue})
        """
        pass

    def save_resume_data(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        save_resume_data( (torrent_handle)arg1 [, (object)flags=0]) -> None :
        
            C++ signature :
                void save_resume_data(libtorrent::torrent_handle {lvalue} [,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::resume_data_flags_tag, void>=0])
        """
        pass

    def scrape_tracker(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        scrape_tracker( (torrent_handle)arg1 [, (int)index=-1]) -> None :
        
            C++ signature :
                void scrape_tracker(libtorrent::torrent_handle {lvalue} [,int=-1])
        """
        pass

    def set_download_limit(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_download_limit( (torrent_handle)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_download_limit(libtorrent::torrent_handle {lvalue},int)
        """
        pass

    def set_flags(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_flags( (torrent_handle)arg1, (object)arg2) -> None :
        
            C++ signature :
                void set_flags(libtorrent::torrent_handle {lvalue},libtorrent::flags::bitfield_flag<unsigned long, libtorrent::torrent_flags_tag, void>)
        
        set_flags( (torrent_handle)arg1, (object)arg2, (object)arg3) -> None :
        
            C++ signature :
                void set_flags(libtorrent::torrent_handle {lvalue},libtorrent::flags::bitfield_flag<unsigned long, libtorrent::torrent_flags_tag, void>,libtorrent::flags::bitfield_flag<unsigned long, libtorrent::torrent_flags_tag, void>)
        """
        pass

    def set_max_connections(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_max_connections( (torrent_handle)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_max_connections(libtorrent::torrent_handle {lvalue},int)
        """
        pass

    def set_max_uploads(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_max_uploads( (torrent_handle)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_max_uploads(libtorrent::torrent_handle {lvalue},int)
        """
        pass

    def set_metadata(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_metadata( (torrent_handle)arg1, (str)arg2) -> None :
        
            C++ signature :
                void set_metadata(libtorrent::torrent_handle {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def set_peer_download_limit(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_peer_download_limit( (torrent_handle)arg1, (object)arg2, (int)arg3) -> None :
        
            C++ signature :
                void set_peer_download_limit(libtorrent::torrent_handle {lvalue},boost::asio::ip::basic_endpoint<boost::asio::ip::tcp>,int)
        """
        pass

    def set_peer_upload_limit(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_peer_upload_limit( (torrent_handle)arg1, (object)arg2, (int)arg3) -> None :
        
            C++ signature :
                void set_peer_upload_limit(libtorrent::torrent_handle {lvalue},boost::asio::ip::basic_endpoint<boost::asio::ip::tcp>,int)
        """
        pass

    def set_piece_deadline(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_piece_deadline( (torrent_handle)arg1, (object)index, (int)deadline [, (object)flags=0]) -> None :
        
            C++ signature :
                void set_piece_deadline(libtorrent::torrent_handle {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>,int [,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::deadline_flags_tag, void>=0])
        """
        pass

    def set_priority(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_priority( (torrent_handle)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_priority(libtorrent::torrent_handle {lvalue},int)
        """
        pass

    def set_ratio(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_ratio( (torrent_handle)arg1, (float)arg2) -> None :
        
            C++ signature :
                void set_ratio(libtorrent::torrent_handle {lvalue},float)
        """
        pass

    def set_sequential_download(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_sequential_download( (torrent_handle)arg1, (bool)arg2) -> None :
        
            C++ signature :
                void set_sequential_download(libtorrent::torrent_handle {lvalue},bool)
        """
        pass

    def set_share_mode(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_share_mode( (torrent_handle)arg1, (bool)arg2) -> None :
        
            C++ signature :
                void set_share_mode(libtorrent::torrent_handle {lvalue},bool)
        """
        pass

    def set_ssl_certificate(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_ssl_certificate( (torrent_handle)arg1, (str)cert, (str)private_key, (str)dh_params [, (str)passphrase='']) -> None :
        
            C++ signature :
                void set_ssl_certificate(libtorrent::torrent_handle {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > [,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >=''])
        """
        pass

    def set_tracker_login(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_tracker_login( (torrent_handle)arg1, (str)arg2, (str)arg3) -> None :
        
            C++ signature :
                void set_tracker_login(libtorrent::torrent_handle {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def set_upload_limit(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_upload_limit( (torrent_handle)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_upload_limit(libtorrent::torrent_handle {lvalue},int)
        """
        pass

    def set_upload_mode(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_upload_mode( (torrent_handle)arg1, (bool)arg2) -> None :
        
            C++ signature :
                void set_upload_mode(libtorrent::torrent_handle {lvalue},bool)
        """
        pass

    def status(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        status( (torrent_handle)arg1 [, (object)flags=4294967295]) -> torrent_status :
        
            C++ signature :
                libtorrent::torrent_status status(libtorrent::torrent_handle {lvalue} [,libtorrent::flags::bitfield_flag<unsigned int, libtorrent::status_flags_tag, void>=4294967295])
        """
        pass

    def stop_when_ready(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        stop_when_ready( (torrent_handle)arg1, (bool)arg2) -> None :
        
            C++ signature :
                void stop_when_ready(libtorrent::torrent_handle {lvalue},bool)
        """
        pass

    def super_seeding(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        super_seeding( (torrent_handle)arg1, (bool)arg2) -> None :
        
            C++ signature :
                void super_seeding(libtorrent::torrent_handle {lvalue},bool)
        
        super_seeding( (torrent_handle)arg1) -> bool :
        
            C++ signature :
                bool super_seeding(libtorrent::torrent_handle {lvalue})
        """
        pass

    def torrent_file(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        torrent_file( (torrent_handle)arg1) -> torrent_info :
        
            C++ signature :
                std::shared_ptr<libtorrent::torrent_info const> torrent_file(libtorrent::torrent_handle {lvalue})
        """
        pass

    def trackers(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        trackers( (torrent_handle)arg1) -> list :
        
            C++ signature :
                boost::python::list trackers(libtorrent::torrent_handle {lvalue})
        """
        pass

    def unset_flags(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        unset_flags( (torrent_handle)arg1, (object)arg2) -> None :
        
            C++ signature :
                void unset_flags(libtorrent::torrent_handle {lvalue},libtorrent::flags::bitfield_flag<unsigned long, libtorrent::torrent_flags_tag, void>)
        """
        pass

    def upload_limit(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        upload_limit( (torrent_handle)arg1) -> int :
        
            C++ signature :
                int upload_limit(libtorrent::torrent_handle {lvalue})
        """
        pass

    def url_seeds(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        url_seeds( (torrent_handle)arg1) -> list :
        
            C++ signature :
                boost::python::list url_seeds(libtorrent::torrent_handle {lvalue})
        """
        pass

    def use_interface(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        use_interface( (torrent_handle)arg1, (str)arg2) -> None :
        
            C++ signature :
                void use_interface(libtorrent::torrent_handle {lvalue},char const*)
        """
        pass

    def write_resume_data(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        write_resume_data( (torrent_handle)arg1) -> object :
        
            C++ signature :
                libtorrent::entry write_resume_data(libtorrent::torrent_handle {lvalue})
        """
        pass

    def __eq__(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (torrent_handle)arg1, (torrent_handle)arg2) -> object :
        
            C++ signature :
                _object* __eq__(libtorrent::torrent_handle {lvalue},libtorrent::torrent_handle)
        """
        pass

    def __hash__(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __hash__( (torrent_handle)arg1) -> int :
        
            C++ signature :
                unsigned long __hash__(libtorrent::torrent_handle)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __lt__(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (torrent_handle)arg1, (torrent_handle)arg2) -> object :
        
            C++ signature :
                _object* __lt__(libtorrent::torrent_handle {lvalue},libtorrent::torrent_handle)
        """
        pass

    def __ne__(self, torrent_handle, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (torrent_handle)arg1, (torrent_handle)arg2) -> object :
        
            C++ signature :
                _object* __ne__(libtorrent::torrent_handle {lvalue},libtorrent::torrent_handle)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    alert_when_available = 1
    flush_disk_cache = 1
    graceful_pause = 1
    ignore_min_interval = 1
    only_if_modified = 4
    overwrite_existing = 1
    piece_granularity = 1
    query_accurate_download_counters = 2
    query_distributed_copies = 1
    query_last_seen_complete = 4
    query_pieces = 8
    query_verified_pieces = 16
    save_info_dict = 2
    __instance_size__ = 32


