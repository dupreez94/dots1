# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KUrlMimeData(__sip.simplewrapper):
    # no doc
    def mimeDataTypes(self, *args, **kwargs): # real signature unknown
        pass

    def setMetaData(self, *args, **kwargs): # real signature unknown
        pass

    def setUrls(self, *args, **kwargs): # real signature unknown
        pass

    def urlsFromMimeData(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    DecodeOptions = None # (!) real value is "<class 'PyKF5.KCoreAddons.KUrlMimeData.DecodeOptions'>"
    PreferKdeUrls = 1
    PreferLocalUrls = 0


