# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KUser(__sip.wrapper):
    # no doc
    def allUserNames(self, *args, **kwargs): # real signature unknown
        pass

    def allUsers(self, *args, **kwargs): # real signature unknown
        pass

    def faceIconPath(self, *args, **kwargs): # real signature unknown
        pass

    def fullName(self, *args, **kwargs): # real signature unknown
        pass

    def gid(self, *args, **kwargs): # real signature unknown
        pass

    def groupId(self, *args, **kwargs): # real signature unknown
        pass

    def groupNames(self, *args, **kwargs): # real signature unknown
        pass

    def groups(self, *args, **kwargs): # real signature unknown
        pass

    def homeDir(self, *args, **kwargs): # real signature unknown
        pass

    def isSuperUser(self, *args, **kwargs): # real signature unknown
        pass

    def isValid(self, *args, **kwargs): # real signature unknown
        pass

    def loginName(self, *args, **kwargs): # real signature unknown
        pass

    def property(self, *args, **kwargs): # real signature unknown
        pass

    def shell(self, *args, **kwargs): # real signature unknown
        pass

    def uid(self, *args, **kwargs): # real signature unknown
        pass

    def userId(self, *args, **kwargs): # real signature unknown
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    FullName = 0
    HomePhone = 3
    RoomNumber = 1
    UIDMode = None # (!) real value is "<class 'PyKF5.KCoreAddons.KUser.UIDMode'>"
    UseEffectiveUID = 0
    UseRealUserID = 1
    UserProperty = None # (!) real value is "<class 'PyKF5.KCoreAddons.KUser.UserProperty'>"
    WorkPhone = 2
    __hash__ = None


