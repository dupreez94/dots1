# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KAboutLicense(__sip.wrapper):
    # no doc
    def byKeyword(self, *args, **kwargs): # real signature unknown
        pass

    def key(self, *args, **kwargs): # real signature unknown
        pass

    def name(self, *args, **kwargs): # real signature unknown
        pass

    def spdx(self, *args, **kwargs): # real signature unknown
        pass

    def text(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    Artistic = 4
    BSDL = 3
    Custom = -2
    File = -1
    FullName = 1
    GPL = 1
    GPL_V2 = 1
    GPL_V3 = 6
    LGPL = 2
    LGPL_V2 = 2
    LGPL_V2_1 = 8
    LGPL_V3 = 7
    LicenseKey = None # (!) real value is "<class 'PyKF5.KCoreAddons.KAboutLicense.LicenseKey'>"
    NameFormat = None # (!) real value is "<class 'PyKF5.KCoreAddons.KAboutLicense.NameFormat'>"
    OnlyThisVersion = 0
    OrLaterVersions = 1
    QPL = 5
    QPL_V1_0 = 5
    ShortName = 0
    Unknown = 0
    VersionRestriction = None # (!) real value is "<class 'PyKF5.KCoreAddons.KAboutLicense.VersionRestriction'>"


