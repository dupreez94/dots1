# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


# functions

def qHash(*args, **kwargs): # real signature unknown
    pass

# classes

from .KAboutData import KAboutData
from .KAboutLicense import KAboutLicense
from .KAboutPerson import KAboutPerson
from .KAutoSaveFile import KAutoSaveFile
from .KBackup import KBackup
from .KMacroExpanderBase import KMacroExpanderBase
from .KCharMacroExpander import KCharMacroExpander
from .KJob import KJob
from .KCompositeJob import KCompositeJob
from .KCoreAddons import KCoreAddons
from .KCrash import KCrash
from .Kdelibs4ConfigMigrator import Kdelibs4ConfigMigrator
from .Kdelibs4Migration import Kdelibs4Migration
from .KDirWatch import KDirWatch
from .KFileSystemType import KFileSystemType
from .KFormat import KFormat
from .KGroupId import KGroupId
from .KJobTrackerInterface import KJobTrackerInterface
from .KJobUiDelegate import KJobUiDelegate
from .KListOpenFilesJob import KListOpenFilesJob
from .KMacroExpander import KMacroExpander
from .KMessage import KMessage
from .KMessageHandler import KMessageHandler
from .KOSRelease import KOSRelease
from .KParts import KParts
from .KPluginFactory import KPluginFactory
from .KPluginLoader import KPluginLoader
from .KPluginMetaData import KPluginMetaData
from .KPluginName import KPluginName
from .KProcess import KProcess
from .KProcessList import KProcessList
from .KRandom import KRandom
from .KRandomSequence import KRandomSequence
from .KSharedDataCache import KSharedDataCache
from .KShell import KShell
from .KStringHandler import KStringHandler
from .KTextToHTML import KTextToHTML
from .KTextToHTMLEmoticonsInterface import KTextToHTMLEmoticonsInterface
from .KUrlMimeData import KUrlMimeData
from .KUser import KUser
from .KUserGroup import KUserGroup
from .KUserId import KUserId
from .KWordMacroExpander import KWordMacroExpander
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>'

__spec__ = None # (!) real value is "ModuleSpec(name='PyKF5.KCoreAddons', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>, origin='/usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so')"

