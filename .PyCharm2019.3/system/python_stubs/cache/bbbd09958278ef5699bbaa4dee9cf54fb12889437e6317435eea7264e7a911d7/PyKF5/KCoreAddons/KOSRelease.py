# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KOSRelease(__sip.wrapper):
    # no doc
    def ansiColor(self, *args, **kwargs): # real signature unknown
        pass

    def bugReportUrl(self, *args, **kwargs): # real signature unknown
        pass

    def buildId(self, *args, **kwargs): # real signature unknown
        pass

    def cpeName(self, *args, **kwargs): # real signature unknown
        pass

    def documentationUrl(self, *args, **kwargs): # real signature unknown
        pass

    def extraKeys(self, *args, **kwargs): # real signature unknown
        pass

    def extraValue(self, *args, **kwargs): # real signature unknown
        pass

    def homeUrl(self, *args, **kwargs): # real signature unknown
        pass

    def id(self, *args, **kwargs): # real signature unknown
        pass

    def idLike(self, *args, **kwargs): # real signature unknown
        pass

    def logo(self, *args, **kwargs): # real signature unknown
        pass

    def name(self, *args, **kwargs): # real signature unknown
        pass

    def prettyName(self, *args, **kwargs): # real signature unknown
        pass

    def privacyPolicyUrl(self, *args, **kwargs): # real signature unknown
        pass

    def supportUrl(self, *args, **kwargs): # real signature unknown
        pass

    def variant(self, *args, **kwargs): # real signature unknown
        pass

    def variantId(self, *args, **kwargs): # real signature unknown
        pass

    def version(self, *args, **kwargs): # real signature unknown
        pass

    def versionCodename(self, *args, **kwargs): # real signature unknown
        pass

    def versionId(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



