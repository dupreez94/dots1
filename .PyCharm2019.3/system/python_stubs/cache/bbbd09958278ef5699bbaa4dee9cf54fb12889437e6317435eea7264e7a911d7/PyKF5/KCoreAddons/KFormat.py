# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KFormat(__sip.wrapper):
    # no doc
    def formatByteSize(self, *args, **kwargs): # real signature unknown
        pass

    def formatDecimalDuration(self, *args, **kwargs): # real signature unknown
        pass

    def formatDuration(self, *args, **kwargs): # real signature unknown
        pass

    def formatRelativeDate(self, *args, **kwargs): # real signature unknown
        pass

    def formatRelativeDateTime(self, *args, **kwargs): # real signature unknown
        pass

    def formatSpelloutDuration(self, *args, **kwargs): # real signature unknown
        pass

    def formatValue(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    BinarySizeUnits = None # (!) real value is "<class 'PyKF5.KCoreAddons.KFormat.BinarySizeUnits'>"
    BinaryUnitDialect = None # (!) real value is "<class 'PyKF5.KCoreAddons.KFormat.BinaryUnitDialect'>"
    DefaultBinaryDialect = -1
    DefaultBinaryUnits = -1
    DefaultDuration = 0
    DurationFormatOption = None # (!) real value is "<class 'PyKF5.KCoreAddons.KFormat.DurationFormatOption'>"
    DurationFormatOptions = None # (!) real value is "<class 'PyKF5.KCoreAddons.KFormat.DurationFormatOptions'>"
    FoldHours = 8
    HideSeconds = 4
    IECBinaryDialect = 0
    InitialDuration = 1
    JEDECBinaryDialect = 1
    LastBinaryDialect = 2
    MetricBinaryDialect = 2
    ShowMilliseconds = 2
    Unit = None # (!) real value is "<enum 'Unit'>"
    UnitByte = 0
    UnitExaByte = 6
    UnitGigaByte = 3
    UnitKiloByte = 1
    UnitLastUnit = 8
    UnitMegaByte = 2
    UnitPetaByte = 5
    UnitPrefix = None # (!) real value is "<enum 'UnitPrefix'>"
    UnitTeraByte = 4
    UnitYottaByte = 8
    UnitZettaByte = 7


