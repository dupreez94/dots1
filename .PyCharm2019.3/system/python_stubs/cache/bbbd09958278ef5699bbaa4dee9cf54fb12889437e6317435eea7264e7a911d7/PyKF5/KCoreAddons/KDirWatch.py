# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KDirWatch(__PyQt5_QtCore.QObject):
    # no doc
    def addDir(self, *args, **kwargs): # real signature unknown
        pass

    def addFile(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def contains(self, *args, **kwargs): # real signature unknown
        pass

    def created(self, *args, **kwargs): # real signature unknown
        pass

    def ctime(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def deleted(self, *args, **kwargs): # real signature unknown
        pass

    def deleteQFSWatcher(self, *args, **kwargs): # real signature unknown
        pass

    def dirty(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def exists(self, *args, **kwargs): # real signature unknown
        pass

    def internalMethod(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def isStopped(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def removeDir(self, *args, **kwargs): # real signature unknown
        pass

    def removeFile(self, *args, **kwargs): # real signature unknown
        pass

    def restartDirScan(self, *args, **kwargs): # real signature unknown
        pass

    def self(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setCreated(self, *args, **kwargs): # real signature unknown
        pass

    def setDeleted(self, *args, **kwargs): # real signature unknown
        pass

    def setDirty(self, *args, **kwargs): # real signature unknown
        pass

    def startScan(self, *args, **kwargs): # real signature unknown
        pass

    def statistics(self, *args, **kwargs): # real signature unknown
        pass

    def stopDirScan(self, *args, **kwargs): # real signature unknown
        pass

    def stopScan(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    FAM = 0
    INotify = 1
    Method = None # (!) real value is "<class 'PyKF5.KCoreAddons.KDirWatch.Method'>"
    QFSWatch = 3
    Stat = 2
    WatchDirOnly = 0
    WatchFiles = 1
    WatchMode = None # (!) real value is "<class 'PyKF5.KCoreAddons.KDirWatch.WatchMode'>"
    WatchModes = None # (!) real value is "<class 'PyKF5.KCoreAddons.KDirWatch.WatchModes'>"
    WatchSubDirs = 2


