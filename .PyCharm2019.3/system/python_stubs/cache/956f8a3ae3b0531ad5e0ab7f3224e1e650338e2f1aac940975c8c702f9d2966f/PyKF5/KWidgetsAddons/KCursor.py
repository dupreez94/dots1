# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


class KCursor(__sip.wrapper):
    # no doc
    def autoHideEventFilter(self, *args, **kwargs): # real signature unknown
        pass

    def hideCursorDelay(self, *args, **kwargs): # real signature unknown
        pass

    def setAutoHideCursor(self, *args, **kwargs): # real signature unknown
        pass

    def setHideCursorDelay(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



