# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


# no functions
# classes

from .KAcceleratorManager import KAcceleratorManager
from .KActionMenu import KActionMenu
from .KActionSelector import KActionSelector
from .KAnimatedButton import KAnimatedButton
from .KPageDialog import KPageDialog
from .KAssistantDialog import KAssistantDialog
from .KBusyIndicatorWidget import KBusyIndicatorWidget
from .KCapacityBar import KCapacityBar
from .KCharSelect import KCharSelect
from .KCollapsibleGroupBox import KCollapsibleGroupBox
from .KColorButton import KColorButton
from .KColorCombo import KColorCombo
from .KColumnResizer import KColumnResizer
from .KCursor import KCursor
from .KDateComboBox import KDateComboBox
from .KDatePicker import KDatePicker
from .KDateTimeEdit import KDateTimeEdit
from .KDragWidgetDecoratorBase import KDragWidgetDecoratorBase
from .KDualAction import KDualAction
from .KEditListWidget import KEditListWidget
from .KSelectAction import KSelectAction
from .KFontAction import KFontAction
from .KFontChooser import KFontChooser
from .KFontRequester import KFontRequester
from .KFontSizeAction import KFontSizeAction
from .KSelector import KSelector
from .KGradientSelector import KGradientSelector
from .KGuiItem import KGuiItem
from .KLed import KLed
from .KMessageWidget import KMessageWidget
from .KMimeTypeChooser import KMimeTypeChooser
from .KMimeTypeChooserDialog import KMimeTypeChooserDialog
from .KMimeTypeEditor import KMimeTypeEditor
from .KNewPasswordDialog import KNewPasswordDialog
from .KNewPasswordWidget import KNewPasswordWidget
from .KPageModel import KPageModel
from .KPageView import KPageView
from .KPageWidget import KPageWidget
from .KPageWidgetItem import KPageWidgetItem
from .KPageWidgetModel import KPageWidgetModel
from .KPasswordDialog import KPasswordDialog
from .KPasswordLineEdit import KPasswordLineEdit
from .KPixmapRegionSelectorDialog import KPixmapRegionSelectorDialog
from .KPixmapRegionSelectorWidget import KPixmapRegionSelectorWidget
from .KPixmapSequence import KPixmapSequence
from .KPixmapSequenceOverlayPainter import KPixmapSequenceOverlayPainter
from .KPixmapSequenceWidget import KPixmapSequenceWidget
from .KPopupFrame import KPopupFrame
from .KRatingPainter import KRatingPainter
from .KRatingWidget import KRatingWidget
from .KRuler import KRuler
from .KSeparator import KSeparator
from .KSplitterCollapserButton import KSplitterCollapserButton
from .KSqueezedTextLabel import KSqueezedTextLabel
from .KStandardGuiItem import KStandardGuiItem
from .KStyleExtensions import KStyleExtensions
from .KTimeComboBox import KTimeComboBox
from .KTitleWidget import KTitleWidget
from .KToggleAction import KToggleAction
from .KToggleFullScreenAction import KToggleFullScreenAction
from .KToolBarLabelAction import KToolBarLabelAction
from .KToolBarPopupAction import KToolBarPopupAction
from .KToolBarSpacerAction import KToolBarSpacerAction
from .KUrlLabel import KUrlLabel
from .KViewStateMaintainerBase import KViewStateMaintainerBase
from .KViewStateSerializer import KViewStateSerializer
from .KXYSelector import KXYSelector
from .LineEditUrlDropEventFilter import LineEditUrlDropEventFilter
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>'

__spec__ = None # (!) real value is "ModuleSpec(name='PyKF5.KWidgetsAddons', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>, origin='/usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so')"

