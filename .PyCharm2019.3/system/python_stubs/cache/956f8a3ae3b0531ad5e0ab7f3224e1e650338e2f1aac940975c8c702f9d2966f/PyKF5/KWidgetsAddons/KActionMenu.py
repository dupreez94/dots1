# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


class KActionMenu(__PyQt5_QtWidgets.QWidgetAction):
    # no doc
    def addAction(self, *args, **kwargs): # real signature unknown
        pass

    def addSeparator(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def createdWidgets(self, *args, **kwargs): # real signature unknown
        pass

    def createWidget(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def delayed(self, *args, **kwargs): # real signature unknown
        pass

    def deleteWidget(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def event(self, *args, **kwargs): # real signature unknown
        pass

    def eventFilter(self, *args, **kwargs): # real signature unknown
        pass

    def insertAction(self, *args, **kwargs): # real signature unknown
        pass

    def insertSeparator(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def popupMenu(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def remove(self, *args, **kwargs): # real signature unknown
        pass

    def removeAction(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setDelayed(self, *args, **kwargs): # real signature unknown
        pass

    def setStickyMenu(self, *args, **kwargs): # real signature unknown
        pass

    def stickyMenu(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


