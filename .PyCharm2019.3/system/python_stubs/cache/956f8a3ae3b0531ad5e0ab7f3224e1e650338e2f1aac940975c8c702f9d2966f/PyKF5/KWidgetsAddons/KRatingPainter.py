# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


class KRatingPainter(__sip.wrapper):
    # no doc
    def alignment(self, *args, **kwargs): # real signature unknown
        pass

    def customPixmap(self, *args, **kwargs): # real signature unknown
        pass

    def getRatingFromPosition(self, *args, **kwargs): # real signature unknown
        pass

    def halfStepsEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def icon(self, *args, **kwargs): # real signature unknown
        pass

    def isEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def layoutDirection(self, *args, **kwargs): # real signature unknown
        pass

    def maxRating(self, *args, **kwargs): # real signature unknown
        pass

    def paint(self, *args, **kwargs): # real signature unknown
        pass

    def paintRating(self, *args, **kwargs): # real signature unknown
        pass

    def ratingFromPosition(self, *args, **kwargs): # real signature unknown
        pass

    def setAlignment(self, *args, **kwargs): # real signature unknown
        pass

    def setCustomPixmap(self, *args, **kwargs): # real signature unknown
        pass

    def setEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def setHalfStepsEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def setIcon(self, *args, **kwargs): # real signature unknown
        pass

    def setLayoutDirection(self, *args, **kwargs): # real signature unknown
        pass

    def setMaxRating(self, *args, **kwargs): # real signature unknown
        pass

    def setSpacing(self, *args, **kwargs): # real signature unknown
        pass

    def spacing(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



