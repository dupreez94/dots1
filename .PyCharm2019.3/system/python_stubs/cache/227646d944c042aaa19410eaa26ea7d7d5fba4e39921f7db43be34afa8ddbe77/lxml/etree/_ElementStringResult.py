# encoding: utf-8
# module lxml.etree
# from /usr/lib/python3.8/site-packages/lxml/etree.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
""" The ``lxml.etree`` module implements the extended ElementTree API for XML. """

# imports
import builtins as __builtins__ # <module 'builtins' (built-in)>

from .bytes import bytes

class _ElementStringResult(bytes):
    # no doc
    def getparent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __dict__ = None # (!) real value is "mappingproxy({'__module__': 'lxml.etree', 'getparent': <cyfunction _ElementStringResult.getparent at 0x7fc7af44cf40>, '__dict__': <attribute '__dict__' of '_ElementStringResult' objects>, '__doc__': None})"


