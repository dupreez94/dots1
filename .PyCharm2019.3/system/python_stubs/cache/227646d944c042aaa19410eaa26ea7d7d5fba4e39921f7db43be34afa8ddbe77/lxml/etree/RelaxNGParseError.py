# encoding: utf-8
# module lxml.etree
# from /usr/lib/python3.8/site-packages/lxml/etree.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
""" The ``lxml.etree`` module implements the extended ElementTree API for XML. """

# imports
import builtins as __builtins__ # <module 'builtins' (built-in)>

from .RelaxNGError import RelaxNGError

class RelaxNGParseError(RelaxNGError):
    """ Error while parsing an XML document as RelaxNG. """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass


