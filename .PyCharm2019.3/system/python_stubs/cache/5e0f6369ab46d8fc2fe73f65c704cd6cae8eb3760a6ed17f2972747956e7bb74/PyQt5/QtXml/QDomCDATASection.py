# encoding: utf-8
# module PyQt5.QtXml
# from /usr/lib/python3.8/site-packages/PyQt5/QtXml.abi3.so
# by generator 1.147
# no doc

# imports
import sip as __sip


from .QDomText import QDomText

class QDomCDATASection(QDomText):
    """
    QDomCDATASection()
    QDomCDATASection(QDomCDATASection)
    """
    def nodeType(self): # real signature unknown; restored from __doc__
        """ nodeType(self) -> QDomNode.NodeType """
        pass

    def __init__(self, QDomCDATASection=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


