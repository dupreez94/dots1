# encoding: utf-8
# module alsaaudio
# from /usr/lib/python3.8/site-packages/alsaaudio.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
"""
This modules provides support for the ALSA audio API.

To control the PCM device, use the PCM class, Mixers
are controlled using the Mixer class.

The following functions are also provided:
mixers() -- Return a list of available mixer names
"""
# no imports

# Variables with simple values

MIXER_CHANNEL_ALL = -1

PCM_ASYNC = 2
PCM_CAPTURE = 1

PCM_FORMAT_A_LAW = 21

PCM_FORMAT_DSD_U16_LE = 49

PCM_FORMAT_DSD_U32_BE = 52
PCM_FORMAT_DSD_U32_LE = 50

PCM_FORMAT_DSD_U8 = 48

PCM_FORMAT_FLOAT64_BE = 17
PCM_FORMAT_FLOAT64_LE = 16

PCM_FORMAT_FLOAT_BE = 15
PCM_FORMAT_FLOAT_LE = 14

PCM_FORMAT_GSM = 24

PCM_FORMAT_IMA_ADPCM = 22

PCM_FORMAT_MPEG = 23

PCM_FORMAT_MU_LAW = 20

PCM_FORMAT_S16_BE = 3
PCM_FORMAT_S16_LE = 2

PCM_FORMAT_S24_BE = 7
PCM_FORMAT_S24_LE = 6

PCM_FORMAT_S32_BE = 11
PCM_FORMAT_S32_LE = 10

PCM_FORMAT_S8 = 0

PCM_FORMAT_U16_BE = 5
PCM_FORMAT_U16_LE = 4

PCM_FORMAT_U24_BE = 9
PCM_FORMAT_U24_LE = 8

PCM_FORMAT_U32_BE = 13
PCM_FORMAT_U32_LE = 12

PCM_FORMAT_U8 = 1

PCM_NONBLOCK = 1
PCM_NORMAL = 0
PCM_PLAYBACK = 0

# functions

def cards(): # real signature unknown; restored from __doc__
    """
    cards()
    
    List the available card ids.
    """
    pass

def card_indexes(): # real signature unknown; restored from __doc__
    """
    card_indexes()
    
    List the available card indexes.
    """
    pass

def card_name(card_index): # real signature unknown; restored from __doc__
    """
    card_name(card_index) -> Tuple of (name, longname)
    
    Return the card name and long name for card 'card_index'.
    """
    pass

def mixers(cardname=None): # real signature unknown; restored from __doc__
    """
    mixers([cardname])
    
    List the available mixers. The optional cardname specifies
    which card should be queried (this is only relevant if you
    have more than one sound card). Omit to use the default sound card.
    """
    pass

def pcms(pcmtype=None): # real signature unknown; restored from __doc__
    """
    pcms([pcmtype])
    
    List the available PCM devices
    """
    pass

# classes

class ALSAAudioError(Exception):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class Mixer(object):
    """ ALSA Mixer Control. """
    def cardname(self): # real signature unknown; restored from __doc__
        """
        cardname() -> string
        
        Returns the name of the sound card used by this Mixer object.
        """
        return ""

    def close(self): # real signature unknown; restored from __doc__
        """
        close() -> None
        
        Close a Mixer.
        """
        pass

    def getenum(self): # real signature unknown; restored from __doc__
        """
        getenum() -> Tuple of (string, list of strings)
        
        Returns a a tuple. The first element is name of the active enumerated item, 
        the second a list available enumerated items.
        """
        pass

    def getmute(self): # real signature unknown; restored from __doc__
        """
        getmute() -> List of mute settings (int)
        
        Return a list indicating the current mute setting for each channel.
        0 means not muted, 1 means muted.
        
        This method will fail if the mixer has no playback switch capabilities.
        """
        return []

    def getrange(self, pcmtype=None): # real signature unknown; restored from __doc__
        """
        getrange([pcmtype]) -> List of (min_volume, max_volume)
        
        Returns a list of tuples with the volume range (ints).
        
        The optional 'direction' argument can be either PCM_PLAYBACK or
        PCM_CAPTURE, which is relevant if the mixer can control both
        playback and capture volume. The default value is 'playback'
        if the mixer has this capability, otherwise 'capture'
        """
        return []

    def getrec(self): # real signature unknown; restored from __doc__
        """
        getrec() -> List of record mute settings (int)
        
        Return a list indicating the current record mute setting for each
        channel. 0 means not recording, 1 means recording.
        This method will fail if the mixer has no capture switch capabilities.
        """
        return []

    def getvolume(self, pcmtype=None): # real signature unknown; restored from __doc__
        """
        getvolume([pcmtype]) -> List of volume settings (int)
        
        Returns a list with the current volume settings for each channel.
        The list elements are integer percentages.
        
        The optional 'pcmtype' argument can be either PCM_PLAYBACK or
        PCM_CAPTURE, which is relevant if the mixer can control both
        playback and capture volume. The default value is PCM_PLAYBACK
        if the mixer has this capability, otherwise PCM_CAPTURE
        """
        return []

    def handleevents(self): # real signature unknown; restored from __doc__
        """
        handleevents() -> int
        
        Acknowledge events on the polldescriptors() file descriptors
        to prevent subsequent polls from returning the same events again.
        Returns the number of events that were acknowledged.
        """
        return 0

    def mixer(self): # real signature unknown; restored from __doc__
        """
        mixer() -> string
        
        Returns the name of the specific mixer controlled by this object,
        for example 'Master' or 'PCM'
        """
        return ""

    def mixerid(self): # real signature unknown; restored from __doc__
        """
        mixerid() -> int
        
        Returns the ID of the ALSA mixer controlled by this object.
        """
        return 0

    def polldescriptors(self): # real signature unknown; restored from __doc__
        """
        polldescriptors() -> List of tuples (fd, eventmask).
        
        Return a list of file descriptors and event masks
        suitable for use with poll to monitor changes on this mixer.
        """
        return []

    def setenum(self, index): # real signature unknown; restored from __doc__
        """
        setenum(index) -> None
        
        Sets the value of the enum, where 'index' is an index into the list of
        available enumerated items returned by getenum().
        """
        pass

    def setmute(self, mute, channel=None): # real signature unknown; restored from __doc__
        """
        setmute(mute [, channel])
        
        Sets the mute flag to a new value. The mute argument is either 0 for
        not muted, or 1 for muted.
        The optional channel argument controls which channel is muted.
        If omitted, the mute flag is set for for all channels.
        
        This method will fail if the mixer has no playback mute capabilities
        """
        pass

    def setrec(self, capture, channel=None): # real signature unknown; restored from __doc__
        """
        setrec(capture [, channel])
        
        Sets the capture mute flag to a new value. The capture argument is
        either 0 for no capture, or 1 for capture.
        The optional channel argument controls which channel is changed.
        If omitted, the capture flag is set for all channels.
        
        This method will fail if the mixer has no capture switch capabilities
        """
        pass

    def setvolume(self, volume, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setvolume(volume[[, channel] [, pcmtype]])
        
        Change the current volume settings for this mixer. The volume argument
        controls the new volume setting as an integer percentage.
        If the optional argument channel is present, the volume is set only for
        this channel. This assumes that the mixer can control the volume for the
        channels independently.
        
        The optional direction argument can be either PCM_PLAYBACK or PCM_CAPTURE.
        It is relevant if the mixer has independent playback and capture volume
        capabilities, and controls which of the volumes will be changed.
        The default is 'playback' if the mixer has this capability, otherwise
        'capture'.
        """
        pass

    def switchcap(self): # real signature unknown; restored from __doc__
        """
        switchcap() -> List of switch capabilities (string)
        
        Returns a list of the switches which are defined by this mixer.
        
        Possible values in this list are:
         - 'Mute'
         - 'Joined Mute'
         - 'Playback Mute'
         - 'Joined Playback Mute'
         - 'Capture Mute'
         - 'Joined Capture Mute'
         - 'Capture Exclusive'
        """
        return []

    def volumecap(self): # real signature unknown; restored from __doc__
        """
        volumecap() -> List of volume capabilities (string)
        
        Returns a list of the volume control capabilities of this mixer.
        Possible values in this list are:
         - 'Volume'
         - 'Joined Volume'
         - 'Playback Volume'
         - 'Joined Playback Mute'
         - 'Capture Volume'
         - 'Joined Capture Volume'
        """
        return []

    def __getattribute__(self, *args, **kwargs): # real signature unknown
        """ Return getattr(self, name). """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass


class PCM(object):
    """ ALSA PCM device. """
    def cardname(self): # real signature unknown; restored from __doc__
        """
        cardname() -> string
        
        Returns the name of the sound card used by this PCM object.
        """
        return ""

    def close(self): # real signature unknown; restored from __doc__
        """
        close() -> None
        
        Close a PCM device.
        """
        pass

    def dumpinfo(self, *args, **kwargs): # real signature unknown
        pass

    def pause(self, enable=1): # real signature unknown; restored from __doc__
        """
        pause(enable=1)
        
        If enable is 1, playback or capture is paused. If enable is 0,
        playback/capture is resumed.
        """
        pass

    def pcmmode(self): # real signature unknown; restored from __doc__
        """
        pcmmode() -> int
        
        Returns the mode of the PCM object. One of:
         - PCM_NONBLOCK
         - PCM_ASYNC
         - PCM_NORMAL.
        """
        return 0

    def pcmtype(self): # real signature unknown; restored from __doc__
        """
        pcmtype() -> int
        
        Returns either PCM_CAPTURE or PCM_PLAYBACK.
        """
        return 0

    def polldescriptors(self): # real signature unknown; restored from __doc__
        """
        polldescriptors() -> List of tuples (fd, eventmask).
        
        Return a list of file descriptors and event masks
        suitable for use with poll.
        """
        return []

    def read(self): # real signature unknown; restored from __doc__
        """
        read() -> (size, data)
        
        In PCM_NORMAL mode, this function blocks until a full period is
        available, and then returns a tuple (length,data) where length is
        the number of frames of the captured data, and data is the captured sound
        frames as bytes (or a string in Python 2.x). The length of the returned data
         will be periodsize*framesize bytes.
        
        In PCM_NONBLOCK mode, the call will not block, but will return (0,'')
        if no new period has become available since the last call to read.
        
        In case of an overrun, this function will return a negative size: -EPIPE.
        This indicates that data was lost, even if the operation itself succeeded.
        Try using a larger periodsize
        """
        pass

    def setchannels(self, numchannels): # real signature unknown; restored from __doc__
        """
        setchannels(numchannels)
        
        Used to set the number of capture or playback channels. Common values
        are: 1 = mono, 2 = stereo, and 6 = full 6 channel audio.
        
        Few sound cards support more than 2 channels.
        """
        pass

    def setformat(self, rate): # real signature unknown; restored from __doc__
        """ setformat(rate) """
        pass

    def setperiodsize(self, period): # real signature unknown; restored from __doc__
        """
        setperiodsize(period) -> int
        
        Sets the actual period size in frames. Each write should consist of
        exactly this number of frames, and each read will return this number of
        frames (unless the device is in PCM_NONBLOCK mode, in which case it
        may return nothing at all).
        """
        return 0

    def setrate(self, rate): # real signature unknown; restored from __doc__
        """
        setrate(rate)
        
        Set the sample rate in Hz for the device. Typical values are
        8000 (telephony), 11025, 44100 (CD), 48000 (DVD audio) and 96000
        """
        pass

    def write(self, data): # real signature unknown; restored from __doc__
        """
        write(data) -> bytes written
        
        Writes (plays) the sound in data. The length of data must be a multiple
        of the frame size, and should be exactly the size of a period. If less
        than 'period size' frames are provided, the actual playout will not
        happen until more data is written.
        If the device is not in PCM_NONBLOCK mode, this call will block if the
        kernel buffer is full, and until enough sound has been played to allow
        the sound data to be buffered. The call always returns the size of the
        data provided.
        
        In PCM_NONBLOCK mode, the call will return immediately, with a return
        value of zero, if the buffer is full. In this case, the data should be
        written at a later time.
        """
        return b""

    def __getattribute__(self, *args, **kwargs): # real signature unknown
        """ Return getattr(self, name). """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass


# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>'

__spec__ = None # (!) real value is "ModuleSpec(name='alsaaudio', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>, origin='/usr/lib/python3.8/site-packages/alsaaudio.cpython-38-x86_64-linux-gnu.so')"

