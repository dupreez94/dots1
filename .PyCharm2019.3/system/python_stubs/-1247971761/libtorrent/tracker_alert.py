# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


from .torrent_alert import torrent_alert

class tracker_alert(torrent_alert):
    # no doc
    def tracker_url(self, tracker_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        tracker_url( (tracker_alert)arg1) -> str :
        
            C++ signature :
                char const* tracker_url(libtorrent::tracker_alert {lvalue})
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    local_endpoint = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    url = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default



