# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


from .torrent_alert import torrent_alert

class storage_moved_failed_alert(torrent_alert):
    # no doc
    def file_path(self, storage_moved_failed_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_path( (storage_moved_failed_alert)arg1) -> str :
        
            C++ signature :
                char const* file_path(libtorrent::storage_moved_failed_alert {lvalue})
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    error = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    op = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    operation = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default



