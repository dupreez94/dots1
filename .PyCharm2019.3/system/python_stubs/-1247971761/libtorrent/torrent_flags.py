# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class torrent_flags(__Boost_Python.instance):
    # no doc
    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    apply_ip_filter = 8
    auto_managed = 32
    default_flags = 352440
    disable_dht = 524288
    disable_lsd = 1048576
    disable_pex = 2097152
    duplicate_is_error = 64
    override_trackers = 2048
    override_web_seeds = 4096
    paused = 16
    seed_mode = 1
    sequential_download = 512
    share_mode = 4
    stop_when_ready = 1024
    super_seeding = 256
    update_subscribe = 128
    upload_mode = 2
    __instance_size__ = 24


