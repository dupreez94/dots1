# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


from .tracker_alert import tracker_alert

class scrape_reply_alert(tracker_alert):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    complete = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    incomplete = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default



