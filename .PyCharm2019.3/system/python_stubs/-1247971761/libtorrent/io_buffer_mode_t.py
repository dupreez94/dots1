# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class io_buffer_mode_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    disable_os_cache = 2
    disable_os_cache_for_aligned_files = 2
    enable_os_cache = 0
    names = {
        'disable_os_cache': 2,
        'disable_os_cache_for_aligned_files': 2,
        'enable_os_cache': 0,
    }
    values = {
        0: 0,
        2: 2,
    }
    __slots__ = ()


