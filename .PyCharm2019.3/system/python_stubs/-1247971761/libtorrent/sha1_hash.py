# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class sha1_hash(__Boost_Python.instance):
    # no doc
    def clear(self, sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        clear( (sha1_hash)arg1) -> None :
        
            C++ signature :
                void clear(libtorrent::digest32<160l> {lvalue})
        """
        pass

    def is_all_zeros(self, sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_all_zeros( (sha1_hash)arg1) -> bool :
        
            C++ signature :
                bool is_all_zeros(libtorrent::digest32<160l> {lvalue})
        """
        pass

    def to_bytes(self, sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        to_bytes( (sha1_hash)arg1) -> object :
        
            C++ signature :
                bytes to_bytes(libtorrent::digest32<160l>)
        """
        pass

    def to_string(self, sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        to_string( (sha1_hash)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > to_string(libtorrent::digest32<160l> {lvalue})
        """
        pass

    def __eq__(self, sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (sha1_hash)arg1, (sha1_hash)arg2) -> object :
        
            C++ signature :
                _object* __eq__(libtorrent::digest32<160l> {lvalue},libtorrent::digest32<160l>)
        """
        pass

    def __hash__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __hash__( (object)arg1) -> int :
        
            C++ signature :
                long __hash__(boost::python::api::object)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (str)arg2) -> None :
        
            C++ signature :
                void __init__(_object*,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def __lt__(self, sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (sha1_hash)arg1, (sha1_hash)arg2) -> object :
        
            C++ signature :
                _object* __lt__(libtorrent::digest32<160l> {lvalue},libtorrent::digest32<160l>)
        """
        pass

    def __ne__(self, sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (sha1_hash)arg1, (sha1_hash)arg2) -> object :
        
            C++ signature :
                _object* __ne__(libtorrent::digest32<160l> {lvalue},libtorrent::digest32<160l>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __str__(self, sha1_hash, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (sha1_hash)arg1) -> object :
        
            C++ signature :
                _object* __str__(libtorrent::digest32<160l> {lvalue})
        """
        pass

    __instance_size__ = 40


