# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


from .peer_alert import peer_alert

class peer_log_alert(peer_alert):
    # no doc
    def log_message(self, peer_log_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        log_message( (peer_log_alert)arg1) -> str :
        
            C++ signature :
                char const* log_message(libtorrent::peer_log_alert {lvalue})
        """
        pass

    def msg(self, peer_log_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        msg( (peer_log_alert)arg1) -> str :
        
            C++ signature :
                char const* msg(libtorrent::peer_log_alert {lvalue})
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass


