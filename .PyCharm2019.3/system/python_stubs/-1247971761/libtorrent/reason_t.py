# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class reason_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    i2p_mixed = 2
    invalid_local_interface = 6
    ip_filter = 0
    names = {
        'i2p_mixed': 2,
        'invalid_local_interface': 6,
        'ip_filter': 0,
        'port_filter': 1,
        'privileged_ports': 3,
        'tcp_disabled': 5,
        'utp_disabled': 4,
    }
    port_filter = 1
    privileged_ports = 3
    tcp_disabled = 5
    utp_disabled = 4
    values = {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
        4: 4,
        5: 5,
        6: 6,
    }
    __slots__ = ()


