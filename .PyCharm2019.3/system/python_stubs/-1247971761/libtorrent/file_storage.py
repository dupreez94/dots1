# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class file_storage(__Boost_Python.instance):
    # no doc
    def add_file(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_file( (file_storage)arg1, (str)path, (int)size [, (object)flags=0 [, (int)mtime=0 [, (str)linkpath='']]]) -> None :
        
            C++ signature :
                void add_file(libtorrent::file_storage {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,long [,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::file_flags_tag, void>=0 [,long=0 [,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >='']]])
        
        add_file( (file_storage)arg1, (file_entry)entry) -> None :
        
            C++ signature :
                void add_file(libtorrent::file_storage {lvalue},libtorrent::file_entry)
        
        add_file( (file_storage)arg1, (str)path, (int)size [, (object)flags=0 [, (int)mtime=0 [, (str)linkpath='']]]) -> None :
        
            C++ signature :
                void add_file(libtorrent::file_storage {lvalue},std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >,long [,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::file_flags_tag, void>=0 [,long=0 [,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >='']]])
        """
        pass

    def at(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        at( (file_storage)arg1, (int)arg2) -> file_entry :
        
            C++ signature :
                libtorrent::file_entry at(libtorrent::file_storage {lvalue},int)
        """
        pass

    def file_flags(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_flags( (file_storage)arg1, (object)arg2) -> object :
        
            C++ signature :
                libtorrent::flags::bitfield_flag<unsigned char, libtorrent::file_flags_tag, void> file_flags(libtorrent::file_storage {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>)
        """
        pass

    def file_name(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_name( (file_storage)arg1, (object)arg2) -> object :
        
            C++ signature :
                boost::basic_string_view<char, std::char_traits<char> > file_name(libtorrent::file_storage {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>)
        """
        pass

    def file_offset(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_offset( (file_storage)arg1, (object)arg2) -> int :
        
            C++ signature :
                long file_offset(libtorrent::file_storage {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>)
        """
        pass

    def file_path(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_path( (file_storage)arg1, (object)idx [, (str)save_path='']) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > file_path(libtorrent::file_storage {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void> [,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >=''])
        """
        pass

    def file_size(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        file_size( (file_storage)arg1, (object)arg2) -> int :
        
            C++ signature :
                long file_size(libtorrent::file_storage {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>)
        """
        pass

    def hash(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        hash( (file_storage)arg1, (object)arg2) -> sha1_hash :
        
            C++ signature :
                libtorrent::digest32<160l> hash(libtorrent::file_storage {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>)
        """
        pass

    def is_valid(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_valid( (file_storage)arg1) -> bool :
        
            C++ signature :
                bool is_valid(libtorrent::file_storage {lvalue})
        """
        pass

    def name(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        name( (file_storage)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > name(libtorrent::file_storage {lvalue})
        """
        pass

    def num_files(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        num_files( (file_storage)arg1) -> int :
        
            C++ signature :
                int num_files(libtorrent::file_storage {lvalue})
        """
        pass

    def num_pieces(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        num_pieces( (file_storage)arg1) -> int :
        
            C++ signature :
                int num_pieces(libtorrent::file_storage {lvalue})
        """
        pass

    def piece_length(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        piece_length( (file_storage)arg1) -> int :
        
            C++ signature :
                int piece_length(libtorrent::file_storage {lvalue})
        """
        pass

    def piece_size(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        piece_size( (file_storage)arg1, (object)arg2) -> int :
        
            C++ signature :
                int piece_size(libtorrent::file_storage {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::piece_index_tag, void>)
        """
        pass

    def rename_file(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rename_file( (file_storage)arg1, (object)arg2, (str)arg3) -> None :
        
            C++ signature :
                void rename_file(libtorrent::file_storage {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        
        rename_file( (file_storage)arg1, (object)arg2, (str)arg3) -> None :
        
            C++ signature :
                void rename_file(libtorrent::file_storage {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>,std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >)
        """
        pass

    def set_name(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_name( (file_storage)arg1, (str)arg2) -> None :
        
            C++ signature :
                void set_name(libtorrent::file_storage {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        
        set_name( (file_storage)arg1, (str)arg2) -> None :
        
            C++ signature :
                void set_name(libtorrent::file_storage {lvalue},std::__cxx11::basic_string<wchar_t, std::char_traits<wchar_t>, std::allocator<wchar_t> >)
        """
        pass

    def set_num_pieces(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_num_pieces( (file_storage)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_num_pieces(libtorrent::file_storage {lvalue},int)
        """
        pass

    def set_piece_length(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_piece_length( (file_storage)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_piece_length(libtorrent::file_storage {lvalue},int)
        """
        pass

    def symlink(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        symlink( (file_storage)arg1, (object)arg2) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > symlink(libtorrent::file_storage {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::aux::file_index_tag, void>)
        """
        pass

    def total_size(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        total_size( (file_storage)arg1) -> int :
        
            C++ signature :
                long total_size(libtorrent::file_storage {lvalue})
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        """
        pass

    def __iter__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iter__( (object)arg1) -> object :
        
            C++ signature :
                boost::python::objects::iterator_range<boost::python::return_value_policy<boost::python::return_by_value, boost::python::default_call_policies>, (anonymous namespace)::FileIter> __iter__(boost::python::back_reference<libtorrent::file_storage const&>)
        """
        pass

    def __len__(self, file_storage, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (file_storage)arg1) -> int :
        
            C++ signature :
                int __len__(libtorrent::file_storage {lvalue})
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    flag_executable = 4
    flag_hidden = 2
    flag_pad_file = 1
    flag_symlink = 8
    __instance_size__ = 184


