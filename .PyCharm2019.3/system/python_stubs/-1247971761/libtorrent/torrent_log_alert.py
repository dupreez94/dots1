# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


from .torrent_alert import torrent_alert

class torrent_log_alert(torrent_alert):
    # no doc
    def log_message(self, torrent_log_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        log_message( (torrent_log_alert)arg1) -> str :
        
            C++ signature :
                char const* log_message(libtorrent::torrent_log_alert {lvalue})
        """
        pass

    def msg(self, torrent_log_alert, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        msg( (torrent_log_alert)arg1) -> str :
        
            C++ signature :
                char const* msg(libtorrent::torrent_log_alert {lvalue})
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass


