# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class session(__Boost_Python.instance):
    # no doc
    def add_dht_node(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_dht_node( (session)arg1, (tuple)arg2) -> None :
        
            C++ signature :
                void add_dht_node(libtorrent::session {lvalue},boost::python::tuple)
        """
        pass

    def add_dht_router(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_dht_router( (session)arg1, (str)router, (int)port) -> None :
        
            C++ signature :
                void add_dht_router(libtorrent::session {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,int)
        """
        pass

    def add_extension(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_extension( (session)arg1, (object)arg2) -> None :
        
            C++ signature :
                void add_extension(libtorrent::session {lvalue},boost::python::api::object)
        """
        pass

    def add_port_mapping(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_port_mapping( (session)arg1, (portmap_protocol)arg2, (int)arg3, (int)arg4) -> object :
        
            C++ signature :
                std::vector<libtorrent::aux::strong_typedef<int, libtorrent::port_mapping_tag, void>, std::allocator<libtorrent::aux::strong_typedef<int, libtorrent::port_mapping_tag, void> > > add_port_mapping(libtorrent::session {lvalue},libtorrent::portmap_protocol,int,int)
        """
        pass

    def add_torrent(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        add_torrent( (session)arg1, (dict)arg2) -> torrent_handle :
        
            C++ signature :
                libtorrent::torrent_handle add_torrent(libtorrent::session {lvalue},boost::python::dict)
        
        add_torrent( (session)arg1, (add_torrent_params)arg2) -> torrent_handle :
        
            C++ signature :
                libtorrent::torrent_handle add_torrent(libtorrent::session {lvalue},libtorrent::add_torrent_params)
        
        add_torrent( (session)arg1, (torrent_info)arg2, (str)arg3 [, (object)resume_data=None [, (storage_mode_t)storage_mode=libtorrent.storage_mode_t.storage_mode_sparse [, (bool)paused=False]]]) -> torrent_handle :
        
            C++ signature :
                libtorrent::torrent_handle add_torrent(libtorrent::session {lvalue},libtorrent::torrent_info,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > [,libtorrent::entry=None [,libtorrent::storage_mode_t=libtorrent.storage_mode_t.storage_mode_sparse [,bool=False]]])
        """
        pass

    def apply_settings(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        apply_settings( (session)arg1, (dict)arg2) -> None :
        
            C++ signature :
                void apply_settings(libtorrent::session {lvalue},boost::python::dict)
        """
        pass

    def async_add_torrent(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        async_add_torrent( (session)arg1, (dict)arg2) -> None :
        
            C++ signature :
                void async_add_torrent(libtorrent::session {lvalue},boost::python::dict)
        
        async_add_torrent( (session)arg1, (add_torrent_params)arg2) -> None :
        
            C++ signature :
                void async_add_torrent(libtorrent::session {lvalue},libtorrent::add_torrent_params)
        """
        pass

    def create_peer_class(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        create_peer_class( (session)arg1, (str)arg2) -> object :
        
            C++ signature :
                libtorrent::aux::strong_typedef<unsigned int, libtorrent::peer_class_tag, void> create_peer_class(libtorrent::session {lvalue},char const*)
        """
        pass

    def delete_peer_class(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        delete_peer_class( (session)arg1, (object)arg2) -> None :
        
            C++ signature :
                void delete_peer_class(libtorrent::session {lvalue},libtorrent::aux::strong_typedef<unsigned int, libtorrent::peer_class_tag, void>)
        """
        pass

    def delete_port_mapping(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        delete_port_mapping( (session)arg1, (object)arg2) -> None :
        
            C++ signature :
                void delete_port_mapping(libtorrent::session {lvalue},libtorrent::aux::strong_typedef<int, libtorrent::port_mapping_tag, void>)
        """
        pass

    def dht_announce(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dht_announce( (session)arg1, (sha1_hash)arg2, (int)arg3, (object)arg4) -> None :
        
            C++ signature :
                void dht_announce(libtorrent::session {lvalue},libtorrent::digest32<160l>,int,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::dht::dht_announce_flag_tag, void>)
        """
        pass

    def dht_get_immutable_item(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dht_get_immutable_item( (session)arg1, (sha1_hash)arg2) -> None :
        
            C++ signature :
                void dht_get_immutable_item(libtorrent::session {lvalue},libtorrent::digest32<160l>)
        """
        pass

    def dht_get_mutable_item(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dht_get_mutable_item( (session)arg1, (str)arg2, (str)arg3) -> None :
        
            C++ signature :
                void dht_get_mutable_item(libtorrent::session {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def dht_get_peers(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dht_get_peers( (session)arg1, (sha1_hash)arg2) -> None :
        
            C++ signature :
                void dht_get_peers(libtorrent::session {lvalue},libtorrent::digest32<160l>)
        """
        pass

    def dht_proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dht_proxy( (session)arg1) -> proxy_settings :
        
            C++ signature :
                libtorrent::aux::proxy_settings dht_proxy(libtorrent::session {lvalue})
        """
        pass

    def dht_put_immutable_item(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dht_put_immutable_item( (session)arg1, (object)arg2) -> sha1_hash :
        
            C++ signature :
                libtorrent::digest32<160l> dht_put_immutable_item(libtorrent::session {lvalue},libtorrent::entry)
        """
        pass

    def dht_put_mutable_item(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dht_put_mutable_item( (session)arg1, (str)arg2, (str)arg3, (str)arg4, (str)arg5) -> None :
        
            C++ signature :
                void dht_put_mutable_item(libtorrent::session {lvalue},std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >,std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> >)
        """
        pass

    def dht_state(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dht_state( (session)arg1) -> object :
        
            C++ signature :
                libtorrent::entry dht_state(libtorrent::session {lvalue})
        """
        pass

    def download_rate_limit(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        download_rate_limit( (session)arg1) -> int :
        
            C++ signature :
                int download_rate_limit(libtorrent::session {lvalue})
        """
        pass

    def find_torrent(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        find_torrent( (session)arg1, (sha1_hash)arg2) -> torrent_handle :
        
            C++ signature :
                libtorrent::torrent_handle find_torrent(libtorrent::session {lvalue},libtorrent::digest32<160l>)
        """
        pass

    def get_cache_info(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_cache_info( (session)arg1 [, (torrent_handle)handle=<libtorrent.torrent_handle object at 0x7fc7b1dc8b70> [, (int)flags=0]]) -> cache_status :
        
            C++ signature :
                libtorrent::cache_status get_cache_info(libtorrent::session {lvalue} [,libtorrent::torrent_handle=<libtorrent.torrent_handle object at 0x7fc7b1dc8b70> [,int=0]])
        
        get_cache_info( (session)arg1, (sha1_hash)arg2) -> list :
        
            C++ signature :
                boost::python::list get_cache_info(libtorrent::session {lvalue},libtorrent::digest32<160l>)
        """
        pass

    def get_cache_status(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_cache_status( (session)arg1) -> cache_status :
        
            C++ signature :
                libtorrent::cache_status get_cache_status(libtorrent::session {lvalue})
        """
        pass

    def get_dht_settings(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_dht_settings( (session)arg1) -> dht_settings :
        
            C++ signature :
                libtorrent::dht::dht_settings get_dht_settings(libtorrent::session {lvalue})
        """
        pass

    def get_ip_filter(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_ip_filter( (session)arg1) -> ip_filter :
        
            C++ signature :
                libtorrent::ip_filter get_ip_filter(libtorrent::session {lvalue})
        """
        pass

    def get_peer_class(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_peer_class( (session)arg1, (object)arg2) -> dict :
        
            C++ signature :
                boost::python::dict get_peer_class(libtorrent::session {lvalue},libtorrent::aux::strong_typedef<unsigned int, libtorrent::peer_class_tag, void>)
        """
        pass

    def get_pe_settings(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_pe_settings( (session)arg1) -> pe_settings :
        
            C++ signature :
                libtorrent::pe_settings get_pe_settings(libtorrent::session {lvalue})
        """
        pass

    def get_settings(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_settings( (session)arg1) -> dict :
        
            C++ signature :
                boost::python::dict get_settings(libtorrent::session)
        """
        pass

    def get_torrents(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_torrents( (session)arg1) -> list :
        
            C++ signature :
                boost::python::list get_torrents(libtorrent::session {lvalue})
        """
        pass

    def get_torrent_status(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        get_torrent_status( (session)session, (object)pred [, (int)flags=0]) -> list :
        
            C++ signature :
                boost::python::list get_torrent_status(libtorrent::session {lvalue},boost::python::api::object [,int=0])
        """
        pass

    def i2p_proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        i2p_proxy( (session)arg1) -> proxy_settings :
        
            C++ signature :
                libtorrent::aux::proxy_settings i2p_proxy(libtorrent::session {lvalue})
        """
        pass

    def id(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        id( (session)arg1) -> sha1_hash :
        
            C++ signature :
                libtorrent::digest32<160l> id(libtorrent::session {lvalue})
        """
        pass

    def is_dht_running(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_dht_running( (session)arg1) -> bool :
        
            C++ signature :
                bool is_dht_running(libtorrent::session {lvalue})
        """
        pass

    def is_listening(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_listening( (session)arg1) -> bool :
        
            C++ signature :
                bool is_listening(libtorrent::session {lvalue})
        """
        pass

    def is_paused(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        is_paused( (session)arg1) -> bool :
        
            C++ signature :
                bool is_paused(libtorrent::session {lvalue})
        """
        pass

    def listen_on(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        listen_on( (session)arg1, (int)min, (int)max [, (str)interface=None [, (int)flags=0]]) -> None :
        
            C++ signature :
                void listen_on(libtorrent::session {lvalue},int,int [,char const*=None [,int=0]])
        """
        pass

    def listen_port(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        listen_port( (session)arg1) -> int :
        
            C++ signature :
                unsigned short listen_port(libtorrent::session {lvalue})
        """
        pass

    def load_state(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        load_state( (session)arg1, (object)entry [, (int)flags=4294967295]) -> None :
        
            C++ signature :
                void load_state(libtorrent::session {lvalue},libtorrent::entry [,unsigned int=4294967295])
        """
        pass

    def local_download_rate_limit(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        local_download_rate_limit( (session)arg1) -> int :
        
            C++ signature :
                int local_download_rate_limit(libtorrent::session {lvalue})
        """
        pass

    def local_upload_rate_limit(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        local_upload_rate_limit( (session)arg1) -> int :
        
            C++ signature :
                int local_upload_rate_limit(libtorrent::session {lvalue})
        """
        pass

    def max_connections(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max_connections( (session)arg1) -> int :
        
            C++ signature :
                int max_connections(libtorrent::session {lvalue})
        """
        pass

    def num_connections(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        num_connections( (session)arg1) -> int :
        
            C++ signature :
                int num_connections(libtorrent::session {lvalue})
        """
        pass

    def outgoing_ports(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        outgoing_ports( (session)arg1, (int)arg2, (int)arg3) -> None :
        
            C++ signature :
                void outgoing_ports(libtorrent::session {lvalue},int,int)
        """
        pass

    def pause(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        pause( (session)arg1) -> None :
        
            C++ signature :
                void pause(libtorrent::session {lvalue})
        """
        pass

    def peer_proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        peer_proxy( (session)arg1) -> proxy_settings :
        
            C++ signature :
                libtorrent::aux::proxy_settings peer_proxy(libtorrent::session {lvalue})
        """
        pass

    def pop_alerts(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        pop_alerts( (session)arg1) -> list :
        
            C++ signature :
                boost::python::list pop_alerts(libtorrent::session {lvalue})
        """
        pass

    def post_dht_stats(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        post_dht_stats( (session)arg1) -> None :
        
            C++ signature :
                void post_dht_stats(libtorrent::session {lvalue})
        """
        pass

    def post_session_stats(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        post_session_stats( (session)arg1) -> None :
        
            C++ signature :
                void post_session_stats(libtorrent::session {lvalue})
        """
        pass

    def post_torrent_updates(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        post_torrent_updates( (session)arg1 [, (object)flags=4294967295]) -> None :
        
            C++ signature :
                void post_torrent_updates(libtorrent::session {lvalue} [,libtorrent::flags::bitfield_flag<unsigned int, libtorrent::status_flags_tag, void>=4294967295])
        """
        pass

    def proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        proxy( (session)arg1) -> proxy_settings :
        
            C++ signature :
                libtorrent::aux::proxy_settings proxy(libtorrent::session {lvalue})
        """
        pass

    def refresh_torrent_status(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        refresh_torrent_status( (session)session, (list)torrents [, (int)flags=0]) -> list :
        
            C++ signature :
                boost::python::list refresh_torrent_status(libtorrent::session {lvalue},boost::python::list [,int=0])
        """
        pass

    def remove_torrent(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        remove_torrent( (session)arg1, (torrent_handle)arg2 [, (object)option=0]) -> None :
        
            C++ signature :
                void remove_torrent(libtorrent::session {lvalue},libtorrent::torrent_handle [,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::remove_flags_tag, void>=0])
        """
        pass

    def reopen_network_sockets(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reopen_network_sockets( (session)arg1, (object)arg2) -> None :
        
            C++ signature :
                void reopen_network_sockets(libtorrent::session {lvalue},libtorrent::flags::bitfield_flag<unsigned char, libtorrent::reopen_network_flags_tag, void>)
        """
        pass

    def resume(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        resume( (session)arg1) -> None :
        
            C++ signature :
                void resume(libtorrent::session {lvalue})
        """
        pass

    def save_state(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        save_state( (session)entry [, (int)flags=4294967295]) -> object :
        
            C++ signature :
                libtorrent::entry save_state(libtorrent::session [,unsigned int=4294967295])
        """
        pass

    def set_alert_mask(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_alert_mask( (session)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_alert_mask(libtorrent::session {lvalue},unsigned int)
        """
        pass

    def set_alert_notify(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_alert_notify( (session)arg1, (object)arg2) -> None :
        
            C++ signature :
                void set_alert_notify(libtorrent::session {lvalue},boost::python::api::object)
        """
        pass

    def set_alert_queue_size_limit(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_alert_queue_size_limit( (session)arg1, (int)arg2) -> int :
        
            C++ signature :
                unsigned long set_alert_queue_size_limit(libtorrent::session {lvalue},unsigned long)
        """
        pass

    def set_dht_proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_dht_proxy( (session)arg1, (proxy_settings)arg2) -> None :
        
            C++ signature :
                void set_dht_proxy(libtorrent::session {lvalue},libtorrent::aux::proxy_settings)
        """
        pass

    def set_dht_settings(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_dht_settings( (session)arg1, (dht_settings)arg2) -> None :
        
            C++ signature :
                void set_dht_settings(libtorrent::session {lvalue},libtorrent::dht::dht_settings)
        """
        pass

    def set_download_rate_limit(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_download_rate_limit( (session)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_download_rate_limit(libtorrent::session {lvalue},int)
        """
        pass

    def set_i2p_proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_i2p_proxy( (session)arg1, (proxy_settings)arg2) -> None :
        
            C++ signature :
                void set_i2p_proxy(libtorrent::session {lvalue},libtorrent::aux::proxy_settings)
        """
        pass

    def set_ip_filter(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_ip_filter( (session)arg1, (ip_filter)arg2) -> None :
        
            C++ signature :
                void set_ip_filter(libtorrent::session {lvalue},libtorrent::ip_filter)
        """
        pass

    def set_local_download_rate_limit(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_local_download_rate_limit( (session)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_local_download_rate_limit(libtorrent::session {lvalue},int)
        """
        pass

    def set_local_upload_rate_limit(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_local_upload_rate_limit( (session)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_local_upload_rate_limit(libtorrent::session {lvalue},int)
        """
        pass

    def set_max_connections(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_max_connections( (session)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_max_connections(libtorrent::session {lvalue},int)
        """
        pass

    def set_max_half_open_connections(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_max_half_open_connections( (session)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_max_half_open_connections(libtorrent::session {lvalue},int)
        """
        pass

    def set_max_uploads(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_max_uploads( (session)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_max_uploads(libtorrent::session {lvalue},int)
        """
        pass

    def set_peer_class(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_peer_class( (session)arg1, (object)arg2, (dict)arg3) -> None :
        
            C++ signature :
                void set_peer_class(libtorrent::session {lvalue},libtorrent::aux::strong_typedef<unsigned int, libtorrent::peer_class_tag, void>,boost::python::dict)
        """
        pass

    def set_peer_class_filter(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_peer_class_filter( (session)arg1, (ip_filter)arg2) -> None :
        
            C++ signature :
                void set_peer_class_filter(libtorrent::session {lvalue},libtorrent::ip_filter)
        """
        pass

    def set_peer_class_type_filter(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_peer_class_type_filter( (session)arg1, (peer_class_type_filter)arg2) -> None :
        
            C++ signature :
                void set_peer_class_type_filter(libtorrent::session {lvalue},libtorrent::peer_class_type_filter)
        """
        pass

    def set_peer_id(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_peer_id( (session)arg1, (sha1_hash)arg2) -> None :
        
            C++ signature :
                void set_peer_id(libtorrent::session {lvalue},libtorrent::digest32<160l>)
        """
        pass

    def set_peer_proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_peer_proxy( (session)arg1, (proxy_settings)arg2) -> None :
        
            C++ signature :
                void set_peer_proxy(libtorrent::session {lvalue},libtorrent::aux::proxy_settings)
        """
        pass

    def set_pe_settings(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_pe_settings( (session)arg1, (pe_settings)arg2) -> None :
        
            C++ signature :
                void set_pe_settings(libtorrent::session {lvalue},libtorrent::pe_settings)
        """
        pass

    def set_proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_proxy( (session)arg1, (proxy_settings)arg2) -> None :
        
            C++ signature :
                void set_proxy(libtorrent::session {lvalue},libtorrent::aux::proxy_settings)
        """
        pass

    def set_severity_level(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_severity_level( (session)arg1, (severity_levels)arg2) -> None :
        
            C++ signature :
                void set_severity_level(libtorrent::session {lvalue},libtorrent::alert::severity_t)
        """
        pass

    def set_tracker_proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_tracker_proxy( (session)arg1, (proxy_settings)arg2) -> None :
        
            C++ signature :
                void set_tracker_proxy(libtorrent::session {lvalue},libtorrent::aux::proxy_settings)
        """
        pass

    def set_upload_rate_limit(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_upload_rate_limit( (session)arg1, (int)arg2) -> None :
        
            C++ signature :
                void set_upload_rate_limit(libtorrent::session {lvalue},int)
        """
        pass

    def set_web_seed_proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set_web_seed_proxy( (session)arg1, (proxy_settings)arg2) -> None :
        
            C++ signature :
                void set_web_seed_proxy(libtorrent::session {lvalue},libtorrent::aux::proxy_settings)
        """
        pass

    def start_dht(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        start_dht( (session)arg1) -> None :
        
            C++ signature :
                void start_dht(libtorrent::session {lvalue})
        
        start_dht( (session)arg1, (object)arg2) -> None :
        
            C++ signature :
                void start_dht(libtorrent::session {lvalue},libtorrent::entry)
        """
        pass

    def start_lsd(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        start_lsd( (session)arg1) -> None :
        
            C++ signature :
                void start_lsd(libtorrent::session {lvalue})
        """
        pass

    def start_natpmp(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        start_natpmp( (session)arg1) -> None :
        
            C++ signature :
                void start_natpmp(libtorrent::session {lvalue})
        """
        pass

    def start_upnp(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        start_upnp( (session)arg1) -> None :
        
            C++ signature :
                void start_upnp(libtorrent::session {lvalue})
        """
        pass

    def status(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        status( (session)arg1) -> session_status :
        
            C++ signature :
                libtorrent::session_status status(libtorrent::session {lvalue})
        """
        pass

    def stop_dht(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        stop_dht( (session)arg1) -> None :
        
            C++ signature :
                void stop_dht(libtorrent::session {lvalue})
        """
        pass

    def stop_lsd(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        stop_lsd( (session)arg1) -> None :
        
            C++ signature :
                void stop_lsd(libtorrent::session {lvalue})
        """
        pass

    def stop_natpmp(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        stop_natpmp( (session)arg1) -> None :
        
            C++ signature :
                void stop_natpmp(libtorrent::session {lvalue})
        """
        pass

    def stop_upnp(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        stop_upnp( (session)arg1) -> None :
        
            C++ signature :
                void stop_upnp(libtorrent::session {lvalue})
        """
        pass

    def tracker_proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        tracker_proxy( (session)arg1) -> proxy_settings :
        
            C++ signature :
                libtorrent::aux::proxy_settings tracker_proxy(libtorrent::session {lvalue})
        """
        pass

    def upload_rate_limit(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        upload_rate_limit( (session)arg1) -> int :
        
            C++ signature :
                int upload_rate_limit(libtorrent::session {lvalue})
        """
        pass

    def wait_for_alert(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        wait_for_alert( (session)arg1, (int)arg2) -> alert :
        
            C++ signature :
                libtorrent::alert const* wait_for_alert(libtorrent::session {lvalue},int)
        """
        pass

    def web_seed_proxy(self, session, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        web_seed_proxy( (session)arg1) -> proxy_settings :
        
            C++ signature :
                libtorrent::aux::proxy_settings web_seed_proxy(libtorrent::session {lvalue})
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (dict)settings [, (object)flags=1]) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::dict [,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::session_flags_tag, void>=1])
        
        __init__( (object)arg1 [, (fingerprint)fingerprint=<libtorrent.fingerprint object at 0x7fc7b1dc8b10> [, (object)flags=3 [, (object)alert_mask=1]]]) -> None :
        
            C++ signature :
                void __init__(_object* [,libtorrent::fingerprint=<libtorrent.fingerprint object at 0x7fc7b1dc8b10> [,libtorrent::flags::bitfield_flag<unsigned char, libtorrent::session_flags_tag, void>=3 [,libtorrent::flags::bitfield_flag<unsigned int, libtorrent::alert_category_tag, void>=1]]])
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    global_peer_class_id = 0
    local_peer_class_id = 2
    reopen_map_ports = 1
    tcp = 1
    tcp_peer_class_id = 1
    udp = 2


