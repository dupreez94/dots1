# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


from .alert import alert

class incoming_connection_alert(alert):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    endpoint = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    ip = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    socket_type = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default



