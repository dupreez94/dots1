# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class peer_class_type_filter_socket_type_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    i2p_socket = 4
    names = {
        'i2p_socket': 4,
        'ssl_tcp_socket': 2,
        'ssl_utp_socket': 3,
        'tcp_socket': 0,
        'utp_socket': 1,
    }
    ssl_tcp_socket = 2
    ssl_utp_socket = 3
    tcp_socket = 0
    utp_socket = 1
    values = {
        0: 0,
        1: 1,
        2: 2,
        3: 3,
        4: 4,
    }
    __slots__ = ()


