# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class portmap_protocol(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    names = {
        'none': 0,
        'tcp': 1,
        'udp': 2,
    }
    none = 0
    tcp = 1
    udp = 2
    values = {
        0: 0,
        1: 1,
        2: 2,
    }
    __slots__ = ()


