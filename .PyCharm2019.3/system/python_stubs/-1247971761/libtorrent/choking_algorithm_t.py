# encoding: utf-8
# module libtorrent
# from /usr/lib/python3.8/site-packages/libtorrent.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import Boost.Python as __Boost_Python


class choking_algorithm_t(__Boost_Python.enum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    auto_expand_choker = 2
    bittyrant_choker = 3
    fixed_slots_choker = 0
    names = {
        'auto_expand_choker': 2,
        'bittyrant_choker': 3,
        'fixed_slots_choker': 0,
        'rate_based_choker': 2,
    }
    rate_based_choker = 2
    values = {
        0: 0,
        2: 2,
        3: 3,
    }
    __slots__ = ()


