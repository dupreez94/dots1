# encoding: utf-8
# module cairo._cairo calls itself cairo
# from /usr/lib/python3.8/site-packages/cairo/_cairo.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import cairo as __cairo


from .tuple import tuple

class TextExtents(tuple):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    height = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    width = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    x_advance = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    x_bearing = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y_advance = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y_bearing = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default



