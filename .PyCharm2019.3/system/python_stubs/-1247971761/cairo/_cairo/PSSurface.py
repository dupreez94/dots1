# encoding: utf-8
# module cairo._cairo calls itself cairo
# from /usr/lib/python3.8/site-packages/cairo/_cairo.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import cairo as __cairo


class PSSurface(__cairo.Surface):
    # no doc
    def dsc_begin_page_setup(self, *args, **kwargs): # real signature unknown
        pass

    def dsc_begin_setup(self, *args, **kwargs): # real signature unknown
        pass

    def dsc_comment(self, *args, **kwargs): # real signature unknown
        pass

    def get_eps(self, *args, **kwargs): # real signature unknown
        pass

    def get_levels(self, *args, **kwargs): # real signature unknown
        pass

    def level_to_string(self, *args, **kwargs): # real signature unknown
        pass

    def ps_level_to_string(self, *args, **kwargs): # real signature unknown
        pass

    def restrict_to_level(self, *args, **kwargs): # real signature unknown
        pass

    def set_eps(self, *args, **kwargs): # real signature unknown
        pass

    def set_size(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass


