# encoding: utf-8
# module cairo._cairo calls itself cairo
# from /usr/lib/python3.8/site-packages/cairo/_cairo.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import cairo as __cairo


class SVGVersion(__cairo._IntEnum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    VERSION_1_1 = 0
    VERSION_1_2 = 1
    __map = {
        0: 'VERSION_1_1',
        1: 'VERSION_1_2',
    }


