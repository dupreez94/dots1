# encoding: utf-8
# module cairo._cairo calls itself cairo
# from /usr/lib/python3.8/site-packages/cairo/_cairo.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import cairo as __cairo


class ImageSurface(__cairo.Surface):
    # no doc
    @classmethod
    def create_for_data(cls, *args, **kwargs): # real signature unknown
        pass

    @classmethod
    def create_from_png(cls, *args, **kwargs): # real signature unknown
        pass

    def format_stride_for_width(self, *args, **kwargs): # real signature unknown
        pass

    def get_data(self, *args, **kwargs): # real signature unknown
        pass

    def get_format(self, *args, **kwargs): # real signature unknown
        pass

    def get_height(self, *args, **kwargs): # real signature unknown
        pass

    def get_stride(self, *args, **kwargs): # real signature unknown
        pass

    def get_width(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass


