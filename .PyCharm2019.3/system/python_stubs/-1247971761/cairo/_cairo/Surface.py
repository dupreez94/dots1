# encoding: utf-8
# module cairo._cairo calls itself cairo
# from /usr/lib/python3.8/site-packages/cairo/_cairo.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import cairo as __cairo


from .object import object

class Surface(object):
    # no doc
    def copy_page(self, *args, **kwargs): # real signature unknown
        pass

    def create_for_rectangle(self, *args, **kwargs): # real signature unknown
        pass

    def create_similar(self, *args, **kwargs): # real signature unknown
        pass

    def create_similar_image(self, *args, **kwargs): # real signature unknown
        pass

    def finish(self, *args, **kwargs): # real signature unknown
        pass

    def flush(self, *args, **kwargs): # real signature unknown
        pass

    def get_content(self, *args, **kwargs): # real signature unknown
        pass

    def get_device(self, *args, **kwargs): # real signature unknown
        pass

    def get_device_offset(self, *args, **kwargs): # real signature unknown
        pass

    def get_device_scale(self, *args, **kwargs): # real signature unknown
        pass

    def get_fallback_resolution(self, *args, **kwargs): # real signature unknown
        pass

    def get_font_options(self, *args, **kwargs): # real signature unknown
        pass

    def get_mime_data(self, *args, **kwargs): # real signature unknown
        pass

    def has_show_text_glyphs(self, *args, **kwargs): # real signature unknown
        pass

    def map_to_image(self, *args, **kwargs): # real signature unknown
        pass

    def mark_dirty(self, *args, **kwargs): # real signature unknown
        pass

    def mark_dirty_rectangle(self, *args, **kwargs): # real signature unknown
        pass

    def set_device_offset(self, *args, **kwargs): # real signature unknown
        pass

    def set_device_scale(self, *args, **kwargs): # real signature unknown
        pass

    def set_fallback_resolution(self, *args, **kwargs): # real signature unknown
        pass

    def set_mime_data(self, *args, **kwargs): # real signature unknown
        pass

    def show_page(self, *args, **kwargs): # real signature unknown
        pass

    def supports_mime_type(self, *args, **kwargs): # real signature unknown
        pass

    def unmap_image(self, *args, **kwargs): # real signature unknown
        pass

    def write_to_png(self, *args, **kwargs): # real signature unknown
        pass

    def __enter__(self, *args, **kwargs): # real signature unknown
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __exit__(self, *args, **kwargs): # real signature unknown
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __hash__(self, *args, **kwargs): # real signature unknown
        """ Return hash(self). """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass


