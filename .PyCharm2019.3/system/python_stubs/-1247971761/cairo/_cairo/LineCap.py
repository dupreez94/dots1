# encoding: utf-8
# module cairo._cairo calls itself cairo
# from /usr/lib/python3.8/site-packages/cairo/_cairo.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
import cairo as __cairo


class LineCap(__cairo._IntEnum):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    BUTT = 0
    ROUND = 1
    SQUARE = 2
    __map = {
        0: 'BUTT',
        1: 'ROUND',
        2: 'SQUARE',
    }


