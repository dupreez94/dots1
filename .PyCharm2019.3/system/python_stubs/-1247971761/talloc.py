# encoding: utf-8
# module talloc
# from /usr/lib/python3.8/site-packages/talloc.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
""" Python wrapping of talloc-maintained objects. """
# no imports

# functions

def enable_null_tracking(*args, **kwargs): # real signature unknown
    """ enable tracking of the NULL object """
    pass

def report_full(*args, **kwargs): # real signature unknown
    """ show a talloc tree for an object """
    pass

def total_blocks(*args, **kwargs): # real signature unknown
    """ return talloc block count """
    pass

# classes

class BaseObject(object):
    """ Python wrapper for a talloc-maintained object. """
    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    __hash__ = None


class GenericObject(BaseObject):
    """ Python wrapper for a talloc-maintained object. """
    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class Object(object):
    """ Python wrapper for a talloc-maintained object. """
    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    def __repr__(self, *args, **kwargs): # real signature unknown
        """ Return repr(self). """
        pass

    __hash__ = None


# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>'

__spec__ = None # (!) real value is "ModuleSpec(name='talloc', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>, origin='/usr/lib/python3.8/site-packages/talloc.cpython-38-x86_64-linux-gnu.so')"

