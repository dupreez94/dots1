# encoding: utf-8
# module PIL._webp
# from /usr/lib/python3.8/site-packages/PIL/_webp.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc
# no imports

# Variables with simple values

HAVE_TRANSPARENCY = True
HAVE_WEBPANIM = True
HAVE_WEBPMUX = True

# functions

def WebPAnimDecoder(*args, **kwargs): # real signature unknown
    """ WebPAnimDecoder """
    pass

def WebPAnimEncoder(*args, **kwargs): # real signature unknown
    """ WebPAnimEncoder """
    pass

def WebPDecode(*args, **kwargs): # real signature unknown
    """ WebPDecode """
    pass

def WebPDecoderBuggyAlpha(*args, **kwargs): # real signature unknown
    """ WebPDecoderBuggyAlpha """
    pass

def WebPDecoderVersion(*args, **kwargs): # real signature unknown
    """ WebPVersion """
    pass

def WebPEncode(*args, **kwargs): # real signature unknown
    """ WebPEncode """
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1520>'

__spec__ = None # (!) real value is "ModuleSpec(name='PIL._webp', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1520>, origin='/usr/lib/python3.8/site-packages/PIL/_webp.cpython-38-x86_64-linux-gnu.so')"

