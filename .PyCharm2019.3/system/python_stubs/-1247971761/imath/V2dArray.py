# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V2dArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Vec2 """
    def bounds(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        bounds( (V2dArray)arg1) -> Box2d :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec2<double> > bounds(PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def cross(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        cross( (V2dArray)arg1, (V2d)x) -> DoubleArray :
            cross(x) - return the cross product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<double> cross(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        cross( (V2dArray)arg1, (V2dArray)x) -> DoubleArray :
            cross(x) - return the cross product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<double> cross(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def dot(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V2dArray)arg1, (V2d)x) -> DoubleArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<double> dot(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        dot( (V2dArray)arg1, (V2dArray)x) -> DoubleArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<double> dot(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def ifelse(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (V2dArray)arg1, (IntArray)arg2, (V2d)arg3) -> V2dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > ifelse(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec2<double>)
        
        ifelse( (V2dArray)arg1, (IntArray)arg2, (V2dArray)arg3) -> V2dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > ifelse(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def length(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V2dArray)arg1) -> DoubleArray :
        
            C++ signature :
                PyImath::FixedArray<double> length(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def length2(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V2dArray)arg1) -> DoubleArray :
        
            C++ signature :
                PyImath::FixedArray<double> length2(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def max(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max( (V2dArray)arg1) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> max(PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def min(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        min( (V2dArray)arg1) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> min(PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def normalize(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V2dArray)arg1) -> V2dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} normalize(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def normalized(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V2dArray)arg1) -> V2dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > normalized(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def reduce(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (V2dArray)arg1) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> reduce(PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def __add__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __add__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __add__( (V2dArray)arg1, (V2dArray)x) -> V2dArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __add__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def __copy__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V2dArray)arg1) -> V2dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __copy__(PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def __deepcopy__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V2dArray)arg1, (dict)arg2) -> V2dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __deepcopy__(PyImath::FixedArray<Imath_2_4::Vec2<double> >,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __div__( (V2dArray)arg1, (V2dArray)x) -> V2dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        __div__( (V2dArray)arg1, (float)x) -> V2dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},double)
        
        __div__( (V2dArray)arg1, (DoubleArray)x) -> V2dArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __eq__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V2dArray)arg1, (V2d)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __eq__( (V2dArray)arg1, (V2dArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def __getitem__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V2dArray)arg1, (object)arg2) -> V2dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},_object*)
        
        __getitem__( (V2dArray)arg1, (IntArray)arg2) -> V2dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (V2dArray)arg1, (int)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},long)
        
        __getitem__( (V2dArray)arg1, (int)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},long)
        """
        pass

    def __iadd__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __iadd__( (V2dArray)arg1, (V2dArray)x) -> V2dArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def __idiv__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __idiv__( (V2dArray)arg1, (V2dArray)x) -> V2dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        __idiv__( (V2dArray)arg1, (float)x) -> V2dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},double)
        
        __idiv__( (V2dArray)arg1, (DoubleArray)x) -> V2dArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __imul__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __imul__( (V2dArray)arg1, (V2dArray)x) -> V2dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        __imul__( (V2dArray)arg1, (float)x) -> V2dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},double)
        
        __imul__( (V2dArray)arg1, (DoubleArray)x) -> V2dArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (V2dArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        __init__( (object)arg1, (V2d)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<double>,unsigned long)
        
        __init__( (object)arg1, (V2iArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        __init__( (object)arg1, (V2fArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        """
        pass

    def __isub__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __isub__( (V2dArray)arg1, (V2dArray)x) -> V2dArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def __itruediv__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __itruediv__( (V2dArray)arg1, (V2dArray)x) -> V2dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        __itruediv__( (V2dArray)arg1, (float)x) -> V2dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},double)
        
        __itruediv__( (V2dArray)arg1, (DoubleArray)x) -> V2dArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __len__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V2dArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def __mul__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __mul__( (V2dArray)arg1, (V2dArray)x) -> V2dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        __mul__( (V2dArray)arg1, (float)x) -> V2dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},double)
        
        __mul__( (V2dArray)arg1, (DoubleArray)x) -> V2dArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __neg__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V2dArray)arg1) -> V2dArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __neg__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue})
        """
        pass

    def __ne__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V2dArray)arg1, (V2d)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __ne__( (V2dArray)arg1, (V2dArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def __radd__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __radd__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __rmul__( (V2dArray)arg1, (float)x) -> V2dArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},double)
        
        __rmul__( (V2dArray)arg1, (DoubleArray)x) -> V2dArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    def __rsub__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __rsub__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        """
        pass

    def __setitem__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V2dArray)arg1, (object)arg2, (V2d)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},_object*,Imath_2_4::Vec2<double>)
        
        __setitem__( (V2dArray)arg1, (IntArray)arg2, (V2d)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec2<double>)
        
        __setitem__( (V2dArray)arg1, (object)arg2, (V2dArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        __setitem__( (V2dArray)arg1, (IntArray)arg2, (V2dArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        __setitem__( (V2dArray)arg1, (int)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},long,boost::python::tuple)
        
        __setitem__( (V2dArray)arg1, (int)arg2, (list)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},long,boost::python::list)
        """
        pass

    def __sub__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __sub__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __sub__( (V2dArray)arg1, (V2dArray)x) -> V2dArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __sub__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def __truediv__(self, V2dArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V2dArray)arg1, (V2d)x) -> V2dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},Imath_2_4::Vec2<double>)
        
        __truediv__( (V2dArray)arg1, (V2dArray)x) -> V2dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        
        __truediv__( (V2dArray)arg1, (float)x) -> V2dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},double)
        
        __truediv__( (V2dArray)arg1, (DoubleArray)x) -> V2dArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<double> > {lvalue},PyImath::FixedArray<double>)
        """
        pass

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


