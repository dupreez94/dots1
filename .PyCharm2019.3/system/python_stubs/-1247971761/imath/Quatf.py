# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Quatf(__Boost_Python.instance):
    """ Quatf """
    def angle(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        angle( (Quatf)arg1) -> float :
            q.angle() -- returns the rotation angle
            (in radians) represented by quaternion q
        
            C++ signature :
                float angle(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def axis(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        axis( (Quatf)arg1) -> V3f :
            q.axis() -- returns the rotation axis
            represented by quaternion q
        
            C++ signature :
                Imath_2_4::Vec3<float> axis(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def exp(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        exp( (Quatf)arg1) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> exp(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def extract(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extract( (Quatf)arg1, (M44f)arg2) -> None :
            q.extract(m) -- extracts the rotation component
            from 4x4 matrix m and stores the result in q
        
            C++ signature :
                void extract(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Matrix44<float>)
        """
        pass

    def identity(self): # real signature unknown; restored from __doc__
        """
        identity() -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> identity()
        """
        return Quatf

    def inverse(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        inverse( (Quatf)arg1) -> Quatf :
            q.inverse() -- returns the inverse of
            quaternion q; q is not modified
            
        
            C++ signature :
                Imath_2_4::Quat<float> inverse(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def invert(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        invert( (Quatf)arg1) -> Quatf :
            q.invert() -- inverts quaternion q
            (modifying q); returns q
        
            C++ signature :
                Imath_2_4::Quat<float> {lvalue} invert(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def length(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (Quatf)arg1) -> float :
        
            C++ signature :
                float length(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def log(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        log( (Quatf)arg1) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> log(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def normalize(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (Quatf)arg1) -> Quatf :
            q.normalize() -- normalizes quaternion q
            (modifying q); returns q
        
            C++ signature :
                Imath_2_4::Quat<float> {lvalue} normalize(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def normalized(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (Quatf)arg1) -> Quatf :
            q.normalized() -- returns a normalized version
            of quaternion q; q is not modified
            
        
            C++ signature :
                Imath_2_4::Quat<float> normalized(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def r(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        r( (Quatf)arg1) -> float :
            q.r() -- returns the r (scalar) component
            of quaternion q
        
            C++ signature :
                float r(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def setAxisAngle(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setAxisAngle( (Quatf)arg1, (V3f)arg2, (float)arg3) -> Quatf :
            q.setAxisAngle(x,r) -- sets the value of
            quaternion q so that q represents a rotation
            of r radians around axis x
        
            C++ signature :
                Imath_2_4::Quat<float> {lvalue} setAxisAngle(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Vec3<float>,float)
        """
        pass

    def setR(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setR( (Quatf)arg1, (float)arg2) -> None :
            q.setR(s) -- sets the r (scalar) component
            of quaternion q to s
        
            C++ signature :
                void setR(Imath_2_4::Quat<float> {lvalue},double)
        """
        pass

    def setRotation(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setRotation( (Quatf)arg1, (V3f)arg2, (V3f)arg3) -> Quatf :
            q.setRotation(v,w) -- sets the value of
            quaternion q so that rotating vector v by
            q produces vector w
        
            C++ signature :
                Imath_2_4::Quat<float> {lvalue} setRotation(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Vec3<float>,Imath_2_4::Vec3<float>)
        """
        pass

    def setV(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setV( (Quatf)arg1, (V3f)arg2) -> None :
            q.setV(w) -- sets the v (vector) component
            of quaternion q to w
        
            C++ signature :
                void setV(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Vec3<float>)
        """
        pass

    def slerp(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        slerp( (Quatf)arg1, (Quatf)arg2, (float)arg3) -> Quatf :
            q.slerp(p,t) -- performs sperical linear
            interpolation between quaternions q and p:
            q.slerp(p,0) returns q; q.slerp(p,1) returns p.
            q and p must be normalized
            
        
            C++ signature :
                Imath_2_4::Quat<float> slerp(Imath_2_4::Quat<float>,Imath_2_4::Quat<float>,float)
        """
        pass

    def toMatrix33(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        toMatrix33( (Quatf)arg1) -> M33f :
            q.toMatrix33() -- returns a 3x3 matrix that
            represents the same rotation as quaternion q
        
            C++ signature :
                Imath_2_4::Matrix33<float> toMatrix33(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def toMatrix44(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        toMatrix44( (Quatf)arg1) -> M44f :
            q.toMatrix44() -- returns a 4x4 matrix that
            represents the same rotation as quaternion q
        
            C++ signature :
                Imath_2_4::Matrix44<float> toMatrix44(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def v(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        v( (Quatf)arg1) -> V3f :
            q.v() -- returns the v (vector) component
            of quaternion q
        
            C++ signature :
                Imath_2_4::Vec3<float> v(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def __add__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (Quatf)arg1, (Quatf)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __add__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def __copy__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Quatf)arg1) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __copy__(Imath_2_4::Quat<float>)
        """
        pass

    def __deepcopy__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Quatf)arg1, (dict)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __deepcopy__(Imath_2_4::Quat<float>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (Quatf)arg1, (Quatf)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __div__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float> {lvalue})
        
        __div__( (Quatf)arg1, (float)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __div__(Imath_2_4::Quat<float> {lvalue},float)
        """
        pass

    def __eq__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Quatf)arg1, (Quatf)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float>)
        """
        pass

    def __iadd__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (Quatf)arg1, (Quatf)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __iadd__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float>)
        """
        pass

    def __idiv__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (Quatf)arg1, (Quatf)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __idiv__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float>)
        
        __idiv__( (Quatf)arg1, (float)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __idiv__(Imath_2_4::Quat<float> {lvalue},float)
        """
        pass

    def __imul__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (Quatf)arg1, (Quatf)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __imul__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float>)
        
        __imul__( (Quatf)arg1, (float)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __imul__(Imath_2_4::Quat<float> {lvalue},float)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (Quatf)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Quat<float>)
        
        __init__( (object)arg1) -> None :
            imath Quat initialization
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (Quatf)arg2) -> None :
            imath Quat copy initialization
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Quat<float>)
        
        __init__( (object)arg1, (Quatd)arg2) -> None :
            imath Quat copy initialization
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Quat<double>)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4, (float)arg5) -> None :
            make Quat from components
        
            C++ signature :
                void __init__(_object*,float,float,float,float)
        
        __init__( (object)arg1, (float)arg2, (V3f)arg3) -> None :
            make Quat from components
        
            C++ signature :
                void __init__(_object*,float,Imath_2_4::Vec3<float>)
        
        __init__( (object)arg1, (Eulerf)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Euler<float>)
        
        __init__( (object)arg1, (M33f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix33<float>)
        
        __init__( (object)arg1, (M44f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix44<float>)
        """
        pass

    def __invert__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __invert__( (Quatf)arg1) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __invert__(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def __isub__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (Quatf)arg1, (Quatf)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __isub__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float>)
        """
        pass

    def __itruediv__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (Quatf)arg1, (Quatf)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __itruediv__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float>)
        
        __itruediv__( (Quatf)arg1, (float)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __itruediv__(Imath_2_4::Quat<float> {lvalue},float)
        """
        pass

    def __mul__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (Quatf)arg1, (M33f)arg2) -> M33f :
        
            C++ signature :
                Imath_2_4::Matrix33<float> __mul__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Matrix33<float> {lvalue})
        
        __mul__( (Quatf)arg1, (Quatf)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __mul__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float> {lvalue})
        
        __mul__( (Quatf)arg1, (float)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __mul__(Imath_2_4::Quat<float> {lvalue},float)
        """
        pass

    def __neg__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (Quatf)arg1) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __neg__(Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def __ne__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Quatf)arg1, (Quatf)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Quatf)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Quat<float>)
        """
        pass

    def __rmul__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (Quatf)arg1, (M33f)arg2) -> M33f :
        
            C++ signature :
                Imath_2_4::Matrix33<float> __rmul__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Matrix33<float> {lvalue})
        
        __rmul__( (Quatf)arg1, (float)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __rmul__(Imath_2_4::Quat<float> {lvalue},float)
        
        __rmul__( (Quatf)arg1, (V3f)arg2) -> V3f :
        
            C++ signature :
                Imath_2_4::Vec3<float> __rmul__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Vec3<float>)
        
        __rmul__( (Quatf)arg1, (V3fArray)arg2) -> V3fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec3<float> > __rmul__(Imath_2_4::Quat<float> {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<float> >)
        """
        pass

    def __str__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (Quatf)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Quat<float>)
        """
        pass

    def __sub__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (Quatf)arg1, (Quatf)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __sub__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float> {lvalue})
        """
        pass

    def __truediv__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (Quatf)arg1, (Quatf)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __truediv__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float> {lvalue})
        
        __truediv__( (Quatf)arg1, (float)arg2) -> Quatf :
        
            C++ signature :
                Imath_2_4::Quat<float> __truediv__(Imath_2_4::Quat<float> {lvalue},float)
        """
        pass

    def __xor__(self, Quatf, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __xor__( (Quatf)arg1, (Quatf)arg2) -> float :
        
            C++ signature :
                float __xor__(Imath_2_4::Quat<float> {lvalue},Imath_2_4::Quat<float> {lvalue})
        """
        pass

    __instance_size__ = 32


