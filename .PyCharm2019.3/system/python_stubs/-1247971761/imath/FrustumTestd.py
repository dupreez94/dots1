# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class FrustumTestd(__Boost_Python.instance):
    """ FrustumTestd """
    def completelyContains(self, FrustumTestd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        completelyContains( (FrustumTestd)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool completelyContains(Imath_2_4::FrustumTest<double> {lvalue},Imath_2_4::Sphere3<double>)
        
        completelyContains( (FrustumTestd)arg1, (Box3d)arg2) -> bool :
        
            C++ signature :
                bool completelyContains(Imath_2_4::FrustumTest<double> {lvalue},Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        """
        pass

    def isVisible(self, FrustumTestd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        isVisible( (FrustumTestd)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool isVisible(Imath_2_4::FrustumTest<double> {lvalue},Imath_2_4::Sphere3<double>)
        
        isVisible( (FrustumTestd)arg1, (Box3d)arg2) -> bool :
        
            C++ signature :
                bool isVisible(Imath_2_4::FrustumTest<double> {lvalue},Imath_2_4::Box<Imath_2_4::Vec3<double> >)
        
        isVisible( (FrustumTestd)arg1, (V3d)arg2) -> bool :
        
            C++ signature :
                bool isVisible(Imath_2_4::FrustumTest<double> {lvalue},Imath_2_4::Vec3<double>)
        
        isVisible( (FrustumTestd)arg1, (V3fArray)arg2) -> IntArray :
        
            C++ signature :
                PyImath::FixedArray<int> isVisible(Imath_2_4::FrustumTest<double> {lvalue},PyImath::FixedArray<Imath_2_4::Vec3<float> >)
        """
        pass

    def __copy__(self, FrustumTestd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (FrustumTestd)arg1) -> FrustumTestd :
        
            C++ signature :
                Imath_2_4::FrustumTest<double> __copy__(Imath_2_4::FrustumTest<double>)
        """
        pass

    def __deepcopy__(self, FrustumTestd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (FrustumTestd)arg1, (dict)arg2) -> FrustumTestd :
        
            C++ signature :
                Imath_2_4::FrustumTest<double> __deepcopy__(Imath_2_4::FrustumTest<double>,boost::python::dict {lvalue})
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (Frustumd)arg2, (M44d)arg3) -> None :
            create a frustum test object from a frustum and transform
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Frustum<double>,Imath_2_4::Matrix44<double>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    __instance_size__ = 544


