# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class ShortArray(__Boost_Python.instance):
    """ Fixed length array of shorts """
    def ifelse(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (ShortArray)arg1, (IntArray)arg2, (int)arg3) -> ShortArray :
        
            C++ signature :
                PyImath::FixedArray<short> ifelse(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<int>,short)
        
        ifelse( (ShortArray)arg1, (IntArray)arg2, (ShortArray)arg3) -> ShortArray :
        
            C++ signature :
                PyImath::FixedArray<short> ifelse(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<short>)
        """
        pass

    def reduce(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (ShortArray)arg1) -> int :
        
            C++ signature :
                short reduce(PyImath::FixedArray<short>)
        """
        pass

    def __add__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (ShortArray)arg1, (int)x) -> ShortArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<short> __add__(PyImath::FixedArray<short> {lvalue},short)
        
        __add__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<short> __add__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __div__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (ShortArray)arg1, (int)x) -> ShortArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<short> __div__(PyImath::FixedArray<short> {lvalue},short)
        
        __div__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<short> __div__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __eq__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (ShortArray)arg1, (int)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<short> {lvalue},short)
        
        __eq__( (ShortArray)arg1, (ShortArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __getitem__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (ShortArray)arg1, (object)arg2) -> ShortArray :
        
            C++ signature :
                PyImath::FixedArray<short> __getitem__(PyImath::FixedArray<short> {lvalue},_object*)
        
        __getitem__( (ShortArray)arg1, (IntArray)arg2) -> ShortArray :
        
            C++ signature :
                PyImath::FixedArray<short> __getitem__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (ShortArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                short __getitem__(PyImath::FixedArray<short> {lvalue},long)
        
        __getitem__( (ShortArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                short __getitem__(PyImath::FixedArray<short> {lvalue},long)
        """
        pass

    def __ge__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (ShortArray)arg1, (int)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<short> {lvalue},short)
        
        __ge__( (ShortArray)arg1, (ShortArray)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __gt__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (ShortArray)arg1, (int)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<short> {lvalue},short)
        
        __gt__( (ShortArray)arg1, (ShortArray)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __iadd__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (ShortArray)arg1, (int)x) -> ShortArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __iadd__(PyImath::FixedArray<short> {lvalue},short)
        
        __iadd__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __iadd__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __idiv__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (ShortArray)arg1, (int)x) -> ShortArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __idiv__(PyImath::FixedArray<short> {lvalue},short)
        
        __idiv__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __idiv__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __imod__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imod__( (ShortArray)arg1, (int)x) -> ShortArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __imod__(PyImath::FixedArray<short> {lvalue},short)
        
        __imod__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __imod__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __imul__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (ShortArray)arg1, (int)x) -> ShortArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __imul__(PyImath::FixedArray<short> {lvalue},short)
        
        __imul__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __imul__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (ShortArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<short>)
        
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,short,unsigned long)
        """
        pass

    def __isub__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (ShortArray)arg1, (int)x) -> ShortArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __isub__(PyImath::FixedArray<short> {lvalue},short)
        
        __isub__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __isub__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __itruediv__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (ShortArray)arg1, (int)x) -> ShortArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __itruediv__(PyImath::FixedArray<short> {lvalue},short)
        
        __itruediv__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<short> {lvalue} __itruediv__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __len__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (ShortArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<short> {lvalue})
        """
        pass

    def __le__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (ShortArray)arg1, (int)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<short> {lvalue},short)
        
        __le__( (ShortArray)arg1, (ShortArray)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __lt__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (ShortArray)arg1, (int)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<short> {lvalue},short)
        
        __lt__( (ShortArray)arg1, (ShortArray)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __mod__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mod__( (ShortArray)arg1, (int)x) -> ShortArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<short> __mod__(PyImath::FixedArray<short> {lvalue},short)
        
        __mod__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<short> __mod__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __mul__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (ShortArray)arg1, (int)x) -> ShortArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<short> __mul__(PyImath::FixedArray<short> {lvalue},short)
        
        __mul__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<short> __mul__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __neg__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (ShortArray)arg1) -> ShortArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<short> __neg__(PyImath::FixedArray<short> {lvalue})
        """
        pass

    def __ne__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (ShortArray)arg1, (int)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<short> {lvalue},short)
        
        __ne__( (ShortArray)arg1, (ShortArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __radd__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (ShortArray)arg1, (int)x) -> ShortArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<short> __radd__(PyImath::FixedArray<short> {lvalue},short)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (ShortArray)arg1, (int)x) -> ShortArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<short> __rmul__(PyImath::FixedArray<short> {lvalue},short)
        """
        pass

    def __rsub__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (ShortArray)arg1, (int)x) -> ShortArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<short> __rsub__(PyImath::FixedArray<short> {lvalue},short)
        """
        pass

    def __setitem__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (ShortArray)arg1, (object)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<short> {lvalue},_object*,short)
        
        __setitem__( (ShortArray)arg1, (IntArray)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<int>,short)
        
        __setitem__( (ShortArray)arg1, (object)arg2, (ShortArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<short> {lvalue},_object*,PyImath::FixedArray<short>)
        
        __setitem__( (ShortArray)arg1, (IntArray)arg2, (ShortArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<short>)
        """
        pass

    def __sub__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (ShortArray)arg1, (int)x) -> ShortArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<short> __sub__(PyImath::FixedArray<short> {lvalue},short)
        
        __sub__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<short> __sub__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    def __truediv__(self, ShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (ShortArray)arg1, (int)x) -> ShortArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<short> __truediv__(PyImath::FixedArray<short> {lvalue},short)
        
        __truediv__( (ShortArray)arg1, (ShortArray)x) -> ShortArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<short> __truediv__(PyImath::FixedArray<short> {lvalue},PyImath::FixedArray<short>)
        """
        pass

    __instance_size__ = 72


