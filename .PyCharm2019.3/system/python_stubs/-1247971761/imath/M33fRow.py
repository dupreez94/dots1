# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class M33fRow(__Boost_Python.instance):
    # no doc
    def __getitem__(self, M33fRow, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (M33fRow)arg1, (int)arg2) -> float :
        
            C++ signature :
                float {lvalue} __getitem__(PyImath::MatrixRow<float, 3> {lvalue},long)
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __len__(self, M33fRow, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (M33fRow)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::MatrixRow<float, 3>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, M33fRow, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (M33fRow)arg1, (int)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::MatrixRow<float, 3> {lvalue},long,float)
        """
        pass


