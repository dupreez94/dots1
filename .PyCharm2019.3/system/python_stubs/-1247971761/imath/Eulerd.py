# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


from .V3d import V3d

class Eulerd(V3d):
    """ Eulerd """
    def angleOrder(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        angleOrder( (Eulerd)arg1) -> V3i :
            angleOrder() set the angle order
        
            C++ signature :
                Imath_2_4::Vec3<int> angleOrder(Imath_2_4::Euler<double> {lvalue})
        """
        pass

    def extract(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extract( (Eulerd)arg1, (M33d)arg2) -> None :
            e.extract(m) -- extracts the rotation component
            from 3x3 matrix m and stores the result in e.
            Assumes that m does not contain shear or non-
            uniform scaling.  If necessary, you can fix m
            by calling m.removeScalingAndShear().
        
            C++ signature :
                void extract(Imath_2_4::Euler<double> {lvalue},Imath_2_4::Matrix33<double>)
        
        extract( (Eulerd)arg1, (M44d)arg2) -> None :
            e.extract(m) -- extracts the rotation component
            from 4x4 matrix m and stores the result in e.
            Assumes that m does not contain shear or non-
            uniform scaling.  If necessary, you can fix m
            by calling m.removeScalingAndShear().
        
            C++ signature :
                void extract(Imath_2_4::Euler<double> {lvalue},Imath_2_4::Matrix44<double>)
        
        extract( (Eulerd)arg1, (Quatd)arg2) -> None :
            e.extract(q) -- extracts the rotation component
            from quaternion q and stores the result in e
        
            C++ signature :
                void extract(Imath_2_4::Euler<double> {lvalue},Imath_2_4::Quat<double>)
        """
        pass

    def frameStatic(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        frameStatic( (Eulerd)arg1) -> bool :
            e.frameStatic() -- returns true if the angles of e
            are measured relative to a set of fixed axes,
            or false if the angles of e are measured relative to
            each other
            
        
            C++ signature :
                bool frameStatic(Imath_2_4::Euler<double> {lvalue})
        """
        pass

    def initialAxis(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        initialAxis( (Eulerd)arg1) -> Axis :
            e.initialAxis() -- returns the initial rotation
            axis of e (EULER_X_AXIS, EULER_Y_AXIS, EULER_Z_AXIS)
        
            C++ signature :
                Imath_2_4::Euler<double>::Axis initialAxis(Imath_2_4::Euler<double> {lvalue})
        """
        pass

    def initialRepeated(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        initialRepeated( (Eulerd)arg1) -> bool :
            e.initialRepeated() -- returns 1 if the initial
            rotation axis of e is repeated (for example,
            e.order() == EULER_XYX); returns 0 if the initial
            rotation axis is not repeated.
            
        
            C++ signature :
                bool initialRepeated(Imath_2_4::Euler<double> {lvalue})
        """
        pass

    def makeNear(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        makeNear( (Eulerd)arg1, (Eulerd)arg2) -> None :
            e.makeNear(t) -- adjusts Euler e so that it
            represents the same rotation as before, but the
            individual angles of e differ from the angles of
            t by as little as possible.
            This method might not make sense if e.order()
            and t.order() are different
            
        
            C++ signature :
                void makeNear(Imath_2_4::Euler<double> {lvalue},Imath_2_4::Euler<double> {lvalue})
        """
        pass

    def order(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        order( (Eulerd)arg1) -> Order :
            e.order() -- returns the rotation order in e
            (EULER_XYZ, EULER_XZY, ...)
        
            C++ signature :
                Imath_2_4::Euler<double>::Order order(Imath_2_4::Euler<double> {lvalue})
        """
        pass

    def parityEven(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        parityEven( (Eulerd)arg1) -> bool :
            e.parityEven() -- returns the parity of the
            axis permutation of e
            
        
            C++ signature :
                bool parityEven(Imath_2_4::Euler<double> {lvalue})
        """
        pass

    def set(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        set( (Eulerd)arg1, (Axis)arg2, (int)arg3, (int)arg4, (int)arg5) -> None :
            e.set(i,r,p,f) -- sets the rotation order in e
            according to the following flags:
            
               i   initial axis (EULER_X_AXIS,
                   EULER_Y_AXIS or EULER_Z_AXIS)
            
               r   rotation angles are measured relative
                   to each other (r == 1), or relative to a
                   set of fixed axes (r == 0)
            
               p   parity of axis permutation is even (r == 1)
                   or odd (r == 0)
            
               f   first rotation axis is repeated (f == 1)
            	or not repeated (f == 0)
            
        
            C++ signature :
                void set(Imath_2_4::Euler<double> {lvalue},Imath_2_4::Euler<float>::Axis,int,int,int)
        """
        pass

    def setOrder(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setOrder( (Eulerd)arg1, (Order)arg2) -> None :
            e.setOrder(o) -- sets the rotation order in e
            to o (EULER_XYZ, EULER_XZY, ...)
        
            C++ signature :
                void setOrder(Imath_2_4::Euler<double> {lvalue},Imath_2_4::Euler<float>::Order)
        """
        pass

    def setXYZVector(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setXYZVector( (Eulerd)arg1, (V3d)arg2) -> None :
            e.setXYZVector(v) -- sets the three rotation
            angles in e to v[0], v[1], v[2]
        
            C++ signature :
                void setXYZVector(Imath_2_4::Euler<double> {lvalue},Imath_2_4::Vec3<double>)
        
        setXYZVector( (Eulerd)arg1, (tuple)arg2) -> None :
        
            C++ signature :
                void setXYZVector(Imath_2_4::Euler<double> {lvalue},boost::python::tuple)
        """
        pass

    def toMatrix33(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        toMatrix33( (Eulerd)arg1) -> M33d :
            e.toMatrix33() -- converts e into a 3x3 matrix
            
        
            C++ signature :
                Imath_2_4::Matrix33<double> toMatrix33(Imath_2_4::Euler<double> {lvalue})
        """
        pass

    def toMatrix44(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        toMatrix44( (Eulerd)arg1) -> M44d :
            e.toMatrix44() -- converts e into a 4x4 matrix
            
        
            C++ signature :
                Imath_2_4::Matrix44<double> toMatrix44(Imath_2_4::Euler<double> {lvalue})
        """
        pass

    def toQuat(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        toQuat( (Eulerd)arg1) -> Quatd :
            e.toQuat() -- converts e into a quaternion
            
        
            C++ signature :
                Imath_2_4::Quat<double> toQuat(Imath_2_4::Euler<double> {lvalue})
        """
        pass

    def toXYZVector(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        toXYZVector( (Eulerd)arg1) -> V3d :
            e.toXYZVector() -- converts e into an XYZ
            rotation vector
        
            C++ signature :
                Imath_2_4::Vec3<double> toXYZVector(Imath_2_4::Euler<double> {lvalue})
        """
        pass

    def __copy__(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Eulerd)arg1) -> Eulerd :
        
            C++ signature :
                Imath_2_4::Euler<double> __copy__(Imath_2_4::Euler<double>)
        """
        pass

    def __deepcopy__(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Eulerd)arg1, (dict)arg2) -> Eulerd :
        
            C++ signature :
                Imath_2_4::Euler<double> __deepcopy__(Imath_2_4::Euler<double>,boost::python::dict {lvalue})
        """
        pass

    def __eq__(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Eulerd)arg1, (Eulerd)arg2) -> bool :
        
            C++ signature :
                bool __eq__(Imath_2_4::Euler<double>,Imath_2_4::Euler<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (Eulerd)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Euler<double>)
        
        __init__( (object)arg1) -> None :
            imath Euler default construction
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (V3d)arg2 [, (Order)arg3]) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Vec3<double> [,Imath_2_4::Euler<float>::Order])
        
        __init__( (object)arg1, (V3d)arg2, (int)arg3) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Vec3<double>,int)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4 [, (Order)arg5]) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,double,double,double [,Imath_2_4::Euler<float>::Order])
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4, (int)arg5) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,double,double,double,int)
        
        __init__( (object)arg1, (M33d)arg2, (Order)arg3) -> object :
            Euler-from-matrix construction assumes, but does
            not verify, that the matrix includes no shear or
            non-uniform scaling.  If necessary, you can fix
            the matrix by calling the removeScalingAndShear()
            function.
            
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix33<double>,Imath_2_4::Euler<float>::Order)
        
        __init__( (object)arg1, (M33d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix33<double>)
        
        __init__( (object)arg1, (M33d)arg2, (int)arg3) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix33<double>,int)
        
        __init__( (object)arg1, (M44d)arg2 [, (Order)arg3]) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix44<double> [,Imath_2_4::Euler<float>::Order])
        
        __init__( (object)arg1, (M44d)arg2, (int)arg3) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Matrix44<double>,int)
        
        __init__( (object)arg1 [, (Order)arg2]) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object [,Imath_2_4::Euler<float>::Order])
        
        __init__( (object)arg1, (int)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,int)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,double,double,double)
        
        __init__( (object)arg1, (Quatd)arg2 [, (Order)arg3]) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Quat<double> [,Imath_2_4::Euler<float>::Order])
        
        __init__( (object)arg1, (Quatd)arg2, (int)arg3) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Quat<double>,int)
        
        __init__( (object)arg1, (Eulerf)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Euler<float>)
        
        __init__( (object)arg1, (Eulerd)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Euler<double>)
        """
        pass

    def __ne__(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Eulerd)arg1, (Eulerd)arg2) -> bool :
        
            C++ signature :
                bool __ne__(Imath_2_4::Euler<double>,Imath_2_4::Euler<double>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Eulerd)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Euler<double>)
        """
        pass

    def __str__(self, Eulerd, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (Eulerd)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Euler<double>)
        """
        pass

    Axis = None # (!) real value is "<class 'imath.Axis'>"
    Default = 257
    IJKLayout = 1
    InputLayout = None # (!) real value is "<class 'imath.InputLayout'>"
    Order = None # (!) real value is "<class 'imath.Order'>"
    X = 0
    XYX = 273
    XYXr = 8208
    XYZ = 257
    XYZLayout = 0
    XYZr = 8192
    XZX = 17
    XZXr = 8464
    XZY = 1
    XZYr = 8448
    Y = 1
    YXY = 4113
    YXYr = 4368
    YXZ = 4097
    YXZr = 4352
    YZX = 4353
    YZXr = 4096
    YZY = 4369
    YZYr = 4112
    Z = 2
    ZXY = 8449
    ZXYr = 0
    ZXZ = 8465
    ZXZr = 16
    ZYX = 8193
    ZYXr = 256
    ZYZ = 8209
    ZYZr = 272
    __instance_size__ = 48


