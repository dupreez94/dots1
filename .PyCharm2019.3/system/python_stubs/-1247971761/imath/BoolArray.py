# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class BoolArray(__Boost_Python.instance):
    """ Fixed length array of bool """
    def ifelse(self, BoolArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (BoolArray)arg1, (IntArray)arg2, (bool)arg3) -> BoolArray :
        
            C++ signature :
                PyImath::FixedArray<bool> ifelse(PyImath::FixedArray<bool> {lvalue},PyImath::FixedArray<int>,bool)
        
        ifelse( (BoolArray)arg1, (IntArray)arg2, (BoolArray)arg3) -> BoolArray :
        
            C++ signature :
                PyImath::FixedArray<bool> ifelse(PyImath::FixedArray<bool> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<bool>)
        """
        pass

    def __eq__(self, BoolArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (BoolArray)arg1, (bool)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<bool> {lvalue},bool)
        
        __eq__( (BoolArray)arg1, (BoolArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<bool> {lvalue},PyImath::FixedArray<bool>)
        """
        pass

    def __getitem__(self, BoolArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (BoolArray)arg1, (object)arg2) -> BoolArray :
        
            C++ signature :
                PyImath::FixedArray<bool> __getitem__(PyImath::FixedArray<bool> {lvalue},_object*)
        
        __getitem__( (BoolArray)arg1, (IntArray)arg2) -> BoolArray :
        
            C++ signature :
                PyImath::FixedArray<bool> __getitem__(PyImath::FixedArray<bool> {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (BoolArray)arg1, (int)arg2) -> bool :
        
            C++ signature :
                bool __getitem__(PyImath::FixedArray<bool> {lvalue},long)
        
        __getitem__( (BoolArray)arg1, (int)arg2) -> bool :
        
            C++ signature :
                bool __getitem__(PyImath::FixedArray<bool> {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (BoolArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<bool>)
        
        __init__( (object)arg1, (bool)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,bool,unsigned long)
        """
        pass

    def __len__(self, BoolArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (BoolArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<bool> {lvalue})
        """
        pass

    def __ne__(self, BoolArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (BoolArray)arg1, (bool)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<bool> {lvalue},bool)
        
        __ne__( (BoolArray)arg1, (BoolArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<bool> {lvalue},PyImath::FixedArray<bool>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, BoolArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (BoolArray)arg1, (object)arg2, (bool)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<bool> {lvalue},_object*,bool)
        
        __setitem__( (BoolArray)arg1, (IntArray)arg2, (bool)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<bool> {lvalue},PyImath::FixedArray<int>,bool)
        
        __setitem__( (BoolArray)arg1, (object)arg2, (BoolArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<bool> {lvalue},_object*,PyImath::FixedArray<bool>)
        
        __setitem__( (BoolArray)arg1, (IntArray)arg2, (BoolArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<bool> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<bool>)
        """
        pass

    __instance_size__ = 72


