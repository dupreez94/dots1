# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V2iArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Vec2 """
    def bounds(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        bounds( (V2iArray)arg1) -> Box2i :
        
            C++ signature :
                Imath_2_4::Box<Imath_2_4::Vec2<int> > bounds(PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def cross(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        cross( (V2iArray)arg1, (V2i)x) -> IntArray :
            cross(x) - return the cross product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<int> cross(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        cross( (V2iArray)arg1, (V2iArray)x) -> IntArray :
            cross(x) - return the cross product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<int> cross(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def dot(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V2iArray)arg1, (V2i)x) -> IntArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<int> dot(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        dot( (V2iArray)arg1, (V2iArray)x) -> IntArray :
            dot(x) - return the inner product of (self,x)
        
            C++ signature :
                PyImath::FixedArray<int> dot(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def ifelse(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (V2iArray)arg1, (IntArray)arg2, (V2i)arg3) -> V2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > ifelse(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec2<int>)
        
        ifelse( (V2iArray)arg1, (IntArray)arg2, (V2iArray)arg3) -> V2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > ifelse(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def length(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V2iArray)arg1) -> IntArray :
        
            C++ signature :
                PyImath::FixedArray<int> length(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def length2(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V2iArray)arg1) -> IntArray :
        
            C++ signature :
                PyImath::FixedArray<int> length2(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def max(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max( (V2iArray)arg1) -> V2i :
        
            C++ signature :
                Imath_2_4::Vec2<int> max(PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def min(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        min( (V2iArray)arg1) -> V2i :
        
            C++ signature :
                Imath_2_4::Vec2<int> min(PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def normalize(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V2iArray)arg1) -> V2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} normalize(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def normalized(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V2iArray)arg1) -> V2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > normalized(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def reduce(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (V2iArray)arg1) -> V2i :
        
            C++ signature :
                Imath_2_4::Vec2<int> reduce(PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def __add__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __add__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __add__( (V2iArray)arg1, (V2iArray)x) -> V2iArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __add__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def __copy__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V2iArray)arg1) -> V2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __copy__(PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def __deepcopy__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V2iArray)arg1, (dict)arg2) -> V2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __deepcopy__(PyImath::FixedArray<Imath_2_4::Vec2<int> >,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __div__( (V2iArray)arg1, (V2iArray)x) -> V2iArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        __div__( (V2iArray)arg1, (int)x) -> V2iArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},int)
        
        __div__( (V2iArray)arg1, (IntArray)x) -> V2iArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __div__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __eq__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V2iArray)arg1, (V2i)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __eq__( (V2iArray)arg1, (V2iArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def __getitem__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V2iArray)arg1, (object)arg2) -> V2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},_object*)
        
        __getitem__( (V2iArray)arg1, (IntArray)arg2) -> V2iArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (V2iArray)arg1, (int)arg2) -> V2i :
        
            C++ signature :
                Imath_2_4::Vec2<int> __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},long)
        
        __getitem__( (V2iArray)arg1, (int)arg2) -> V2i :
        
            C++ signature :
                Imath_2_4::Vec2<int> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},long)
        """
        pass

    def __iadd__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __iadd__( (V2iArray)arg1, (V2iArray)x) -> V2iArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __iadd__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def __idiv__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __idiv__( (V2iArray)arg1, (V2iArray)x) -> V2iArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        __idiv__( (V2iArray)arg1, (int)x) -> V2iArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},int)
        
        __idiv__( (V2iArray)arg1, (IntArray)x) -> V2iArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __idiv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __imul__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __imul__( (V2iArray)arg1, (V2iArray)x) -> V2iArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        __imul__( (V2iArray)arg1, (int)x) -> V2iArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},int)
        
        __imul__( (V2iArray)arg1, (IntArray)x) -> V2iArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __imul__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (V2iArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        __init__( (object)arg1, (V2i)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<int>,unsigned long)
        
        __init__( (object)arg1, (V2fArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec2<float> >)
        
        __init__( (object)arg1, (V2dArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def __isub__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __isub__( (V2iArray)arg1, (V2iArray)x) -> V2iArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __isub__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def __itruediv__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __itruediv__( (V2iArray)arg1, (V2iArray)x) -> V2iArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        __itruediv__( (V2iArray)arg1, (int)x) -> V2iArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},int)
        
        __itruediv__( (V2iArray)arg1, (IntArray)x) -> V2iArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue} __itruediv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __len__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V2iArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def __mul__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __mul__( (V2iArray)arg1, (V2iArray)x) -> V2iArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        __mul__( (V2iArray)arg1, (int)x) -> V2iArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},int)
        
        __mul__( (V2iArray)arg1, (IntArray)x) -> V2iArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __mul__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __neg__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V2iArray)arg1) -> V2iArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __neg__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def __ne__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V2iArray)arg1, (V2i)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __ne__( (V2iArray)arg1, (V2iArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def __radd__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __radd__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __rmul__( (V2iArray)arg1, (int)x) -> V2iArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},int)
        
        __rmul__( (V2iArray)arg1, (IntArray)x) -> V2iArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __rmul__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __rsub__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __rsub__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        """
        pass

    def __setitem__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V2iArray)arg1, (object)arg2, (V2i)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},_object*,Imath_2_4::Vec2<int>)
        
        __setitem__( (V2iArray)arg1, (IntArray)arg2, (V2i)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Vec2<int>)
        
        __setitem__( (V2iArray)arg1, (object)arg2, (V2iArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        __setitem__( (V2iArray)arg1, (IntArray)arg2, (V2iArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        __setitem__( (V2iArray)arg1, (int)arg2, (tuple)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},long,boost::python::tuple)
        
        __setitem__( (V2iArray)arg1, (int)arg2, (list)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},long,boost::python::list)
        """
        pass

    def __sub__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __sub__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __sub__( (V2iArray)arg1, (V2iArray)x) -> V2iArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __sub__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        """
        pass

    def __truediv__(self, V2iArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V2iArray)arg1, (V2i)x) -> V2iArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        __truediv__( (V2iArray)arg1, (V2iArray)x) -> V2iArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        __truediv__( (V2iArray)arg1, (int)x) -> V2iArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},int)
        
        __truediv__( (V2iArray)arg1, (IntArray)x) -> V2iArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<int> > __truediv__(PyImath::FixedArray<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<int>)
        """
        pass

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


