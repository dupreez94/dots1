# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class UnsignedCharArray(__Boost_Python.instance):
    """ Fixed length array of unsigned chars """
    def ifelse(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (UnsignedCharArray)arg1, (IntArray)arg2, (int)arg3) -> UnsignedCharArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned char> ifelse(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<int>,unsigned char)
        
        ifelse( (UnsignedCharArray)arg1, (IntArray)arg2, (UnsignedCharArray)arg3) -> UnsignedCharArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned char> ifelse(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<unsigned char>)
        """
        pass

    def reduce(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (UnsignedCharArray)arg1) -> int :
        
            C++ signature :
                unsigned char reduce(PyImath::FixedArray<unsigned char>)
        """
        pass

    def __add__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __add__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __add__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __add__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __div__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __div__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __div__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __div__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __eq__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (UnsignedCharArray)arg1, (int)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __eq__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __getitem__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (UnsignedCharArray)arg1, (object)arg2) -> UnsignedCharArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __getitem__(PyImath::FixedArray<unsigned char> {lvalue},_object*)
        
        __getitem__( (UnsignedCharArray)arg1, (IntArray)arg2) -> UnsignedCharArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __getitem__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (UnsignedCharArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                unsigned char __getitem__(PyImath::FixedArray<unsigned char> {lvalue},long)
        
        __getitem__( (UnsignedCharArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                unsigned char __getitem__(PyImath::FixedArray<unsigned char> {lvalue},long)
        """
        pass

    def __ge__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (UnsignedCharArray)arg1, (int)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __ge__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __gt__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (UnsignedCharArray)arg1, (int)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __gt__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __iadd__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __iadd__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __iadd__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __iadd__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __idiv__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __idiv__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __idiv__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __idiv__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __imod__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imod__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __imod__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __imod__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __imod__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __imul__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __imul__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __imul__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __imul__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (UnsignedCharArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<unsigned char>)
        
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,unsigned char,unsigned long)
        """
        pass

    def __isub__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __isub__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __isub__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __isub__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __itruediv__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __itruediv__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __itruediv__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> {lvalue} __itruediv__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __len__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (UnsignedCharArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<unsigned char> {lvalue})
        """
        pass

    def __le__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (UnsignedCharArray)arg1, (int)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __le__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __lt__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (UnsignedCharArray)arg1, (int)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __lt__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __mod__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mod__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __mod__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __mod__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __mod__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __mul__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __mul__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __mul__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __mul__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __neg__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (UnsignedCharArray)arg1) -> UnsignedCharArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __neg__(PyImath::FixedArray<unsigned char> {lvalue})
        """
        pass

    def __ne__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (UnsignedCharArray)arg1, (int)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __ne__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __radd__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __radd__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __rmul__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        """
        pass

    def __rsub__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __rsub__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        """
        pass

    def __setitem__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (UnsignedCharArray)arg1, (object)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned char> {lvalue},_object*,unsigned char)
        
        __setitem__( (UnsignedCharArray)arg1, (IntArray)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<int>,unsigned char)
        
        __setitem__( (UnsignedCharArray)arg1, (object)arg2, (UnsignedCharArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned char> {lvalue},_object*,PyImath::FixedArray<unsigned char>)
        
        __setitem__( (UnsignedCharArray)arg1, (IntArray)arg2, (UnsignedCharArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<unsigned char>)
        """
        pass

    def __sub__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __sub__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __sub__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __sub__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    def __truediv__(self, UnsignedCharArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (UnsignedCharArray)arg1, (int)x) -> UnsignedCharArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __truediv__(PyImath::FixedArray<unsigned char> {lvalue},unsigned char)
        
        __truediv__( (UnsignedCharArray)arg1, (UnsignedCharArray)x) -> UnsignedCharArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned char> __truediv__(PyImath::FixedArray<unsigned char> {lvalue},PyImath::FixedArray<unsigned char>)
        """
        pass

    __instance_size__ = 72


