# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class M44fArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Matrix44 """
    def ifelse(self, M44fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (M44fArray)arg1, (IntArray)arg2, (M44f)arg3) -> M44fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix44<float> > ifelse(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Matrix44<float>)
        
        ifelse( (M44fArray)arg1, (IntArray)arg2, (M44fArray)arg3) -> M44fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix44<float> > ifelse(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Matrix44<float> >)
        """
        pass

    def __getitem__(self, M44fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (M44fArray)arg1, (object)arg2) -> M44fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix44<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue},_object*)
        
        __getitem__( (M44fArray)arg1, (IntArray)arg2) -> M44fArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Matrix44<float> > __getitem__(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (M44fArray)arg1, (int)arg2) -> M44f :
        
            C++ signature :
                Imath_2_4::Matrix44<float> __getitem__(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue},long)
        
        __getitem__( (M44fArray)arg1, (int)arg2) -> M44f :
        
            C++ signature :
                Imath_2_4::Matrix44<float> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (M44fArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Matrix44<float> >)
        
        __init__( (object)arg1, (M44f)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Matrix44<float>,unsigned long)
        
        __init__( (object)arg1, (M44fArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Matrix44<float> >)
        """
        pass

    def __len__(self, M44fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (M44fArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue})
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, M44fArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (M44fArray)arg1, (object)arg2, (M44f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue},_object*,Imath_2_4::Matrix44<float>)
        
        __setitem__( (M44fArray)arg1, (IntArray)arg2, (M44f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Matrix44<float>)
        
        __setitem__( (M44fArray)arg1, (object)arg2, (M44fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Matrix44<float> >)
        
        __setitem__( (M44fArray)arg1, (IntArray)arg2, (M44fArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Matrix44<float> >)
        
        __setitem__( (M44fArray)arg1, (int)arg2, (M44f)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Matrix44<float> > {lvalue},long,Imath_2_4::Matrix44<float>)
        """
        pass

    __instance_size__ = 72


