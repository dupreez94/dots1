# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class C4cArray(__Boost_Python.instance):
    """ Fixed length array of IMATH_NAMESPACE::Color4 """
    def ifelse(self, C4cArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (C4cArray)arg1, (IntArray)arg2, (Color4c)arg3) -> C4cArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > ifelse(PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Color4<unsigned char>)
        
        ifelse( (C4cArray)arg1, (IntArray)arg2, (C4cArray)arg3) -> C4cArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > ifelse(PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Color4<unsigned char> >)
        """
        pass

    def __getitem__(self, C4cArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (C4cArray)arg1, (object)arg2) -> C4cArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > __getitem__(PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > {lvalue},_object*)
        
        __getitem__( (C4cArray)arg1, (IntArray)arg2) -> C4cArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > __getitem__(PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (C4cArray)arg1, (int)arg2) -> Color4c :
        
            C++ signature :
                Imath_2_4::Color4<unsigned char> __getitem__(PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > {lvalue},long)
        
        __getitem__( (C4cArray)arg1, (int)arg2) -> Color4c :
        
            C++ signature :
                Imath_2_4::Color4<unsigned char> {lvalue} __getitem__(PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > {lvalue},long)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (C4cArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<Imath_2_4::Color4<unsigned char> >)
        
        __init__( (object)arg1, (Color4c)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Color4<unsigned char>,unsigned long)
        """
        pass

    def __len__(self, C4cArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (C4cArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > {lvalue})
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, C4cArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (C4cArray)arg1, (object)arg2, (Color4c)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > {lvalue},_object*,Imath_2_4::Color4<unsigned char>)
        
        __setitem__( (C4cArray)arg1, (IntArray)arg2, (Color4c)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray<int>,Imath_2_4::Color4<unsigned char>)
        
        __setitem__( (C4cArray)arg1, (object)arg2, (C4cArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > {lvalue},_object*,PyImath::FixedArray<Imath_2_4::Color4<unsigned char> >)
        
        __setitem__( (C4cArray)arg1, (IntArray)arg2, (C4cArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<Imath_2_4::Color4<unsigned char> > {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<Imath_2_4::Color4<unsigned char> >)
        """
        pass

    a = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    b = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    g = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    r = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 72


