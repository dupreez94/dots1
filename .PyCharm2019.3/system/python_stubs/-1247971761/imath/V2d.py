# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V2d(__Boost_Python.instance):
    """ V2d """
    def baseTypeEpsilon(self): # real signature unknown; restored from __doc__
        """
        baseTypeEpsilon() -> float :
            baseTypeEpsilon() epsilon value of the base type of the vector
        
            C++ signature :
                double baseTypeEpsilon()
        """
        return 0.0

    def baseTypeMax(self): # real signature unknown; restored from __doc__
        """
        baseTypeMax() -> float :
            baseTypeMax() max value of the base type of the vector
        
            C++ signature :
                double baseTypeMax()
        """
        return 0.0

    def baseTypeMin(self): # real signature unknown; restored from __doc__
        """
        baseTypeMin() -> float :
            baseTypeMin() min value of the base type of the vector
        
            C++ signature :
                double baseTypeMin()
        """
        return 0.0

    def baseTypeSmallest(self): # real signature unknown; restored from __doc__
        """
        baseTypeSmallest() -> float :
            baseTypeSmallest() smallest value of the base type of the vector
        
            C++ signature :
                double baseTypeSmallest()
        """
        return 0.0

    def closestVertex(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        closestVertex( (V2d)arg1, (V2d)arg2, (V2d)arg3, (V2d)arg4) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> closestVertex(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        """
        pass

    def cross(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        cross( (V2d)arg1, (V2d)arg2) -> float :
            v1.cross(v2) right handed cross product
        
            C++ signature :
                double cross(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        
        cross( (V2d)arg1, (V2dArray)arg2) -> DoubleArray :
            v1.cross(v2) right handed array cross product
        
            C++ signature :
                PyImath::FixedArray<double> cross(Imath_2_4::Vec2<double>,PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def dimensions(self): # real signature unknown; restored from __doc__
        """
        dimensions() -> int :
            dimensions() number of dimensions in the vector
        
            C++ signature :
                unsigned int dimensions()
        """
        return 0

    def dot(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V2d)arg1, (V2d)arg2) -> float :
            v1.dot(v2) inner product of the two vectors
        
            C++ signature :
                double dot(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        
        dot( (V2d)arg1, (V2dArray)arg2) -> DoubleArray :
            v1.dot(v2) array inner product
        
            C++ signature :
                PyImath::FixedArray<double> dot(Imath_2_4::Vec2<double>,PyImath::FixedArray<Imath_2_4::Vec2<double> >)
        """
        pass

    def equalWithAbsError(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithAbsError( (V2d)arg1, (V2d)arg2, (float)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double>,double)
        
        equalWithAbsError( (V2d)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec2<double>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def equalWithRelError(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithRelError( (V2d)arg1, (V2d)arg2, (float)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e * abs(v1[i])
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double>,double)
        
        equalWithRelError( (V2d)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec2<double>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def length(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V2d)arg1) -> float :
            length() magnitude of the vector
        
            C++ signature :
                double length(Imath_2_4::Vec2<double>)
        """
        pass

    def length2(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V2d)arg1) -> float :
            length2() square magnitude of the vector
        
            C++ signature :
                double length2(Imath_2_4::Vec2<double>)
        """
        pass

    def negate(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        negate( (V2d)arg1) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> negate(Imath_2_4::Vec2<double> {lvalue})
        """
        pass

    def normalize(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V2d)arg1) -> V2d :
            v.normalize() destructively normalizes v and returns a reference to it
        
            C++ signature :
                Imath_2_4::Vec2<double> normalize(Imath_2_4::Vec2<double> {lvalue})
        """
        pass

    def normalized(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V2d)arg1) -> V2d :
            v.normalized() returns a normalized copy of v
        
            C++ signature :
                Imath_2_4::Vec2<double> normalized(Imath_2_4::Vec2<double>)
        """
        pass

    def normalizedExc(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedExc( (V2d)arg1) -> V2d :
            v.normalizedExc() returns a normalized copy of v, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec2<double> normalizedExc(Imath_2_4::Vec2<double>)
        """
        pass

    def normalizedNonNull(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedNonNull( (V2d)arg1) -> V2d :
            v.normalizedNonNull() returns a normalized copy of v, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec2<double> normalizedNonNull(Imath_2_4::Vec2<double>)
        """
        pass

    def normalizeExc(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeExc( (V2d)arg1) -> V2d :
            v.normalizeExc() destructively normalizes V and returns a reference to it, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec2<double> normalizeExc(Imath_2_4::Vec2<double> {lvalue})
        """
        pass

    def normalizeNonNull(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeNonNull( (V2d)arg1) -> V2d :
            v.normalizeNonNull() destructively normalizes V and returns a reference to it, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec2<double> normalizeNonNull(Imath_2_4::Vec2<double> {lvalue})
        """
        pass

    def orthogonal(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        orthogonal( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> orthogonal(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        """
        pass

    def project(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        project( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> project(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        """
        pass

    def reflect(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reflect( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> reflect(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        """
        pass

    def setValue(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setValue( (V2d)arg1, (float)arg2, (float)arg3) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Vec2<double> {lvalue},double,double)
        """
        pass

    def __add__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __add__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        
        __add__( (V2d)arg1, (V2i)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __add__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<int>)
        
        __add__( (V2d)arg1, (V2f)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __add__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<float>)
        
        __add__( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __add__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        
        __add__( (V2d)arg1, (float)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __add__(Imath_2_4::Vec2<double>,double)
        
        __add__( (V2d)arg1, (tuple)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __add__(Imath_2_4::Vec2<double>,boost::python::tuple)
        
        __add__( (V2d)arg1, (list)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __add__(Imath_2_4::Vec2<double>,boost::python::list)
        """
        pass

    def __copy__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V2d)arg1) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __copy__(Imath_2_4::Vec2<double>)
        """
        pass

    def __deepcopy__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V2d)arg1, (dict)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __deepcopy__(Imath_2_4::Vec2<double>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V2d)arg1, (V2i)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __div__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<int> {lvalue})
        
        __div__( (V2d)arg1, (V2f)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __div__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<float> {lvalue})
        
        __div__( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __div__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double> {lvalue})
        
        __div__( (V2d)arg1, (tuple)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __div__(Imath_2_4::Vec2<double>,boost::python::tuple)
        
        __div__( (V2d)arg1, (list)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __div__(Imath_2_4::Vec2<double>,boost::python::list)
        
        __div__( (V2d)arg1, (float)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __div__(Imath_2_4::Vec2<double>,double)
        """
        pass

    def __eq__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V2d)arg1, (V2d)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double>)
        
        __eq__( (V2d)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __eq__(Imath_2_4::Vec2<double>,boost::python::tuple)
        """
        pass

    def __getitem__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V2d)arg1, (int)arg2) -> float :
        
            C++ signature :
                double {lvalue} __getitem__(Imath_2_4::Vec2<double> {lvalue},long)
        """
        pass

    def __ge__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (V2d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __ge__(Imath_2_4::Vec2<double>,boost::python::api::object)
        """
        pass

    def __gt__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (V2d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __gt__(Imath_2_4::Vec2<double>,boost::python::api::object)
        """
        pass

    def __iadd__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V2d)arg1, (V2i)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __iadd__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<int>)
        
        __iadd__( (V2d)arg1, (V2f)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __iadd__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<float>)
        
        __iadd__( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __iadd__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double>)
        """
        pass

    def __idiv__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V2d)arg1, (object)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __idiv__(Imath_2_4::Vec2<double> {lvalue},boost::python::api::object)
        """
        pass

    def __imul__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V2d)arg1, (V2i)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __imul__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<int>)
        
        __imul__( (V2d)arg1, (V2f)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __imul__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<float>)
        
        __imul__( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __imul__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double>)
        
        __imul__( (V2d)arg1, (float)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __imul__(Imath_2_4::Vec2<double> {lvalue},double)
        
        __imul__( (V2d)arg1, (M33f)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __imul__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Matrix33<float>)
        
        __imul__( (V2d)arg1, (M33d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __imul__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Matrix33<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (V2d)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<double>)
        
        __init__( (object)arg1) -> object :
            initialize to (0,0)
        
            C++ signature :
                void* __init__(boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2, (object)arg3) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object,boost::python::api::object)
        """
        pass

    def __isub__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V2d)arg1, (V2i)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __isub__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<int>)
        
        __isub__( (V2d)arg1, (V2f)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __isub__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<float>)
        
        __isub__( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __isub__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double>)
        """
        pass

    def __itruediv__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V2d)arg1, (object)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __itruediv__(Imath_2_4::Vec2<double> {lvalue},boost::python::api::object)
        """
        pass

    def __len__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V2d)arg1) -> int :
        
            C++ signature :
                long __len__(Imath_2_4::Vec2<double>)
        """
        pass

    def __le__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (V2d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __le__(Imath_2_4::Vec2<double>,boost::python::api::object)
        """
        pass

    def __lt__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (V2d)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __lt__(Imath_2_4::Vec2<double>,boost::python::api::object)
        """
        pass

    def __mod__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mod__( (V2d)arg1, (V2d)arg2) -> float :
        
            C++ signature :
                double __mod__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        """
        pass

    def __mul__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V2d)arg1, (V2i)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __mul__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<int>)
        
        __mul__( (V2d)arg1, (V2f)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __mul__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<float>)
        
        __mul__( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __mul__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        
        __mul__( (V2d)arg1, (float)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __mul__(Imath_2_4::Vec2<double>,double)
        
        __mul__( (V2d)arg1, (DoubleArray)arg2) -> V2dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __mul__(Imath_2_4::Vec2<double>,PyImath::FixedArray<double>)
        
        __mul__( (V2d)arg1, (tuple)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __mul__(Imath_2_4::Vec2<double>,boost::python::tuple)
        
        __mul__( (V2d)arg1, (list)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __mul__(Imath_2_4::Vec2<double>,boost::python::list)
        
        __mul__( (V2d)arg1, (V2d)arg2) -> object :
        
            C++ signature :
                _object* __mul__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double>)
        
        __mul__( (V2d)arg1, (M33f)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __mul__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Matrix33<float>)
        
        __mul__( (V2d)arg1, (M33d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __mul__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Matrix33<double>)
        """
        pass

    def __neg__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V2d)arg1) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __neg__(Imath_2_4::Vec2<double>)
        """
        pass

    def __ne__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V2d)arg1, (V2d)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double>)
        
        __ne__( (V2d)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __ne__(Imath_2_4::Vec2<double>,boost::python::tuple)
        """
        pass

    def __radd__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __radd__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        
        __radd__( (V2d)arg1, (float)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __radd__(Imath_2_4::Vec2<double>,double)
        
        __radd__( (V2d)arg1, (tuple)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __radd__(Imath_2_4::Vec2<double>,boost::python::tuple)
        
        __radd__( (V2d)arg1, (list)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __radd__(Imath_2_4::Vec2<double>,boost::python::list)
        """
        pass

    def __rdiv__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rdiv__( (V2d)arg1, (tuple)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __rdiv__(Imath_2_4::Vec2<double>,boost::python::tuple)
        
        __rdiv__( (V2d)arg1, (list)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __rdiv__(Imath_2_4::Vec2<double>,boost::python::list)
        
        __rdiv__( (V2d)arg1, (float)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __rdiv__(Imath_2_4::Vec2<double>,double)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (V2d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Vec2<double>)
        """
        pass

    def __rmul__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V2d)arg1, (float)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __rmul__(Imath_2_4::Vec2<double> {lvalue},double)
        
        __rmul__( (V2d)arg1, (DoubleArray)arg2) -> V2dArray :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec2<double> > __rmul__(Imath_2_4::Vec2<double>,PyImath::FixedArray<double>)
        
        __rmul__( (V2d)arg1, (tuple)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __rmul__(Imath_2_4::Vec2<double>,boost::python::tuple)
        
        __rmul__( (V2d)arg1, (list)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __rmul__(Imath_2_4::Vec2<double>,boost::python::list)
        """
        pass

    def __rsub__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V2d)arg1, (float)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __rsub__(Imath_2_4::Vec2<double>,double)
        
        __rsub__( (V2d)arg1, (tuple)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __rsub__(Imath_2_4::Vec2<double>,boost::python::tuple)
        
        __rsub__( (V2d)arg1, (list)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __rsub__(Imath_2_4::Vec2<double>,boost::python::list)
        """
        pass

    def __setitem__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V2d)arg1, (int)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(Imath_2_4::Vec2<double> {lvalue},long,double)
        """
        pass

    def __str__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (V2d)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Vec2<double>)
        """
        pass

    def __sub__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __sub__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        
        __sub__( (V2d)arg1, (V2i)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __sub__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<int>)
        
        __sub__( (V2d)arg1, (V2f)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __sub__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<float>)
        
        __sub__( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __sub__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        
        __sub__( (V2d)arg1, (float)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __sub__(Imath_2_4::Vec2<double>,double)
        
        __sub__( (V2d)arg1, (tuple)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __sub__(Imath_2_4::Vec2<double>,boost::python::tuple)
        
        __sub__( (V2d)arg1, (list)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __sub__(Imath_2_4::Vec2<double>,boost::python::list)
        """
        pass

    def __truediv__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V2d)arg1, (V2d)arg2) -> object :
        
            C++ signature :
                _object* __truediv__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double>)
        
        __truediv__( (V2d)arg1, (V2i)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __truediv__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<int> {lvalue})
        
        __truediv__( (V2d)arg1, (V2f)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __truediv__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<float> {lvalue})
        
        __truediv__( (V2d)arg1, (V2d)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __truediv__(Imath_2_4::Vec2<double> {lvalue},Imath_2_4::Vec2<double> {lvalue})
        
        __truediv__( (V2d)arg1, (tuple)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __truediv__(Imath_2_4::Vec2<double>,boost::python::tuple)
        
        __truediv__( (V2d)arg1, (list)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __truediv__(Imath_2_4::Vec2<double>,boost::python::list)
        
        __truediv__( (V2d)arg1, (float)arg2) -> V2d :
        
            C++ signature :
                Imath_2_4::Vec2<double> __truediv__(Imath_2_4::Vec2<double>,double)
        """
        pass

    def __xor__(self, V2d, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __xor__( (V2d)arg1, (V2d)arg2) -> float :
        
            C++ signature :
                double __xor__(Imath_2_4::Vec2<double>,Imath_2_4::Vec2<double>)
        """
        pass

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 32


