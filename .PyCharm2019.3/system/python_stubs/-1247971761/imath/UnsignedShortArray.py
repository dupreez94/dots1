# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class UnsignedShortArray(__Boost_Python.instance):
    """ Fixed length array of unsigned shorts """
    def ifelse(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (UnsignedShortArray)arg1, (IntArray)arg2, (int)arg3) -> UnsignedShortArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned short> ifelse(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<int>,unsigned short)
        
        ifelse( (UnsignedShortArray)arg1, (IntArray)arg2, (UnsignedShortArray)arg3) -> UnsignedShortArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned short> ifelse(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<unsigned short>)
        """
        pass

    def reduce(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (UnsignedShortArray)arg1) -> int :
        
            C++ signature :
                unsigned short reduce(PyImath::FixedArray<unsigned short>)
        """
        pass

    def __add__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __add__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __add__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __add__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __div__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __div__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __div__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __div__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __eq__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (UnsignedShortArray)arg1, (int)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __eq__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __getitem__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (UnsignedShortArray)arg1, (object)arg2) -> UnsignedShortArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __getitem__(PyImath::FixedArray<unsigned short> {lvalue},_object*)
        
        __getitem__( (UnsignedShortArray)arg1, (IntArray)arg2) -> UnsignedShortArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __getitem__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (UnsignedShortArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                unsigned short __getitem__(PyImath::FixedArray<unsigned short> {lvalue},long)
        
        __getitem__( (UnsignedShortArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                unsigned short __getitem__(PyImath::FixedArray<unsigned short> {lvalue},long)
        """
        pass

    def __ge__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (UnsignedShortArray)arg1, (int)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __ge__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __gt__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (UnsignedShortArray)arg1, (int)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __gt__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __iadd__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __iadd__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __iadd__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __iadd__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __idiv__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __idiv__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __idiv__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __idiv__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __imod__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imod__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __imod__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __imod__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __imod__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __imul__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __imul__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __imul__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __imul__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (UnsignedShortArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<unsigned short>)
        
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,unsigned short,unsigned long)
        """
        pass

    def __isub__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __isub__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __isub__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __isub__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __itruediv__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __itruediv__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __itruediv__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> {lvalue} __itruediv__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __len__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (UnsignedShortArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<unsigned short> {lvalue})
        """
        pass

    def __le__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (UnsignedShortArray)arg1, (int)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __le__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __lt__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (UnsignedShortArray)arg1, (int)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __lt__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __mod__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mod__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __mod__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __mod__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __mod__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __mul__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __mul__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __mul__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __mul__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __neg__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (UnsignedShortArray)arg1) -> UnsignedShortArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __neg__(PyImath::FixedArray<unsigned short> {lvalue})
        """
        pass

    def __ne__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (UnsignedShortArray)arg1, (int)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __ne__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __radd__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __radd__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __rmul__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        """
        pass

    def __rsub__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __rsub__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        """
        pass

    def __setitem__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (UnsignedShortArray)arg1, (object)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned short> {lvalue},_object*,unsigned short)
        
        __setitem__( (UnsignedShortArray)arg1, (IntArray)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<int>,unsigned short)
        
        __setitem__( (UnsignedShortArray)arg1, (object)arg2, (UnsignedShortArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned short> {lvalue},_object*,PyImath::FixedArray<unsigned short>)
        
        __setitem__( (UnsignedShortArray)arg1, (IntArray)arg2, (UnsignedShortArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<unsigned short>)
        """
        pass

    def __sub__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __sub__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __sub__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __sub__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    def __truediv__(self, UnsignedShortArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (UnsignedShortArray)arg1, (int)x) -> UnsignedShortArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __truediv__(PyImath::FixedArray<unsigned short> {lvalue},unsigned short)
        
        __truediv__( (UnsignedShortArray)arg1, (UnsignedShortArray)x) -> UnsignedShortArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned short> __truediv__(PyImath::FixedArray<unsigned short> {lvalue},PyImath::FixedArray<unsigned short>)
        """
        pass

    __instance_size__ = 72


