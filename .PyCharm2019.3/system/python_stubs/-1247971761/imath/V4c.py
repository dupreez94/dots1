# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class V4c(__Boost_Python.instance):
    """ V4c """
    def baseTypeEpsilon(self): # real signature unknown; restored from __doc__
        """
        baseTypeEpsilon() -> int :
            baseTypeEpsilon() epsilon value of the base type of the vector
        
            C++ signature :
                unsigned char baseTypeEpsilon()
        """
        return 0

    def baseTypeMax(self): # real signature unknown; restored from __doc__
        """
        baseTypeMax() -> int :
            baseTypeMax() max value of the base type of the vector
        
            C++ signature :
                unsigned char baseTypeMax()
        """
        return 0

    def baseTypeMin(self): # real signature unknown; restored from __doc__
        """
        baseTypeMin() -> int :
            baseTypeMin() min value of the base type of the vector
        
            C++ signature :
                unsigned char baseTypeMin()
        """
        return 0

    def baseTypeSmallest(self): # real signature unknown; restored from __doc__
        """
        baseTypeSmallest() -> int :
            baseTypeSmallest() smallest value of the base type of the vector
        
            C++ signature :
                unsigned char baseTypeSmallest()
        """
        return 0

    def dimensions(self): # real signature unknown; restored from __doc__
        """
        dimensions() -> int :
            dimensions() number of dimensions in the vector
        
            C++ signature :
                unsigned int dimensions()
        """
        return 0

    def dot(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        dot( (V4c)arg1, (V4c)arg2) -> int :
            v1.dot(v2) inner product of the two vectors
        
            C++ signature :
                unsigned char dot(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<unsigned char>)
        
        dot( (V4c)arg1, (object)arg2) -> UnsignedCharArray :
            v1.dot(v2) array inner product
        
            C++ signature :
                PyImath::FixedArray<unsigned char> dot(Imath_2_4::Vec4<unsigned char>,PyImath::FixedArray<Imath_2_4::Vec4<unsigned char> >)
        """
        pass

    def equalWithAbsError(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithAbsError( (V4c)arg1, (V4c)arg2, (int)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<unsigned char>,unsigned char)
        
        equalWithAbsError( (V4c)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithAbsError(Imath_2_4::Vec4<unsigned char>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def equalWithRelError(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        equalWithRelError( (V4c)arg1, (V4c)arg2, (int)arg3) -> bool :
            v1.equalWithAbsError(v2) true if the elements of v1 and v2 are the same with an absolute error of no more than e, i.e., abs(v1[i] - v2[i]) <= e * abs(v1[i])
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<unsigned char>,unsigned char)
        
        equalWithRelError( (V4c)arg1, (object)arg2, (object)arg3) -> bool :
        
            C++ signature :
                bool equalWithRelError(Imath_2_4::Vec4<unsigned char>,boost::python::api::object,boost::python::api::object)
        """
        pass

    def length(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length( (V4c)arg1) -> int :
            length() magnitude of the vector
        
            C++ signature :
                unsigned char length(Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def length2(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        length2( (V4c)arg1) -> int :
            length2() square magnitude of the vector
        
            C++ signature :
                unsigned char length2(Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def negate(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        negate( (V4c)arg1) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> negate(Imath_2_4::Vec4<unsigned char> {lvalue})
        """
        pass

    def normalize(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalize( (V4c)arg1) -> V4c :
            v.normalize() destructively normalizes v and returns a reference to it
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> normalize(Imath_2_4::Vec4<unsigned char> {lvalue})
        """
        pass

    def normalized(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalized( (V4c)arg1) -> V4c :
            v.normalized() returns a normalized copy of v
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> normalized(Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def normalizedExc(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedExc( (V4c)arg1) -> V4c :
            v.normalizedExc() returns a normalized copy of v, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> normalizedExc(Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def normalizedNonNull(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizedNonNull( (V4c)arg1) -> V4c :
            v.normalizedNonNull() returns a normalized copy of v, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> normalizedNonNull(Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def normalizeExc(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeExc( (V4c)arg1) -> V4c :
            v.normalizeExc() destructively normalizes V and returns a reference to it, throwing an exception if length() == 0
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> normalizeExc(Imath_2_4::Vec4<unsigned char> {lvalue})
        """
        pass

    def normalizeNonNull(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        normalizeNonNull( (V4c)arg1) -> V4c :
            v.normalizeNonNull() destructively normalizes V and returns a reference to it, faster if lngth() != 0
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> normalizeNonNull(Imath_2_4::Vec4<unsigned char> {lvalue})
        """
        pass

    def orthogonal(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        orthogonal( (V4c)arg1, (V4c)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> orthogonal(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def project(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        project( (V4c)arg1, (V4c)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> project(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def reflect(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reflect( (V4c)arg1, (V4c)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> reflect(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def setValue(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setValue( (V4c)arg1, (int)arg2, (int)arg3, (int)arg4, (int)arg5) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Vec4<unsigned char> {lvalue},unsigned char,unsigned char,unsigned char,unsigned char)
        """
        pass

    def __add__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (V4c)arg1, (V4c)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __add__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<unsigned char>)
        
        __add__( (V4c)arg1, (V4i)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __add__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<int>)
        
        __add__( (V4c)arg1, (V4f)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __add__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<float>)
        
        __add__( (V4c)arg1, (V4d)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __add__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<double>)
        
        __add__( (V4c)arg1, (int)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __add__(Imath_2_4::Vec4<unsigned char>,unsigned char)
        
        __add__( (V4c)arg1, (tuple)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __add__(Imath_2_4::Vec4<unsigned char>,boost::python::tuple)
        
        __add__( (V4c)arg1, (list)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __add__(Imath_2_4::Vec4<unsigned char>,boost::python::list)
        """
        pass

    def __copy__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (V4c)arg1) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __copy__(Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def __deepcopy__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (V4c)arg1, (dict)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __deepcopy__(Imath_2_4::Vec4<unsigned char>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (V4c)arg1, (V4c)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __div__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<unsigned char>)
        
        __div__( (V4c)arg1, (V4i)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __div__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<int> {lvalue})
        
        __div__( (V4c)arg1, (V4f)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __div__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<float> {lvalue})
        
        __div__( (V4c)arg1, (V4d)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __div__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<double> {lvalue})
        
        __div__( (V4c)arg1, (tuple)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __div__(Imath_2_4::Vec4<unsigned char>,boost::python::tuple)
        
        __div__( (V4c)arg1, (list)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __div__(Imath_2_4::Vec4<unsigned char>,boost::python::list)
        
        __div__( (V4c)arg1, (int)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __div__(Imath_2_4::Vec4<unsigned char>,unsigned char)
        """
        pass

    def __eq__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (V4c)arg1, (V4c)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<unsigned char>)
        
        __eq__( (V4c)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __eq__(Imath_2_4::Vec4<unsigned char>,boost::python::tuple)
        """
        pass

    def __getitem__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (V4c)arg1, (int)arg2) -> int :
        
            C++ signature :
                unsigned char {lvalue} __getitem__(Imath_2_4::Vec4<unsigned char> {lvalue},long)
        """
        pass

    def __ge__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (V4c)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __ge__(Imath_2_4::Vec4<unsigned char>,boost::python::api::object)
        """
        pass

    def __gt__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (V4c)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __gt__(Imath_2_4::Vec4<unsigned char>,boost::python::api::object)
        """
        pass

    def __iadd__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (V4c)arg1, (V4i)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __iadd__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<int>)
        
        __iadd__( (V4c)arg1, (V4f)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __iadd__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<float>)
        
        __iadd__( (V4c)arg1, (V4d)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __iadd__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<double>)
        """
        pass

    def __idiv__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (V4c)arg1, (object)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __idiv__(Imath_2_4::Vec4<unsigned char> {lvalue},boost::python::api::object)
        """
        pass

    def __imul__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (V4c)arg1, (V4i)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __imul__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<int>)
        
        __imul__( (V4c)arg1, (V4f)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __imul__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<float>)
        
        __imul__( (V4c)arg1, (V4d)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __imul__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<double>)
        
        __imul__( (V4c)arg1, (int)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __imul__(Imath_2_4::Vec4<unsigned char> {lvalue},unsigned char)
        
        __imul__( (V4c)arg1, (M44f)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __imul__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Matrix44<float>)
        
        __imul__( (V4c)arg1, (M44d)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __imul__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Matrix44<double>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (V4c)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec4<unsigned char>)
        
        __init__( (object)arg1) -> object :
            initialize to (0,0,0,0)
        
            C++ signature :
                void* __init__(boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object)
        
        __init__( (object)arg1, (object)arg2, (object)arg3, (object)arg4, (object)arg5) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::api::object,boost::python::api::object,boost::python::api::object,boost::python::api::object)
        """
        pass

    def __isub__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (V4c)arg1, (V4i)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __isub__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<int>)
        
        __isub__( (V4c)arg1, (V4f)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __isub__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<float>)
        
        __isub__( (V4c)arg1, (V4d)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __isub__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<double>)
        """
        pass

    def __itruediv__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (V4c)arg1, (object)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __itruediv__(Imath_2_4::Vec4<unsigned char> {lvalue},boost::python::api::object)
        """
        pass

    def __len__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (V4c)arg1) -> int :
        
            C++ signature :
                long __len__(Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def __le__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (V4c)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __le__(Imath_2_4::Vec4<unsigned char>,boost::python::api::object)
        """
        pass

    def __lt__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (V4c)arg1, (object)arg2) -> bool :
        
            C++ signature :
                bool __lt__(Imath_2_4::Vec4<unsigned char>,boost::python::api::object)
        """
        pass

    def __mul__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (V4c)arg1, (V4i)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __mul__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<int> {lvalue})
        
        __mul__( (V4c)arg1, (V4f)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __mul__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<float> {lvalue})
        
        __mul__( (V4c)arg1, (V4d)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __mul__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<double> {lvalue})
        
        __mul__( (V4c)arg1, (int)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __mul__(Imath_2_4::Vec4<unsigned char>,unsigned char)
        
        __mul__( (V4c)arg1, (UnsignedCharArray)arg2) -> object :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<unsigned char> > __mul__(Imath_2_4::Vec4<unsigned char>,PyImath::FixedArray<unsigned char>)
        
        __mul__( (V4c)arg1, (M44f)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __mul__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Matrix44<float>)
        
        __mul__( (V4c)arg1, (M44d)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __mul__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Matrix44<double>)
        
        __mul__( (V4c)arg1, (V4c)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __mul__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<unsigned char>)
        
        __mul__( (V4c)arg1, (tuple)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __mul__(Imath_2_4::Vec4<unsigned char>,boost::python::tuple)
        """
        pass

    def __neg__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (V4c)arg1) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __neg__(Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def __ne__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (V4c)arg1, (V4c)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<unsigned char>)
        
        __ne__( (V4c)arg1, (tuple)arg2) -> bool :
        
            C++ signature :
                bool __ne__(Imath_2_4::Vec4<unsigned char>,boost::python::tuple)
        """
        pass

    def __radd__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (V4c)arg1, (int)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __radd__(Imath_2_4::Vec4<unsigned char>,unsigned char)
        
        __radd__( (V4c)arg1, (tuple)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __radd__(Imath_2_4::Vec4<unsigned char>,boost::python::tuple)
        
        __radd__( (V4c)arg1, (list)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __radd__(Imath_2_4::Vec4<unsigned char>,boost::python::list)
        
        __radd__( (V4c)arg1, (V4c)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __radd__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def __rdiv__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rdiv__( (V4c)arg1, (tuple)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __rdiv__(Imath_2_4::Vec4<unsigned char>,boost::python::tuple)
        
        __rdiv__( (V4c)arg1, (list)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __rdiv__(Imath_2_4::Vec4<unsigned char>,boost::python::list)
        
        __rdiv__( (V4c)arg1, (int)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __rdiv__(Imath_2_4::Vec4<unsigned char>,unsigned char)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (V4c)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def __rmul__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (V4c)arg1, (int)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __rmul__(Imath_2_4::Vec4<unsigned char> {lvalue},unsigned char)
        
        __rmul__( (V4c)arg1, (UnsignedCharArray)arg2) -> object :
        
            C++ signature :
                PyImath::FixedArray<Imath_2_4::Vec4<unsigned char> > __rmul__(Imath_2_4::Vec4<unsigned char>,PyImath::FixedArray<unsigned char>)
        
        __rmul__( (V4c)arg1, (tuple)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __rmul__(Imath_2_4::Vec4<unsigned char>,boost::python::tuple)
        """
        pass

    def __rsub__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (V4c)arg1, (int)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __rsub__(Imath_2_4::Vec4<unsigned char>,unsigned char)
        
        __rsub__( (V4c)arg1, (tuple)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __rsub__(Imath_2_4::Vec4<unsigned char>,boost::python::tuple)
        
        __rsub__( (V4c)arg1, (list)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __rsub__(Imath_2_4::Vec4<unsigned char>,boost::python::list)
        """
        pass

    def __setitem__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (V4c)arg1, (int)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(Imath_2_4::Vec4<unsigned char> {lvalue},long,unsigned char)
        """
        pass

    def __str__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (V4c)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Vec4<unsigned char>)
        """
        pass

    def __sub__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (V4c)arg1, (V4c)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __sub__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<unsigned char>)
        
        __sub__( (V4c)arg1, (V4i)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __sub__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<int>)
        
        __sub__( (V4c)arg1, (V4f)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __sub__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<float>)
        
        __sub__( (V4c)arg1, (V4d)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __sub__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<double>)
        
        __sub__( (V4c)arg1, (int)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __sub__(Imath_2_4::Vec4<unsigned char>,unsigned char)
        
        __sub__( (V4c)arg1, (tuple)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __sub__(Imath_2_4::Vec4<unsigned char>,boost::python::tuple)
        
        __sub__( (V4c)arg1, (list)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __sub__(Imath_2_4::Vec4<unsigned char>,boost::python::list)
        """
        pass

    def __truediv__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (V4c)arg1, (V4c)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __truediv__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<unsigned char>)
        
        __truediv__( (V4c)arg1, (V4i)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __truediv__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<int> {lvalue})
        
        __truediv__( (V4c)arg1, (V4f)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __truediv__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<float> {lvalue})
        
        __truediv__( (V4c)arg1, (V4d)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __truediv__(Imath_2_4::Vec4<unsigned char> {lvalue},Imath_2_4::Vec4<double> {lvalue})
        
        __truediv__( (V4c)arg1, (tuple)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __truediv__(Imath_2_4::Vec4<unsigned char>,boost::python::tuple)
        
        __truediv__( (V4c)arg1, (list)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __truediv__(Imath_2_4::Vec4<unsigned char>,boost::python::list)
        
        __truediv__( (V4c)arg1, (int)arg2) -> V4c :
        
            C++ signature :
                Imath_2_4::Vec4<unsigned char> __truediv__(Imath_2_4::Vec4<unsigned char>,unsigned char)
        """
        pass

    def __xor__(self, V4c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __xor__( (V4c)arg1, (V4c)arg2) -> int :
        
            C++ signature :
                unsigned char __xor__(Imath_2_4::Vec4<unsigned char>,Imath_2_4::Vec4<unsigned char>)
        """
        pass

    w = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    x = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    y = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    z = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 24


