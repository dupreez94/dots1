# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


from .V3c import V3c

class Color3c(V3c):
    """ Color3c """
    def baseTypeEpsilon(self): # real signature unknown; restored from __doc__
        """
        baseTypeEpsilon() -> int :
            baseTypeEpsilon() epsilon value of the base type of the color
        
            C++ signature :
                unsigned char baseTypeEpsilon()
        """
        return 0

    def baseTypeMax(self): # real signature unknown; restored from __doc__
        """
        baseTypeMax() -> int :
            baseTypeMax() max value of the base type of the color
        
            C++ signature :
                unsigned char baseTypeMax()
        """
        return 0

    def baseTypeMin(self): # real signature unknown; restored from __doc__
        """
        baseTypeMin() -> int :
            baseTypeMin() min value of the base type of the color
        
            C++ signature :
                unsigned char baseTypeMin()
        """
        return 0

    def baseTypeSmallest(self): # real signature unknown; restored from __doc__
        """
        baseTypeSmallest() -> int :
            baseTypeSmallest() smallest value of the base type of the color
        
            C++ signature :
                unsigned char baseTypeSmallest()
        """
        return 0

    def dimensions(self): # real signature unknown; restored from __doc__
        """
        dimensions() -> int :
            dimensions() number of dimensions in the color
        
            C++ signature :
                unsigned int dimensions()
        """
        return 0

    def hsv2rgb(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        hsv2rgb( (Color3c)arg1) -> Color3c :
            C.hsv2rgb() -- returns a new color which is C converted from RGB to HSV
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> hsv2rgb(Imath_2_4::Color3<unsigned char> {lvalue})
        
        hsv2rgb( (tuple)arg1) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> hsv2rgb(boost::python::tuple)
        """
        pass

    def negate(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        negate( (Color3c)arg1) -> Color3c :
            component-wise multiplication by -1
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> negate(Imath_2_4::Color3<unsigned char> {lvalue})
        """
        pass

    def rgb2hsv(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rgb2hsv( (Color3c)arg1) -> Color3c :
            C.rgb2hsv() -- returns a new color which is C converted from HSV to RGB
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> rgb2hsv(Imath_2_4::Color3<unsigned char> {lvalue})
        
        rgb2hsv( (tuple)arg1) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> rgb2hsv(boost::python::tuple)
        """
        pass

    def setValue(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setValue( (Color3c)arg1, (int)arg2, (int)arg3, (int)arg4) -> None :
            C1.setValue(C2)
            C1.setValue(a,b,c) -- set C1's  elements
        
            C++ signature :
                void setValue(Imath_2_4::Color3<unsigned char> {lvalue},unsigned char,unsigned char,unsigned char)
        
        setValue( (Color3c)arg1, (Color3c)arg2) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        
        setValue( (Color3c)arg1, (tuple)arg2) -> None :
        
            C++ signature :
                void setValue(Imath_2_4::Color3<unsigned char> {lvalue},boost::python::tuple)
        """
        pass

    def __add__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (Color3c)arg1, (Color3c)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __add__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        
        __add__( (Color3c)arg1, (tuple)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __add__(Imath_2_4::Color3<unsigned char> {lvalue},boost::python::tuple)
        
        __add__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __add__(Imath_2_4::Color3<unsigned char> {lvalue},unsigned char)
        """
        pass

    def __copy__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __copy__( (Color3c)arg1) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __copy__(Imath_2_4::Color3<unsigned char>)
        """
        pass

    def __deepcopy__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __deepcopy__( (Color3c)arg1, (dict)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __deepcopy__(Imath_2_4::Color3<unsigned char>,boost::python::dict {lvalue})
        """
        pass

    def __div__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (Color3c)arg1, (Color3c)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __div__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        
        __div__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __div__(Imath_2_4::Color3<unsigned char> {lvalue},unsigned char)
        
        __div__( (Color3c)arg1, (tuple)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __div__(Imath_2_4::Color3<unsigned char> {lvalue},boost::python::tuple)
        """
        pass

    def __eq__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Color3c)arg1, (Color3c)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        """
        pass

    def __ge__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (Color3c)arg1, (Color3c)arg2) -> bool :
        
            C++ signature :
                bool __ge__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        """
        pass

    def __gt__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (Color3c)arg1, (Color3c)arg2) -> bool :
        
            C++ signature :
                bool __gt__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        """
        pass

    def __iadd__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (Color3c)arg1, (Color3c)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __iadd__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        """
        pass

    def __idiv__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (Color3c)arg1, (Color3c)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __idiv__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        
        __idiv__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __idiv__(Imath_2_4::Color3<unsigned char> {lvalue},unsigned char)
        """
        pass

    def __imul__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (Color3c)arg1, (Color3c)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __imul__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        
        __imul__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __imul__(Imath_2_4::Color3<unsigned char> {lvalue},unsigned char)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (Color3c)arg2) -> None :
            copy construction
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Color3<unsigned char>)
        
        __init__( (object)arg1) -> object :
            initialize to (0,0,0)
        
            C++ signature :
                void* __init__(boost::python::api::object)
        
        __init__( (object)arg1, (tuple)arg2) -> object :
            initialize to (r,g,b) with a python tuple
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple)
        
        __init__( (object)arg1, (list)arg2) -> object :
            initialize to (r,g,b) with a python list
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::list)
        
        __init__( (object)arg1, (float)arg2, (float)arg3, (float)arg4) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,float,float,float)
        
        __init__( (object)arg1, (int)arg2, (int)arg3, (int)arg4) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,int,int,int)
        
        __init__( (object)arg1, (float)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,float)
        
        __init__( (object)arg1, (int)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,int)
        
        __init__( (object)arg1, (Color3f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Color3<float>)
        
        __init__( (object)arg1, (object)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Color3<int>)
        
        __init__( (object)arg1, (Color3c)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Color3<unsigned char>)
        
        __init__( (object)arg1, (V3f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Vec3<float>)
        
        __init__( (object)arg1, (V3d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Vec3<double>)
        
        __init__( (object)arg1, (V3i)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Vec3<int>)
        """
        pass

    def __isub__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (Color3c)arg1, (Color3c)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __isub__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        """
        pass

    def __itruediv__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (Color3c)arg1, (Color3c)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __itruediv__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        
        __itruediv__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __itruediv__(Imath_2_4::Color3<unsigned char> {lvalue},unsigned char)
        """
        pass

    def __le__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (Color3c)arg1, (Color3c)arg2) -> bool :
        
            C++ signature :
                bool __le__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        """
        pass

    def __lt__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (Color3c)arg1, (Color3c)arg2) -> bool :
        
            C++ signature :
                bool __lt__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        """
        pass

    def __mul__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (Color3c)arg1, (Color3c)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __mul__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        
        __mul__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __mul__(Imath_2_4::Color3<unsigned char> {lvalue},unsigned char)
        
        __mul__( (Color3c)arg1, (tuple)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __mul__(Imath_2_4::Color3<unsigned char> {lvalue},boost::python::tuple)
        """
        pass

    def __neg__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (Color3c)arg1) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __neg__(Imath_2_4::Color3<unsigned char> {lvalue})
        """
        pass

    def __ne__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Color3c)arg1, (Color3c)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        """
        pass

    def __radd__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (Color3c)arg1, (tuple)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __radd__(Imath_2_4::Color3<unsigned char> {lvalue},boost::python::tuple)
        
        __radd__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __radd__(Imath_2_4::Color3<unsigned char> {lvalue},unsigned char)
        """
        pass

    def __rdiv__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rdiv__( (Color3c)arg1, (tuple)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __rdiv__(Imath_2_4::Color3<unsigned char> {lvalue},boost::python::tuple)
        
        __rdiv__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __rdiv__(Imath_2_4::Color3<unsigned char> {lvalue},unsigned char)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Color3c)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Color3<unsigned char>)
        """
        pass

    def __rmul__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __rmul__(Imath_2_4::Color3<unsigned char> {lvalue},unsigned char)
        
        __rmul__( (Color3c)arg1, (tuple)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __rmul__(Imath_2_4::Color3<unsigned char> {lvalue},boost::python::tuple)
        """
        pass

    def __rsub__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (Color3c)arg1, (tuple)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __rsub__(Imath_2_4::Color3<unsigned char> {lvalue},boost::python::tuple)
        
        __rsub__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __rsub__(Imath_2_4::Color3<unsigned char>,unsigned char)
        """
        pass

    def __str__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __str__( (Color3c)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __str__(Imath_2_4::Color3<unsigned char>)
        """
        pass

    def __sub__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (Color3c)arg1, (Color3c)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __sub__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        
        __sub__( (Color3c)arg1, (tuple)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __sub__(Imath_2_4::Color3<unsigned char> {lvalue},boost::python::tuple)
        
        __sub__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __sub__(Imath_2_4::Color3<unsigned char>,unsigned char)
        """
        pass

    def __truediv__(self, Color3c, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (Color3c)arg1, (Color3c)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __truediv__(Imath_2_4::Color3<unsigned char> {lvalue},Imath_2_4::Color3<unsigned char>)
        
        __truediv__( (Color3c)arg1, (int)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __truediv__(Imath_2_4::Color3<unsigned char> {lvalue},unsigned char)
        
        __truediv__( (Color3c)arg1, (tuple)arg2) -> Color3c :
        
            C++ signature :
                Imath_2_4::Color3<unsigned char> __truediv__(Imath_2_4::Color3<unsigned char> {lvalue},boost::python::tuple)
        """
        pass

    b = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    g = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default

    r = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default


    __instance_size__ = 24


