# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class DoubleMatrix(__Boost_Python.instance):
    """ Fixed size matrix of doubles """
    def columns(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        columns( (DoubleMatrix)arg1) -> int :
        
            C++ signature :
                int columns(PyImath::FixedMatrix<double> {lvalue})
        """
        pass

    def rows(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        rows( (DoubleMatrix)arg1) -> int :
        
            C++ signature :
                int rows(PyImath::FixedMatrix<double> {lvalue})
        """
        pass

    def __add__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __add__(PyImath::FixedMatrix<double>,PyImath::FixedMatrix<double>)
        
        __add__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __add__(PyImath::FixedMatrix<double>,double)
        """
        pass

    def __div__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __div__(PyImath::FixedMatrix<double>,PyImath::FixedMatrix<double>)
        
        __div__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __div__(PyImath::FixedMatrix<double>,double)
        """
        pass

    def __getitem__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (DoubleMatrix)arg1, (object)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __getitem__(PyImath::FixedMatrix<double> {lvalue},_object*)
        
        __getitem__( (DoubleMatrix)arg1, (int)arg2) -> DoubleArray :
        
            C++ signature :
                PyImath::FixedArray<double> const* __getitem__(PyImath::FixedMatrix<double> {lvalue},int)
        """
        pass

    def __iadd__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __iadd__(PyImath::FixedMatrix<double> {lvalue},PyImath::FixedMatrix<double>)
        
        __iadd__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __iadd__(PyImath::FixedMatrix<double> {lvalue},double)
        """
        pass

    def __idiv__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __idiv__(PyImath::FixedMatrix<double> {lvalue},PyImath::FixedMatrix<double>)
        
        __idiv__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __idiv__(PyImath::FixedMatrix<double> {lvalue},double)
        """
        pass

    def __imul__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __imul__(PyImath::FixedMatrix<double> {lvalue},PyImath::FixedMatrix<double>)
        
        __imul__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __imul__(PyImath::FixedMatrix<double> {lvalue},double)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            return an unitialized array of the specified rows and cols
        
            C++ signature :
                void __init__(_object*,int,int)
        """
        pass

    def __ipow__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ipow__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __ipow__(PyImath::FixedMatrix<double> {lvalue},double)
        
        __ipow__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __ipow__(PyImath::FixedMatrix<double> {lvalue},PyImath::FixedMatrix<double>)
        """
        pass

    def __isub__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __isub__(PyImath::FixedMatrix<double> {lvalue},PyImath::FixedMatrix<double>)
        
        __isub__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __isub__(PyImath::FixedMatrix<double> {lvalue},double)
        """
        pass

    def __itruediv__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __itruediv__(PyImath::FixedMatrix<double> {lvalue},PyImath::FixedMatrix<double>)
        
        __itruediv__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> {lvalue} __itruediv__(PyImath::FixedMatrix<double> {lvalue},double)
        """
        pass

    def __len__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (DoubleMatrix)arg1) -> int :
        
            C++ signature :
                int __len__(PyImath::FixedMatrix<double> {lvalue})
        """
        pass

    def __mul__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __mul__(PyImath::FixedMatrix<double>,PyImath::FixedMatrix<double>)
        
        __mul__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __mul__(PyImath::FixedMatrix<double>,double)
        """
        pass

    def __neg__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (DoubleMatrix)arg1) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __neg__(PyImath::FixedMatrix<double>)
        """
        pass

    def __pow__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __pow__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __pow__(PyImath::FixedMatrix<double>,double)
        
        __pow__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __pow__(PyImath::FixedMatrix<double>,PyImath::FixedMatrix<double>)
        """
        pass

    def __radd__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __radd__(PyImath::FixedMatrix<double>,double)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __rmul__(PyImath::FixedMatrix<double>,double)
        """
        pass

    def __rsub__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __rsub__(PyImath::FixedMatrix<double>,double)
        """
        pass

    def __setitem__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (DoubleMatrix)arg1, (object)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedMatrix<double> {lvalue},_object*,double)
        
        __setitem__( (DoubleMatrix)arg1, (object)arg2, (DoubleArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedMatrix<double> {lvalue},_object*,PyImath::FixedArray<double>)
        
        __setitem__( (DoubleMatrix)arg1, (object)arg2, (DoubleMatrix)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedMatrix<double> {lvalue},_object*,PyImath::FixedMatrix<double>)
        """
        pass

    def __sub__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __sub__(PyImath::FixedMatrix<double>,PyImath::FixedMatrix<double>)
        
        __sub__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __sub__(PyImath::FixedMatrix<double>,double)
        """
        pass

    def __truediv__(self, DoubleMatrix, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (DoubleMatrix)arg1, (DoubleMatrix)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __truediv__(PyImath::FixedMatrix<double>,PyImath::FixedMatrix<double>)
        
        __truediv__( (DoubleMatrix)arg1, (float)arg2) -> DoubleMatrix :
        
            C++ signature :
                PyImath::FixedMatrix<double> __truediv__(PyImath::FixedMatrix<double>,double)
        """
        pass

    __instance_size__ = 48


