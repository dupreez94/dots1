# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class IntArray(__Boost_Python.instance):
    """ Fixed length array of ints """
    def ifelse(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (IntArray)arg1, (IntArray)arg2, (int)arg3) -> IntArray :
        
            C++ signature :
                PyImath::FixedArray<int> ifelse(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>,int)
        
        ifelse( (IntArray)arg1, (IntArray)arg2, (IntArray)arg3) -> IntArray :
        
            C++ signature :
                PyImath::FixedArray<int> ifelse(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<int>)
        """
        pass

    def reduce(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (IntArray)arg1) -> int :
        
            C++ signature :
                int reduce(PyImath::FixedArray<int>)
        """
        pass

    def __add__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (IntArray)arg1, (int)x) -> IntArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<int> __add__(PyImath::FixedArray<int> {lvalue},int)
        
        __add__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<int> __add__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __div__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (IntArray)arg1, (int)x) -> IntArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<int> __div__(PyImath::FixedArray<int> {lvalue},int)
        
        __div__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<int> __div__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __eq__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (IntArray)arg1, (int)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<int> {lvalue},int)
        
        __eq__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __getitem__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (IntArray)arg1, (object)arg2) -> IntArray :
        
            C++ signature :
                PyImath::FixedArray<int> __getitem__(PyImath::FixedArray<int> {lvalue},_object*)
        
        __getitem__( (IntArray)arg1, (IntArray)arg2) -> IntArray :
        
            C++ signature :
                PyImath::FixedArray<int> __getitem__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (IntArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                int __getitem__(PyImath::FixedArray<int> {lvalue},long)
        
        __getitem__( (IntArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                int __getitem__(PyImath::FixedArray<int> {lvalue},long)
        """
        pass

    def __ge__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (IntArray)arg1, (int)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<int> {lvalue},int)
        
        __ge__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __gt__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (IntArray)arg1, (int)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<int> {lvalue},int)
        
        __gt__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __iadd__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (IntArray)arg1, (int)x) -> IntArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __iadd__(PyImath::FixedArray<int> {lvalue},int)
        
        __iadd__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __iadd__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __idiv__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (IntArray)arg1, (int)x) -> IntArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __idiv__(PyImath::FixedArray<int> {lvalue},int)
        
        __idiv__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __idiv__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __imod__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imod__( (IntArray)arg1, (int)x) -> IntArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __imod__(PyImath::FixedArray<int> {lvalue},int)
        
        __imod__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __imod__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __imul__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (IntArray)arg1, (int)x) -> IntArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __imul__(PyImath::FixedArray<int> {lvalue},int)
        
        __imul__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __imul__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (IntArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<int>)
        
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,int,unsigned long)
        
        __init__( (object)arg1, (FloatArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<float>)
        
        __init__( (object)arg1, (DoubleArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<double>)
        """
        pass

    def __isub__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (IntArray)arg1, (int)x) -> IntArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __isub__(PyImath::FixedArray<int> {lvalue},int)
        
        __isub__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __isub__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __itruediv__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (IntArray)arg1, (int)x) -> IntArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __itruediv__(PyImath::FixedArray<int> {lvalue},int)
        
        __itruediv__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<int> {lvalue} __itruediv__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __len__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (IntArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<int> {lvalue})
        """
        pass

    def __le__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (IntArray)arg1, (int)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<int> {lvalue},int)
        
        __le__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __lt__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (IntArray)arg1, (int)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<int> {lvalue},int)
        
        __lt__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __mod__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mod__( (IntArray)arg1, (int)x) -> IntArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<int> __mod__(PyImath::FixedArray<int> {lvalue},int)
        
        __mod__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<int> __mod__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __mul__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (IntArray)arg1, (int)x) -> IntArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<int> __mul__(PyImath::FixedArray<int> {lvalue},int)
        
        __mul__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<int> __mul__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __neg__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (IntArray)arg1) -> IntArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<int> __neg__(PyImath::FixedArray<int> {lvalue})
        """
        pass

    def __ne__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (IntArray)arg1, (int)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<int> {lvalue},int)
        
        __ne__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __radd__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (IntArray)arg1, (int)x) -> IntArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<int> __radd__(PyImath::FixedArray<int> {lvalue},int)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (IntArray)arg1, (int)x) -> IntArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<int> __rmul__(PyImath::FixedArray<int> {lvalue},int)
        """
        pass

    def __rsub__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (IntArray)arg1, (int)x) -> IntArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<int> __rsub__(PyImath::FixedArray<int> {lvalue},int)
        """
        pass

    def __setitem__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (IntArray)arg1, (object)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<int> {lvalue},_object*,int)
        
        __setitem__( (IntArray)arg1, (IntArray)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>,int)
        
        __setitem__( (IntArray)arg1, (object)arg2, (IntArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<int> {lvalue},_object*,PyImath::FixedArray<int>)
        
        __setitem__( (IntArray)arg1, (IntArray)arg2, (IntArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<int>)
        """
        pass

    def __sub__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (IntArray)arg1, (int)x) -> IntArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<int> __sub__(PyImath::FixedArray<int> {lvalue},int)
        
        __sub__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<int> __sub__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    def __truediv__(self, IntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (IntArray)arg1, (int)x) -> IntArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<int> __truediv__(PyImath::FixedArray<int> {lvalue},int)
        
        __truediv__( (IntArray)arg1, (IntArray)x) -> IntArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<int> __truediv__(PyImath::FixedArray<int> {lvalue},PyImath::FixedArray<int>)
        """
        pass

    __instance_size__ = 72


