# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class UnsignedIntArray(__Boost_Python.instance):
    """ Fixed length array of unsigned ints """
    def ifelse(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        ifelse( (UnsignedIntArray)arg1, (IntArray)arg2, (int)arg3) -> UnsignedIntArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned int> ifelse(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<int>,unsigned int)
        
        ifelse( (UnsignedIntArray)arg1, (IntArray)arg2, (UnsignedIntArray)arg3) -> UnsignedIntArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned int> ifelse(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<unsigned int>)
        """
        pass

    def reduce(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        reduce( (UnsignedIntArray)arg1) -> int :
        
            C++ signature :
                unsigned int reduce(PyImath::FixedArray<unsigned int>)
        """
        pass

    def __add__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __add__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __add__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __add__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __add__(x) - self+x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __add__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __div__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __div__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __div__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __div__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __div__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __div__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __eq__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (UnsignedIntArray)arg1, (int)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __eq__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> IntArray :
            __eq__(x) - self==x
        
            C++ signature :
                PyImath::FixedArray<int> __eq__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __getitem__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (UnsignedIntArray)arg1, (object)arg2) -> UnsignedIntArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __getitem__(PyImath::FixedArray<unsigned int> {lvalue},_object*)
        
        __getitem__( (UnsignedIntArray)arg1, (IntArray)arg2) -> UnsignedIntArray :
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __getitem__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<int>)
        
        __getitem__( (UnsignedIntArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                unsigned int __getitem__(PyImath::FixedArray<unsigned int> {lvalue},long)
        
        __getitem__( (UnsignedIntArray)arg1, (int)arg2) -> int :
        
            C++ signature :
                unsigned int __getitem__(PyImath::FixedArray<unsigned int> {lvalue},long)
        """
        pass

    def __ge__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ge__( (UnsignedIntArray)arg1, (int)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __ge__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> IntArray :
            __ge__(x) - self>=x
        
            C++ signature :
                PyImath::FixedArray<int> __ge__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __gt__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __gt__( (UnsignedIntArray)arg1, (int)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __gt__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> IntArray :
            __gt__(x) - self>x
        
            C++ signature :
                PyImath::FixedArray<int> __gt__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __iadd__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __iadd__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __iadd__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __iadd__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __iadd__(x) - self+=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __iadd__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __idiv__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __idiv__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __idiv__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __idiv__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __idiv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __idiv__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __imod__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imod__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __imod__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __imod__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __imod__(x) - self%=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __imod__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __imul__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __imul__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __imul__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __imul__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __imul__(x) - self*=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __imul__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1, (int)arg2) -> None :
            construct an array of the specified length initialized to the default value for the type
        
            C++ signature :
                void __init__(_object*,unsigned long)
        
        __init__( (object)arg1, (UnsignedIntArray)arg2) -> None :
            construct an array with the same values as the given array
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<unsigned int>)
        
        __init__( (object)arg1, (int)arg2, (int)arg3) -> None :
            construct an array of the specified length initialized to the specified default value
        
            C++ signature :
                void __init__(_object*,unsigned int,unsigned long)
        
        __init__( (object)arg1, (FloatArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<float>)
        
        __init__( (object)arg1, (DoubleArray)arg2) -> None :
            copy contents of other array into this one
        
            C++ signature :
                void __init__(_object*,PyImath::FixedArray<double>)
        """
        pass

    def __isub__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __isub__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __isub__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __isub__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __isub__(x) - self-=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __isub__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __itruediv__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __itruediv__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __itruediv__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __itruediv__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __itruediv__(x) - self/=x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> {lvalue} __itruediv__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __len__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (UnsignedIntArray)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::FixedArray<unsigned int> {lvalue})
        """
        pass

    def __le__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __le__( (UnsignedIntArray)arg1, (int)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __le__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> IntArray :
            __le__(x) - self<=x
        
            C++ signature :
                PyImath::FixedArray<int> __le__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __lt__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __lt__( (UnsignedIntArray)arg1, (int)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __lt__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> IntArray :
            __lt__(x) - self<x
        
            C++ signature :
                PyImath::FixedArray<int> __lt__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __mod__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mod__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __mod__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __mod__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __mod__(x) - self%x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __mod__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __mul__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __mul__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __mul__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __mul__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __mul__(x) - self*x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __mul__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __neg__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __neg__( (UnsignedIntArray)arg1) -> UnsignedIntArray :
            -x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __neg__(PyImath::FixedArray<unsigned int> {lvalue})
        """
        pass

    def __ne__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (UnsignedIntArray)arg1, (int)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __ne__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> IntArray :
            __ne__(x) - self!=x
        
            C++ signature :
                PyImath::FixedArray<int> __ne__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __radd__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __radd__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __radd__(x) - x+self
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __radd__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __rmul__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rmul__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __rmul__(x) - x*self
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __rmul__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        """
        pass

    def __rsub__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __rsub__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __rsub__(x) - x-self
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __rsub__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        """
        pass

    def __setitem__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (UnsignedIntArray)arg1, (object)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned int> {lvalue},_object*,unsigned int)
        
        __setitem__( (UnsignedIntArray)arg1, (IntArray)arg2, (int)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<int>,unsigned int)
        
        __setitem__( (UnsignedIntArray)arg1, (object)arg2, (UnsignedIntArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned int> {lvalue},_object*,PyImath::FixedArray<unsigned int>)
        
        __setitem__( (UnsignedIntArray)arg1, (IntArray)arg2, (UnsignedIntArray)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<int>,PyImath::FixedArray<unsigned int>)
        """
        pass

    def __sub__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __sub__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __sub__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __sub__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __sub__(x) - self-x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __sub__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    def __truediv__(self, UnsignedIntArray, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __truediv__( (UnsignedIntArray)arg1, (int)x) -> UnsignedIntArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __truediv__(PyImath::FixedArray<unsigned int> {lvalue},unsigned int)
        
        __truediv__( (UnsignedIntArray)arg1, (UnsignedIntArray)x) -> UnsignedIntArray :
            __truediv__(x) - self/x
        
            C++ signature :
                PyImath::FixedArray<unsigned int> __truediv__(PyImath::FixedArray<unsigned int> {lvalue},PyImath::FixedArray<unsigned int>)
        """
        pass

    __instance_size__ = 72


