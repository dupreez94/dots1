# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class M44dRow(__Boost_Python.instance):
    # no doc
    def __getitem__(self, M44dRow, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __getitem__( (M44dRow)arg1, (int)arg2) -> float :
        
            C++ signature :
                double {lvalue} __getitem__(PyImath::MatrixRow<double, 4> {lvalue},long)
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        """
        Raises an exception
        This class cannot be instantiated from Python
        """
        pass

    def __len__(self, M44dRow, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __len__( (M44dRow)arg1) -> int :
        
            C++ signature :
                long __len__(PyImath::MatrixRow<double, 4>)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __setitem__(self, M44dRow, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __setitem__( (M44dRow)arg1, (int)arg2, (float)arg3) -> None :
        
            C++ signature :
                void __setitem__(PyImath::MatrixRow<double, 4> {lvalue},long,double)
        """
        pass


