# encoding: utf-8
# module imath
# from /usr/lib/python3.8/site-packages/imath.so
# by generator 1.147
""" Imath module """

# imports
import iex as iex # /usr/lib/python3.8/site-packages/iex.so
import Boost.Python as __Boost_Python


class Box2i(__Boost_Python.instance):
    # no doc
    def center(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        center( (Box2i)arg1) -> V2i :
            center() center of the box
        
            C++ signature :
                Imath_2_4::Vec2<int> center(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def extendBy(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        extendBy( (Box2i)arg1, (V2i)arg2) -> None :
            extendBy(point) extend the box by a point
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        extendBy( (Box2i)arg1, (V2iArray)arg2) -> None :
            extendBy(array) extend the box the values in the array
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue},PyImath::FixedArray<Imath_2_4::Vec2<int> >)
        
        extendBy( (Box2i)arg1, (Box2i)arg2) -> None :
            extendBy(box) extend the box by a box
        
            C++ signature :
                void extendBy(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<int> >)
        """
        pass

    def hasVolume(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        hasVolume( (Box2i)arg1) -> bool :
            hasVolume() returns true if the box has volume
        
            C++ signature :
                bool hasVolume(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def intersects(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        intersects( (Box2i)arg1, (V2i)arg2) -> bool :
            intersects(point) returns true if the box intersects the given point
        
            C++ signature :
                bool intersects(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        
        intersects( (Box2i)arg1, (Box2i)arg2) -> bool :
            intersects(box) returns true if the box intersects the given box
        
            C++ signature :
                bool intersects(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<int> >)
        """
        pass

    def isEmpty(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        isEmpty( (Box2i)arg1) -> bool :
            isEmpty() returns true if the box is empty
        
            C++ signature :
                bool isEmpty(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def isInfinite(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        isInfinite( (Box2i)arg1) -> bool :
            isInfinite() returns true if the box covers all space
        
            C++ signature :
                bool isInfinite(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def majorAxis(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        majorAxis( (Box2i)arg1) -> int :
            majorAxis() major axis of the box
        
            C++ signature :
                unsigned int majorAxis(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def makeEmpty(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        makeEmpty( (Box2i)arg1) -> None :
            makeEmpty() make the box empty
        
            C++ signature :
                void makeEmpty(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def makeInfinite(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        makeInfinite( (Box2i)arg1) -> None :
            makeInfinite() make the box cover all space
        
            C++ signature :
                void makeInfinite(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def max(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        max( (Box2i)arg1) -> V2i :
        
            C++ signature :
                Imath_2_4::Vec2<int> max(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def min(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        min( (Box2i)arg1) -> V2i :
        
            C++ signature :
                Imath_2_4::Vec2<int> min(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def setMax(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setMax( (Box2i)arg1, (V2i)arg2) -> None :
            setMax() sets the max value of the box
        
            C++ signature :
                void setMax(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        """
        pass

    def setMin(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        setMin( (Box2i)arg1, (V2i)arg2) -> None :
            setMin() sets the min value of the box
        
            C++ signature :
                void setMin(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Vec2<int>)
        """
        pass

    def size(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        size( (Box2i)arg1) -> V2i :
            size() size of the box
        
            C++ signature :
                Imath_2_4::Vec2<int> size(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue})
        """
        pass

    def __eq__(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __eq__( (Box2i)arg1, (Box2i)arg2) -> object :
        
            C++ signature :
                _object* __eq__(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<int> >)
        """
        pass

    def __init__(self, p_object, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __init__( (object)arg1) -> None :
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1) -> None :
            Box() create empty box
        
            C++ signature :
                void __init__(_object*)
        
        __init__( (object)arg1, (V2i)arg2) -> None :
            Box(point)create box containing the given point
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<int>)
        
        __init__( (object)arg1, (V2i)arg2, (V2i)arg3) -> None :
            Box(point,point) create box continaing min and max
        
            C++ signature :
                void __init__(_object*,Imath_2_4::Vec2<int>,Imath_2_4::Vec2<int>)
        
        __init__( (object)arg1, (tuple)arg2) -> object :
            Box(point) where point is a python tuple
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple)
        
        __init__( (object)arg1, (tuple)arg2, (tuple)arg3) -> object :
            Box(point,point) where point is a python tuple
        
            C++ signature :
                void* __init__(boost::python::api::object,boost::python::tuple,boost::python::tuple)
        
        __init__( (object)arg1, (Box2f)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec2<float> >)
        
        __init__( (object)arg1, (Box2d)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec2<double> >)
        
        __init__( (object)arg1, (Box2i)arg2) -> object :
        
            C++ signature :
                void* __init__(boost::python::api::object,Imath_2_4::Box<Imath_2_4::Vec2<int> >)
        """
        pass

    def __ne__(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __ne__( (Box2i)arg1, (Box2i)arg2) -> object :
        
            C++ signature :
                _object* __ne__(Imath_2_4::Box<Imath_2_4::Vec2<int> > {lvalue},Imath_2_4::Box<Imath_2_4::Vec2<int> >)
        """
        pass

    def __reduce__(self, *args, **kwargs): # real signature unknown
        pass

    def __repr__(self, Box2i, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """
        __repr__( (Box2i)arg1) -> str :
        
            C++ signature :
                std::__cxx11::basic_string<char, std::char_traits<char>, std::allocator<char> > __repr__(Imath_2_4::Box<Imath_2_4::Vec2<int> >)
        """
        pass

    __instance_size__ = 32


