# encoding: utf-8
# module LibAppArmor._LibAppArmor
# from /usr/lib/python3.8/site-packages/LibAppArmor/_LibAppArmor.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc
# no imports

# Variables with simple values

AA_RECORD_ALLOWED = 3
AA_RECORD_AUDIT = 2
AA_RECORD_DENIED = 4
AA_RECORD_ERROR = 1
AA_RECORD_EXEC = 8

AA_RECORD_EXEC_MMAP = 1

AA_RECORD_HINT = 5
AA_RECORD_INVALID = 0
AA_RECORD_LINK = 16
AA_RECORD_READ = 2
AA_RECORD_STATUS = 6

AA_RECORD_SYNTAX_UNKNOWN = 2
AA_RECORD_SYNTAX_V1 = 0
AA_RECORD_SYNTAX_V2 = 1

AA_RECORD_WRITE = 4

# functions

def aa_change_hat(*args, **kwargs): # real signature unknown
    pass

def aa_change_hatv(*args, **kwargs): # real signature unknown
    pass

def aa_change_hat_vargs(*args, **kwargs): # real signature unknown
    pass

def aa_change_onexec(*args, **kwargs): # real signature unknown
    pass

def aa_change_profile(*args, **kwargs): # real signature unknown
    pass

def aa_find_mountpoint(*args, **kwargs): # real signature unknown
    pass

def aa_getcon(*args, **kwargs): # real signature unknown
    pass

def aa_getpeercon(*args, **kwargs): # real signature unknown
    pass

def aa_getpeercon_raw(*args, **kwargs): # real signature unknown
    pass

def aa_getprocattr(*args, **kwargs): # real signature unknown
    pass

def aa_getprocattr_raw(*args, **kwargs): # real signature unknown
    pass

def aa_gettaskcon(*args, **kwargs): # real signature unknown
    pass

def aa_is_enabled(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_active_hat_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_active_hat_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_attribute_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_attribute_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_audit_id_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_audit_id_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_audit_sub_id_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_audit_sub_id_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_bitmask_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_bitmask_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_comm_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_comm_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_dbus_bus_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_dbus_bus_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_dbus_interface_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_dbus_interface_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_dbus_member_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_dbus_member_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_dbus_path_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_dbus_path_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_denied_mask_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_denied_mask_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_epoch_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_epoch_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_error_code_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_error_code_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_event_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_event_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_flags_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_flags_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_fsuid_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_fsuid_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_fs_type_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_fs_type_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_info_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_info_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_magic_token_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_magic_token_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_name2_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_name2_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_namespace_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_namespace_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_name_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_name_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_family_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_family_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_foreign_addr_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_foreign_addr_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_foreign_port_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_foreign_port_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_local_addr_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_local_addr_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_local_port_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_local_port_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_protocol_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_protocol_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_sock_type_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_net_sock_type_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_operation_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_operation_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_ouid_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_ouid_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_parent_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_parent_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_peer_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_peer_info_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_peer_info_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_peer_pid_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_peer_pid_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_peer_profile_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_peer_profile_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_peer_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_pid_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_pid_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_profile_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_profile_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_requested_mask_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_requested_mask_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_signal_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_signal_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_src_name_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_src_name_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_swiginit(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_swigregister(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_task_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_task_set(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_version_get(*args, **kwargs): # real signature unknown
    pass

def aa_log_record_version_set(*args, **kwargs): # real signature unknown
    pass

def aa_query_file_path(*args, **kwargs): # real signature unknown
    pass

def aa_query_file_path_len(*args, **kwargs): # real signature unknown
    pass

def aa_query_label(*args, **kwargs): # real signature unknown
    pass

def aa_query_link_path(*args, **kwargs): # real signature unknown
    pass

def aa_query_link_path_len(*args, **kwargs): # real signature unknown
    pass

def aa_splitcon(*args, **kwargs): # real signature unknown
    pass

def aa_stack_onexec(*args, **kwargs): # real signature unknown
    pass

def aa_stack_profile(*args, **kwargs): # real signature unknown
    pass

def delete_aa_log_record(*args, **kwargs): # real signature unknown
    pass

def free_record(*args, **kwargs): # real signature unknown
    pass

def new_aa_log_record(*args, **kwargs): # real signature unknown
    pass

def parse_record(*args, **kwargs): # real signature unknown
    pass

def SWIG_PyInstanceMethod_New(*args, **kwargs): # real signature unknown
    pass

def _aa_is_blacklisted(*args, **kwargs): # real signature unknown
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b18e1580>'

__spec__ = None # (!) real value is "ModuleSpec(name='LibAppArmor._LibAppArmor', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b18e1580>, origin='/usr/lib/python3.8/site-packages/LibAppArmor/_LibAppArmor.cpython-38-x86_64-linux-gnu.so')"

