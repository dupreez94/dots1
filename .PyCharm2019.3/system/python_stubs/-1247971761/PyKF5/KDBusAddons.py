# encoding: utf-8
# module PyKF5.KDBusAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KDBusAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


# no functions
# classes

class KDBusConnectionPool(__sip.simplewrapper):
    # no doc
    def threadConnection(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KDBusInterProcessLock(__PyQt5_QtCore.QObject):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def lock(self, *args, **kwargs): # real signature unknown
        pass

    def lockGranted(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def resource(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def unlock(self, *args, **kwargs): # real signature unknown
        pass

    def waitForLockGranted(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KDBusService(__PyQt5_QtCore.QObject):
    # no doc
    def activateActionRequested(self, *args, **kwargs): # real signature unknown
        pass

    def activateRequested(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def errorMessage(self, *args, **kwargs): # real signature unknown
        pass

    def isRegistered(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def openRequested(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def serviceName(self, *args, **kwargs): # real signature unknown
        pass

    def setExitValue(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def unregister(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    Multiple = 2
    NoExitOnFailure = 4
    Replace = 8
    StartupOption = None # (!) real value is "<class 'PyKF5.KDBusAddons.KDBusService.StartupOption'>"
    StartupOptions = None # (!) real value is "<class 'PyKF5.KDBusAddons.KDBusService.StartupOptions'>"
    Unique = 1


class KDEDModule(__PyQt5_QtCore.QObject):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def moduleDeleted(self, *args, **kwargs): # real signature unknown
        pass

    def moduleForMessage(self, *args, **kwargs): # real signature unknown
        pass

    def moduleName(self, *args, **kwargs): # real signature unknown
        pass

    def moduleRegistered(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setModuleName(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KDEInitInterface(__sip.simplewrapper):
    # no doc
    def ensureKdeinitRunning(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>'

__spec__ = None # (!) real value is "ModuleSpec(name='PyKF5.KDBusAddons', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>, origin='/usr/lib/python3.8/site-packages/PyKF5/KDBusAddons.so')"

