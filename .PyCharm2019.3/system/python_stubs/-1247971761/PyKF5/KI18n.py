# encoding: utf-8
# module PyKF5.KI18n
# from /usr/lib/python3.8/site-packages/PyKF5/KI18n.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


# functions

def i18n(*args, **kwargs): # real signature unknown
    pass

def i18nc(*args, **kwargs): # real signature unknown
    pass

def i18ncp(*args, **kwargs): # real signature unknown
    pass

def i18nd(*args, **kwargs): # real signature unknown
    pass

def i18ndc(*args, **kwargs): # real signature unknown
    pass

def i18np(*args, **kwargs): # real signature unknown
    pass

def ki18n(*args, **kwargs): # real signature unknown
    pass

def ki18nc(*args, **kwargs): # real signature unknown
    pass

def ki18ncp(*args, **kwargs): # real signature unknown
    pass

def ki18nd(*args, **kwargs): # real signature unknown
    pass

def ki18ndc(*args, **kwargs): # real signature unknown
    pass

def ki18ndcp(*args, **kwargs): # real signature unknown
    pass

def ki18ndp(*args, **kwargs): # real signature unknown
    pass

def ki18np(*args, **kwargs): # real signature unknown
    pass

def kxi18n(*args, **kwargs): # real signature unknown
    pass

def kxi18nc(*args, **kwargs): # real signature unknown
    pass

def kxi18ncp(*args, **kwargs): # real signature unknown
    pass

def kxi18nd(*args, **kwargs): # real signature unknown
    pass

def kxi18ndc(*args, **kwargs): # real signature unknown
    pass

def kxi18ndcp(*args, **kwargs): # real signature unknown
    pass

def kxi18ndp(*args, **kwargs): # real signature unknown
    pass

def kxi18np(*args, **kwargs): # real signature unknown
    pass

def tr2i18n(*args, **kwargs): # real signature unknown
    pass

def tr2i18nd(*args, **kwargs): # real signature unknown
    pass

def tr2xi18n(*args, **kwargs): # real signature unknown
    pass

def tr2xi18nd(*args, **kwargs): # real signature unknown
    pass

def xi18n(*args, **kwargs): # real signature unknown
    pass

def xi18nc(*args, **kwargs): # real signature unknown
    pass

def xi18nd(*args, **kwargs): # real signature unknown
    pass

def xi18ndc(*args, **kwargs): # real signature unknown
    pass

# classes

class KLocalizedContext(__PyQt5_QtCore.QObject):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def i18n(self, *args, **kwargs): # real signature unknown
        pass

    def i18nc(self, *args, **kwargs): # real signature unknown
        pass

    def i18ncp(self, *args, **kwargs): # real signature unknown
        pass

    def i18nd(self, *args, **kwargs): # real signature unknown
        pass

    def i18ndc(self, *args, **kwargs): # real signature unknown
        pass

    def i18ndcp(self, *args, **kwargs): # real signature unknown
        pass

    def i18ndp(self, *args, **kwargs): # real signature unknown
        pass

    def i18np(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setTranslationDomain(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def translationDomain(self, *args, **kwargs): # real signature unknown
        pass

    def translationDomainChanged(self, *args, **kwargs): # real signature unknown
        pass

    def xi18n(self, *args, **kwargs): # real signature unknown
        pass

    def xi18nc(self, *args, **kwargs): # real signature unknown
        pass

    def xi18ncp(self, *args, **kwargs): # real signature unknown
        pass

    def xi18nd(self, *args, **kwargs): # real signature unknown
        pass

    def xi18ndc(self, *args, **kwargs): # real signature unknown
        pass

    def xi18ndcp(self, *args, **kwargs): # real signature unknown
        pass

    def xi18ndp(self, *args, **kwargs): # real signature unknown
        pass

    def xi18np(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KLocalizedString(__sip.wrapper):
    # no doc
    def addDomainLocaleDir(self, *args, **kwargs): # real signature unknown
        pass

    def applicationDomain(self, *args, **kwargs): # real signature unknown
        pass

    def availableApplicationTranslations(self, *args, **kwargs): # real signature unknown
        pass

    def availableDomainTranslations(self, *args, **kwargs): # real signature unknown
        pass

    def clearLanguages(self, *args, **kwargs): # real signature unknown
        pass

    def ignoreMarkup(self, *args, **kwargs): # real signature unknown
        pass

    def inContext(self, *args, **kwargs): # real signature unknown
        pass

    def insertQtDomain(self, *args, **kwargs): # real signature unknown
        pass

    def isApplicationTranslatedInto(self, *args, **kwargs): # real signature unknown
        pass

    def isEmpty(self, *args, **kwargs): # real signature unknown
        pass

    def languages(self, *args, **kwargs): # real signature unknown
        pass

    def localizedFilePath(self, *args, **kwargs): # real signature unknown
        pass

    def relaxSubs(self, *args, **kwargs): # real signature unknown
        pass

    def removeAcceleratorMarker(self, *args, **kwargs): # real signature unknown
        pass

    def removeQtDomain(self, *args, **kwargs): # real signature unknown
        pass

    def setApplicationDomain(self, *args, **kwargs): # real signature unknown
        pass

    def setLanguages(self, *args, **kwargs): # real signature unknown
        pass

    def toString(self, *args, **kwargs): # real signature unknown
        pass

    def translateQt(self, *args, **kwargs): # real signature unknown
        pass

    def untranslatedText(self, *args, **kwargs): # real signature unknown
        pass

    def withDomain(self, *args, **kwargs): # real signature unknown
        pass

    def withFormat(self, *args, **kwargs): # real signature unknown
        pass

    def withLanguages(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KLocalizedTranslator(__PyQt5_QtCore.QTranslator):
    # no doc
    def addContextToMonitor(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def removeContextToMonitor(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setTranslationDomain(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def translate(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class Kuit(__sip.simplewrapper):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    PhraseTag = 0
    PlainText = 10
    RichText = 20
    StructTag = 1
    TagClass = None # (!) real value is "<class 'PyKF5.KI18n.Kuit.TagClass'>"
    TermText = 30
    UndefinedFormat = 0
    VisualFormat = None # (!) real value is "<class 'PyKF5.KI18n.Kuit.VisualFormat'>"


class KuitSetup(__sip.wrapper):
    # no doc
    def setFormatForMarker(self, *args, **kwargs): # real signature unknown
        pass

    def setTagClass(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>'

__spec__ = None # (!) real value is "ModuleSpec(name='PyKF5.KI18n', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>, origin='/usr/lib/python3.8/site-packages/PyKF5/KI18n.so')"

