# encoding: utf-8
# module PyKF5.KJobWidgets
# from /usr/lib/python3.8/site-packages/PyKF5/KJobWidgets.so
# by generator 1.147
# no doc

# imports
import PyKF5.KCoreAddons as __PyKF5_KCoreAddons
import sip as __sip


# no functions
# classes

class KAbstractWidgetJobTracker(__PyKF5_KCoreAddons.KJobTrackerInterface):
    # no doc
    def autoDelete(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def finished(self, *args, **kwargs): # real signature unknown
        pass

    def infoMessage(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def percent(self, *args, **kwargs): # real signature unknown
        pass

    def processedAmount(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def registerJob(self, *args, **kwargs): # real signature unknown
        pass

    def resume(self, *args, **kwargs): # real signature unknown
        pass

    def resumed(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setAutoDelete(self, *args, **kwargs): # real signature unknown
        pass

    def setStopOnClose(self, *args, **kwargs): # real signature unknown
        pass

    def slotClean(self, *args, **kwargs): # real signature unknown
        pass

    def slotResume(self, *args, **kwargs): # real signature unknown
        pass

    def slotStop(self, *args, **kwargs): # real signature unknown
        pass

    def slotSuspend(self, *args, **kwargs): # real signature unknown
        pass

    def speed(self, *args, **kwargs): # real signature unknown
        pass

    def stopOnClose(self, *args, **kwargs): # real signature unknown
        pass

    def stopped(self, *args, **kwargs): # real signature unknown
        pass

    def suspend(self, *args, **kwargs): # real signature unknown
        pass

    def suspended(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def totalAmount(self, *args, **kwargs): # real signature unknown
        pass

    def unregisterJob(self, *args, **kwargs): # real signature unknown
        pass

    def warning(self, *args, **kwargs): # real signature unknown
        pass

    def widget(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KDialogJobUiDelegate(__PyKF5_KCoreAddons.KJobUiDelegate):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def job(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setJob(self, *args, **kwargs): # real signature unknown
        pass

    def setWindow(self, *args, **kwargs): # real signature unknown
        pass

    def showErrorMessage(self, *args, **kwargs): # real signature unknown
        pass

    def slotWarning(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def updateUserTimestamp(self, *args, **kwargs): # real signature unknown
        pass

    def userTimestamp(self, *args, **kwargs): # real signature unknown
        pass

    def window(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KJobWidgets(__sip.simplewrapper):
    # no doc
    def setWindow(self, *args, **kwargs): # real signature unknown
        pass

    def updateUserTimestamp(self, *args, **kwargs): # real signature unknown
        pass

    def userTimestamp(self, *args, **kwargs): # real signature unknown
        pass

    def window(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KJobWindows(__sip.simplewrapper):
    # no doc
    def setWindow(self, *args, **kwargs): # real signature unknown
        pass

    def window(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class KStatusBarJobTracker(KAbstractWidgetJobTracker):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def finished(self, *args, **kwargs): # real signature unknown
        pass

    def infoMessage(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def percent(self, *args, **kwargs): # real signature unknown
        pass

    def processedAmount(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def registerJob(self, *args, **kwargs): # real signature unknown
        pass

    def resumed(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setStatusBarMode(self, *args, **kwargs): # real signature unknown
        pass

    def slotClean(self, *args, **kwargs): # real signature unknown
        pass

    def slotResume(self, *args, **kwargs): # real signature unknown
        pass

    def slotStop(self, *args, **kwargs): # real signature unknown
        pass

    def slotSuspend(self, *args, **kwargs): # real signature unknown
        pass

    def speed(self, *args, **kwargs): # real signature unknown
        pass

    def suspended(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def totalAmount(self, *args, **kwargs): # real signature unknown
        pass

    def unregisterJob(self, *args, **kwargs): # real signature unknown
        pass

    def warning(self, *args, **kwargs): # real signature unknown
        pass

    def widget(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    LabelOnly = 1
    NoInformation = 0
    ProgressOnly = 2
    StatusBarMode = None # (!) real value is "<class 'PyKF5.KJobWidgets.KStatusBarJobTracker.StatusBarMode'>"
    StatusBarModes = None # (!) real value is "<class 'PyKF5.KJobWidgets.KStatusBarJobTracker.StatusBarModes'>"


class KUiServerJobTracker(__PyKF5_KCoreAddons.KJobTrackerInterface):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def finished(self, *args, **kwargs): # real signature unknown
        pass

    def infoMessage(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def percent(self, *args, **kwargs): # real signature unknown
        pass

    def processedAmount(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def registerJob(self, *args, **kwargs): # real signature unknown
        pass

    def resumed(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def speed(self, *args, **kwargs): # real signature unknown
        pass

    def suspended(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def totalAmount(self, *args, **kwargs): # real signature unknown
        pass

    def unregisterJob(self, *args, **kwargs): # real signature unknown
        pass

    def warning(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


class KWidgetJobTracker(KAbstractWidgetJobTracker):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def finished(self, *args, **kwargs): # real signature unknown
        pass

    def infoMessage(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def keepOpen(self, *args, **kwargs): # real signature unknown
        pass

    def percent(self, *args, **kwargs): # real signature unknown
        pass

    def processedAmount(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def registerJob(self, *args, **kwargs): # real signature unknown
        pass

    def resumed(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def slotClean(self, *args, **kwargs): # real signature unknown
        pass

    def slotResume(self, *args, **kwargs): # real signature unknown
        pass

    def slotStop(self, *args, **kwargs): # real signature unknown
        pass

    def slotSuspend(self, *args, **kwargs): # real signature unknown
        pass

    def speed(self, *args, **kwargs): # real signature unknown
        pass

    def suspended(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def totalAmount(self, *args, **kwargs): # real signature unknown
        pass

    def unregisterJob(self, *args, **kwargs): # real signature unknown
        pass

    def warning(self, *args, **kwargs): # real signature unknown
        pass

    def widget(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>'

__spec__ = None # (!) real value is "ModuleSpec(name='PyKF5.KJobWidgets', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1ca0>, origin='/usr/lib/python3.8/site-packages/PyKF5/KJobWidgets.so')"

