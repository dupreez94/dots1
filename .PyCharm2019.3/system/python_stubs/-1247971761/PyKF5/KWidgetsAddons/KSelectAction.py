# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


class KSelectAction(__PyQt5_QtWidgets.QWidgetAction):
    # no doc
    def action(self, *args, **kwargs): # real signature unknown
        pass

    def actions(self, *args, **kwargs): # real signature unknown
        pass

    def actionTriggered(self, *args, **kwargs): # real signature unknown
        pass

    def addAction(self, *args, **kwargs): # real signature unknown
        pass

    def changeItem(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def clear(self, *args, **kwargs): # real signature unknown
        pass

    def comboWidth(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def createdWidgets(self, *args, **kwargs): # real signature unknown
        pass

    def createWidget(self, *args, **kwargs): # real signature unknown
        pass

    def currentAction(self, *args, **kwargs): # real signature unknown
        pass

    def currentItem(self, *args, **kwargs): # real signature unknown
        pass

    def currentText(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def deleteWidget(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def event(self, *args, **kwargs): # real signature unknown
        pass

    def eventFilter(self, *args, **kwargs): # real signature unknown
        pass

    def insertAction(self, *args, **kwargs): # real signature unknown
        pass

    def isEditable(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def items(self, *args, **kwargs): # real signature unknown
        pass

    def menuAccelsEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def removeAction(self, *args, **kwargs): # real signature unknown
        pass

    def removeAllActions(self, *args, **kwargs): # real signature unknown
        pass

    def selectableActionGroup(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setComboWidth(self, *args, **kwargs): # real signature unknown
        pass

    def setCurrentAction(self, *args, **kwargs): # real signature unknown
        pass

    def setCurrentItem(self, *args, **kwargs): # real signature unknown
        pass

    def setEditable(self, *args, **kwargs): # real signature unknown
        pass

    def setItems(self, *args, **kwargs): # real signature unknown
        pass

    def setMaxComboViewCount(self, *args, **kwargs): # real signature unknown
        pass

    def setMenuAccelsEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def setToolBarMode(self, *args, **kwargs): # real signature unknown
        pass

    def setToolButtonPopupMode(self, *args, **kwargs): # real signature unknown
        pass

    def slotToggled(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def toolBarMode(self, *args, **kwargs): # real signature unknown
        pass

    def toolButtonPopupMode(self, *args, **kwargs): # real signature unknown
        pass

    def triggered(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    ComboBoxMode = 1
    MenuMode = 0
    ToolBarMode = None # (!) real value is "<class 'PyKF5.KWidgetsAddons.KSelectAction.ToolBarMode'>"


