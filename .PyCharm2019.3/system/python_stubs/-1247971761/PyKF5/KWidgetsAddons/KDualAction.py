# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


class KDualAction(__PyQt5_QtWidgets.QAction):
    # no doc
    def activeChanged(self, *args, **kwargs): # real signature unknown
        pass

    def activeChangedByUser(self, *args, **kwargs): # real signature unknown
        pass

    def activeGuiItem(self, *args, **kwargs): # real signature unknown
        pass

    def activeIcon(self, *args, **kwargs): # real signature unknown
        pass

    def activeText(self, *args, **kwargs): # real signature unknown
        pass

    def activeToolTip(self, *args, **kwargs): # real signature unknown
        pass

    def autoToggle(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def event(self, *args, **kwargs): # real signature unknown
        pass

    def inactiveGuiItem(self, *args, **kwargs): # real signature unknown
        pass

    def inactiveIcon(self, *args, **kwargs): # real signature unknown
        pass

    def inactiveText(self, *args, **kwargs): # real signature unknown
        pass

    def inactiveToolTip(self, *args, **kwargs): # real signature unknown
        pass

    def isActive(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setActive(self, *args, **kwargs): # real signature unknown
        pass

    def setActiveGuiItem(self, *args, **kwargs): # real signature unknown
        pass

    def setActiveIcon(self, *args, **kwargs): # real signature unknown
        pass

    def setActiveText(self, *args, **kwargs): # real signature unknown
        pass

    def setActiveToolTip(self, *args, **kwargs): # real signature unknown
        pass

    def setAutoToggle(self, *args, **kwargs): # real signature unknown
        pass

    def setIconForStates(self, *args, **kwargs): # real signature unknown
        pass

    def setInactiveGuiItem(self, *args, **kwargs): # real signature unknown
        pass

    def setInactiveIcon(self, *args, **kwargs): # real signature unknown
        pass

    def setInactiveText(self, *args, **kwargs): # real signature unknown
        pass

    def setInactiveToolTip(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


