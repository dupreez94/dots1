# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


class KActionSelector(__PyQt5_QtWidgets.QWidget):
    # no doc
    def actionEvent(self, *args, **kwargs): # real signature unknown
        pass

    def added(self, *args, **kwargs): # real signature unknown
        pass

    def availableInsertionPolicy(self, *args, **kwargs): # real signature unknown
        pass

    def availableLabel(self, *args, **kwargs): # real signature unknown
        pass

    def availableListWidget(self, *args, **kwargs): # real signature unknown
        pass

    def changeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def closeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def contextMenuEvent(self, *args, **kwargs): # real signature unknown
        pass

    def create(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def destroy(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def dragEnterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragLeaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dropEvent(self, *args, **kwargs): # real signature unknown
        pass

    def enterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def event(self, *args, **kwargs): # real signature unknown
        pass

    def eventFilter(self, *args, **kwargs): # real signature unknown
        pass

    def focusInEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextPrevChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusOutEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusPreviousChild(self, *args, **kwargs): # real signature unknown
        pass

    def hideEvent(self, *args, **kwargs): # real signature unknown
        pass

    def initPainter(self, *args, **kwargs): # real signature unknown
        pass

    def inputMethodEvent(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def keyboardEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def keyPressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def keyReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def leaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def metric(self, *args, **kwargs): # real signature unknown
        pass

    def mouseDoubleClickEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mousePressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def movedDown(self, *args, **kwargs): # real signature unknown
        pass

    def movedUp(self, *args, **kwargs): # real signature unknown
        pass

    def moveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def moveOnDoubleClick(self, *args, **kwargs): # real signature unknown
        pass

    def nativeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def paintEvent(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def removed(self, *args, **kwargs): # real signature unknown
        pass

    def resizeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def selectedInsertionPolicy(self, *args, **kwargs): # real signature unknown
        pass

    def selectedLabel(self, *args, **kwargs): # real signature unknown
        pass

    def selectedListWidget(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setAvailableInsertionPolicy(self, *args, **kwargs): # real signature unknown
        pass

    def setAvailableLabel(self, *args, **kwargs): # real signature unknown
        pass

    def setButtonIcon(self, *args, **kwargs): # real signature unknown
        pass

    def setButtonIconSet(self, *args, **kwargs): # real signature unknown
        pass

    def setButtonsEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def setButtonTooltip(self, *args, **kwargs): # real signature unknown
        pass

    def setButtonWhatsThis(self, *args, **kwargs): # real signature unknown
        pass

    def setKeyboardEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def setMoveOnDoubleClick(self, *args, **kwargs): # real signature unknown
        pass

    def setSelectedInsertionPolicy(self, *args, **kwargs): # real signature unknown
        pass

    def setSelectedLabel(self, *args, **kwargs): # real signature unknown
        pass

    def setShowUpDownButtons(self, *args, **kwargs): # real signature unknown
        pass

    def sharedPainter(self, *args, **kwargs): # real signature unknown
        pass

    def showEvent(self, *args, **kwargs): # real signature unknown
        pass

    def showUpDownButtons(self, *args, **kwargs): # real signature unknown
        pass

    def tabletEvent(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def updateMicroFocus(self, *args, **kwargs): # real signature unknown
        pass

    def wheelEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    AtBottom = 3
    AtTop = 2
    BelowCurrent = 0
    ButtonAdd = 0
    ButtonDown = 3
    ButtonRemove = 1
    ButtonUp = 2
    InsertionPolicy = None # (!) real value is "<class 'PyKF5.KWidgetsAddons.KActionSelector.InsertionPolicy'>"
    MoveButton = None # (!) real value is "<class 'PyKF5.KWidgetsAddons.KActionSelector.MoveButton'>"
    Sorted = 1


