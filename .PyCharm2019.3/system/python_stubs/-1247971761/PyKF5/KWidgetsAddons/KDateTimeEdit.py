# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


class KDateTimeEdit(__PyQt5_QtWidgets.QWidget):
    # no doc
    def actionEvent(self, *args, **kwargs): # real signature unknown
        pass

    def assignDate(self, *args, **kwargs): # real signature unknown
        pass

    def assignDateTime(self, *args, **kwargs): # real signature unknown
        pass

    def assignTime(self, *args, **kwargs): # real signature unknown
        pass

    def assignTimeZone(self, *args, **kwargs): # real signature unknown
        pass

    def calendarChanged(self, *args, **kwargs): # real signature unknown
        pass

    def calendarEntered(self, *args, **kwargs): # real signature unknown
        pass

    def calendarLocalesList(self, *args, **kwargs): # real signature unknown
        pass

    def changeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def closeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def contextMenuEvent(self, *args, **kwargs): # real signature unknown
        pass

    def create(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def date(self, *args, **kwargs): # real signature unknown
        pass

    def dateChanged(self, *args, **kwargs): # real signature unknown
        pass

    def dateDisplayFormat(self, *args, **kwargs): # real signature unknown
        pass

    def dateEdited(self, *args, **kwargs): # real signature unknown
        pass

    def dateEntered(self, *args, **kwargs): # real signature unknown
        pass

    def dateMap(self, *args, **kwargs): # real signature unknown
        pass

    def dateTime(self, *args, **kwargs): # real signature unknown
        pass

    def dateTimeChanged(self, *args, **kwargs): # real signature unknown
        pass

    def dateTimeEdited(self, *args, **kwargs): # real signature unknown
        pass

    def dateTimeEntered(self, *args, **kwargs): # real signature unknown
        pass

    def destroy(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def dragEnterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragLeaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dragMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def dropEvent(self, *args, **kwargs): # real signature unknown
        pass

    def enterEvent(self, *args, **kwargs): # real signature unknown
        pass

    def event(self, *args, **kwargs): # real signature unknown
        pass

    def eventFilter(self, *args, **kwargs): # real signature unknown
        pass

    def focusInEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusNextPrevChild(self, *args, **kwargs): # real signature unknown
        pass

    def focusOutEvent(self, *args, **kwargs): # real signature unknown
        pass

    def focusPreviousChild(self, *args, **kwargs): # real signature unknown
        pass

    def hideEvent(self, *args, **kwargs): # real signature unknown
        pass

    def initPainter(self, *args, **kwargs): # real signature unknown
        pass

    def inputMethodEvent(self, *args, **kwargs): # real signature unknown
        pass

    def isNull(self, *args, **kwargs): # real signature unknown
        pass

    def isNullDate(self, *args, **kwargs): # real signature unknown
        pass

    def isNullTime(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def isValid(self, *args, **kwargs): # real signature unknown
        pass

    def isValidDate(self, *args, **kwargs): # real signature unknown
        pass

    def isValidTime(self, *args, **kwargs): # real signature unknown
        pass

    def keyPressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def keyReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def leaveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def maximumDateTime(self, *args, **kwargs): # real signature unknown
        pass

    def metric(self, *args, **kwargs): # real signature unknown
        pass

    def minimumDateTime(self, *args, **kwargs): # real signature unknown
        pass

    def mouseDoubleClickEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseMoveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mousePressEvent(self, *args, **kwargs): # real signature unknown
        pass

    def mouseReleaseEvent(self, *args, **kwargs): # real signature unknown
        pass

    def moveEvent(self, *args, **kwargs): # real signature unknown
        pass

    def nativeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def options(self, *args, **kwargs): # real signature unknown
        pass

    def paintEvent(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def resetDateTimeRange(self, *args, **kwargs): # real signature unknown
        pass

    def resetMaximumDateTime(self, *args, **kwargs): # real signature unknown
        pass

    def resetMinimumDateTime(self, *args, **kwargs): # real signature unknown
        pass

    def resizeEvent(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setCalendarLocalesList(self, *args, **kwargs): # real signature unknown
        pass

    def setDate(self, *args, **kwargs): # real signature unknown
        pass

    def setDateDisplayFormat(self, *args, **kwargs): # real signature unknown
        pass

    def setDateMap(self, *args, **kwargs): # real signature unknown
        pass

    def setDateTime(self, *args, **kwargs): # real signature unknown
        pass

    def setDateTimeRange(self, *args, **kwargs): # real signature unknown
        pass

    def setMaximumDateTime(self, *args, **kwargs): # real signature unknown
        pass

    def setMinimumDateTime(self, *args, **kwargs): # real signature unknown
        pass

    def setOptions(self, *args, **kwargs): # real signature unknown
        pass

    def setTime(self, *args, **kwargs): # real signature unknown
        pass

    def setTimeDisplayFormat(self, *args, **kwargs): # real signature unknown
        pass

    def setTimeList(self, *args, **kwargs): # real signature unknown
        pass

    def setTimeListInterval(self, *args, **kwargs): # real signature unknown
        pass

    def setTimeZone(self, *args, **kwargs): # real signature unknown
        pass

    def setTimeZones(self, *args, **kwargs): # real signature unknown
        pass

    def sharedPainter(self, *args, **kwargs): # real signature unknown
        pass

    def showEvent(self, *args, **kwargs): # real signature unknown
        pass

    def tabletEvent(self, *args, **kwargs): # real signature unknown
        pass

    def time(self, *args, **kwargs): # real signature unknown
        pass

    def timeChanged(self, *args, **kwargs): # real signature unknown
        pass

    def timeDisplayFormat(self, *args, **kwargs): # real signature unknown
        pass

    def timeEdited(self, *args, **kwargs): # real signature unknown
        pass

    def timeEntered(self, *args, **kwargs): # real signature unknown
        pass

    def timeList(self, *args, **kwargs): # real signature unknown
        pass

    def timeListInterval(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def timeZone(self, *args, **kwargs): # real signature unknown
        pass

    def timeZoneChanged(self, *args, **kwargs): # real signature unknown
        pass

    def timeZoneEntered(self, *args, **kwargs): # real signature unknown
        pass

    def timeZones(self, *args, **kwargs): # real signature unknown
        pass

    def updateMicroFocus(self, *args, **kwargs): # real signature unknown
        pass

    def wheelEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    DateKeywords = 8192
    DatePicker = 4096
    EditDate = 32
    EditTime = 64
    ForceTime = 16384
    Option = None # (!) real value is "<class 'PyKF5.KWidgetsAddons.KDateTimeEdit.Option'>"
    Options = None # (!) real value is "<class 'PyKF5.KWidgetsAddons.KDateTimeEdit.Options'>"
    SelectCalendar = 256
    SelectDate = 512
    SelectTime = 1024
    SelectTimeZone = 2048
    ShowCalendar = 1
    ShowDate = 2
    ShowTime = 4
    ShowTimeZone = 8
    WarnOnInvalid = 32768


