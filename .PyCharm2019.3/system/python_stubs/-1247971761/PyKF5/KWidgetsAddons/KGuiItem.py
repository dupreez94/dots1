# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


class KGuiItem(__sip.wrapper):
    # no doc
    def assign(self, *args, **kwargs): # real signature unknown
        pass

    def hasIcon(self, *args, **kwargs): # real signature unknown
        pass

    def icon(self, *args, **kwargs): # real signature unknown
        pass

    def iconName(self, *args, **kwargs): # real signature unknown
        pass

    def isEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def plainText(self, *args, **kwargs): # real signature unknown
        pass

    def setEnabled(self, *args, **kwargs): # real signature unknown
        pass

    def setIcon(self, *args, **kwargs): # real signature unknown
        pass

    def setIconName(self, *args, **kwargs): # real signature unknown
        pass

    def setText(self, *args, **kwargs): # real signature unknown
        pass

    def setToolTip(self, *args, **kwargs): # real signature unknown
        pass

    def setWhatsThis(self, *args, **kwargs): # real signature unknown
        pass

    def text(self, *args, **kwargs): # real signature unknown
        pass

    def toolTip(self, *args, **kwargs): # real signature unknown
        pass

    def whatsThis(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



