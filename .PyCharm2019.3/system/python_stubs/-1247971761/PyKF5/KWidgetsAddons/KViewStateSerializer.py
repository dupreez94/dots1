# encoding: utf-8
# module PyKF5.KWidgetsAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KWidgetsAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtWidgets as __PyQt5_QtWidgets
import sip as __sip


class KViewStateSerializer(__PyQt5_QtCore.QObject):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def currentIndexKey(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def expansionKeys(self, *args, **kwargs): # real signature unknown
        pass

    def indexFromConfigString(self, *args, **kwargs): # real signature unknown
        pass

    def indexToConfigString(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def restoreCurrentItem(self, *args, **kwargs): # real signature unknown
        pass

    def restoreExpanded(self, *args, **kwargs): # real signature unknown
        pass

    def restoreScrollState(self, *args, **kwargs): # real signature unknown
        pass

    def restoreSelection(self, *args, **kwargs): # real signature unknown
        pass

    def restoreState(self, *args, **kwargs): # real signature unknown
        pass

    def selectionKeys(self, *args, **kwargs): # real signature unknown
        pass

    def selectionModel(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setSelectionModel(self, *args, **kwargs): # real signature unknown
        pass

    def setView(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def view(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


