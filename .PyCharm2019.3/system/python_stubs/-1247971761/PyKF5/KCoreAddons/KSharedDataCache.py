# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KSharedDataCache(__sip.wrapper):
    # no doc
    def clear(self, *args, **kwargs): # real signature unknown
        pass

    def contains(self, *args, **kwargs): # real signature unknown
        pass

    def deleteCache(self, *args, **kwargs): # real signature unknown
        pass

    def evictionPolicy(self, *args, **kwargs): # real signature unknown
        pass

    def find(self, *args, **kwargs): # real signature unknown
        pass

    def freeSize(self, *args, **kwargs): # real signature unknown
        pass

    def insert(self, *args, **kwargs): # real signature unknown
        pass

    def setEvictionPolicy(self, *args, **kwargs): # real signature unknown
        pass

    def setTimestamp(self, *args, **kwargs): # real signature unknown
        pass

    def timestamp(self, *args, **kwargs): # real signature unknown
        pass

    def totalSize(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    EvictionPolicy = None # (!) real value is "<class 'PyKF5.KCoreAddons.KSharedDataCache.EvictionPolicy'>"
    EvictLeastOftenUsed = 2
    EvictLeastRecentlyUsed = 1
    EvictOldest = 3
    NoEvictionPreference = 0


