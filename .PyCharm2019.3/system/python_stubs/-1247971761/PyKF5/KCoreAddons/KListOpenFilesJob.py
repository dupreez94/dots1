# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .KJob import KJob

class KListOpenFilesJob(KJob):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def doKill(self, *args, **kwargs): # real signature unknown
        pass

    def doResume(self, *args, **kwargs): # real signature unknown
        pass

    def doSuspend(self, *args, **kwargs): # real signature unknown
        pass

    def emitPercent(self, *args, **kwargs): # real signature unknown
        pass

    def emitResult(self, *args, **kwargs): # real signature unknown
        pass

    def emitSpeed(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def processInfoList(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setCapabilities(self, *args, **kwargs): # real signature unknown
        pass

    def setError(self, *args, **kwargs): # real signature unknown
        pass

    def setErrorText(self, *args, **kwargs): # real signature unknown
        pass

    def setPercent(self, *args, **kwargs): # real signature unknown
        pass

    def setProcessedAmount(self, *args, **kwargs): # real signature unknown
        pass

    def setTotalAmount(self, *args, **kwargs): # real signature unknown
        pass

    def start(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    Error = None # (!) real value is "<enum 'Error'>"


