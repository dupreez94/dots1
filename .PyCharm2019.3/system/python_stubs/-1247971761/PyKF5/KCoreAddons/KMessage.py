# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KMessage(__sip.simplewrapper):
    # no doc
    def message(self, *args, **kwargs): # real signature unknown
        pass

    def setMessageHandler(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    Error = 0
    Fatal = 4
    Information = 1
    MessageType = None # (!) real value is "<class 'PyKF5.KCoreAddons.KMessage.MessageType'>"
    Sorry = 3
    Warning = 2


