# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KJob(__PyQt5_QtCore.QObject):
    # no doc
    def capabilities(self, *args, **kwargs): # real signature unknown
        pass

    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def doKill(self, *args, **kwargs): # real signature unknown
        pass

    def doResume(self, *args, **kwargs): # real signature unknown
        pass

    def doSuspend(self, *args, **kwargs): # real signature unknown
        pass

    def emitPercent(self, *args, **kwargs): # real signature unknown
        pass

    def emitResult(self, *args, **kwargs): # real signature unknown
        pass

    def emitSpeed(self, *args, **kwargs): # real signature unknown
        pass

    def error(self, *args, **kwargs): # real signature unknown
        pass

    def errorString(self, *args, **kwargs): # real signature unknown
        pass

    def errorText(self, *args, **kwargs): # real signature unknown
        pass

    def exec(self, *args, **kwargs): # real signature unknown
        pass

    def infoMessage(self, *args, **kwargs): # real signature unknown
        pass

    def isAutoDelete(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def isSuspended(self, *args, **kwargs): # real signature unknown
        pass

    def kill(self, *args, **kwargs): # real signature unknown
        pass

    def percent(self, *args, **kwargs): # real signature unknown
        pass

    def processedAmount(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def resume(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setAutoDelete(self, *args, **kwargs): # real signature unknown
        pass

    def setCapabilities(self, *args, **kwargs): # real signature unknown
        pass

    def setError(self, *args, **kwargs): # real signature unknown
        pass

    def setErrorText(self, *args, **kwargs): # real signature unknown
        pass

    def setPercent(self, *args, **kwargs): # real signature unknown
        pass

    def setProcessedAmount(self, *args, **kwargs): # real signature unknown
        pass

    def setTotalAmount(self, *args, **kwargs): # real signature unknown
        pass

    def setUiDelegate(self, *args, **kwargs): # real signature unknown
        pass

    def speed(self, *args, **kwargs): # real signature unknown
        pass

    def start(self, *args, **kwargs): # real signature unknown
        pass

    def suspend(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def totalAmount(self, *args, **kwargs): # real signature unknown
        pass

    def uiDelegate(self, *args, **kwargs): # real signature unknown
        pass

    def warning(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    Bytes = 0
    Capabilities = None # (!) real value is "<class 'PyKF5.KCoreAddons.KJob.Capabilities'>"
    Capability = None # (!) real value is "<class 'PyKF5.KCoreAddons.KJob.Capability'>"
    Directories = 2
    EmitResult = 1
    Files = 1
    Killable = 1
    KilledJobError = 1
    KillVerbosity = None # (!) real value is "<class 'PyKF5.KCoreAddons.KJob.KillVerbosity'>"
    NoCapabilities = 0
    NoError = 0
    Quietly = 0
    Suspendable = 2
    Unit = None # (!) real value is "<class 'PyKF5.KCoreAddons.KJob.Unit'>"
    UserDefinedError = 100


