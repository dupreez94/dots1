# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KShell(__sip.simplewrapper):
    # no doc
    def joinArgs(self, *args, **kwargs): # real signature unknown
        pass

    def quoteArg(self, *args, **kwargs): # real signature unknown
        pass

    def splitArgs(self, *args, **kwargs): # real signature unknown
        pass

    def tildeExpand(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    AbortOnMeta = 2
    BadQuoting = 1
    Errors = None # (!) real value is "<class 'PyKF5.KCoreAddons.KShell.Errors'>"
    FoundMeta = 2
    NoError = 0
    NoOptions = 0
    Option = None # (!) real value is "<class 'PyKF5.KCoreAddons.KShell.Option'>"
    Options = None # (!) real value is "<class 'PyKF5.KCoreAddons.KShell.Options'>"
    TildeExpand = 1


