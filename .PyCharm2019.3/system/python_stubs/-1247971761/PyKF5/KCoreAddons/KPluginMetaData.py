# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KPluginMetaData(__sip.wrapper):
    # no doc
    def authors(self, *args, **kwargs): # real signature unknown
        pass

    def category(self, *args, **kwargs): # real signature unknown
        pass

    def copyrightText(self, *args, **kwargs): # real signature unknown
        pass

    def dependencies(self, *args, **kwargs): # real signature unknown
        pass

    def description(self, *args, **kwargs): # real signature unknown
        pass

    def extraInformation(self, *args, **kwargs): # real signature unknown
        pass

    def fileName(self, *args, **kwargs): # real signature unknown
        pass

    def formFactors(self, *args, **kwargs): # real signature unknown
        pass

    def fromDesktopFile(self, *args, **kwargs): # real signature unknown
        pass

    def iconName(self, *args, **kwargs): # real signature unknown
        pass

    def instantiate(self, *args, **kwargs): # real signature unknown
        pass

    def isEnabledByDefault(self, *args, **kwargs): # real signature unknown
        pass

    def isHidden(self, *args, **kwargs): # real signature unknown
        pass

    def isValid(self, *args, **kwargs): # real signature unknown
        pass

    def license(self, *args, **kwargs): # real signature unknown
        pass

    def metaDataFileName(self, *args, **kwargs): # real signature unknown
        pass

    def mimeTypes(self, *args, **kwargs): # real signature unknown
        pass

    def name(self, *args, **kwargs): # real signature unknown
        pass

    def otherContributors(self, *args, **kwargs): # real signature unknown
        pass

    def pluginId(self, *args, **kwargs): # real signature unknown
        pass

    def rawData(self, *args, **kwargs): # real signature unknown
        pass

    def readStringList(self, *args, **kwargs): # real signature unknown
        pass

    def readTranslatedString(self, *args, **kwargs): # real signature unknown
        pass

    def readTranslatedValue(self, *args, **kwargs): # real signature unknown
        pass

    def serviceTypes(self, *args, **kwargs): # real signature unknown
        pass

    def translators(self, *args, **kwargs): # real signature unknown
        pass

    def value(self, *args, **kwargs): # real signature unknown
        pass

    def version(self, *args, **kwargs): # real signature unknown
        pass

    def website(self, *args, **kwargs): # real signature unknown
        pass

    def __eq__(self, *args, **kwargs): # real signature unknown
        """ Return self==value. """
        pass

    def __ge__(self, *args, **kwargs): # real signature unknown
        """ Return self>=value. """
        pass

    def __gt__(self, *args, **kwargs): # real signature unknown
        """ Return self>value. """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __le__(self, *args, **kwargs): # real signature unknown
        """ Return self<=value. """
        pass

    def __lt__(self, *args, **kwargs): # real signature unknown
        """ Return self<value. """
        pass

    def __ne__(self, *args, **kwargs): # real signature unknown
        """ Return self!=value. """
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    __hash__ = None


