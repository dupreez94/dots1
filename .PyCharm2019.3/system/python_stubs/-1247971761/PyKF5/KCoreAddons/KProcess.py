# encoding: utf-8
# module PyKF5.KCoreAddons
# from /usr/lib/python3.8/site-packages/PyKF5/KCoreAddons.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class KProcess(__PyQt5_QtCore.QProcess):
    # no doc
    def childEvent(self, *args, **kwargs): # real signature unknown
        pass

    def clearEnvironment(self, *args, **kwargs): # real signature unknown
        pass

    def clearProgram(self, *args, **kwargs): # real signature unknown
        pass

    def connectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def customEvent(self, *args, **kwargs): # real signature unknown
        pass

    def disconnectNotify(self, *args, **kwargs): # real signature unknown
        pass

    def execute(self, *args, **kwargs): # real signature unknown
        pass

    def isSignalConnected(self, *args, **kwargs): # real signature unknown
        pass

    def outputChannelMode(self, *args, **kwargs): # real signature unknown
        pass

    def pid(self, *args, **kwargs): # real signature unknown
        pass

    def program(self, *args, **kwargs): # real signature unknown
        pass

    def readData(self, *args, **kwargs): # real signature unknown
        pass

    def readLineData(self, *args, **kwargs): # real signature unknown
        pass

    def receivers(self, *args, **kwargs): # real signature unknown
        pass

    def sender(self, *args, **kwargs): # real signature unknown
        pass

    def senderSignalIndex(self, *args, **kwargs): # real signature unknown
        pass

    def setEnv(self, *args, **kwargs): # real signature unknown
        pass

    def setErrorString(self, *args, **kwargs): # real signature unknown
        pass

    def setNextOpenMode(self, *args, **kwargs): # real signature unknown
        pass

    def setOpenMode(self, *args, **kwargs): # real signature unknown
        pass

    def setOutputChannelMode(self, *args, **kwargs): # real signature unknown
        pass

    def setProcessState(self, *args, **kwargs): # real signature unknown
        pass

    def setProgram(self, *args, **kwargs): # real signature unknown
        pass

    def setShellCommand(self, *args, **kwargs): # real signature unknown
        pass

    def setupChildProcess(self, *args, **kwargs): # real signature unknown
        pass

    def start(self, *args, **kwargs): # real signature unknown
        pass

    def startDetached(self, *args, **kwargs): # real signature unknown
        pass

    def timerEvent(self, *args, **kwargs): # real signature unknown
        pass

    def unsetEnv(self, *args, **kwargs): # real signature unknown
        pass

    def writeData(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    def __lshift__(self, *args, **kwargs): # real signature unknown
        """ Return self<<value. """
        pass

    def __rlshift__(self, *args, **kwargs): # real signature unknown
        """ Return value<<self. """
        pass

    ForwardedChannels = 2
    MergedChannels = 1
    OnlyStderrChannel = 3
    OnlyStdoutChannel = 4
    OutputChannelMode = None # (!) real value is "<class 'PyKF5.KCoreAddons.KProcess.OutputChannelMode'>"
    SeparateChannels = 0


