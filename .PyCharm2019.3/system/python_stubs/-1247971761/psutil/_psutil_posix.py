# encoding: utf-8
# module psutil._psutil_posix
# from /usr/lib/python3.8/site-packages/psutil/_psutil_posix.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc
# no imports

# functions

def getpriority(*args, **kwargs): # real signature unknown
    """ Return process priority """
    pass

def net_if_addrs(*args, **kwargs): # real signature unknown
    """ Retrieve NICs information """
    pass

def net_if_flags(*args, **kwargs): # real signature unknown
    """ Retrieve NIC flags """
    pass

def net_if_mtu(*args, **kwargs): # real signature unknown
    """ Retrieve NIC MTU """
    pass

def setpriority(*args, **kwargs): # real signature unknown
    """ Set process priority """
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b1890a00>'

__spec__ = None # (!) real value is "ModuleSpec(name='psutil._psutil_posix', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b1890a00>, origin='/usr/lib/python3.8/site-packages/psutil/_psutil_posix.cpython-38-x86_64-linux-gnu.so')"

