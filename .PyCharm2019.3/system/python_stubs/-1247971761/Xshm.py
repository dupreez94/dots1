# encoding: utf-8
# module Xshm
# from /usr/lib/python3.8/site-packages/Xshm.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
""" Modul which implements the interaction with the Xshm extension. """
# no imports

# no functions
# classes

class Image(object):
    """
    An shared memory X11 Image
    
    Args:
        width (int): the width of this image
        height (int): the height of this image
    """
    def copy_to(self, *args, **kwargs): # real signature unknown
        """
        Draws the image on the surface at the passed coordinate.
        
        Args:
            drawable (int): the surface to draw on
            x (int): the x position where this image should be placed
            y (int): the y position where this image should be placed
            width (int): the width of the area
                         which should be copied to the drawable
            height (int): the height of the area
                          which should be copied to the drawable
        """
        pass

    def draw(self, *args, **kwargs): # real signature unknown
        """
        Places the pixels on the image at the passed coordinate.
        
        Args:
            x (int): the x position where the pixels should be placed
            y (int): the y position where the pixels should be placed
            width (int): amount of pixels per row in the passed data
            height (int): amount of pixels per column in the passed data
            pixels (bytes): the pixels to place on the image
        """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass


# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1640>'

__spec__ = None # (!) real value is "ModuleSpec(name='Xshm', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1640>, origin='/usr/lib/python3.8/site-packages/Xshm.cpython-38-x86_64-linux-gnu.so')"

