# encoding: utf-8
# module gi._gi
# from /usr/lib/python3.8/site-packages/gi/_gi.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc

# imports
from gobject import (GBoxed, GEnum, GFlags, GInterface, GParamSpec, GPointer, 
    GType, Warning)

import gi as __gi
import gobject as __gobject


class StructInfo(__gi.RegisteredTypeInfo):
    # no doc
    def find_field(self, *args, **kwargs): # real signature unknown
        pass

    def find_method(self, *args, **kwargs): # real signature unknown
        pass

    def get_alignment(self, *args, **kwargs): # real signature unknown
        pass

    def get_fields(self, *args, **kwargs): # real signature unknown
        pass

    def get_methods(self, *args, **kwargs): # real signature unknown
        pass

    def get_size(self, *args, **kwargs): # real signature unknown
        pass

    def is_foreign(self, *args, **kwargs): # real signature unknown
        pass

    def is_gtype_struct(self, *args, **kwargs): # real signature unknown
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


