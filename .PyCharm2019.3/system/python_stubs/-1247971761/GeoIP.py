# encoding: utf-8
# module GeoIP
# from /usr/lib/python3.8/site-packages/GeoIP.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
""" MaxMind GeoIP databases - Python API """
# no imports

# Variables with simple values

GEOIP_CABLEDSL_SPEED = 2

GEOIP_CHARSET_ISO_8859_1 = 0

GEOIP_CHARSET_UTF8 = 1

GEOIP_CHECK_CACHE = 2

GEOIP_CORPORATE_SPEED = 3

GEOIP_DIALUP_SPEED = 1

GEOIP_INDEX_CACHE = 4

GEOIP_MEMORY_CACHE = 1

GEOIP_MMAP_CACHE = 8

GEOIP_STANDARD = 0

GEOIP_UNKNOWN_SPEED = 0

# functions

def lib_version(*args, **kwargs): # real signature unknown
    """ Returns the CAPI version """
    pass

def new(*args, **kwargs): # real signature unknown
    """ GeoIP Constructor """
    pass

def open(*args, **kwargs): # real signature unknown
    """ GeoIP Constructor with database filename argument """
    pass

def time_zone_by_country_and_region(*args, **kwargs): # real signature unknown
    """ Returns time_zone for country, region """
    pass

# classes

class error(OSError):
    # no doc
    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



class GeoIP(object):
    """ GeoIP database object """
    def charset(self, either_GEOIP_CHARSET_ISO_8859_1_or_GEOIP_CHARSET_UTF8): # real signature unknown; restored from __doc__
        """ Return the current charset ( either GEOIP_CHARSET_ISO_8859_1 or GEOIP_CHARSET_UTF8 ) """
        pass

    def country_code_by_addr(self, *args, **kwargs): # real signature unknown
        """ Lookup Country Code By IP Address """
        pass

    def country_code_by_addr_v6(self, *args, **kwargs): # real signature unknown
        """ Lookup IPv6 Country Code By IP Address """
        pass

    def country_code_by_name(self, *args, **kwargs): # real signature unknown
        """ Lookup Country Code By Name """
        pass

    def country_code_by_name_v6(self, *args, **kwargs): # real signature unknown
        """ Lookup IPv6 Country Code By Name """
        pass

    def country_name_by_addr(self, *args, **kwargs): # real signature unknown
        """ Lookup Country Name By IP Address """
        pass

    def country_name_by_addr_v6(self, *args, **kwargs): # real signature unknown
        """ Lookup IPv6 Country Name By IP Address """
        pass

    def country_name_by_name(self, *args, **kwargs): # real signature unknown
        """ Lookup Country Name By Name """
        pass

    def country_name_by_name_v6(self, *args, **kwargs): # real signature unknown
        """ Lookup IPv6 Country Name By Name """
        pass

    def enable_teredo(self, *args, **kwargs): # real signature unknown
        """ Enable / disable teredo """
        pass

    def id_by_addr(self, *args, **kwargs): # real signature unknown
        """ Lookup Netspeed By IP Address """
        pass

    def id_by_name(self, *args, **kwargs): # real signature unknown
        """ Lookup Netspeed By Name """
        pass

    def last_netmask(self, *args, **kwargs): # real signature unknown
        """ Return the netmask depth of the last lookup """
        pass

    def name_by_addr(self, *args, **kwargs): # real signature unknown
        """ Lookup ASN, Domain, ISP and Organisation By IP Address """
        pass

    def name_by_addr_v6(self, *args, **kwargs): # real signature unknown
        """ Lookup IPv6 ASN, Domain, ISP and Organisation By IP Address """
        pass

    def name_by_name(self, *args, **kwargs): # real signature unknown
        """ Lookup ASN, Domain, ISP and Organisation By Name """
        pass

    def name_by_name_v6(self, *args, **kwargs): # real signature unknown
        """ Lookup IPv6 ASN, Domain, ISP and Organisation By Name """
        pass

    def org_by_addr(self, *args, **kwargs): # real signature unknown
        """ Lookup ASN, Domain, ISP and Organisation By IP Address ( deprecated use name_by_addr instead ) """
        pass

    def org_by_name(self, *args, **kwargs): # real signature unknown
        """ Lookup ASN, Domain, ISP and Organisation By Name ( deprecated use name_by_name instead ) """
        pass

    def range_by_ip(self, *args, **kwargs): # real signature unknown
        """ Lookup start and end IP's for a given IP """
        pass

    def record_by_addr(self, *args, **kwargs): # real signature unknown
        """ Lookup City Region By IP Address """
        pass

    def record_by_addr_v6(self, *args, **kwargs): # real signature unknown
        """ Lookup City Region By IP Address """
        pass

    def record_by_name(self, *args, **kwargs): # real signature unknown
        """ Lookup City Region By Name """
        pass

    def record_by_name_v6(self, *args, **kwargs): # real signature unknown
        """ Lookup City Region By Name """
        pass

    def region_by_addr(self, *args, **kwargs): # real signature unknown
        """ Lookup Region By IP Address """
        pass

    def region_by_name(self, *args, **kwargs): # real signature unknown
        """ Lookup Region By Name """
        pass

    def set_charset(self, *args, **kwargs): # real signature unknown
        """ Set the charset for city records """
        pass

    def teredo(self, *args, **kwargs): # real signature unknown
        """ Returns true if teredo is enabled """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass

    @staticmethod # known case of __new__
    def __new__(*args, **kwargs): # real signature unknown
        """ Create and return a new object.  See help(type) for accurate signature. """
        pass

    database_edition = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Edition of the database ( country, city, ISP, etc )"""

    database_info = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Information about the database"""

    GEOIP_STANDARD = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """Same as module-level GEOIP_STANDARD constant ( deprecated )"""



# variables with complex values

country_codes = (
    '--',
    'AP',
    'EU',
    'AD',
    'AE',
    'AF',
    'AG',
    'AI',
    'AL',
    'AM',
    'CW',
    'AO',
    'AQ',
    'AR',
    'AS',
    'AT',
    'AU',
    'AW',
    'AZ',
    'BA',
    'BB',
    'BD',
    'BE',
    'BF',
    'BG',
    'BH',
    'BI',
    'BJ',
    'BM',
    'BN',
    'BO',
    'BR',
    'BS',
    'BT',
    'BV',
    'BW',
    'BY',
    'BZ',
    'CA',
    'CC',
    'CD',
    'CF',
    'CG',
    'CH',
    'CI',
    'CK',
    'CL',
    'CM',
    'CN',
    'CO',
    'CR',
    'CU',
    'CV',
    'CX',
    'CY',
    'CZ',
    'DE',
    'DJ',
    'DK',
    'DM',
    'DO',
    'DZ',
    'EC',
    'EE',
    'EG',
    'EH',
    'ER',
    'ES',
    'ET',
    'FI',
    'FJ',
    'FK',
    'FM',
    'FO',
    'FR',
    'SX',
    'GA',
    'GB',
    'GD',
    'GE',
    'GF',
    'GH',
    'GI',
    'GL',
    'GM',
    'GN',
    'GP',
    'GQ',
    'GR',
    'GS',
    'GT',
    'GU',
    'GW',
    'GY',
    'HK',
    'HM',
    'HN',
    'HR',
    'HT',
    'HU',
    'ID',
    'IE',
    'IL',
    'IN',
    'IO',
    'IQ',
    'IR',
    'IS',
    'IT',
    'JM',
    'JO',
    'JP',
    'KE',
    'KG',
    'KH',
    'KI',
    'KM',
    'KN',
    'KP',
    'KR',
    'KW',
    'KY',
    'KZ',
    'LA',
    'LB',
    'LC',
    'LI',
    'LK',
    'LR',
    'LS',
    'LT',
    'LU',
    'LV',
    'LY',
    'MA',
    'MC',
    'MD',
    'MG',
    'MH',
    'MK',
    'ML',
    'MM',
    'MN',
    'MO',
    'MP',
    'MQ',
    'MR',
    'MS',
    'MT',
    'MU',
    'MV',
    'MW',
    'MX',
    'MY',
    'MZ',
    'NA',
    'NC',
    'NE',
    'NF',
    'NG',
    'NI',
    'NL',
    'NO',
    'NP',
    'NR',
    'NU',
    'NZ',
    'OM',
    'PA',
    'PE',
    'PF',
    'PG',
    'PH',
    'PK',
    'PL',
    'PM',
    'PN',
    'PR',
    'PS',
    'PT',
    'PW',
    'PY',
    'QA',
    'RE',
    'RO',
    'RU',
    'RW',
    'SA',
    'SB',
    'SC',
    'SD',
    'SE',
    'SG',
    'SH',
    'SI',
    'SJ',
    'SK',
    'SL',
    'SM',
    'SN',
    'SO',
    'SR',
    'ST',
    'SV',
    'SY',
    'SZ',
    'TC',
    'TD',
    'TF',
    'TG',
    'TH',
    'TJ',
    'TK',
    'TM',
    'TN',
    'TO',
    'TL',
    'TR',
    'TT',
    'TV',
    'TW',
    'TZ',
    'UA',
    'UG',
    'UM',
    'US',
    'UY',
    'UZ',
    'VA',
    'VC',
    'VE',
    'VG',
    'VI',
    'VN',
    'VU',
    'WF',
    'WS',
    'YE',
    'YT',
    'RS',
    'ZA',
    'ZM',
    'ME',
    'ZW',
    'A1',
    'A2',
    'O1',
    'AX',
    'GG',
    'IM',
    'JE',
    'BL',
    'MF',
    'BQ',
    'SS',
    'O1',
)

country_continents = {
    '--': '--',
    'A1': '--',
    'A2': '--',
    'AD': 'EU',
    'AE': 'AS',
    'AF': 'AS',
    'AG': 'NA',
    'AI': 'NA',
    'AL': 'EU',
    'AM': 'AS',
    'AO': 'AF',
    'AP': 'AS',
    'AQ': 'AN',
    'AR': 'SA',
    'AS': 'OC',
    'AT': 'EU',
    'AU': 'OC',
    'AW': 'NA',
    'AX': 'EU',
    'AZ': 'AS',
    'BA': 'EU',
    'BB': 'NA',
    'BD': 'AS',
    'BE': 'EU',
    'BF': 'AF',
    'BG': 'EU',
    'BH': 'AS',
    'BI': 'AF',
    'BJ': 'AF',
    'BL': 'NA',
    'BM': 'NA',
    'BN': 'AS',
    'BO': 'SA',
    'BQ': 'NA',
    'BR': 'SA',
    'BS': 'NA',
    'BT': 'AS',
    'BV': 'AN',
    'BW': 'AF',
    'BY': 'EU',
    'BZ': 'NA',
    'CA': 'NA',
    'CC': 'AS',
    'CD': 'AF',
    'CF': 'AF',
    'CG': 'AF',
    'CH': 'EU',
    'CI': 'AF',
    'CK': 'OC',
    'CL': 'SA',
    'CM': 'AF',
    'CN': 'AS',
    'CO': 'SA',
    'CR': 'NA',
    'CU': 'NA',
    'CV': 'AF',
    'CW': 'NA',
    'CX': 'AS',
    'CY': 'AS',
    'CZ': 'EU',
    'DE': 'EU',
    'DJ': 'AF',
    'DK': 'EU',
    'DM': 'NA',
    'DO': 'NA',
    'DZ': 'AF',
    'EC': 'SA',
    'EE': 'EU',
    'EG': 'AF',
    'EH': 'AF',
    'ER': 'AF',
    'ES': 'EU',
    'ET': 'AF',
    'EU': 'EU',
    'FI': 'EU',
    'FJ': 'OC',
    'FK': 'SA',
    'FM': 'OC',
    'FO': 'EU',
    'FR': 'EU',
    'GA': 'AF',
    'GB': 'EU',
    'GD': 'NA',
    'GE': 'AS',
    'GF': 'SA',
    'GG': 'EU',
    'GH': 'AF',
    'GI': 'EU',
    'GL': 'NA',
    'GM': 'AF',
    'GN': 'AF',
    'GP': 'NA',
    'GQ': 'AF',
    'GR': 'EU',
    'GS': 'AN',
    'GT': 'NA',
    'GU': 'OC',
    'GW': 'AF',
    'GY': 'SA',
    'HK': 'AS',
    'HM': 'AN',
    'HN': 'NA',
    'HR': 'EU',
    'HT': 'NA',
    'HU': 'EU',
    'ID': 'AS',
    'IE': 'EU',
    'IL': 'AS',
    'IM': 'EU',
    'IN': 'AS',
    'IO': 'AS',
    'IQ': 'AS',
    'IR': 'AS',
    'IS': 'EU',
    'IT': 'EU',
    'JE': 'EU',
    'JM': 'NA',
    'JO': 'AS',
    'JP': 'AS',
    'KE': 'AF',
    'KG': 'AS',
    'KH': 'AS',
    'KI': 'OC',
    'KM': 'AF',
    'KN': 'NA',
    'KP': 'AS',
    'KR': 'AS',
    'KW': 'AS',
    'KY': 'NA',
    'KZ': 'AS',
    'LA': 'AS',
    'LB': 'AS',
    'LC': 'NA',
    'LI': 'EU',
    'LK': 'AS',
    'LR': 'AF',
    'LS': 'AF',
    'LT': 'EU',
    'LU': 'EU',
    'LV': 'EU',
    'LY': 'AF',
    'MA': 'AF',
    'MC': 'EU',
    'MD': 'EU',
    'ME': 'EU',
    'MF': 'NA',
    'MG': 'AF',
    'MH': 'OC',
    'MK': 'EU',
    'ML': 'AF',
    'MM': 'AS',
    'MN': 'AS',
    'MO': 'AS',
    'MP': 'OC',
    'MQ': 'NA',
    'MR': 'AF',
    'MS': 'NA',
    'MT': 'EU',
    'MU': 'AF',
    'MV': 'AS',
    'MW': 'AF',
    'MX': 'NA',
    'MY': 'AS',
    'MZ': 'AF',
    'NA': 'AF',
    'NC': 'OC',
    'NE': 'AF',
    'NF': 'OC',
    'NG': 'AF',
    'NI': 'NA',
    'NL': 'EU',
    'NO': 'EU',
    'NP': 'AS',
    'NR': 'OC',
    'NU': 'OC',
    'NZ': 'OC',
    'O1': '--',
    'OM': 'AS',
    'PA': 'NA',
    'PE': 'SA',
    'PF': 'OC',
    'PG': 'OC',
    'PH': 'AS',
    'PK': 'AS',
    'PL': 'EU',
    'PM': 'NA',
    'PN': 'OC',
    'PR': 'NA',
    'PS': 'AS',
    'PT': 'EU',
    'PW': 'OC',
    'PY': 'SA',
    'QA': 'AS',
    'RE': 'AF',
    'RO': 'EU',
    'RS': 'EU',
    'RU': 'EU',
    'RW': 'AF',
    'SA': 'AS',
    'SB': 'OC',
    'SC': 'AF',
    'SD': 'AF',
    'SE': 'EU',
    'SG': 'AS',
    'SH': 'AF',
    'SI': 'EU',
    'SJ': 'EU',
    'SK': 'EU',
    'SL': 'AF',
    'SM': 'EU',
    'SN': 'AF',
    'SO': 'AF',
    'SR': 'SA',
    'SS': 'AF',
    'ST': 'AF',
    'SV': 'NA',
    'SX': 'NA',
    'SY': 'AS',
    'SZ': 'AF',
    'TC': 'NA',
    'TD': 'AF',
    'TF': 'AN',
    'TG': 'AF',
    'TH': 'AS',
    'TJ': 'AS',
    'TK': 'OC',
    'TL': 'AS',
    'TM': 'AS',
    'TN': 'AF',
    'TO': 'OC',
    'TR': 'EU',
    'TT': 'NA',
    'TV': 'OC',
    'TW': 'AS',
    'TZ': 'AF',
    'UA': 'EU',
    'UG': 'AF',
    'UM': 'OC',
    'US': 'NA',
    'UY': 'SA',
    'UZ': 'AS',
    'VA': 'EU',
    'VC': 'NA',
    'VE': 'SA',
    'VG': 'NA',
    'VI': 'NA',
    'VN': 'AS',
    'VU': 'OC',
    'WF': 'OC',
    'WS': 'OC',
    'YE': 'AS',
    'YT': 'AF',
    'ZA': 'AF',
    'ZM': 'AF',
    'ZW': 'AF',
}

country_names = {
    '--': 'N/A',
    'A1': 'Anonymous Proxy',
    'A2': 'Satellite Provider',
    'AD': 'Andorra',
    'AE': 'United Arab Emirates',
    'AF': 'Afghanistan',
    'AG': 'Antigua and Barbuda',
    'AI': 'Anguilla',
    'AL': 'Albania',
    'AM': 'Armenia',
    'AO': 'Angola',
    'AP': 'Asia/Pacific Region',
    'AQ': 'Antarctica',
    'AR': 'Argentina',
    'AS': 'American Samoa',
    'AT': 'Austria',
    'AU': 'Australia',
    'AW': 'Aruba',
    'AX': 'Aland Islands',
    'AZ': 'Azerbaijan',
    'BA': 'Bosnia and Herzegovina',
    'BB': 'Barbados',
    'BD': 'Bangladesh',
    'BE': 'Belgium',
    'BF': 'Burkina Faso',
    'BG': 'Bulgaria',
    'BH': 'Bahrain',
    'BI': 'Burundi',
    'BJ': 'Benin',
    'BL': 'Saint Barthelemy',
    'BM': 'Bermuda',
    'BN': 'Brunei Darussalam',
    'BO': 'Bolivia',
    'BQ': 'Bonaire, Saint Eustatius and Saba',
    'BR': 'Brazil',
    'BS': 'Bahamas',
    'BT': 'Bhutan',
    'BV': 'Bouvet Island',
    'BW': 'Botswana',
    'BY': 'Belarus',
    'BZ': 'Belize',
    'CA': 'Canada',
    'CC': 'Cocos (Keeling) Islands',
    'CD': 'Congo, The Democratic Republic of the',
    'CF': 'Central African Republic',
    'CG': 'Congo',
    'CH': 'Switzerland',
    'CI': "Cote D'Ivoire",
    'CK': 'Cook Islands',
    'CL': 'Chile',
    'CM': 'Cameroon',
    'CN': 'China',
    'CO': 'Colombia',
    'CR': 'Costa Rica',
    'CU': 'Cuba',
    'CV': 'Cape Verde',
    'CW': 'Curaçao',
    'CX': 'Christmas Island',
    'CY': 'Cyprus',
    'CZ': 'Czech Republic',
    'DE': 'Germany',
    'DJ': 'Djibouti',
    'DK': 'Denmark',
    'DM': 'Dominica',
    'DO': 'Dominican Republic',
    'DZ': 'Algeria',
    'EC': 'Ecuador',
    'EE': 'Estonia',
    'EG': 'Egypt',
    'EH': 'Western Sahara',
    'ER': 'Eritrea',
    'ES': 'Spain',
    'ET': 'Ethiopia',
    'EU': 'Europe',
    'FI': 'Finland',
    'FJ': 'Fiji',
    'FK': 'Falkland Islands (Malvinas)',
    'FM': 'Micronesia, Federated States of',
    'FO': 'Faroe Islands',
    'FR': 'France',
    'GA': 'Gabon',
    'GB': 'United Kingdom',
    'GD': 'Grenada',
    'GE': 'Georgia',
    'GF': 'French Guiana',
    'GG': 'Guernsey',
    'GH': 'Ghana',
    'GI': 'Gibraltar',
    'GL': 'Greenland',
    'GM': 'Gambia',
    'GN': 'Guinea',
    'GP': 'Guadeloupe',
    'GQ': 'Equatorial Guinea',
    'GR': 'Greece',
    'GS': 'South Georgia and the South Sandwich Islands',
    'GT': 'Guatemala',
    'GU': 'Guam',
    'GW': 'Guinea-Bissau',
    'GY': 'Guyana',
    'HK': 'Hong Kong',
    'HM': 'Heard Island and McDonald Islands',
    'HN': 'Honduras',
    'HR': 'Croatia',
    'HT': 'Haiti',
    'HU': 'Hungary',
    'ID': 'Indonesia',
    'IE': 'Ireland',
    'IL': 'Israel',
    'IM': 'Isle of Man',
    'IN': 'India',
    'IO': 'British Indian Ocean Territory',
    'IQ': 'Iraq',
    'IR': 'Iran, Islamic Republic of',
    'IS': 'Iceland',
    'IT': 'Italy',
    'JE': 'Jersey',
    'JM': 'Jamaica',
    'JO': 'Jordan',
    'JP': 'Japan',
    'KE': 'Kenya',
    'KG': 'Kyrgyzstan',
    'KH': 'Cambodia',
    'KI': 'Kiribati',
    'KM': 'Comoros',
    'KN': 'Saint Kitts and Nevis',
    'KP': "Korea, Democratic People's Republic of",
    'KR': 'Korea, Republic of',
    'KW': 'Kuwait',
    'KY': 'Cayman Islands',
    'KZ': 'Kazakhstan',
    'LA': "Lao People's Democratic Republic",
    'LB': 'Lebanon',
    'LC': 'Saint Lucia',
    'LI': 'Liechtenstein',
    'LK': 'Sri Lanka',
    'LR': 'Liberia',
    'LS': 'Lesotho',
    'LT': 'Lithuania',
    'LU': 'Luxembourg',
    'LV': 'Latvia',
    'LY': 'Libya',
    'MA': 'Morocco',
    'MC': 'Monaco',
    'MD': 'Moldova, Republic of',
    'ME': 'Montenegro',
    'MF': 'Saint Martin',
    'MG': 'Madagascar',
    'MH': 'Marshall Islands',
    'MK': 'Macedonia',
    'ML': 'Mali',
    'MM': 'Myanmar',
    'MN': 'Mongolia',
    'MO': 'Macau',
    'MP': 'Northern Mariana Islands',
    'MQ': 'Martinique',
    'MR': 'Mauritania',
    'MS': 'Montserrat',
    'MT': 'Malta',
    'MU': 'Mauritius',
    'MV': 'Maldives',
    'MW': 'Malawi',
    'MX': 'Mexico',
    'MY': 'Malaysia',
    'MZ': 'Mozambique',
    'NA': 'Namibia',
    'NC': 'New Caledonia',
    'NE': 'Niger',
    'NF': 'Norfolk Island',
    'NG': 'Nigeria',
    'NI': 'Nicaragua',
    'NL': 'Netherlands',
    'NO': 'Norway',
    'NP': 'Nepal',
    'NR': 'Nauru',
    'NU': 'Niue',
    'NZ': 'New Zealand',
    'O1': 'Other',
    'OM': 'Oman',
    'PA': 'Panama',
    'PE': 'Peru',
    'PF': 'French Polynesia',
    'PG': 'Papua New Guinea',
    'PH': 'Philippines',
    'PK': 'Pakistan',
    'PL': 'Poland',
    'PM': 'Saint Pierre and Miquelon',
    'PN': 'Pitcairn Islands',
    'PR': 'Puerto Rico',
    'PS': 'Palestinian Territory',
    'PT': 'Portugal',
    'PW': 'Palau',
    'PY': 'Paraguay',
    'QA': 'Qatar',
    'RE': 'Reunion',
    'RO': 'Romania',
    'RS': 'Serbia',
    'RU': 'Russian Federation',
    'RW': 'Rwanda',
    'SA': 'Saudi Arabia',
    'SB': 'Solomon Islands',
    'SC': 'Seychelles',
    'SD': 'Sudan',
    'SE': 'Sweden',
    'SG': 'Singapore',
    'SH': 'Saint Helena',
    'SI': 'Slovenia',
    'SJ': 'Svalbard and Jan Mayen',
    'SK': 'Slovakia',
    'SL': 'Sierra Leone',
    'SM': 'San Marino',
    'SN': 'Senegal',
    'SO': 'Somalia',
    'SR': 'Suriname',
    'SS': 'South Sudan',
    'ST': 'Sao Tome and Principe',
    'SV': 'El Salvador',
    'SX': 'Sint Maarten (Dutch part)',
    'SY': 'Syrian Arab Republic',
    'SZ': 'Swaziland',
    'TC': 'Turks and Caicos Islands',
    'TD': 'Chad',
    'TF': 'French Southern Territories',
    'TG': 'Togo',
    'TH': 'Thailand',
    'TJ': 'Tajikistan',
    'TK': 'Tokelau',
    'TL': 'Timor-Leste',
    'TM': 'Turkmenistan',
    'TN': 'Tunisia',
    'TO': 'Tonga',
    'TR': 'Turkey',
    'TT': 'Trinidad and Tobago',
    'TV': 'Tuvalu',
    'TW': 'Taiwan',
    'TZ': 'Tanzania, United Republic of',
    'UA': 'Ukraine',
    'UG': 'Uganda',
    'UM': 'United States Minor Outlying Islands',
    'US': 'United States',
    'UY': 'Uruguay',
    'UZ': 'Uzbekistan',
    'VA': 'Holy See (Vatican City State)',
    'VC': 'Saint Vincent and the Grenadines',
    'VE': 'Venezuela',
    'VG': 'Virgin Islands, British',
    'VI': 'Virgin Islands, U.S.',
    'VN': 'Vietnam',
    'VU': 'Vanuatu',
    'WF': 'Wallis and Futuna',
    'WS': 'Samoa',
    'YE': 'Yemen',
    'YT': 'Mayotte',
    'ZA': 'South Africa',
    'ZM': 'Zambia',
    'ZW': 'Zimbabwe',
}

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1520>'

__spec__ = None # (!) real value is "ModuleSpec(name='GeoIP', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1520>, origin='/usr/lib/python3.8/site-packages/GeoIP.cpython-38-x86_64-linux-gnu.so')"

