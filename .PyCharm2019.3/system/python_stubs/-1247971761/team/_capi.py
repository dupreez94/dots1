# encoding: utf-8
# module team._capi
# from /usr/lib/python3.8/site-packages/team/_capi.cpython-38-x86_64-linux-gnu.so
# by generator 1.147
# no doc
# no imports

# Variables with simple values

TEAM_ANY_CHANGE = 7

TEAM_IFINFO_CHANGE = 4
TEAM_IFINFO_REFRESH = 8

TEAM_OPTION_CHANGE = 2

TEAM_OPTION_TYPE_BINARY = 2
TEAM_OPTION_TYPE_BOOL = 3
TEAM_OPTION_TYPE_S32 = 4
TEAM_OPTION_TYPE_STRING = 1
TEAM_OPTION_TYPE_U32 = 0

TEAM_PORT_CHANGE = 1

# functions

def delete_team_change_handler(*args, **kwargs): # real signature unknown
    pass

def new_team_change_handler(*args, **kwargs): # real signature unknown
    pass

def py_team_change_handler_register(*args, **kwargs): # real signature unknown
    pass

def py_team_change_handler_unregister(*args, **kwargs): # real signature unknown
    pass

def SWIG_PyInstanceMethod_New(*args, **kwargs): # real signature unknown
    pass

def SWIG_PyStaticMethod_New(*args, **kwargs): # real signature unknown
    pass

def team_alloc(*args, **kwargs): # real signature unknown
    pass

def team_call_eventfd_handler(*args, **kwargs): # real signature unknown
    pass

def team_carrier_get(*args, **kwargs): # real signature unknown
    pass

def team_carrier_set(*args, **kwargs): # real signature unknown
    pass

def team_change_handler_func_get(*args, **kwargs): # real signature unknown
    pass

def team_change_handler_func_set(*args, **kwargs): # real signature unknown
    pass

def team_change_handler_register(*args, **kwargs): # real signature unknown
    pass

def team_change_handler_register_head(*args, **kwargs): # real signature unknown
    pass

def team_change_handler_swiginit(*args, **kwargs): # real signature unknown
    pass

def team_change_handler_swigregister(*args, **kwargs): # real signature unknown
    pass

def team_change_handler_type_mask_get(*args, **kwargs): # real signature unknown
    pass

def team_change_handler_type_mask_set(*args, **kwargs): # real signature unknown
    pass

def team_change_handler_unregister(*args, **kwargs): # real signature unknown
    pass

def team_check_events(*args, **kwargs): # real signature unknown
    pass

def team_create(*args, **kwargs): # real signature unknown
    pass

def team_destroy(*args, **kwargs): # real signature unknown
    pass

def team_free(*args, **kwargs): # real signature unknown
    pass

def team_get_active_port(*args, **kwargs): # real signature unknown
    pass

def team_get_bpf_hash_func(*args, **kwargs): # real signature unknown
    pass

def team_get_eventfd_fd(*args, **kwargs): # real signature unknown
    pass

def team_get_event_fd(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo_admin_state(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo_hwaddr(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo_hwaddr_len(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo_ifindex(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo_ifname(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo_master_ifindex(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo_orig_hwaddr(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo_orig_hwaddr_len(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo_phys_port_id(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo_phys_port_id_len(*args, **kwargs): # real signature unknown
    pass

def team_get_ifinfo_port(*args, **kwargs): # real signature unknown
    pass

def team_get_log_priority(*args, **kwargs): # real signature unknown
    pass

def team_get_mcast_rejoin_count(*args, **kwargs): # real signature unknown
    pass

def team_get_mcast_rejoin_interval(*args, **kwargs): # real signature unknown
    pass

def team_get_mode_name(*args, **kwargs): # real signature unknown
    pass

def team_get_next_eventfd(*args, **kwargs): # real signature unknown
    pass

def team_get_next_ifinfo(*args, **kwargs): # real signature unknown
    pass

def team_get_next_option(*args, **kwargs): # real signature unknown
    pass

def team_get_next_port(*args, **kwargs): # real signature unknown
    pass

def team_get_notify_peers_count(*args, **kwargs): # real signature unknown
    pass

def team_get_notify_peers_interval(*args, **kwargs): # real signature unknown
    pass

def team_get_option(*args, **kwargs): # real signature unknown
    pass

def team_get_option_array_index(*args, **kwargs): # real signature unknown
    pass

def team_get_option_name(*args, **kwargs): # real signature unknown
    pass

def team_get_option_port_ifindex(*args, **kwargs): # real signature unknown
    pass

def team_get_option_type(*args, **kwargs): # real signature unknown
    pass

def team_get_option_value_binary(*args, **kwargs): # real signature unknown
    pass

def team_get_option_value_bool(*args, **kwargs): # real signature unknown
    pass

def team_get_option_value_len(*args, **kwargs): # real signature unknown
    pass

def team_get_option_value_s32(*args, **kwargs): # real signature unknown
    pass

def team_get_option_value_string(*args, **kwargs): # real signature unknown
    pass

def team_get_option_value_u32(*args, **kwargs): # real signature unknown
    pass

def team_get_port_duplex(*args, **kwargs): # real signature unknown
    pass

def team_get_port_enabled(*args, **kwargs): # real signature unknown
    pass

def team_get_port_ifindex(*args, **kwargs): # real signature unknown
    pass

def team_get_port_ifinfo(*args, **kwargs): # real signature unknown
    pass

def team_get_port_priority(*args, **kwargs): # real signature unknown
    pass

def team_get_port_speed(*args, **kwargs): # real signature unknown
    pass

def team_get_port_user_linkup(*args, **kwargs): # real signature unknown
    pass

def team_handle_events(*args, **kwargs): # real signature unknown
    pass

def team_hwaddr_get(*args, **kwargs): # real signature unknown
    pass

def team_hwaddr_len_get(*args, **kwargs): # real signature unknown
    pass

def team_hwaddr_set(*args, **kwargs): # real signature unknown
    pass

def team_ifindex2ifname(*args, **kwargs): # real signature unknown
    pass

def team_ifinfo_str(*args, **kwargs): # real signature unknown
    pass

def team_ifname2ifindex(*args, **kwargs): # real signature unknown
    pass

def team_init(*args, **kwargs): # real signature unknown
    pass

def team_is_ifinfo_admin_state_changed(*args, **kwargs): # real signature unknown
    pass

def team_is_ifinfo_changed(*args, **kwargs): # real signature unknown
    pass

def team_is_ifinfo_hwaddr_changed(*args, **kwargs): # real signature unknown
    pass

def team_is_ifinfo_hwaddr_len_changed(*args, **kwargs): # real signature unknown
    pass

def team_is_ifinfo_ifname_changed(*args, **kwargs): # real signature unknown
    pass

def team_is_ifinfo_master_ifindex_changed(*args, **kwargs): # real signature unknown
    pass

def team_is_ifinfo_phys_port_id_changed(*args, **kwargs): # real signature unknown
    pass

def team_is_ifinfo_phys_port_id_len_changed(*args, **kwargs): # real signature unknown
    pass

def team_is_ifinfo_removed(*args, **kwargs): # real signature unknown
    pass

def team_is_option_array(*args, **kwargs): # real signature unknown
    pass

def team_is_option_changed(*args, **kwargs): # real signature unknown
    pass

def team_is_option_changed_locally(*args, **kwargs): # real signature unknown
    pass

def team_is_option_initialized(*args, **kwargs): # real signature unknown
    pass

def team_is_option_per_port(*args, **kwargs): # real signature unknown
    pass

def team_is_our_port(*args, **kwargs): # real signature unknown
    pass

def team_is_port_changed(*args, **kwargs): # real signature unknown
    pass

def team_is_port_link_up(*args, **kwargs): # real signature unknown
    pass

def team_is_port_present(*args, **kwargs): # real signature unknown
    pass

def team_is_port_removed(*args, **kwargs): # real signature unknown
    pass

def team_option_str(*args, **kwargs): # real signature unknown
    pass

def team_option_value_str(*args, **kwargs): # real signature unknown
    pass

def team_port_add(*args, **kwargs): # real signature unknown
    pass

def team_port_remove(*args, **kwargs): # real signature unknown
    pass

def team_port_str(*args, **kwargs): # real signature unknown
    pass

def team_recreate(*args, **kwargs): # real signature unknown
    pass

def team_refresh(*args, **kwargs): # real signature unknown
    pass

def team_set_active_port(*args, **kwargs): # real signature unknown
    pass

def team_set_bpf_hash_func(*args, **kwargs): # real signature unknown
    pass

def team_set_log_fn(*args, **kwargs): # real signature unknown
    pass

def team_set_log_priority(*args, **kwargs): # real signature unknown
    pass

def team_set_mcast_rejoin_count(*args, **kwargs): # real signature unknown
    pass

def team_set_mcast_rejoin_interval(*args, **kwargs): # real signature unknown
    pass

def team_set_mode_name(*args, **kwargs): # real signature unknown
    pass

def team_set_notify_peers_count(*args, **kwargs): # real signature unknown
    pass

def team_set_notify_peers_interval(*args, **kwargs): # real signature unknown
    pass

def team_set_option_value_binary(*args, **kwargs): # real signature unknown
    pass

def team_set_option_value_bool(*args, **kwargs): # real signature unknown
    pass

def team_set_option_value_from_string(*args, **kwargs): # real signature unknown
    pass

def team_set_option_value_s32(*args, **kwargs): # real signature unknown
    pass

def team_set_option_value_string(*args, **kwargs): # real signature unknown
    pass

def team_set_option_value_u32(*args, **kwargs): # real signature unknown
    pass

def team_set_port_enabled(*args, **kwargs): # real signature unknown
    pass

def team_set_port_priority(*args, **kwargs): # real signature unknown
    pass

def team_set_port_queue_id(*args, **kwargs): # real signature unknown
    pass

def team_set_port_user_linkup(*args, **kwargs): # real signature unknown
    pass

def team_set_port_user_linkup_enabled(*args, **kwargs): # real signature unknown
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1d60>'

__spec__ = None # (!) real value is "ModuleSpec(name='team._capi', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d1d60>, origin='/usr/lib/python3.8/site-packages/team/_capi.cpython-38-x86_64-linux-gnu.so')"

