# encoding: utf-8
# module PyQt5.QtSensors
# from /usr/lib/python3.8/site-packages/PyQt5/QtSensors.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QSensorReading import QSensorReading

class QProximityReading(QSensorReading):
    # no doc
    def close(self): # real signature unknown; restored from __doc__
        """ close(self) -> bool """
        return False

    def setClose(self, bool): # real signature unknown; restored from __doc__
        """ setClose(self, bool) """
        pass

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


