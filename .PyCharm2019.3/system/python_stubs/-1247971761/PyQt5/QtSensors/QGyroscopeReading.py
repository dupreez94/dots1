# encoding: utf-8
# module PyQt5.QtSensors
# from /usr/lib/python3.8/site-packages/PyQt5/QtSensors.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QSensorReading import QSensorReading

class QGyroscopeReading(QSensorReading):
    # no doc
    def setX(self, p_float): # real signature unknown; restored from __doc__
        """ setX(self, float) """
        pass

    def setY(self, p_float): # real signature unknown; restored from __doc__
        """ setY(self, float) """
        pass

    def setZ(self, p_float): # real signature unknown; restored from __doc__
        """ setZ(self, float) """
        pass

    def x(self): # real signature unknown; restored from __doc__
        """ x(self) -> float """
        return 0.0

    def y(self): # real signature unknown; restored from __doc__
        """ y(self) -> float """
        return 0.0

    def z(self): # real signature unknown; restored from __doc__
        """ z(self) -> float """
        return 0.0

    def __init__(self, *args, **kwargs): # real signature unknown
        pass


