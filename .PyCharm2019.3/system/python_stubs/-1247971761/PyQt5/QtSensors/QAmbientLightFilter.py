# encoding: utf-8
# module PyQt5.QtSensors
# from /usr/lib/python3.8/site-packages/PyQt5/QtSensors.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QSensorFilter import QSensorFilter

class QAmbientLightFilter(QSensorFilter):
    """
    QAmbientLightFilter()
    QAmbientLightFilter(QAmbientLightFilter)
    """
    def filter(self, QAmbientLightReading): # real signature unknown; restored from __doc__
        """ filter(self, QAmbientLightReading) -> bool """
        return False

    def __init__(self, QAmbientLightFilter=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


