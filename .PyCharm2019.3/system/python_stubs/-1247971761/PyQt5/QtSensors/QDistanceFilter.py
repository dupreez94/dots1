# encoding: utf-8
# module PyQt5.QtSensors
# from /usr/lib/python3.8/site-packages/PyQt5/QtSensors.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QSensorFilter import QSensorFilter

class QDistanceFilter(QSensorFilter):
    """
    QDistanceFilter()
    QDistanceFilter(QDistanceFilter)
    """
    def filter(self, QDistanceReading): # real signature unknown; restored from __doc__
        """ filter(self, QDistanceReading) -> bool """
        return False

    def __init__(self, QDistanceFilter=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass


