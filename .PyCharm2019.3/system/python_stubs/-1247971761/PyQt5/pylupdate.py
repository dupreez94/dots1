# encoding: utf-8
# module PyQt5.pylupdate
# from /usr/lib/python3.8/site-packages/PyQt5/pylupdate.abi3.so
# by generator 1.147
# no doc

# imports
import sip as __sip


# functions

def fetchtr_py(p_str, MetaTranslator, p_str_1, bool, p_str_2, p_str_3, p_str_4): # real signature unknown; restored from __doc__
    """ fetchtr_py(str, MetaTranslator, str, bool, str, str, str) """
    pass

def fetchtr_ui(p_str, MetaTranslator, p_str_1, bool): # real signature unknown; restored from __doc__
    """ fetchtr_ui(str, MetaTranslator, str, bool) """
    pass

def merge(MetaTranslator, MetaTranslator_1, MetaTranslator_2, bool, bool_1, p_str): # real signature unknown; restored from __doc__
    """ merge(MetaTranslator, MetaTranslator, MetaTranslator, bool, bool, str) """
    pass

def proFileTagMap(p_str): # real signature unknown; restored from __doc__
    """ proFileTagMap(str) -> Dict[str, str] """
    return {}

# classes

class MetaTranslator(__sip.simplewrapper):
    """
    MetaTranslator()
    MetaTranslator(MetaTranslator)
    """
    def load(self, p_str): # real signature unknown; restored from __doc__
        """ load(self, str) -> bool """
        return False

    def save(self, p_str): # real signature unknown; restored from __doc__
        """ save(self, str) -> bool """
        return False

    def setCodec(self, p_str): # real signature unknown; restored from __doc__
        """ setCodec(self, str) """
        pass

    def stripEmptyContexts(self): # real signature unknown; restored from __doc__
        """ stripEmptyContexts(self) """
        pass

    def stripObsoleteMessages(self): # real signature unknown; restored from __doc__
        """ stripObsoleteMessages(self) """
        pass

    def __init__(self, MetaTranslator=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



# variables with complex values



