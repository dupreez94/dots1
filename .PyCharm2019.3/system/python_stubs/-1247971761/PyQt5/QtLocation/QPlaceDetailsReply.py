# encoding: utf-8
# module PyQt5.QtLocation
# from /usr/lib/python3.8/site-packages/PyQt5/QtLocation.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QPlaceReply import QPlaceReply

class QPlaceDetailsReply(QPlaceReply):
    """ QPlaceDetailsReply(parent: QObject = None) """
    def place(self): # real signature unknown; restored from __doc__
        """ place(self) -> QPlace """
        return QPlace

    def setPlace(self, QPlace): # real signature unknown; restored from __doc__
        """ setPlace(self, QPlace) """
        pass

    def type(self): # real signature unknown; restored from __doc__
        """ type(self) -> QPlaceReply.Type """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


