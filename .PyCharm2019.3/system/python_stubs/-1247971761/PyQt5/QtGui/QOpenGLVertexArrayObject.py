# encoding: utf-8
# module PyQt5.QtGui
# from /usr/lib/python3.8/site-packages/PyQt5/QtGui.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class QOpenGLVertexArrayObject(__PyQt5_QtCore.QObject):
    """ QOpenGLVertexArrayObject(parent: QObject = None) """
    def bind(self): # real signature unknown; restored from __doc__
        """ bind(self) """
        pass

    def create(self): # real signature unknown; restored from __doc__
        """ create(self) -> bool """
        return False

    def destroy(self): # real signature unknown; restored from __doc__
        """ destroy(self) """
        pass

    def isCreated(self): # real signature unknown; restored from __doc__
        """ isCreated(self) -> bool """
        return False

    def objectId(self): # real signature unknown; restored from __doc__
        """ objectId(self) -> int """
        return 0

    def release(self): # real signature unknown; restored from __doc__
        """ release(self) """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass



