# encoding: utf-8
# module PyQt5.QtGui
# from /usr/lib/python3.8/site-packages/PyQt5/QtGui.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


class QColorTransform(__sip.simplewrapper):
    """
    QColorTransform()
    QColorTransform(QColorTransform)
    """
    def map(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        """
        map(self, int) -> int
        map(self, QRgba64) -> QRgba64
        map(self, Union[QColor, Qt.GlobalColor, QGradient]) -> QColor
        """
        return 0 or QRgba64 or QColor

    def swap(self, QColorTransform): # real signature unknown; restored from __doc__
        """ swap(self, QColorTransform) """
        pass

    def __init__(self, QColorTransform=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""



