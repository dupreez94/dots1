# encoding: utf-8
# module PyQt5.QtGui
# from /usr/lib/python3.8/site-packages/PyQt5/QtGui.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import sip as __sip


from .QValidator import QValidator

class QRegExpValidator(QValidator):
    """
    QRegExpValidator(parent: QObject = None)
    QRegExpValidator(QRegExp, parent: QObject = None)
    """
    def regExp(self): # real signature unknown; restored from __doc__
        """ regExp(self) -> QRegExp """
        pass

    def setRegExp(self, QRegExp): # real signature unknown; restored from __doc__
        """ setRegExp(self, QRegExp) """
        pass

    def validate(self, p_str, p_int): # real signature unknown; restored from __doc__
        """ validate(self, str, int) -> Tuple[QValidator.State, str, int] """
        pass

    def __init__(self, *__args): # real signature unknown; restored from __doc__ with multiple overloads
        pass


