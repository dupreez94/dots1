# encoding: utf-8
# module PyQt5.QtQuick
# from /usr/lib/python3.8/site-packages/PyQt5/QtQuick.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import PyQt5.QtQml as __PyQt5_QtQml
import sip as __sip


class QSGTextureProvider(__PyQt5_QtCore.QObject):
    """ QSGTextureProvider() """
    def texture(self): # real signature unknown; restored from __doc__
        """ texture(self) -> QSGTexture """
        return QSGTexture

    def textureChanged(self): # real signature unknown; restored from __doc__
        """ textureChanged(self) [signal] """
        pass

    def __init__(self): # real signature unknown; restored from __doc__
        pass


