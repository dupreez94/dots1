# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


from .QDialog import QDialog

class QErrorMessage(QDialog):
    """ QErrorMessage(parent: QWidget = None) """
    def changeEvent(self, QEvent): # real signature unknown; restored from __doc__
        """ changeEvent(self, QEvent) """
        pass

    def done(self, p_int): # real signature unknown; restored from __doc__
        """ done(self, int) """
        pass

    def qtHandler(self): # real signature unknown; restored from __doc__
        """ qtHandler() -> QErrorMessage """
        return QErrorMessage

    def showMessage(self, p_str, p_str_1=None): # real signature unknown; restored from __doc__ with multiple overloads
        """
        showMessage(self, str)
        showMessage(self, str, str)
        """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


