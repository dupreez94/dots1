# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


class QGraphicsTransform(__PyQt5_QtCore.QObject):
    """ QGraphicsTransform(parent: QObject = None) """
    def applyTo(self, QMatrix4x4): # real signature unknown; restored from __doc__
        """ applyTo(self, QMatrix4x4) """
        pass

    def update(self): # real signature unknown; restored from __doc__
        """ update(self) """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


