# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


from .QGraphicsEffect import QGraphicsEffect

class QGraphicsColorizeEffect(QGraphicsEffect):
    """ QGraphicsColorizeEffect(parent: QObject = None) """
    def color(self): # real signature unknown; restored from __doc__
        """ color(self) -> QColor """
        pass

    def colorChanged(self, Union, QColor=None, Qt_GlobalColor=None, QGradient=None): # real signature unknown; restored from __doc__
        """ colorChanged(self, Union[QColor, Qt.GlobalColor, QGradient]) [signal] """
        pass

    def draw(self, QPainter): # real signature unknown; restored from __doc__
        """ draw(self, QPainter) """
        pass

    def setColor(self, Union, QColor=None, Qt_GlobalColor=None, QGradient=None): # real signature unknown; restored from __doc__
        """ setColor(self, Union[QColor, Qt.GlobalColor, QGradient]) """
        pass

    def setStrength(self, p_float): # real signature unknown; restored from __doc__
        """ setStrength(self, float) """
        pass

    def strength(self): # real signature unknown; restored from __doc__
        """ strength(self) -> float """
        return 0.0

    def strengthChanged(self, p_float): # real signature unknown; restored from __doc__
        """ strengthChanged(self, float) [signal] """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


