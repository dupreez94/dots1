# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


from .QGesture import QGesture

class QTapAndHoldGesture(QGesture):
    """ QTapAndHoldGesture(parent: QObject = None) """
    def position(self): # real signature unknown; restored from __doc__
        """ position(self) -> QPointF """
        pass

    def setPosition(self, Union, QPointF=None, QPoint=None): # real signature unknown; restored from __doc__
        """ setPosition(self, Union[QPointF, QPoint]) """
        pass

    def setTimeout(self, p_int): # real signature unknown; restored from __doc__
        """ setTimeout(int) """
        pass

    def timeout(self): # real signature unknown; restored from __doc__
        """ timeout() -> int """
        return 0

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


