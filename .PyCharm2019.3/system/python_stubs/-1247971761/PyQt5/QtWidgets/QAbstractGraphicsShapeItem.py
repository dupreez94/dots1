# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


from .QGraphicsItem import QGraphicsItem

class QAbstractGraphicsShapeItem(QGraphicsItem):
    """ QAbstractGraphicsShapeItem(parent: QGraphicsItem = None) """
    def brush(self): # real signature unknown; restored from __doc__
        """ brush(self) -> QBrush """
        pass

    def isObscuredBy(self, QGraphicsItem): # real signature unknown; restored from __doc__
        """ isObscuredBy(self, QGraphicsItem) -> bool """
        return False

    def opaqueArea(self): # real signature unknown; restored from __doc__
        """ opaqueArea(self) -> QPainterPath """
        pass

    def pen(self): # real signature unknown; restored from __doc__
        """ pen(self) -> QPen """
        pass

    def setBrush(self, Union, QBrush=None, QColor=None, Qt_GlobalColor=None, QGradient=None): # real signature unknown; restored from __doc__
        """ setBrush(self, Union[QBrush, QColor, Qt.GlobalColor, QGradient]) """
        pass

    def setPen(self, Union, QPen=None, QColor=None, Qt_GlobalColor=None, QGradient=None): # real signature unknown; restored from __doc__
        """ setPen(self, Union[QPen, QColor, Qt.GlobalColor, QGradient]) """
        pass

    def __init__(self, parent=None): # real signature unknown; restored from __doc__
        pass


