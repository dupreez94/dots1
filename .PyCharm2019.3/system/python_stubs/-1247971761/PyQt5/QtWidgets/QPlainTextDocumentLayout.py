# encoding: utf-8
# module PyQt5.QtWidgets
# from /usr/lib/python3.8/site-packages/PyQt5/QtWidgets.abi3.so
# by generator 1.147
# no doc

# imports
import PyQt5.QtCore as __PyQt5_QtCore
import PyQt5.QtGui as __PyQt5_QtGui
import sip as __sip


class QPlainTextDocumentLayout(__PyQt5_QtGui.QAbstractTextDocumentLayout):
    """ QPlainTextDocumentLayout(QTextDocument) """
    def blockBoundingRect(self, QTextBlock): # real signature unknown; restored from __doc__
        """ blockBoundingRect(self, QTextBlock) -> QRectF """
        pass

    def cursorWidth(self): # real signature unknown; restored from __doc__
        """ cursorWidth(self) -> int """
        return 0

    def documentChanged(self, p_int, p_int_1, p_int_2): # real signature unknown; restored from __doc__
        """ documentChanged(self, int, int, int) """
        pass

    def documentSize(self): # real signature unknown; restored from __doc__
        """ documentSize(self) -> QSizeF """
        pass

    def draw(self, QPainter, QAbstractTextDocumentLayout_PaintContext): # real signature unknown; restored from __doc__
        """ draw(self, QPainter, QAbstractTextDocumentLayout.PaintContext) """
        pass

    def ensureBlockLayout(self, QTextBlock): # real signature unknown; restored from __doc__
        """ ensureBlockLayout(self, QTextBlock) """
        pass

    def frameBoundingRect(self, QTextFrame): # real signature unknown; restored from __doc__
        """ frameBoundingRect(self, QTextFrame) -> QRectF """
        pass

    def hitTest(self, Union, QPointF=None, QPoint=None, *args, **kwargs): # real signature unknown; NOTE: unreliably restored from __doc__ 
        """ hitTest(self, Union[QPointF, QPoint], Qt.HitTestAccuracy) -> int """
        pass

    def pageCount(self): # real signature unknown; restored from __doc__
        """ pageCount(self) -> int """
        return 0

    def requestUpdate(self): # real signature unknown; restored from __doc__
        """ requestUpdate(self) """
        pass

    def setCursorWidth(self, p_int): # real signature unknown; restored from __doc__
        """ setCursorWidth(self, int) """
        pass

    def __init__(self, QTextDocument): # real signature unknown; restored from __doc__
        pass


