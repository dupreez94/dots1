# encoding: utf-8
# module PyQt5.QtCore
# from /usr/lib/python3.8/site-packages/PyQt5/QtCore.abi3.so
# by generator 1.147
# no doc

# imports
import enum as __enum
import sip as __sip


class QSysInfo(__sip.simplewrapper):
    """
    QSysInfo()
    QSysInfo(QSysInfo)
    """
    def buildAbi(self): # real signature unknown; restored from __doc__
        """ buildAbi() -> str """
        return ""

    def buildCpuArchitecture(self): # real signature unknown; restored from __doc__
        """ buildCpuArchitecture() -> str """
        return ""

    def currentCpuArchitecture(self): # real signature unknown; restored from __doc__
        """ currentCpuArchitecture() -> str """
        return ""

    def kernelType(self): # real signature unknown; restored from __doc__
        """ kernelType() -> str """
        return ""

    def kernelVersion(self): # real signature unknown; restored from __doc__
        """ kernelVersion() -> str """
        return ""

    def machineHostName(self): # real signature unknown; restored from __doc__
        """ machineHostName() -> str """
        return ""

    def prettyProductName(self): # real signature unknown; restored from __doc__
        """ prettyProductName() -> str """
        return ""

    def productType(self): # real signature unknown; restored from __doc__
        """ productType() -> str """
        return ""

    def productVersion(self): # real signature unknown; restored from __doc__
        """ productVersion() -> str """
        return ""

    def __init__(self, QSysInfo=None): # real signature unknown; restored from __doc__ with multiple overloads
        pass

    __weakref__ = property(lambda self: object(), lambda self, v: None, lambda self: None)  # default
    """list of weak references to the object (if defined)"""


    BigEndian = 0
    ByteOrder = 1
    LittleEndian = 1
    WordSize = 64


