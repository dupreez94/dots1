# encoding: utf-8
# module _volume_key
# from /usr/lib/python3.8/site-packages/_volume_key.so
# by generator 1.147
# no doc
# no imports

# Variables with simple values

PACKET_FORMAT_ASSYMETRIC = 1
PACKET_FORMAT_ASYMMETRIC = 1

PACKET_FORMAT_ASYMMETRIC_WRAP_SECRET_ONLY = 3

PACKET_FORMAT_CLEARTEXT = 0
PACKET_FORMAT_PASSPHRASE = 2

PACKET_FORMAT_SYMMETRIC_WRAP_SECRET_ONLY = 4

PACKET_FORMAT_UNKNOWN = -1

PACKET_MATCH_OK = 0
PACKET_MATCH_UNSURE = 2

SECRET_DATA_ENCRYPTION_KEY = 1

SECRET_DEFAULT = 0
SECRET_PASSPHRASE = 2

VOLUME_FORMAT_LUKS = 'crypt_LUKS'

VP_CONFIGURATION = 1
VP_IDENTIFICATION = 0
VP_SECRET = 2

# functions

def delete_libvk_volume_property(*args, **kwargs): # real signature unknown
    pass

def delete_UI(*args, **kwargs): # real signature unknown
    pass

def delete_Volume(*args, **kwargs): # real signature unknown
    pass

def libvk_volume_property_label_get(*args, **kwargs): # real signature unknown
    pass

def libvk_volume_property_name_get(*args, **kwargs): # real signature unknown
    pass

def libvk_volume_property_swigregister(*args, **kwargs): # real signature unknown
    pass

def libvk_volume_property_type_get(*args, **kwargs): # real signature unknown
    pass

def libvk_volume_property_value_get(*args, **kwargs): # real signature unknown
    pass

def new_UI(*args, **kwargs): # real signature unknown
    pass

def Packet_get_format(*args, **kwargs): # real signature unknown
    pass

def Packet_open(*args, **kwargs): # real signature unknown
    pass

def Packet_open_unencrypted(*args, **kwargs): # real signature unknown
    pass

def Packet_swigregister(*args, **kwargs): # real signature unknown
    pass

def SWIG_PyInstanceMethod_New(*args, **kwargs): # real signature unknown
    pass

def UI_generic_cb_get(*args, **kwargs): # real signature unknown
    pass

def UI_generic_cb_set(*args, **kwargs): # real signature unknown
    pass

def UI_passphrase_cb_get(*args, **kwargs): # real signature unknown
    pass

def UI_passphrase_cb_set(*args, **kwargs): # real signature unknown
    pass

def UI_set_nss_pwfn_arg(*args, **kwargs): # real signature unknown
    pass

def UI_swigregister(*args, **kwargs): # real signature unknown
    pass

def Volume_add_secret(*args, **kwargs): # real signature unknown
    pass

def Volume_apply_packet(*args, **kwargs): # real signature unknown
    pass

def Volume_create_packet_assymetric(*args, **kwargs): # real signature unknown
    pass

def Volume_create_packet_assymetric_from_cert_data(*args, **kwargs): # real signature unknown
    pass

def Volume_create_packet_asymmetric(*args, **kwargs): # real signature unknown
    pass

def Volume_create_packet_asymmetric_from_cert_data(*args, **kwargs): # real signature unknown
    pass

def Volume_create_packet_cleartext(*args, **kwargs): # real signature unknown
    pass

def Volume_create_packet_with_passphrase(*args, **kwargs): # real signature unknown
    pass

def Volume_dump_properties(*args, **kwargs): # real signature unknown
    pass

def Volume_format_get(*args, **kwargs): # real signature unknown
    pass

def Volume_get_secret(*args, **kwargs): # real signature unknown
    pass

def Volume_hostname_get(*args, **kwargs): # real signature unknown
    pass

def Volume_label_get(*args, **kwargs): # real signature unknown
    pass

def Volume_load_packet(*args, **kwargs): # real signature unknown
    pass

def Volume_open(*args, **kwargs): # real signature unknown
    pass

def Volume_open_with_packet(*args, **kwargs): # real signature unknown
    pass

def Volume_packet_match_volume(*args, **kwargs): # real signature unknown
    pass

def Volume_path_get(*args, **kwargs): # real signature unknown
    pass

def Volume_swigregister(*args, **kwargs): # real signature unknown
    pass

def Volume_uuid_get(*args, **kwargs): # real signature unknown
    pass

# no classes
# variables with complex values

__loader__ = None # (!) real value is '<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>'

__spec__ = None # (!) real value is "ModuleSpec(name='_volume_key', loader=<_frozen_importlib_external.ExtensionFileLoader object at 0x7fc7b19d15e0>, origin='/usr/lib/python3.8/site-packages/_volume_key.so')"

