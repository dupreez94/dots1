# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

export ZSH="/home/michael/.config/oh-my-zsh"
ZSH_THEME="powerlevel10k/powerlevel10k"
POWERLEVEL9K_MODE=awesome-patched
POWERLEVEL9K_LEFT_SUBSEGMENT_SEPARATOR='\e0c0'




function powerline_precmd() {
    PS1="$(powerline-shell --shell zsh $?)"
}

function install_powerline_precmd() {
  for s in "${precmd_functions[@]}"; do
    if [ "$s" = "powerline_precmd" ]; then
      return
    fi
  done
  precmd_functions+=(powerline_precmd)
}

if [ "$TERM" != "linux" ]; then
    install_powerline_precmd
fi











# Command auto-correction.
ENABLE_CORRECTION="true"

# Command execution time stamp shown in the history command output.
HIST_STAMPS="mm/dd/yyyy"

# Plugins to load
plugins=(git
         sudo
	 zsh-autosuggestions
         zsh-completions
         fast-syntax-highlighting)

autoload -U compinit && compinit
source $ZSH/oh-my-zsh.sh

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/bin/vault vault


# Bind home and end keys
bindkey '\e[1~' beginning-of-line
bindkey '\e[4~' end-of-line

# Fixes strange cursor position / formating bug

# Alias to open with corresponding default program
alias open=xdg-open
fetch=$(fetch)
logo="\e[H\e[2J
          \e[1;36m.
         \e[1;36m/#\\      \e[1;37m          _      \e[1;36m _ _
        \e[1;36m/###\\     \e[1;37m         | |     \e[1;36m| (_)
       \e[1;36m/p^###\\    \e[1;37m _ __ ___| |__   \e[1;36m| |_ _ __  _   ___  __
      \e[1;36m/##P^q##\\   \e[1;37m| '__/ __| '_ \\  \e[1;36m| | | '_ \\| | | \\ \\/ /
     \e[1;36m/##(   )##\\  \e[1;37m| | | (__| | | | \e[1;36m| | | | | | |_| |>  <
    \e[1;36m/###P   q#,^\\ \e[1;37m|_|  \\___|_| |_| \e[1;36m|_|_|_| |_|\\__,_/_/\\_\\ \e[0;37mTM
   \e[1;36m/P^         ^q\\"

echo ${logo}
echo ${fetch}
echo ""
echo "\e[0m================================================================================"
echo ""
echo "    Welcome \e[34m$USER\e[0m"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
typeset -g POWERLEVEL9K_INSTANT_PROMPT=off
alias gogit='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias vim='nvim'
alias svim='sudo nvim'
#alias l='colorls -lA --sf --group-directories-first --report'
#alias ls='colorls -a  --sf --group-directories-first --report'
#source $(dirname $(gem which colorls))/tab_complete.sh


